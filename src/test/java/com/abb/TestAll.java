package com.abb;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


/**
 * The Class TestAll.
 */
@RunWith(Suite.class)
@SuiteClasses({ com.abb.framework.transformation.jetStar.TestAll.class, com.abb.framework.transformation.jetAirways.TestAll.class, com.abb.framework.serviceprovider.session.TestAll.class, com.abb.framework.webserviceclient.TestAll.class,
		com.abb.serviceprovider.jetAirways.service.TestAll.class, com.abb.serviceprovider.jetStar.service.TestAll.class, com.abb.framework.util.TestAll.class, com.abb.framework.merge.TestAll.class, com.abb.framework.authentication.dao.TestAll.class,
		com.abb.framework.authentication.service.TestAll.class, com.abb.framework.controller.TestAll.class })
public class TestAll {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
