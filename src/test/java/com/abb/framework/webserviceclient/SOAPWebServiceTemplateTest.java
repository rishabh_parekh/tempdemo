package com.abb.framework.webserviceclient;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.entity.SOAPServiceEnvelopXML;
import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;


/**
 * The Class SOAPWebServiceTemplateTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class SOAPWebServiceTemplateTest {

	/** The soap web service template. */
	@Autowired
	@Qualifier("SOAPWebServiceTemplate")
	private SOAPWebServiceTemplate soapWebServiceTemplate;

	/**
	 * Test send and receive.
	 */
	@Test
	public void testSendAndReceive() {
		soapWebServiceTemplate.setDefaultUri("http://220.226.185.57/jettaobeapi/FlightMatrix.asmx");

		String securityHeader = "<OpenAirSOAPHeader><sec:Security xmlns:sec=\"http://schemas.xmlsoap.org/ws/2002/12/secext\">" + "<sec:UsernameToken>" + "<sec:Username>T2IMPACT</sec:Username>" + "<sec:Password>T2IOBEAPI</sec:Password>"
				+ "<Organization>9W</Organization>" + "</sec:UsernameToken>" + "</sec:Security>" + "<MessageHeader xmlns=\"http://www.ebxml.org/namespaces/messageHeader\">" + "<CPAId>9W</CPAId>"
				+ "<ConversationId>session:2013-03-29T12:22:00@airblackbox.com</ConversationId>" + "<Service>Create</Service>" + "<Action>CreateSession</Action>" + "<MessageData>"
				+ "<MessageId>session:2013-03-29T12:22:00@airblackbox.com.session.msg-1</MessageId>" + "<Timestamp>2013-03-29T12:22:00</Timestamp>" + "</MessageData>" + "</MessageHeader></OpenAirSOAPHeader>";

		String loginRequest = "<Logon xmlns=\"http://www.vedaleon.com/webservices\"/>";

		SOAPServiceEnvelopXML soapEnvelop = new SOAPServiceEnvelopXML();
		soapEnvelop.setSpSOAPAction("http://www.vedaleon.com/webservices/Logon");
		soapEnvelop.setSpSOAPRequestHeaderXML(securityHeader);
		soapEnvelop.setSpSOAPRequestBodyXML(loginRequest);
		soapEnvelop.setSpSOAPServiceURL("http://220.226.185.57/jettaobeapi/SessionCreate.asmx");

		String responseXML = soapWebServiceTemplate.sendAndReceive(soapEnvelop);
		System.out.println("Resposne XML " + responseXML);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SOAPWebServiceTemplateTest.class);
	}
}
