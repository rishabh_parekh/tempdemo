package com.abb.framework.serviceprovider.session;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.enums.ServiceProvider;
import com.abb.framework.serviceprovider.session.SPSession;
import com.abb.framework.serviceprovider.session.SPSessionManager;


/**
 * The Class SPSessionManagerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class SPSessionManagerTest {

	/** The sp session manager. */
	@Autowired
	private SPSessionManager spSessionManager;

	/**
	 * Test session pool.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testSessionPool() throws Exception {
		System.out.println("No of Active = " + spSessionManager.getSpSessionPool().getNumActive());
		System.out.println("No of Idle = " + spSessionManager.getSpSessionPool().getNumIdle());

		SPSession session1 = spSessionManager.openSession(ServiceProvider.JET_STAR.getSessionKey());
		System.out.println(" " + session1.getResponseXML());
		SPSession session2 = spSessionManager.openSession(ServiceProvider.JET_STAR.getSessionKey());
		System.out.println(" " + session2.getResponseXML());

		SPSession session3 = spSessionManager.openSession(ServiceProvider.JET_AIRWAYS.getSessionKey());
		System.out.println(" " + session3.getResponseXML());
		SPSession session4 = spSessionManager.openSession(ServiceProvider.JET_AIRWAYS.getSessionKey());
		System.out.println(" " + session4.getResponseXML());

		this.printStatus();
		spSessionManager.closeSession(session1);
		spSessionManager.closeSession(session2);
		this.printStatus();
		spSessionManager.closeSession(session3);
		spSessionManager.closeSession(session4);
		this.printStatus();
	}

	/**
	 * Prints the status.
	 */
	private void printStatus() {
		System.out.println("No of Idle = " + spSessionManager.getSpSessionPool().getNumIdle());
		System.out.println("No of Active = " + spSessionManager.getSpSessionPool().getNumActive());
		System.out.println("No of Idle JS = " + spSessionManager.getSpSessionPool().getNumIdle(ServiceProvider.JET_STAR.getSessionKey()));
		System.out.println("No of Active JS = " + spSessionManager.getSpSessionPool().getNumActive(ServiceProvider.JET_STAR.getSessionKey()));
		System.out.println("No of Idle JA = " + spSessionManager.getSpSessionPool().getNumIdle(ServiceProvider.JET_AIRWAYS.getSessionKey()));
		System.out.println("No of Active JA = " + spSessionManager.getSpSessionPool().getNumActive(ServiceProvider.JET_AIRWAYS.getSessionKey()));
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(SPSessionManagerTest.class);
	}
}
