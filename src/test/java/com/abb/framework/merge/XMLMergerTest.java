package com.abb.framework.merge;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.abb.framework.exception.OpenAirMergingException;


/**
 * The Class XMLMergerTest.
 */
public class XMLMergerTest {

	/** The xml merger. */
	private static OpenAirMerger xmlMerger;

	/**
	 * Sets the up.
	 */
	@BeforeClass
	public static void setUp() {
		xmlMerger = new XMLMerger();
	}

	/**
	 * Test_ blank_ x paths.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws OpenAirMergingException the open air merging exception
	 */
	@Test(expected = OpenAirMergingException.class)
	public void test_Blank_XPaths() throws IOException, OpenAirMergingException {
		String xml1st = FileUtils.readFileToString(FileUtils.getFile("src/test/resources/com/abb/framework/merge/AirAvailabilityRS.1.xml").getAbsoluteFile());
		String xml2nd = FileUtils.readFileToString(FileUtils.getFile("src/test/resources/com/abb/framework/merge/AirAvailabilityRS.2.xml").getAbsoluteFile());

		String resultXML = xmlMerger.merge("", "", xml1st, xml2nd);
		// System.out.println("resultXML = " + resultXML);
		assertNotNull(resultXML);
	}

	/**
	 * Test_ valid.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws OpenAirMergingException the open air merging exception
	 */
	@Test
	public void test_Valid() throws IOException, OpenAirMergingException {
		String xml1st = FileUtils.readFileToString(FileUtils.getFile("src/test/resources/com/abb/framework/merge/AirAvailabilityRS.1.xml").getAbsoluteFile());
		String xml2nd = FileUtils.readFileToString(FileUtils.getFile("src/test/resources/com/abb/framework/merge/AirAvailabilityRS.2.xml").getAbsoluteFile());

		String resultXML = xmlMerger.merge("AirAvailabilityRS/OriginDestination/Flight,AirAvailabilityRS/InfoGroup/ForInfo/Text", "AirAvailabilityRS/OriginDestination,AirAvailabilityRS/InfoGroup/ForInfo", xml1st, xml2nd);
		// System.out.println("resultXML = " + resultXML);
		assertNotNull(resultXML);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(XMLMergerTest.class);
	}
}
