package com.abb.framework.transformation.jetStar;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


/**
 * The Class TestAll.
 */
@RunWith(Suite.class)
@SuiteClasses({ AddPayment_Booking_ComitRequestTest.class, AddPayment_GetBookingPaymentsRequestTest.class, AddPayment_GetBookingTest.class, FareSearchRQTest.class, FlightPrice_GetAvailabilityTest.class, FlightPriceRQ_PriceIteneraryTest.class,
		PNRCreate_VerifyPricingVarianceRQTest.class, PNRCreateRQ_CommitTest.class, PNRCreateRQ_GetAvailabilityTest.class, PNRCreateRQ_PriceIteneraryTest.class, PNRCreateRQ_SellRequest_SSRTest.class, PNRCreateRQ_SellRequestTest.class,
		PNRCreateRS_HoldTest.class, PNRRetrieveRSTest.class, PNRCreateRQ_PassiveSegment_SellRequestTest.class, PNRCreateRQ_PassiveSegment_GetBookingRequestTest.class, PNRCreateRQ_PassiveSegment_BookingCommitRequestTest.class,
		PNRCreateRS_PassiveSegmentTest.class })
public class TestAll {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
