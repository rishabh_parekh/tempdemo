package com.abb.framework.transformation.jetStar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;

/**
 * The Class PNRCreateRS_HoldTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRS_HoldTest {

	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;

	/** The param. */
	private static String param;

	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/response/";

	/**
	 * Sets the xml transformer.
	 * 
	 * @param xmlTransformation
	 *            the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		// param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetStar/xslt/PNRCreateRS_Hold.xsl").getAbsolutePath();
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetStar/xslt/PNRRetrieveRS.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 * 
	 * @param sourceXML
	 *            the source xml
	 * @param destinationXML
	 *            the destination xml
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/************************ ONE WAY **********************/
	/**
	 * Test transform_ o w_ ad t_ inf.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_ADT_INF() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRS_JS_OneWay_1ADT_1INF.xml", "PNRCreateRS_JS_OneWay_1ADT_1INF.out.xml");
	}

	/**
	 * Test transform_ o w_ adt.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_ADT() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_Hold_JS.xml", "PNRCreateRQ_Hold_OW_JS.out.xml");
	}

	// 1ADT-25KG 2ADT-NOBAG 1CHD-20KG
	/**
	 * Test transform_ o w_2 ad t_1 chd.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_2ADT_1CHD() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_Hold_JS_OW_2ADT_1CHD.xml", "PNRCreateRS_Hold_OW_2ADT_1CHD.out.xml");
	}

	// TODO As MultiSegment PNRCreateRQ flights are not available
	/**
	 * Test transform_ o w_2 ad t_1 ch d_ multi segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testTransform_OW_2ADT_1CHD_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRS_Hold_OW_2ADT_1CHD_MultiSegment.xml", "PNRCreateRS_Hold_OW_2ADT_1CHD_MultiSegment.out.xml");
	}

	/************************ ROUND TRIP **********************/

	@Test
	public void testTransform_RT_ADT() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_RT_Hold_JS.xml", "PNRCreateRQ_Hold_RT_JS.out.xml");
	}

	// TODO As MultiSegment PNRCreateRQ flights are not available
	/**
	 * Test transform_ r t_ ad t_ multi segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testTransform_RT_ADT_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_Hold_JS_RT_MultiSegment.xml", "PNRCreateRQ_Hold_JS_RT_MultiSegment.out.xml");
	}

	/**
	 * Test transform_ o w_ ad t_ bag_ tst.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_ADT_Bag_TST() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_Hold_JS_OW_Bag.xml", "PNRCreateRS_JS_OneWay_1ADT_Bag.out.xml");
	}

	/**
	 * Test transform_ general remarks. INTA-60
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_GeneralRemarks() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_Hold_JS_GeneralRemark.xml", "PNRCreateRS_JS_GeneralRemark.out.xml");
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRS_HoldTest.class);
	}
}
