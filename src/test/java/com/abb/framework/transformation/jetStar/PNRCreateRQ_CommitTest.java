package com.abb.framework.transformation.jetStar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class PNRCreateRQ_CommitTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRQ_CommitTest {
	
	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;
	
	/** The param. */
	private static String param;
	
	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/request/";

	/**
	 * Sets the xml transformer.
	 *
	 * @param xmlTransformation the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceProvider/jetStar/xslt/PNRCreateRQ_CommitRequest.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 *
	 * @param sourceXML the source xml
	 * @param destinationXML the destination xml
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = null;
		try {
			resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();
		} catch (OpenAirTransformationException e) {
			e.printStackTrace();
		}

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(destinationStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test transform_ o w_1 adt.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_1ADT() throws IOException {
		this.transform("PNRCreateRQ_JS_OneWay_1ADT.xml", "js_out/Commit/PNRCreateRQ_OneWay_1ADT.CommitRQ.out.xml");
	}

	/**
	 * Test transform_2 ad t_1 ch d_ ow.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_2ADT_1CHD_OW() throws IOException {
		this.transform("PNRCreateRQ_JS_OneWay_2ADT_1CHD.xml", "js_out/Commit/PNRCreateRQ_OneWay_2ADT_1CHD.CommitRQ.out.xml");
	}

	/**
	 * Test transform_ r t_2 ad t_1 ch d_1 inf.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_2ADT_1CHD_1INF() throws IOException {
		this.transform("PNRCreateRQ_JS_RT_2ADT_1CHD_1INF.xml", "js_out/Commit/PNRCreateRQ_RT_2ADT_1CHD_1INF.CommitRQ.out.xml");
	}

}
