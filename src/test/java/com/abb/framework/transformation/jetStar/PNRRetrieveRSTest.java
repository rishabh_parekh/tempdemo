package com.abb.framework.transformation.jetStar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;

/**
 * The Class PNRRetrieveRSTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRRetrieveRSTest {

	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;

	/** The param. */
	private static String param;

	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrretrieve/response/";

	/**
	 * Sets the xml transformer.
	 * 
	 * @param xmlTransformation
	 *            the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetStar/xslt/PNRRetrieveRS.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 * 
	 * @param sourceXML
	 *            the source xml
	 * @param destinationXML
	 *            the destination xml
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test transform_ o w_1 adt.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_1ADT() throws OpenAirTransformationException, IOException {
		this.transform("Booking_1ADT.xml", "js_out/Booking_1ADT.out.xml");
	}

	/**
	 * Test transform_ o w_2 ad t_2 ch d_ multi segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_2ADT_2CHD_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("Booking_2ADT_2CHD.xml", "js_out/Booking_2ADT_2CHD.out.xml");
	}

	/**
	 * Test transform_ r t_2 ad t_2 ch d_ multi segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_2ADT_2CHD_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("Booking_RT_2ADT_2CHD.xml", "js_out/Booking_RT_2ADT_2CHD.out.xml");
	}

	/**
	 * Test transform_ o w_2 ad t_1 ch d_baggage. // 1ADT=25KG, 2ADT=NO BAGS, 1CHD=20KG
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_2ADT_1CHD_baggage() throws OpenAirTransformationException, IOException {
		this.transform("Booking_OW_2ADT_CHD_Baggage.xml", "js_out/Booking_OW_2ADT_CHD_Baggage.out.xml");
	}

	/**
	 * 1ADT RT bags
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_1ADT_Baggage() throws OpenAirTransformationException, IOException {
		this.transform("Booking_RT_1ADT.xml", "js_out/Booking_RT_1ADT.out.xml");
	}

	/**
	 * Test transform_ r t_1 ad t_1 ch d_1 in f_baggage.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_1ADT_1CHD_1INF_baggage() throws OpenAirTransformationException, IOException {
		this.transform("Booking_RT_ADT_CHD_INF_Baggage.xml", "js_out/Booking_RT_ADT_CHD_INF_baggage.out.xml");
	}

	// test case for AddProcessPayment. (CC details also added in response)
	/**
	 * Test transform_ add process payment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_AddProcessPayment() throws OpenAirTransformationException, IOException {
		this.transform("Booking_Process_Payment.xml", "js_out/Booking_Process_Payment.out.xml");
	}

	/**
	 * Test transform_ passive segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_PassiveSegment() throws OpenAirTransformationException, IOException {
		this.transform("Booking_PassiveSegment.xml", "js_out/Booking_PassiveSegment.out.xml");
	}

	@Test
	public void testTransform_OW_2ADT_1CHD_1INF() throws OpenAirTransformationException, IOException {
		this.transform("Booking_OW_2ADT_1CHD_1INF.xml", "js_out/Booking_OW_2ADT_1CHD_1INF.out.xml");
	}

	/**
	 * GeneralRemarks. INTA-60
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_GeneralRemarks() throws OpenAirTransformationException, IOException {
		this.transform("Booking_OW_GeneralRemark.xml", "js_out/Booking_OW_GeneralRemarks.out.xml");
	}

	/**
	 * INTA-65 PNRRetrieve for Cancelled PNR
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_CancelledPNR() throws OpenAirTransformationException, IOException {
		this.transform("Booking_OW_CancelledPNR.xml", "js_out/Booking_OW_CancelledPNR.out.xml");
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRRetrieveRSTest.class);
	}
}
