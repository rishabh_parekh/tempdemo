package com.abb.framework.transformation.jetStar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class PNRCreateRQ_PassiveSegment_SellRequestTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRQ_PassiveSegment_SellRequestTest {
	
	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;
	
	/** The param. */
	private static String param;
	
	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/passiveSegment/request/";

	/**
	 * Sets the xml transformer.
	 *
	 * @param xmlTransformation the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceProvider/jetStar/xslt/PNRCreateRQ_PassiveSegment_SellRequest.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 *
	 * @param sourceXML the source xml
	 * @param destinationXML the destination xml
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = null;
		try {
			resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();
		} catch (OpenAirTransformationException e) {
			e.printStackTrace();
		}

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(destinationStr);
		assertEquals(destinationStr, resultStr);
	}

	// ONE WAY
	/**
	 * Test transform_1 ad t_ ow.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_1ADT_OW() throws IOException {
		this.transform("PNRCreateRQ_JS_OW_1ADT_Passive.xml", "js_out/Sell/PNRCreateRQ_OW_1ADT_Passive.SellRQ.out.xml");
	}

	/**
	 * Test transform_ o w_ multi segment.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_MultiSegment() throws IOException {
		this.transform("PNRCreateRQ_JS_OW_MultiSegment_Passive.xml", "js_out/Sell/PNRCreateRQ_OW_MultiSegment_Passive.SellRQ.out.xml");
	}

	// ROUND TRIP
	/**
	 * Test transform_ rt.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT() throws IOException {
		this.transform("PNRCreateRQ_JS_RT_1ADT_Passive.xml", "js_out/Sell/PNRCreateRQ_RT_1ADT_Passive.SellRQ.out.xml");
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRQ_PassiveSegment_SellRequestTest.class);
	}
}
