package com.abb.framework.transformation.jetStar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;

/**
 * The Class PNRCreateRQ_SellRequest_SSRTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRQ_SellRequest_SSRTest {

	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;

	/** The param. */
	private static String param;

	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/request/";

	/**
	 * Sets the xml transformer.
	 * 
	 * @param xmlTransformation
	 *            the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceProvider/jetStar/xslt/PNRCreateRQ_SellRequest_SSR.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 * 
	 * @param sourceXML
	 *            the source xml
	 * @param destinationXML
	 *            the destination xml
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = null;
		try {
			resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();
		} catch (OpenAirTransformationException e) {
			e.printStackTrace();
		}

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(destinationStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * ONEWAY 1ADT 1INF (ssr)
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW() throws IOException {
		this.transform("PNRCreateRQ_JS_OneWay_1ADT_1INF.xml", "js_out/Sell_SSR/PNRCreateRQ_OneWay_1ADT_1INF_SellRQ.out.xml");
	}

	/**
	 * RT 2ADT 1INF
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT() throws IOException {
		this.transform("PNRCreateRQ_JS_RoundTrip_2ADT_1INF.xml", "js_out/Sell_SSR/PNRCreateRQ_RoundTrip_2ADT_1INF.SellRQ.out.xml");
	}

	/**
	 * OW 1ADT 1INF (SSR) + 1BAG (SSR)
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_INF_Bag() throws IOException {
		this.transform("PNRCreateRQ_JS_OneWay_1ADT_1INF_Baggage.xml", "js_out/Sell_SSR/PNRCreateRQ_OW_1ADT_1INF_Baggage_SellRQ.out.xml");
	}

	/**
	 * RT 1ADT 1INF BAGS
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_INF_Bag() throws IOException {
		this.transform("PNRCreateRQ_JS_RT_1ADT_1INF_Baggage.xml", "js_out/Sell_SSR/PNRCreateRQ_RT_1ADT_1INF_Baggage_SellRQ.out.xml");
	}

	/**
	 * ONLY INBOUND BAGS/MISSING OUTBOUND BAGS
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_INF_OnlyInboundBags() throws IOException {
		this.transform("PNRCreateRQ_JS_RT_1ADT_OnlyInboundBag.xml", "js_out/Sell_SSR/PNRCreateRQ_RT_1ADT_OnlyInboundBag_SellRQ.out.xml");
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRQ_SellRequest_SSRTest.class);
	}
}
