package com.abb.framework.transformation.jetStar;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class PNRCreateRQ_PriceIteneraryTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRQ_PriceIteneraryTest {
	
	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;
	
	/** The param. */
	private static String param;
	
	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/request/";

	/**
	 * Sets the xml transformer.
	 *
	 * @param xmlTransformation the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceProvider/jetStar/xslt/PNRCreateRQ_PriceIteneraryRequest.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 *
	 * @param sourceXML the source xml
	 * @param destinationXML the destination xml
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = null;
		try {
			resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();
		} catch (OpenAirTransformationException e) {
			e.printStackTrace();
		}

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(destinationStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test transform_ ow.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW() throws IOException {
		this.transform("PNRCreateRQ_JS_OneWay_1ADT_1INF.xml", "js_out/PriceItenerary_Sell/PNRCreateRQ_OneWay_1ADT_1INF.PriceIteneraryRQ.out.xml");
	}

	/**
	 * Test transform_ o w_ ssr.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_SSR() throws IOException {
		this.transform("PNRCreateRQ_JS_OneWay_1ADT_1CHD_1INF_SSR.xml", "js_out/PriceItenerary_Sell/PNRCreateRQ_OneWay_1ADT_1CHD_1INF_SSR.PriceIteneraryRQ.out.xml");
	}

	/**
	 * Test transform_ rt.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT() throws IOException {
		this.transform("PNRCreateRQ_JS_RoundTrip_2ADT_1CHD.xml", "js_out/PriceItenerary_Sell/PNRCreateRQ_RoundTrip_2ADT_1CHD.PriceItineraryRQ.out.xml");
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRQ_PriceIteneraryTest.class);
	}
}
