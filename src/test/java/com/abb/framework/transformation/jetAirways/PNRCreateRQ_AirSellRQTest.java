package com.abb.framework.transformation.jetAirways;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class PNRCreateRQ_AirSellRQTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRQ_AirSellRQTest {

	/** The xml transformer. */
	@Autowired
	@Qualifier("XMLTransformation")
	private OpenAirTransformation xmlTransformer;
	
	/** The param. */
	private static String param;
	
	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/request/";

	// private Map<String, String> parameterMap;

	/**
	 * Sets the up.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetAirways/xslt/PNRCreateRQ_AirSellRQ.xsl").getAbsolutePath();
	}

	/*
	 * @Before public void setupForTest() { parameterMap = new HashMap<String, String>(); }
	 */

	/**
	 * Transform.
	 *
	 * @param sourceXML the source xml
	 * @param destinationXML the destination xml
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		// String resultStr = xmlTransformer.transform(sourceXMLStr, "", param,
		// parameterMap).trim();
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();
		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test one way_1 adt.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testOneWay_1ADT() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/AirSell/PNRCreateRQ_OneWay_1ADT.xml", "ja_out/AirSell/PNRCreateRQ_OneWay_1ADT.AirSellRQ.out.xml");
	}

	/**
	 * Test one way_2 ad t_1 ch d_1 in f_ ssr.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testOneWay_2ADT_1CHD_1INF_SSR() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/AirSell/PNRCreateRQ_OneWay_1ADT_1CHD_1INF_SSR.xml", "ja_out/AirSell/PNRCreateRQ_OneWay_1ADT_1CHD_1INF_SSR.AirSellRQ.out.xml");
	}

	/**
	 * Test round trip_2 ad t_1 chd.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testRoundTrip_2ADT_1CHD() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/AirSell/PNRCreateRQ_RoundTrip_2ADT_1CHD.xml", "ja_out/AirSell/PNRCreateRQ_RoundTrip_2ADT_1CHD.AirSellRQ.out.xml");
	}

	/**
	 * Test one way_1 ad t_ multi segment.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testOneWay_1ADT_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/AirSell/PNRCreateRQ_OneWay_1ADT_MultiSegment.xml", "ja_out/AirSell/PNRCreateRQ_OneWay_1ADT_MutliSegment.AirSellRQ.out.xml");
	}

	/**
	 * Test round trip_1 ad t_ multi segment.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testRoundTrip_1ADT_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/AirSell/PNRCreateRQ_RoundTrip_1ADT_MultiSegment.xml", "ja_out/AirSell/PNRCreateRQ_RoundTrip_1ADT_MutliSegment.AirSellRQ.out.xml");
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRQ_AirSellRQTest.class);
	}
}
