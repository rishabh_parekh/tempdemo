package com.abb.framework.transformation.jetAirways;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class PNRCreateRQ_AirRulesRQTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRQ_AirRulesRQTest {

	/** The xml transformer. */
	@Autowired
	@Qualifier("XMLTransformation")
	private OpenAirTransformation xmlTransformer;
	
	/** The param. */
	private static String param;
	
	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/request/";

	/*
	 * Commented as now we are about to get separate requests for Jetstar and JetAirways
	 */
	// private Map<String, String> parameterMap;

	/**
	 * Sets the up.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetAirways/xslt/PNRCreateRQ_AirRulesRQ.xsl").getAbsolutePath();
	}

	/*
	 * @Before public void setupForTest() { parameterMap = new HashMap<String, String>(); }
	 */

	/**
	 * Transform.
	 *
	 * @param sourceXML the source xml
	 * @param destinationXML the destination xml
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		// String resultStr = xmlTransformer.transform(sourceXMLStr, "", param,
		// parameterMap).trim();
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();
		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test one way_1 adt.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testOneWay_1ADT() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/AirRules/PNRCreateRQ_OneWay_1ADT.xml", "ja_out/AirRules/PNRCreateRQ_OneWay_1ADT.AirRulesRQ.out.xml");
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRQ_AirRulesRQTest.class);
	}
}
