package com.abb.framework.transformation.jetAirways;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;

/**
 * The Class JA_PNRCreateRSTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class JA_PNRCreateRSTest {

	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;

	/** The param. */
	private static String param;

	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/response/";

	/**
	 * Sets the xml transformer.
	 * 
	 * @param xmlTransformation
	 *            the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetAirways/xslt/JA_PNRCreateRS.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 * 
	 * @param sourceXML
	 *            the source xml
	 * @param destinationXML
	 *            the destination xml
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test transform_ o w_ ad t_ inf.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_ADT_INF() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_OneWay_1ADT_1INF.xml", "PNRCreateRS_JA_OneWay_1ADT_1INF.out.xml");
	}

	/**
	 * Test transform_ o w_2 ad t_1 ch d_ in f_ multi segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_2ADT_1CHD_INF_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_OneWay_2ADT_1CHD_1INF.xml", "PNRCreateRS_JA_OneWay_2ADT_1CHD_1INF.out.xml");
	}

	/**
	 * Test transform_ r t_1 ad t_ multi segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_1ADT_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_RT_MultiSegment.xml", "PNRCreateRS_JA_RT_1ADT_MultiSegment.out.xml");
	}

	@Test
	public void testTransform_OW_GeneralRemarks() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_GeneralRemarks.xml", "PNRCreateRS_JA_GeneralRemarks.out.xml");
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(JA_PNRCreateRSTest.class);
	}
}
