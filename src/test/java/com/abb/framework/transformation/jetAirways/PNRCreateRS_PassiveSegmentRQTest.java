package com.abb.framework.transformation.jetAirways;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class PNRCreateRS_PassiveSegmentRQTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRS_PassiveSegmentRQTest {
	
	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;
	
	/** The param. */
	private static String param;
	
	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/response/";

	/**
	 * Sets the xml transformer.
	 *
	 * @param xmlTransformation the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetAirways/xslt/JA_PNRCreateRS_PassiveSegment.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 *
	 * @param sourceXML the source xml
	 * @param destinationXML the destination xml
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test transform_ o w_ adt.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_ADT() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_PassiveSegment_JA.xml", "/ja_out/PNRCreateRS_OW_PassiveSegment.out.xml");
	}

	/**
	 * Test transform_ r t_ adt.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_ADT() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_RT_PassiveSegment_JA.xml", "/ja_out/PNRCreateRS_RT_PassiveSegment.out.xml");
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRS_PassiveSegmentRQTest.class);
	}
}
