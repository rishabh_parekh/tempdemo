package com.abb.framework.transformation.jetAirways;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class PNRCreateRQ_PassiveSegmentsRQTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class PNRCreateRQ_PassiveSegmentsRQTest {

	/** The xml transformer. */
	@Autowired
	@Qualifier("XMLTransformation")
	private OpenAirTransformation xmlTransformer;
	
	/** The param. */
	private static String param;
	
	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrcreate/passiveSegment/request/";

	/**
	 * Sets the up.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetAirways/xslt/PNRCreateRQ_ProcessPaymentRQ.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 *
	 * @param sourceXML the source xml
	 * @param destinationXML the destination xml
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * ONEWAY SINGLE SEGMENT.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testOneWay_1ADT() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_OW_1ADT_Passive.xml", "ja_out/ProcessPassiveSegment/PNRCreateRQ_OW_1ADT_Passive.PassiveSegmentsRQ.out.xml");
	}

	/**
	 * ONEWAY TWO/MULTI SEGMENT.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testOneWay_1ADT_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_OW_MultiSegment_Passive.xml", "ja_out/ProcessPassiveSegment/PNRCreateRQ_OW_MultiSegment_PassiveSegmentsRQ.out.xml");
	}

	/**
	 * ROUND TRIP SINGLE SEGMENT.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testRT_1ADT() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_RT_1ADT_Passive.xml", "ja_out/ProcessPassiveSegment/PNRCreateRQ_RT_1ADT_Passive.PassiveSegmentsRQ.out.xml");
	}

	/**
	 * ROUND TRIP SINGLE SEGMENT.
	 *
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testRT_1ADT_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("PNRCreateRQ_JA_RT_MultiSegment_Passive.xml", "ja_out/ProcessPassiveSegment/PNRCreateRQ_RT_MultiSegment_PassiveSegmentsRQ.out.xml");
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(PNRCreateRQ_PassiveSegmentsRQTest.class);
	}
}
