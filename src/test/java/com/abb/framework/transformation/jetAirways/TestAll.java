package com.abb.framework.transformation.jetAirways;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


/**
 * The Class TestAll.
 */
@RunWith(Suite.class)
@SuiteClasses({ PNRCreateRQ_AirBookRQTest.class, PNRCreateRQ_AirRulesRQTest.class, PNRCreateRQ_AirSellRQTest.class, PNRCreateRQ_FlightMatrixRQTest.class, JA_PNRCreate_VerifyPricingVarianceRQTest.class, JA_PNRCreateRSTest.class,
		JA_PNRRetrieveRSTest.class, JA_TicketIssueRSTest.class, PNRCreateRQ_AirTicketRQTest.class, PNRCreateRQ_PassiveSegmentsRQTest.class, PNRCreateRS_PassiveSegmentRQTest.class })
public class TestAll {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
