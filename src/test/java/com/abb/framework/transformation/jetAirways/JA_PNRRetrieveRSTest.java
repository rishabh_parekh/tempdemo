package com.abb.framework.transformation.jetAirways;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;

/**
 * The Class JA_PNRRetrieveRSTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class JA_PNRRetrieveRSTest {

	/** The xml transformer. */
	private static OpenAirTransformation xmlTransformer;

	/** The param. */
	private static String param;

	/** The test root. */
	private final String TEST_ROOT = "src/test/resources/com/abb/framework/transformation/openaxis/pnrretrieve/response/";

	/**
	 * Sets the xml transformer.
	 * 
	 * @param xmlTransformation
	 *            the new xml transformer
	 */
	@Autowired
	@Qualifier("XMLTransformation")
	public void setXmlTransformer(OpenAirTransformation xmlTransformation) {
		xmlTransformer = xmlTransformation;
	}

	/**
	 * Sets the up.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@BeforeClass
	public static void setUp() throws IOException {
		param = FileUtils.getFile("src/main/java/com/abb/serviceprovider/jetAirways/xslt/JA_PNRRetrieveRS.xsl").getAbsolutePath();
	}

	/**
	 * Transform.
	 * 
	 * @param sourceXML
	 *            the source xml
	 * @param destinationXML
	 *            the destination xml
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 */
	private void transform(String sourceXML, String destinationXML) throws IOException, OpenAirTransformationException {
		String sourceXMLStr = FileUtils.readFileToString(new File(TEST_ROOT + sourceXML));
		String resultStr = xmlTransformer.transform(sourceXMLStr, "", param, null).trim();

		String destinationStr = FileUtils.readFileToString(new File(TEST_ROOT + destinationXML)).replaceAll("\\t+", "").trim();

		assertNotNull(resultStr);
		assertEquals(destinationStr, resultStr);
	}

	/**
	 * Test transform_ o w_1 ad t_1 cn n_1 inf.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_1ADT_1CNN_1INF() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/PNRRetrieveRS_JA_OneWay_1ADT_1INF.xml", "ja_out/PNRRetrieveRS_JA_OneWay_1ADT_1INF.out.xml");
	}

	/**
	 * Test transform_ o w_2 ad t_2 chd.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_2ADT_2CHD() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/PNRRetrieveRS_JA_OneWay_2ADT_2CHD.xml", "ja_out/PNRRetrieveRS_JA_OneWay_2ADT_2CHD.out.xml");
	}

	/**
	 * Test transform_ o w_1 ad t_1 chd.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_1ADT_1CHD() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/PNRRetrieveRS_JA_OneWay_1ADT_1CHD.xml", "ja_out/PNRRetrieveRS_JA_OneWay_1ADT_1CHD.out.xml");
	}

	/**
	 * Test transform_ r t_1 ad t_ multi segment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_RT_1ADT_MultiSegment() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/PNRRetrieveRS_JA_RT_1ADT_MultiSegment.xml", "ja_out/PNRRetrieveRS_JA_RT_1ADT_MultiSegment.out.xml");
	}

	/**
	 * INTA-60 general remarks for fulfillment.
	 * 
	 * @throws OpenAirTransformationException
	 *             the open air transformation exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTransform_OW_GeneralRemarks() throws OpenAirTransformationException, IOException {
		this.transform("ja_in/PNRRetrieveRS_JA_GeneralRemarks.xml", "ja_out/PNRRetrieveRS_JA_GeneralRemarks.out.xml");
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(JA_PNRRetrieveRSTest.class);
	}
}
