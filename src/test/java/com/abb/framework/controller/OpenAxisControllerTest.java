package com.abb.framework.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.serviceAccessor.farelogix.manager.FarelogixConfiguration;

/**
 * The Class OpenAxisControllerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class OpenAxisControllerTest {

	/** The controller. */
	@Autowired
	private OpenAxisController controller;

	/**
	 * Test service end point_ blank xml.
	 */
	@Test
	public void testServiceEndPoint_BlankXML() {
		String responseXML = controller.serviceEndPoint("PNRCreate", "", null);
		System.out.println(responseXML);
		assertNotNull(responseXML);
		assertEquals("Request XML not found.", responseXML);
	}

	/**
	 * Test tc header authentiation_ valid.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testTCHeaderAuthentiation_InValid() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_Authenticate_TCHeader_Invalid.xml");

		FarelogixConfiguration flxConfig = new FarelogixConfiguration();
		flxConfig.setValidateXML("true");

		String requestXML = FileUtils.readFileToString(xmlResource.getFile());
		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);

		assertNotNull(responseXML);
		System.out.println(responseXML);
		assertTrue(responseXML.contains("<PNRViewRS><InfoGroup>"));
		assertTrue(responseXML.contains("<Error ErrorType=\"PNRCreate\" Source=\"\">"));
		assertTrue(responseXML.contains("<Code>1002</Code>"));
		assertTrue(responseXML.contains("<Text>Authentication Failed: Unauthorized request.</Text>"));
		assertTrue(responseXML.contains("</Error></InfoGroup></PNRViewRS>"));
		flxConfig.setValidateXML("false");
	}

	/**
	 * Test tc header authentiation_ valid.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testTCHeaderAuthentiation_Valid() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_Authenticate_TCHeader_Valid.xml");

		String requestXML = FileUtils.readFileToString(xmlResource.getFile());
		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);

		assertNotNull(responseXML);
		assertTrue(responseXML.contains("<RecordLocator"));
	}

	// ************************Jet Star ******************************//
	/**
	 * Test service end point_ pnr create j s_ bookand hold.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreateJS_BookandHold() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create j s_ process payment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreateJS_ProcessPayment() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/ProcessPaymentRQ_JS.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("AddPaymentInJetstar", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	@Test
	public void testServiceEndPoint_PNRCreateJS_ProcessPayment_FLX() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/ProcessPaymentRQ_JS.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("ProcessPaymentRQ", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr retrieve_ js.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRRetrieve_JS() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRRetrieveRQ_JS.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRRetrieve", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create j s_1 ad t_ bookand hold.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreateJS_1ADT_BookandHold() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_1ADT_STG.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ cancel_ j s_1 adt.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_Cancel_JS_1ADT() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCancel_RQ_JS_1ADT.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCancel", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_2 ad t_1 chd.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_2ADT_1CHD() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_2ADT_1CHD.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * FOR INF PAXSSRs would be added in CustomItenaryData same as of Baggage
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_1ADT_1INF() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_1ADT_1INF.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_2 ad t_1 ch d_1 inf.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_2ADT_1CHD_1INF() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_2ADT_1CHD_1INF.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ rt.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ r t_2 ad t_1 ch d_1 inf.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_2ADT_1CHD_1INF() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_2ADT_1CHD_1INF.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ o w_1 ad t_ baggage.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_OW_1ADT_Baggage() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_1ADT_Baggage.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_1ADT_Baggage() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_1ADT_Baggage.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_1ADT_OutBound_Baggage() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_1ADT_Baggage.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_2ADT_Baggage() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_2ADT_Baggage.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ o w_2 ad t_1 ch d_ baggage.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_OW_2ADT_1CHD_Baggage() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_2ADT_1CHD_Baggage.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	@Test
	public void testServiceEndPoint_PNRCreate_JS_OW_1ADT_1INF_Baggage() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_1ADT_1INF_Baggage.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ o w_ multi segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_OW_MultiSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_MultiSegment.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ o w_2 ad t_2 ch d_ multi segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_OW_2ADT_2CHD_MultiSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_2ADT_2CHD_MultiSegment.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ r t_2 ad t_2 ch d_ multi segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JS_RT_2ADT_2CHD_MultiSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_2ADT_2CHD_MultiSegment.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ o w_ pricing tolerance.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_OW_PricingTolerance() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_PricingVariance.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * <CallSellRQ>true</CallSellRQ> tag FOUND IN PNRCREATERQ, CALL ONLY SELLRQ AND REVERT WITH PRICING RESPONSE AND TERMINATE BOOKING
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_PricingVariance_CallSellRQ() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_PricingVariance_CallSellRQ.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	// TEST CASES SUGGESTED BY TIMOTHY

	// JQ.1 1ADT 1CHD 20KG EACH RT
	/**
	 * Test service end point_ pnr create_ j s_ r t_ bag.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_Bag() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_1ADT_1CHD_20KGBag.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	// JQ.2 4ADT 2ADT with 20KG EACH RT
	/**
	 * Test service end point_ pnr create_ j s_ r t_4 ad t_ bag.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_4ADT_Bag() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_4ADT_20KGBag.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	// JQ.3 1ADT 1CHD with VF/ValueAir Flights
	/**
	 * Test service end point_ pnr create_ j s_ r t_1 ad t_1 ch d_ vf.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_1ADT_1CHD_VF() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_1ADT_1CHD_VF.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	// JQ.4 3ADT 2CHD with VF/ValueAir Flights
	/**
	 * Test service end point_ pnr create_ j s_ r t_3 ad t_2 ch d_ vf.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_3ADT_2CHD_VF() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_3ADT_2CHD_VF.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ o w_ passive segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JS_OW_PassiveSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_1ADT_Passive.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j s_ r t_ passive segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JS_RT_PassiveSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_1ADT_Passive.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	@Test
	public void testServiceEndPoint_PNRCreate_JS_RT_MultiPax_PassiveSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_RT_2ADT_1CHD_1INF_Passive.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test 1ADT OW MultiSegment journey for both JQ and 9W
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JS_OW_MultiSegment_PassiveSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_1ADT_MultiSegment_Passive.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * INTA-60 GeneralRemarks for PNR Fullfilment
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_GeneralRemarks() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_GeneralRemarks.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * INTA-65 PNRRetrieve of Cancelled PNR
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRRetrieve_JS_CancelledPNR() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRRetrieveRQ_JS_CancelledPNR.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRRetrieve", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Booking Request for Special Product Class S4/Y4
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_ProductClass() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_ProductClass.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * INTA-69 Passenger Name Validation. Removing Special char and space from name
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate_JS_OW_1ADT_1INF_RemoveSpecialChar() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JS_OW_1ADT_1INF_SpecialChar.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	// ************************Jet Airways ******************************//
	/**
	 * Test service end point_ pnr create.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void testServiceEndPoint_PNRCreate() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ low budget.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_LowBudget() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_LowBudget_JA.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ ticket issue j a_1 adt.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_TicketIssueJA_1ADT() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/TicketissueRQ_JA_1ADT.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("TicketIssue", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ retrieve_ j a_1 adt.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_Retrieve_JA_1ADT() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRRetrive_RQ_JA_1ADT.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRRetrieve", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ retrieve_ j a_2 ad t_1 ch d_1 inf.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_Retrieve_JA_2ADT_1CHD_1INF() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRRetrive_RQ_JA_2ADT_1CHD_1INF.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRRetrieve", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_2 ad t_1 chd.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_2ADT_1CHD() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_2ADT_1CHD.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_2 ad t_1 ch d_1 inf.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_2ADT_1CHD_1INF() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_2ADT_1CHD_1INF.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ rt.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_RT.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ r t_ new.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT_New() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_RT_Vivek.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ o w_ pricing variance.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_OW_PricingVariance() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_OW_PricingVariance.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ o w_ pricing variance_ dummy sell rq.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_OW_PricingVariance_DummySellRQ() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_OW_PricingVariance_CallSellRQ.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ o w_ multi segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_OW_MultiSegment() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_OW_MultiSegment.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ r t_ multi segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT_MultiSegment() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_RT_MultiSegment.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ r t_ multi segment_ lower budget.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT_MultiSegment_LowerBudget() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_RT_MultiSegment_LCC.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ r t_ multi segment_ ticketing.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT_MultiSegment_Ticketing() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/TicketissueRQ_JA_1ADT_RT_MutliSegment.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("TicketIssue", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr retrieve_ j a_ r t_ multi segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRRetrieve_JA_RT_MultiSegment() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRRetrive_RQ_JA_1ADT_RT_MultiSegment.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRRetrieve", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	// Test case Timothy : 9W.1 + 9W.2 1adt 1chd DEL-SIN RT
	/**
	 * Test service end point_ pnr create_ j a_ r t_1 ad t_1 chd.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT_1ADT_1CHD() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_RT_1ADT_1CHD.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	// Test case Timothy : 9W.4 3adt 2chd DEL-SIN RT
	/**
	 * Test service end point_ pnr create_ j a_ r t_3 ad t_2 chd.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT_3ADT_2CHD() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_RT_3ADT_2CHD.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ o w_ passive segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_OW_PassiveSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_OW_1ADT_Passive.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * Test service end point_ pnr create_ j a_ r t_ passive segment.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_JA_RT_PassiveSegment() throws IOException {

		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_RT_1ADT_Passive.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	@Test
	@Ignore
	public void testServiceEndPoint_PNRCreate_GeneralRemarks() throws IOException {
		Resource xmlResource = new ClassPathResource("com/abb/framework/controller/PNRCreateRQ_JA_GeneralReamarks.xml");
		String requestXML = FileUtils.readFileToString(xmlResource.getFile());

		String responseXML = controller.serviceEndPoint("PNRCreate", requestXML, null);
		System.out.println(responseXML);
		assertNotNull(responseXML);

	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(OpenAxisControllerTest.class);
	}
}
