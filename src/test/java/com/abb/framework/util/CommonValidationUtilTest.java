package com.abb.framework.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.w3c.dom.Document;

import com.abb.framework.exception.OpenAirException;

/**
 * The Class CommonValidationUtilTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class CommonValidationUtilTest {

	/**
	 * INTA-69 Removes the space char from First and Last name.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void removeSpaceChar() throws OpenAirException, IOException {
		String xmlStr = "<PNRCreateRQ><EndTransaction /><CompletePNRElements><ReceivedFrom>luteapi</ReceivedFrom><Traveler AssociationID=\"T1\" Type=\"ADT\"><TravelerName><Surname>FIRST LA ST</Surname><GivenName>F I R S T</GivenName><Initial>FIRSTMIDDLE</Initial><Title>MR</Title><DateOfBirth>1980-12-11</DateOfBirth><Gender>M</Gender><NameRemark>RE</NameRemark></TravelerName></Traveler><Traveler AssociationID=\"T1\" Type=\"ADT\"><TravelerName><Surname>Parekh Rishabh</Surname><GivenName>Rishabh PArekh</GivenName><Initial>FIRSTMIDDLE</Initial><Title>MR</Title><DateOfBirth>1980-12-11</DateOfBirth><Gender>M</Gender><NameRemark>RE</NameRemark></TravelerName></Traveler></CompletePNRElements></PNRCreateRQ>";
		CommonValidationUtil validationUtil = new CommonValidationUtil();
		xmlStr = validationUtil.removeSpaceAndReplaceAccentChars(xmlStr);
		System.out.println(xmlStr);

	}

	/**
	 * Replace accent chars.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void replaceAccentChars() throws OpenAirException, IOException {
		String xmlStr = "<PNRCreateRQ><EndTransaction /><CompletePNRElements><ReceivedFrom>luteapi</ReceivedFrom><Traveler AssociationID=\"T1\" Type=\"ADT\"><TravelerName><Surname>Hellko $@# sur CD �</Surname><GivenName>AB�</GivenName><Initial>FIRSTMIDDLE</Initial><Title>MR</Title><DateOfBirth>1980-12-11</DateOfBirth><Gender>M</Gender><NameRemark>RE</NameRemark></TravelerName><Infant AssociationID=\"T1.1\" Type=\"INF\"><Surname>Smith Bhai</Surname><GivenName>Joe Ben@</GivenName><DateOfBirth>2013-06-19</DateOfBirth><Gender>M</Gender></Infant></Traveler><Traveler AssociationID=\"T1\" Type=\"ADT\"><TravelerName><Surname>Parekh Rishabh</Surname><GivenName>Rishabh GivenName With Space</GivenName><Initial>FIRSTMIDDLE</Initial><Title>MR</Title><DateOfBirth>1980-12-11</DateOfBirth><Gender>M</Gender><NameRemark>RE</NameRemark></TravelerName></Traveler></CompletePNRElements></PNRCreateRQ>";
		CommonValidationUtil validationUtil = new CommonValidationUtil();
		xmlStr = validationUtil.removeSpaceAndReplaceAccentChars(xmlStr);
		System.out.println("Transformed XML string ==" + xmlStr);
		Document doc = XMLUtils.loadXMLFromString(xmlStr);
		assertNotNull(doc);
		String givenName = XMLUtils.getTagValue(xmlStr, "GivenName");
		assertEquals("ABE", givenName);

	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(CommonValidationUtilTest.class);
	}
}
