package com.abb.framework.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.w3c.dom.Document;

import com.abb.framework.exception.OpenAirException;

/**
 * The Class XMLUtilsTest.
 */
public class XMLUtilsTest {

	/**
	 * Test get child node as string from xml.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetChildNodeAsStringFromXML() throws OpenAirException {
		String soapRequestXML = "<SOAPENV:Envelope xmlns:SOAPENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "<SOAPENV:Header>" + "<t:Transaction xmlns:t=\"xxs\">" + "<tc>"
				+ "<iden u=\"Cybage\" p=\"2VnjcpcYRVF6\" pseudocity=\"AD61\" agt=\"xmlboo01\" agtrole=\"Ticketing Agent\" agy=\"10627890\" agtpwd=\"farelogix1\" />" + "<provider session=\"EMPTY\" u=\"xmlboo01\" reset=\"yes\" system=\"DM\" />"
				+ "<agent user=\"xmlboo01\" />" + "<trace>xmlboo01</trace>" + "<script engine=\"FLXDM\" name=\"cybagedispatch.flxdm\" />" + "</tc>" + "</t:Transaction>" + "</SOAPENV:Header>" + "<SOAPENV:Body>"
				+ "<ns1:XXTransaction xmlns:ns1=\"xxs\">" + "<REQ>" + "<AirAvailabilityRQ>" + "<NumberInParty>1</NumberInParty>" + "<OriginDestination>" + "<Departure>" + "<CityCode>JFK</CityCode>" + "<Date>20100915</Date>" + "</Departure>"
				+ "<Arrival>" + "<CityCode>DXB</CityCode>" + "</Arrival>" + "</OriginDestination>" + "</AirAvailabilityRQ>" + "</REQ>" + "</ns1:XXTransaction>" + "</SOAPENV:Body>" + "</SOAPENV:Envelope>";
		String resultXML = XMLUtils.getChildNodeAsStringFromXML(soapRequestXML, "REQ");

		assertNotNull(resultXML);
		assertTrue("Result XML is blank.", resultXML.length() > 0);
		assertFalse(resultXML.contains("<SOAPENV:Header>"));
		assertFalse(resultXML.contains("<REQ>"));
		assertFalse(resultXML.contains("</REQ>"));
		assertTrue(resultXML.contains("<AirAvailabilityRQ>"));
		assertTrue(resultXML.contains("<NumberInParty>1</NumberInParty>"));
		assertTrue(resultXML.contains("<CityCode>DXB</CityCode>"));
		assertTrue(resultXML.contains("</AirAvailabilityRQ>"));
	}

	/**
	 * Test get child node as string from xm l_ blank_ xml.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test(expected = OpenAirException.class)
	public void testGetChildNodeAsStringFromXML_Blank_XML() throws OpenAirException {
		String soapRequestXML = "";
		String resultXML = XMLUtils.getChildNodeAsStringFromXML(soapRequestXML, "REQ");

		// Check Result
		assertNotNull(resultXML);
		assertEquals(0, resultXML.length());
	}

	/**
	 * Test get child node as string from xm l_ missing_ requested_ node.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetChildNodeAsStringFromXML_Missing_Requested_Node() throws OpenAirException {
		String soapRequestXML = "<SOAPENV:Envelope xmlns:SOAPENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "<SOAPENV:Header>" + "<t:Transaction xmlns:t=\"xxs\">" + "<tc>"
				+ "<iden u=\"Cybage\" p=\"2VnjcpcYRVF6\" pseudocity=\"AD61\" agt=\"xmlboo01\" agtrole=\"Ticketing Agent\" agy=\"10627890\" agtpwd=\"farelogix1\" />" + "<provider session=\"EMPTY\" u=\"xmlboo01\" reset=\"yes\" system=\"DM\" />"
				+ "<agent user=\"xmlboo01\" />" + "<trace>xmlboo01</trace>" + "<script engine=\"FLXDM\" name=\"cybagedispatch.flxdm\" />" + "</tc>" + "</t:Transaction>" + "</SOAPENV:Header>" + "<SOAPENV:Body>"
				+ "<ns1:XXTransaction xmlns:ns1=\"xxs\">" + "</ns1:XXTransaction>" + "</SOAPENV:Body>" + "</SOAPENV:Envelope>";
		String resultXML = XMLUtils.getChildNodeAsStringFromXML(soapRequestXML, "REQ");

		// Check Result
		assertNotNull(resultXML);
		assertEquals(0, resultXML.length());
	}

	/**
	 * Test load xml from string.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testLoadXMLFromString() throws OpenAirException {
		String soapRequestXML = "<SOAPENV:Envelope xmlns:SOAPENV=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "<SOAPENV:Header>" + "<t:Transaction xmlns:t=\"xxs\">" + "<tc>"
				+ "<iden u=\"Cybage\" p=\"2VnjcpcYRVF6\" pseudocity=\"AD61\" agt=\"xmlboo01\" agtrole=\"Ticketing Agent\" agy=\"10627890\" agtpwd=\"farelogix1\" />" + "<provider session=\"EMPTY\" u=\"xmlboo01\" reset=\"yes\" system=\"DM\" />"
				+ "<agent user=\"xmlboo01\" />" + "<trace>xmlboo01</trace>" + "<script engine=\"FLXDM\" name=\"cybagedispatch.flxdm\" />" + "</tc>" + "</t:Transaction>" + "</SOAPENV:Header>" + "<SOAPENV:Body>"
				+ "<ns1:XXTransaction xmlns:ns1=\"xxs\">" + "<REQ>" + "<AirAvailabilityRQ>" + "<NumberInParty>1</NumberInParty>" + "<OriginDestination>" + "<Departure>" + "<CityCode>JFK</CityCode>" + "<Date>20100915</Date>" + "</Departure>"
				+ "<Arrival>" + "<CityCode>DXB</CityCode>" + "</Arrival>" + "</OriginDestination>" + "</AirAvailabilityRQ>" + "</REQ>" + "</ns1:XXTransaction>" + "</SOAPENV:Body>" + "</SOAPENV:Envelope>";
		Document doc = XMLUtils.loadXMLFromString(soapRequestXML);

		assertNotNull(doc);
		assertTrue(doc.getElementsByTagName("tc").getLength() > 0);
		assertTrue(doc.getElementsByTagName("REQ").getLength() > 0);
	}

	/**
	 * Test load xml from string_ exception.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test(expected = OpenAirException.class)
	public void testLoadXMLFromString_Exception() throws OpenAirException {
		String soapRequestXML = "";
		Document doc = XMLUtils.loadXMLFromString(soapRequestXML);

		assertNotNull(doc);
	}

	/**
	 * Test get tag value_ valid.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetTagValue_Valid() throws OpenAirException {
		String xmlFile = "<PokeShoppingFileResult><HasError>true</HasError><ServiceError><Message>Error</Message></ServiceError></PokeShoppingFileResult>";
		String returnValue = XMLUtils.getTagValue(xmlFile, "HasError");

		assertEquals("true", returnValue);
	}

	/**
	 * Test get tag value_ null.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetTagValue_Null() throws OpenAirException {
		String xmlFile = "<PokeShoppingFileResult><HasError>true</HasError><ServiceError><Message>Error</Message></ServiceError></PokeShoppingFileResult>";
		String returnValue = XMLUtils.getTagValue(xmlFile, "HashError");

		assertEquals(null, returnValue);
	}

	/**
	 * Test get child node as string from xm l_ blank_ parent.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetChildNodeAsStringFromXML_Blank_Parent() throws OpenAirException {
		String xmlFile = "<PokeShoppingFileResult><HasError>true</HasError><a:ShoppingFile xmlns:a=\"\"/><ServiceError><Message>Error</Message></ServiceError></PokeShoppingFileResult>";
		String returnValue = XMLUtils.getChildNodeAsStringFromXML(xmlFile, "a:ShoppingFile");

		assertEquals("", returnValue);
	}

	/**
	 * Test get complete tag as string.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetCompleteTagAsString() throws OpenAirException {
		String xmlFile = "<Flight><Airline>AA</Airline></Flight>";
		String resultValue = XMLUtils.getCompleteTagAsString(xmlFile, "Airline");

		assertNotNull(resultValue);
	}

	/**
	 * Test get attribute value.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetAttributeValue() throws OpenAirException {
		String xmlFile = "<Flight Source=\"ga\"><Airline>AA</Airline></Flight>";
		String resultValue = XMLUtils.getAttributeValue(xmlFile, "Flight", "Source");

		assertEquals("ga", resultValue);
	}

	/**
	 * Test get attribute values.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetAttributeValues() throws OpenAirException {
		String xmlFile = "<response><auth><token>8%2F9%2FFdolISvg2kd8HJbds%3D</token><expiry>2012-08-25T19:40:55Z</expiry></auth><fields><field name=\"status\" value=\"RECEIVED\"/></fields><data><orders><oid id=\"2012-08-24-5-2700\" rloc=\"%FZREQ\"/><oid id=\"DE872982AE\" rloc=\"%TYUUE\"/></orders></data></response>";
		String[] resultValues = XMLUtils.getAttributeValues(xmlFile, "oid", "rloc");

		assertNotNull(resultValues);
		assertEquals(2, resultValues.length);
	}

	/**
	 * Test get tag values.
	 * 
	 * @throws OpenAirException
	 *             the open air exception
	 */
	@Test
	public void testGetTagValues() throws OpenAirException {
		String xmlFile = "<CompletePNRElements><Itinerary><Flight Source=\"AA\" AssociationID=\"F1\" OriginDestinationID=\"O1\" linenumber=\"2\"><Departure><AirportCode>LGA</AirportCode><Date>2011-11-10</Date><Time>08:20</Time><Terminal>B</Terminal></Departure><Arrival><AirportCode>MIA</AirportCode><Date>2011-11-10</Date><Time>11:30</Time><Terminal></Terminal><ChangeOfDay>0</ChangeOfDay></Arrival><Carrier><AirlineCode>AA</AirlineCode><FlightNumber>2015</FlightNumber></Carrier><Equipment><Code>757</Code><EquipmentName>Boeing 757 (Passenger)</EquipmentName></Equipment><NumberInParty>1</NumberInParty><ActionCode>NN</ActionCode><ClassOfService>Q</ClassOfService></Flight><Flight Source=\"AA\" AssociationID=\"F2\" OriginDestinationID=\"O2\" linenumber=\"3\"><Departure><AirportCode>MIA</AirportCode><Date>2011-11-15</Date><Time>10:00</Time><Terminal></Terminal></Departure><Arrival><AirportCode>LGA</AirportCode><Date>2011-11-15</Date><Time>12:55</Time><Terminal>B</Terminal><ChangeOfDay>0</ChangeOfDay></Arrival><Carrier><AirlineCode>AA</AirlineCode><FlightNumber>1124</FlightNumber></Carrier><Equipment><Code>757</Code><EquipmentName>Boeing 757 (Passenger)</EquipmentName></Equipment><NumberInParty>1</NumberInParty><ActionCode>NN</ActionCode><ClassOfService>Q</ClassOfService></Flight></Itinerary></CompletePNRElements>";
		List<String> resultValues = XMLUtils.getTagValues(xmlFile, "FlightNumber");

		assertNotNull(resultValues);
		assertEquals(2, resultValues.size());
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(XMLUtilsTest.class);
	}
}
