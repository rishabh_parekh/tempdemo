package com.abb.framework.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.abb.framework.util.GeneratePNR;


/**
 * The Class GeneratePNRTest.
 */
public class GeneratePNRTest {

	/**
	 * Test get unique pnr.
	 */
	@Test
	public void testGetUniquePNR() {
		String uniquePNR = GeneratePNR.getPNR();

		assertNotNull(uniquePNR);
		assertEquals(6, uniquePNR.length());
		assertTrue(GeneratePNR.validatePNR(uniquePNR));
	}

	/**
	 * Test validate unique pnr.
	 */
	@Test
	public void testValidateUniquePNR() {
		boolean result = GeneratePNR.validatePNR("Lmd3M5");

		assertTrue(result);
	}

	/**
	 * Test validate unique pn r_ false.
	 */
	@Test
	public void testValidateUniquePNR_False() {
		boolean result = GeneratePNR.validatePNR("Rpm3d5");

		assertFalse(result);
	}

	/**
	 * Test validate unique pn r_ wrong_ pnr.
	 */
	@Test
	public void testValidateUniquePNR_Wrong_PNR() {
		boolean result = GeneratePNR.validatePNR("Lmd3MD");

		assertFalse(result);
	}
}
