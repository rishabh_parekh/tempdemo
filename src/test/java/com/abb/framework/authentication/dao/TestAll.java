package com.abb.framework.authentication.dao;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


/**
 * The Class TestAll.
 */
@RunWith(Suite.class)
@SuiteClasses({ AuthenticationDAOTest.class })
public class TestAll {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
