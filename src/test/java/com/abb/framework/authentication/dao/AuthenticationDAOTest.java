package com.abb.framework.authentication.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * The Class AuthenticationDAOTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class AuthenticationDAOTest {

	/** The authentication dao. */
	@Autowired
	private AuthenticationDAO authenticationDao;

	/**
	 * Test authenticate user.
	 */
	@Test
	public void testAuthenticateUser() {
		boolean result = authenticationDao.authenticateUser("oaws", "oaws");

		assertTrue(result);
	}

	/**
	 * Test authenticate user_ invalid.
	 */
	@Test
	public void testAuthenticateUser_Invalid() {
		boolean result = authenticationDao.authenticateUser("oaws", "dummy");

		assertFalse(result);
	}

	/**
	 * Test authenticate user_ null value.
	 */
	@Test
	public void testAuthenticateUser_NullValue() {
		boolean result = authenticationDao.authenticateUser(null, null);

		assertFalse(result);
	}

	/**
	 * Test authenticate user_ valid.
	 */
	@Test
	public void testAuthenticateUser_Valid() {

		boolean authResult = authenticationDao.authenticateUser("Farelogix", "TEMP02", "sprkuser01", "farelogix1");

		// Check Result
		assertEquals(true, authResult);
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(AuthenticationDAOTest.class);
	}
}
