package com.abb.framework.authentication.service;

import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.authentication.service.AuthenticationService;
import com.abb.framework.exception.OpenAirException;


/**
 * The Class AuthenticationServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class AuthenticationServiceTest {

	/** The authentication service. */
	@Autowired
	private AuthenticationService authenticationService;

	/**
	 * Test authenticate request.
	 *
	 * @throws OpenAirException the open air exception
	 */
	@Test
	public void testAuthenticateRequest() throws OpenAirException {
		boolean result = authenticationService.authenticateRequest("");

		assertFalse(result);
	}
}
