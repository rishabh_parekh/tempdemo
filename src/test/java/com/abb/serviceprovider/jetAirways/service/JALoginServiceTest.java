package com.abb.serviceprovider.jetAirways.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.rmi.RemoteException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.serviceprovider.SPService;


/**
 * The Class JALoginServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class JALoginServiceTest {

	/** The ja login service. */
	@Autowired
	private SPService jaLoginService;

	/**
	 * Test execute.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testExecute() throws Exception {
		jaLoginService.execute();
		String loginResponseXML = jaLoginService.getOpenAirlinesServiceEnvelopXML().getSpLoginResponseXML();

		assertNotNull(loginResponseXML);
		assertTrue(loginResponseXML.contains(">OK<"));
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(JALoginServiceTest.class);
	}
}
