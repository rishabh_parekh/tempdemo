package com.abb.serviceprovider.jetAirways.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.rmi.RemoteException;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.serviceprovider.SPService;
import com.abb.serviceprovider.jetAirways.service.JALogoutService;


/**
 * The Class JALogoutServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class JALogoutServiceTest {

	/** The ja login service. */
	@Autowired
	private SPService jaLoginService;

	/** The ja logout service. */
	@Autowired
	private SPService jaLogoutService;

	/**
	 * Test execute.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testExecute() throws Exception {
		jaLoginService.execute();
		String loginResponseXML = jaLoginService.getOpenAirlinesServiceEnvelopXML().getSpLoginResponseXML();

		String token = StringUtils.substringBetween(loginResponseXML, "<BinarySecurityToken>", "</BinarySecurityToken>");
		assertNotNull(loginResponseXML);
		assertTrue(loginResponseXML.contains(">OK<"));

		((JALogoutService) jaLogoutService).setBinarySecurityToken(token);
		jaLogoutService.execute();
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(JALogoutServiceTest.class);
	}
}
