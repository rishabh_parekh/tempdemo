package com.abb.serviceprovider.jetStar.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.rmi.RemoteException;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.abb.framework.serviceprovider.SPService;


/**
 * The Class JSLoginServiceTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ws-servlet.xml")
public class JSLoginServiceTest {

	/** The js login service. */
	@Autowired
	private SPService jsLoginService;

	/**
	 * Test execute.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void testExecute() throws Exception {
		jsLoginService.execute();
		String loginResponseXML = jsLoginService.getOpenAirlinesServiceEnvelopXML().getSpLoginResponseXML();

		assertNotNull(loginResponseXML);
		String token = StringUtils.substringBetween(loginResponseXML, "<Signature>", "</Signature>");
		assertTrue(token.length() > 0);
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(JSLoginServiceTest.class);
	}
}
