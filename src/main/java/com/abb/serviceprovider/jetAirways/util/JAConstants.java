package com.abb.serviceprovider.jetAirways.util;


/**
 * The Class JAConstants.
 */
public class JAConstants {

	// INTA-30 true=enable false=disable
	/** The enable pricing variance check. */
	public static boolean enablePricingVarianceCheck = true;
}
