package com.abb.serviceprovider.jetAirways;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.abb.framework.exception.OpenAirServiceNotFoundForProviderException;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.serviceprovider.ServiceProvider;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.jetAirways.service.JAPNRCreateService;
import com.abb.serviceprovider.jetAirways.service.JAPNRRetrieveService;
import com.abb.serviceprovider.jetAirways.service.JATicketIssueService;

/**
 * The Class JAServiceProvider.
 */
@Service("jaServiceProvider")
public class JAServiceProvider implements ServiceProvider {

	private static final Logger LOG = LoggerFactory.getLogger(JAServiceProvider.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getTicketIssueService()
	 */
	public SPService getTicketIssueService() throws OpenAirServiceNotFoundForProviderException {
		SPService jaTicketIssueService = ApplicationContextUtil.getBean("jaTicketIssueService", JATicketIssueService.class);
		LOG.debug("JATicketIssueService Hashcode:{}", jaTicketIssueService.hashCode());
		return jaTicketIssueService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getFareSearchService()
	 */
	@Override
	public SPService getFareSearchService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getFlightPriceService()
	 */
	@Override
	public SPService getFlightPriceService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getPNRCreateService()
	 */
	@Override
	public SPService getPNRCreateService() throws OpenAirServiceNotFoundForProviderException {
		SPService jaPNRCreateService = ApplicationContextUtil.getBean("jaPNRCreateService", JAPNRCreateService.class);
		LOG.debug("JAPNRCreateService Hashcode:{}", jaPNRCreateService.hashCode());
		return jaPNRCreateService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getPNRRetrieveService()
	 */
	@Override
	public SPService getPNRRetrieveService() throws OpenAirServiceNotFoundForProviderException {
		SPService jaPNRRetrieveService = ApplicationContextUtil.getBean("jaPNRRetrieveService", JAPNRRetrieveService.class);
		LOG.debug("JAPNRRetrieveService Hashcode:{}", jaPNRRetrieveService.hashCode());
		return jaPNRRetrieveService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getPNRCancelService()
	 */
	@Override
	public SPService getPNRCancelService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getFareRulesService()
	 */
	@Override
	public SPService getFareRulesService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getServiceProviderName()
	 */
	@Override
	public String getServiceProviderName() {
		return "JAServiceProvider";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getAddPaymentsInJetStar()
	 */
	@Override
	public SPService getAddPaymentsInJetStar() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

}
