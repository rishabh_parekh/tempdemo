<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sec="http://schemas.xmlsoap.org/ws/2002/12/secext"
	xmlns:mes="http://www.ebxml.org/namespaces/messageHeader">

	<xsl:output method="xml" indent="yes" />
	<xsl:param name="CPA_id" />
	<xsl:param name="conversation_id" />
	<xsl:param name="service" />
	<xsl:param name="action" />
	<xsl:param name="message_id" />
	<xsl:param name="time_stamp" />
	<xsl:param name="BinarySecurityToken" />

	<xsl:template match="/">
		<xsl:element name="OpenAirSOAPHeader">
			<xsl:element name="sec:Security">
				<xsl:element name="sec:BinarySecurityToken">
					<xsl:choose>
						<xsl:when test="not($BinarySecurityToken = '')">
							<xsl:value-of select="$BinarySecurityToken" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="//Security/BinarySecurityToken" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<xsl:element name="mes:MessageHeader">
				<xsl:element name="mes:CPAId">
					<xsl:value-of select="$CPA_id" />
				</xsl:element>
				<xsl:element name="mes:ConversationId">
					<xsl:value-of select="$conversation_id" />
				</xsl:element>
				<xsl:element name="mes:Service">
					<xsl:value-of select="$service" />
				</xsl:element>
				<xsl:element name="mes:Action">
					<xsl:value-of select="$action" />
				</xsl:element>

				<xsl:element name="mes:MessageData">
					<xsl:element name="mes:MessageId">
						<xsl:value-of select="$message_id" />
					</xsl:element>
					<xsl:element name="mes:Timestamp">
						<xsl:value-of select="$time_stamp" />
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>