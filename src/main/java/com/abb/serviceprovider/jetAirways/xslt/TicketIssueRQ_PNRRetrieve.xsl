<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="/">
	<xsl:apply-templates
				select="//SARequest//TicketIssueRQ" />
	</xsl:template>	
	
	<xsl:template match="TicketIssueRQ">
		<xsl:element name="OTA_TravelItineraryReadRQ" xmlns="http://www.vedaleon.com/webservices">
			<xsl:element name="OTA_TravelItineraryReadRQ">
			<xsl:attribute name="Version">
			<xsl:value-of select="'1.0'"></xsl:value-of>
			</xsl:attribute>
			<xsl:attribute name="Target">
			<xsl:value-of select="'Test'"></xsl:value-of>
			</xsl:attribute>
				<xsl:element name="UniqueID">
					<xsl:attribute name="ID">
						<xsl:value-of select="//RecordLocator"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

