<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns="http://www.vedaleon.com/webservices" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="JA_Constants.xsl" />
	<xsl:variable name="intInc" select="intinc:new()" />
	<xsl:template match="/">
		<xsl:element name="ProcessPassiveSegments">
			<!-- PassiveSegmentsRQ -->
			<xsl:element name="PassiveSegmentsRQ">
				<!-- Default values as per API Schema shared by 9W -->
				<xsl:attribute name="Target">
					<xsl:value-of select="'Test'"/>
				</xsl:attribute>
				<xsl:attribute name="Version">
					<xsl:value-of select="'1.0'"/>
				</xsl:attribute>
				
				<!-- BookingReloc : 9W recordLocator for Retrieve -->
				<xsl:element name="BookingReloc">
					<xsl:value-of select="//SARequest/PNRCreateRQ/RecordLocator"/>
				</xsl:element>
				
				<xsl:apply-templates
							select="//SARequest//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]"
							mode="uniqueFlightTag" />
				
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- OriginDestinationOption -->
	<xsl:template match="Flight" mode="uniqueFlightTag">
		<xsl:variable name="originDestId" select="current()/@OriginDestinationID" />
			<xsl:apply-templates
				select="//SARequest//Flight[@OriginDestinationID=$originDestId]" />
	</xsl:template>

	<!-- FlightSegment -->
	<xsl:template match="Flight">
		<xsl:element name="FlightSegment">
			<xsl:attribute name="DepartureDateTime">
				<xsl:value-of select="concat(Departure/Date,'T',Departure/Time,':00')" />
			</xsl:attribute>
			<xsl:attribute name="ArrivalDateTime">
				<xsl:value-of select="concat(Arrival/Date,'T',Arrival/Time,':00')" />
			</xsl:attribute>
			<xsl:attribute name="RPH">
				<xsl:value-of select="intinc:getNext($intInc)" />
			</xsl:attribute>
			<xsl:attribute name="FlightNumber">
				<xsl:value-of select="Carrier/FlightNumber" />
			</xsl:attribute>
			<xsl:attribute name="ResBookDesigCode">
				<xsl:value-of select="ClassOfService" />
			</xsl:attribute>
			<xsl:attribute name="NumberInParty">
				<xsl:value-of select="NumberInParty" />
			</xsl:attribute>
			<xsl:attribute name="Status">
				<xsl:value-of select="$ORIGIN_DEST_OPTION_STATUS"></xsl:value-of>
			</xsl:attribute>
			<xsl:element name="DepartureAirport">
				<xsl:attribute name="LocationCode">
					<xsl:value-of select="Departure/AirportCode" />
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="ArrivalAirport">
				<xsl:attribute name="LocationCode">
					<xsl:value-of select="Arrival/AirportCode" />
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="OperatingAirline">
				<xsl:attribute name="Code">
					<xsl:value-of select="Carrier/AirlineCode" />
				</xsl:attribute>
				<xsl:attribute name="CodeContext">
					<xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute>
				<xsl:attribute name="FlightNumber">
					<xsl:value-of select="Carrier/FlightNumber" />
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="MarketingAirline">
				<xsl:attribute name="Code">
					<xsl:value-of select="Carrier/AirlineCode" />
				</xsl:attribute>
				<xsl:attribute name="CodeContext">
					<xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute>
			</xsl:element>
			
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>