<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" xmlns:exslt="http://exslt.org/common" xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:strip-space elements="*" />
	<xsl:import href="JA_Constants.xsl" />
	<xsl:import href="../../common/xslt/Lookups.xsl" />
	<xsl:import href="../../common/xslt/FormatDate.xsl" />
	<xsl:import href="JA_ErrorHandling.xsl" />
	<xsl:variable name="Equipment_Lookup"
		select="document('lookupXMLs/EquipmentCode_Lookup.xml')//Equipment" />
	<xsl:variable name="Cabin_Lookup"
		select="document('lookupXMLs/Cabin_Classes_Lookup.xml')//Cabin" />
	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />

	<xsl:output method="xml" indent="yes" />
	<xsl:variable name="CRSID" select="'F1'" />
	<xsl:variable name="TicketIssued" select="'N'" />
	<xsl:variable name="QueueRetrieved" select="'N'" />
	<xsl:variable name="CCCode" select="'K'" />
	<xsl:variable name="ActionCode" select="'K'" />
	<xsl:variable name="FareType" select="'PUBL'" />
	<xsl:variable name="FareNumber" select="'1'" />
	<xsl:variable name="FarePriced" select="'AUTO'" />
	<xsl:variable name="NumberOfDecimals" select="'2'" />
	<xsl:variable name="CommissionType" select="'A'" />
	<xsl:variable name="BaseAmountType" select="'A'" />
	<xsl:variable name="TaxAmountType" select="'A'" />
	<xsl:variable name="InfantElementNumber" select="'.1'" />
	<xsl:variable name="SourceName" select="'9W'" />
	<xsl:variable name="Title" select="'JetAirways'" />
	<xsl:variable name="TaxDescription" select="'Tax'" />
	<xsl:variable name="FormOfPayment" select="'CreditCard'" />
	<xsl:variable name="RecordLocatorInRequest" select="//PNRCreateRQ/RecordLocator" />
	<xsl:variable name="serviceFeeNotification" select="'Service Fee is applied.'" />
	<xsl:variable name="cnt" select="'1'" />
	<xsl:variable name="counter_global" select="intinc:new(30)" />
	<xsl:variable name="counter_segmentObject" select="intinc:new(1)" />

	<!-- Stop Creation of PNRViewRS other tags, when error message received -->
	<xsl:template match="/">
		<!-- PNRVewRS -->
		<xsl:element name="PNRViewRS">
			<xsl:choose>
				<!-- SoapFaultExcepptionHandling -->
				<xsl:when test="//Fault/* or //error/* or //Errors/* or //Warning/*">
					<xsl:call-template name="errorHandling" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="//SPResponse" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="SPResponse">

		<xsl:variable name="recordLocator"
			select="//OTA_AirBookRQResponse/OTA_AirBookRS/AirReservation//BookingReferenceID/@ID" />

		<xsl:element name="PNRIdentification">
			<xsl:attribute name="TicketIssued">
				<xsl:value-of select="$TicketIssued" />
			</xsl:attribute>
			<xsl:attribute name="QueueRetrieved">
				<xsl:value-of select="$QueueRetrieved" />
			</xsl:attribute>
			<xsl:attribute name="FareDataExists">
				<xsl:value-of select="'Y'" />
			</xsl:attribute>
			<xsl:element name="RecordLocator">
				<xsl:value-of select="$recordLocator" />
			</xsl:element>
			<xsl:element name="CreationDate">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr"
						select="//OTA_AirBookRQResponse/OTA_AirBookRS/@TimeStamp" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="CreationTime">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr"
						select="//OTA_AirBookRQResponse/OTA_AirBookRS/@TimeStamp" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="CreationAgent">
				<xsl:value-of select="//tc/iden/@agt" />
			</xsl:element>
			<xsl:element name="Ownership">
				<xsl:element name="CRSID">
					<xsl:value-of select="$CRSID" />
				</xsl:element>
				<xsl:element name="Agency">
					<xsl:value-of select="//tc/iden/@agy" />
				</xsl:element>
				<xsl:element name="PseudoCityCode">
					<xsl:value-of select="//tc/iden/@pseudocity" />
				</xsl:element>
			</xsl:element>
			<xsl:element name="CurrentPseudoCityCode">
				<xsl:value-of select="//tc/iden/@pseudocity" />
			</xsl:element>
			<xsl:element name="ReceivedFrom">
				<xsl:value-of select="//PNRCreateRQ//ReceivedFrom" />
			</xsl:element>
		</xsl:element>

		<!-- FareGroup -->
		<xsl:apply-templates
			select="//SPResponse//AirSellRQResponse//PricedItineraries/PricedItinerary" />

		<!-- Traveler -->
		<xsl:copy-of select="//SARequest//PNRCreateRQ/CompletePNRElements/Traveler" />

		<!-- Telephone -->
		<xsl:copy-of select="//SARequest/PNRCreateRQ/CompletePNRElements/Telephone" />

		<!-- EmailAddress -->
		<xsl:copy-of select="//SARequest/PNRCreateRQ/OtherPNRElements/EmailAddress" />

		<!-- AirGroup -->
		<xsl:element name="AirGroup">
			<xsl:copy-of
				select="//SARequest/PNRCreateRQ/CompletePNRElements/Itinerary/Flight" />
		</xsl:element>

		<xsl:element name="Ticketing">
			<xsl:attribute name="Source"> 
					<xsl:value-of select="$SourceName" />
			</xsl:attribute>
			<xsl:attribute name="SourceRef"> 
					<xsl:value-of select="$recordLocator" />
				 </xsl:attribute>
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Date">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr"
						select="//OTA_AirBookRQResponse/OTA_AirBookRS/AirReservation/Ticketing/@TicketTimeLimit" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="Time">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr"
						select="//OTA_AirBookRQResponse/OTA_AirBookRS/AirReservation/Ticketing/@TicketTimeLimit" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>
		</xsl:element>

		<xsl:element name="BillingAndDeliveryData">
			<xsl:element name="FormOfPayment">
				<xsl:attribute name="Source">
					<xsl:value-of select="$SourceName" />
				</xsl:attribute>
				<xsl:attribute name="SourceRef">
					<xsl:value-of select="$recordLocator" />
				</xsl:attribute>
				<xsl:element name="ElementNumber">
					<xsl:value-of select="intinc:getNext($counter_global)" />
				</xsl:element>
				<xsl:element name="Other">
					<xsl:value-of select="$FormOfPayment" />
				</xsl:element>
			</xsl:element>

			<!-- BookingContact <xsl:apply-templates select="//CommitRequest/BookingRequest/Booking/BookingContacts/BookingContact"> 
				</xsl:apply-templates> -->

		</xsl:element>

		<!-- AccountingLine <xsl:apply-templates select="" /> -->

		<!-- GeneralRemark -->
		<xsl:apply-templates
			select="//SARequest//PNRCreateRQ/OtherPNRElements/GeneralRemark">
		</xsl:apply-templates>
	</xsl:template>

	<!-- FareGroup -->
	<xsl:template match="PricedItinerary">
		<xsl:element name="FareGroup">
			<xsl:attribute name="FareType">
			 	<xsl:value-of select="$FareType" />
			</xsl:attribute>

			<xsl:attribute name="FareNumber">
				 <xsl:value-of select="$FareNumber" /> 
			</xsl:attribute>
			<xsl:attribute name="FarePriced">
				<xsl:value-of select="$FarePriced" />
			</xsl:attribute>

			<xsl:attribute name="TravelerCount">
				<xsl:value-of
				select="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler)+count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler/Infant)" />
			</xsl:attribute>

			<xsl:variable name="AllPaxTotalPrice">
				<xsl:for-each
					select="AirItineraryPricingInfo/PTC_FareBreakdowns/PTC_FareBreakdown/PassengerFare/TotalFare">
					<PriceForSum>
						<xsl:value-of select="@Amount" />
					</PriceForSum>
				</xsl:for-each>
			</xsl:variable>

			<xsl:variable name="Final_Total"
				select="(sum(exslt:node-set($AllPaxTotalPrice)/PriceForSum))" />

			<xsl:attribute name="TotalPrice">
				<xsl:value-of select="round(format-number($Final_Total,'#.00') * 100)" />						
			</xsl:attribute>

			<xsl:attribute name="Source">
				<xsl:value-of select="$SourceName" />
			</xsl:attribute>

			<!-- CurrencyCode -->
			<xsl:element name="CurrencyCode">
				<xsl:attribute name="NumberOfDecimals">
					<xsl:value-of select="$CURRENCY_NUMBER_OF_DECIMAL" />
				</xsl:attribute>
				<xsl:value-of
					select="AirItineraryPricingInfo/PTC_FareBreakdowns/PTC_FareBreakdown/PassengerFare/TotalFare/@CurrencyCode" />
			</xsl:element>

			<!-- TravelerGroup -->
			<xsl:for-each
				select="AirItineraryPricingInfo/PTC_FareBreakdowns/PTC_FareBreakdown">

				<xsl:variable name="CurrentPaxType">
					<xsl:value-of select="PassengerTypeQuantity/@Code" />
				</xsl:variable>
				<xsl:variable name="CurrentPaxType_Calculation" select="PassengerTypeQuantity/@Code" />

				<xsl:if
					test="not($CurrentPaxType_Calculation=preceding-sibling::node()/PassengerTypeQuantity/@Code)">

					<xsl:variable name="PaxTotal_BasePrice">
						<xsl:value-of select="PassengerFare/BaseFare/@Amount" />
					</xsl:variable>

					<xsl:variable name="Taxes">
						<xsl:for-each select="PassengerFare/Taxes/Tax">
							<Tax_Amount>
								<xsl:value-of select="@Amount" />
							</Tax_Amount>
							<Tax_Description>
								<xsl:value-of select="@TaxCode" />
							</Tax_Description>
						</xsl:for-each>
					</xsl:variable>

					<xsl:variable name="PaxTotalPrice">
						<xsl:value-of select="PassengerFare/TotalFare/@Amount" />
					</xsl:variable>

					<xsl:element name="TravelerGroup">

						<xsl:variable name="Counter" select="intinc:new()" />
						<xsl:variable name="counterforSegment_reset" select="intinc:resetCounter($counter_segmentObject)" />
						<xsl:if test="$counterforSegment_reset"></xsl:if>
						
						<xsl:variable name="TotalTravelerType">
							<!-- <xsl:value-of
								select="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler[@Type=$CurrentPaxType])" /> -->
								
							<!-- changed for counting INF PAX -->
							<xsl:choose>
								<xsl:when test="$CurrentPaxType = 'CHD' or $CurrentPaxType = 'CNN'">
									<xsl:if test="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler[@Type='CHD']) > 0">
										<xsl:value-of select="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler[@Type='CHD'])" />
									</xsl:if>
									<xsl:if test="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler[@Type='CNN']) > 0">
										<xsl:value-of select="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler[@Type='CNN'])" />
									</xsl:if>
								</xsl:when>
								<xsl:when test="$CurrentPaxType = 'ADT'">
									<xsl:value-of
										select="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler[@Type=$CurrentPaxType])" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of
										select="count(//SARequest//PNRCreateRQ/CompletePNRElements/Traveler/Infant[@Type=$CurrentPaxType])" />
								</xsl:otherwise>
								
							</xsl:choose>
						</xsl:variable>

						<xsl:attribute name="TypeRequested"> 
							<xsl:value-of select="$CurrentPaxType" />
						 </xsl:attribute>
						<xsl:attribute name="TypePriced"> 
							<xsl:value-of select="$CurrentPaxType" />
						 </xsl:attribute>

						<xsl:attribute name="TypeTotalPrice">
							<xsl:value-of
							select="round(format-number(($PaxTotalPrice)*$TotalTravelerType,'#.00') * 100)" />
						</xsl:attribute>

						<xsl:attribute name="TypeCount"> 
							<xsl:value-of select="$TotalTravelerType" /> 
						</xsl:attribute>
						<xsl:element name="Price">

							<xsl:attribute name="Total">
								<xsl:value-of
								select="round(format-number($PaxTotalPrice,'#.00') * 100)" />
							</xsl:attribute>

							<xsl:element name="BaseFare">
								<xsl:attribute name="Amount"> 
					       			<xsl:value-of
									select="round(format-number($PaxTotal_BasePrice,'#.00') * 100)" />
								</xsl:attribute>
							</xsl:element>

							<xsl:element name="Taxes">
								<xsl:if test="count(exslt:node-set($Taxes)/*) &gt; 0">
									<xsl:attribute name="Amount">
									<xsl:value-of
										select="round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount),'#.00') * 100)" />
								</xsl:attribute>
									<xsl:for-each select="exslt:node-set($Taxes)/Tax_Amount">
										<xsl:variable name="TaxNode_Count" select="intinc:getNext($Counter)" />
										<xsl:element name="Tax">
											<xsl:attribute name="Amount">
											<xsl:value-of
												select="round(format-number(exslt:node-set($Taxes)/Tax_Amount[position()=$TaxNode_Count],'#.00') * 100)" />
										</xsl:attribute>
											<xsl:element name="Description">
												<xsl:value-of
													select="exslt:node-set($Taxes)/Tax_Description[position()=$TaxNode_Count]" />
											</xsl:element>
										</xsl:element>
									</xsl:for-each>
								</xsl:if>
							</xsl:element>
						</xsl:element>

						<!-- FareRules -->
						<xsl:element name="FareRules">
						
						<xsl:for-each
								select="//SARequest//PNRCreateRQ//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]">
								<xsl:call-template name="JourneyInfo"/>
						</xsl:for-each>
							
						<!-- Rules -->
						<xsl:for-each select="//SPResponse//OTA_AirRulesRQResponse//Text">
							<xsl:element name="Remark">
								<xsl:value-of select="current()" />
							</xsl:element>
						</xsl:for-each>

						</xsl:element>
					</xsl:element>
				</xsl:if>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>

	<!-- Rules -->
	<xsl:template name="JourneyInfo">
		<xsl:element name="FareInfo">
		
			<xsl:variable name="originDestId" select="current()/@OriginDestinationID" />
			<xsl:variable name="associationID" select="current()/@AssociationID" />
			
			<xsl:element name="DepartureCode">
				<xsl:value-of select="Departure/AirportCode" />
			</xsl:element>
			<xsl:element name="DepartureDate">
				<xsl:value-of select="Departure/Date" />
			</xsl:element>
			<xsl:element name="DepartureTime">
				<xsl:value-of select="Departure/Time" />
			</xsl:element>
			
			<xsl:element name="ArrivalCode">
				<xsl:value-of select="//SARequest/PNRCreateRQ//Flight[@OriginDestinationID=$originDestId][last()]/Arrival/AirportCode" />
			</xsl:element>
			<xsl:element name="ArrivalDate">
				<xsl:value-of select="//SARequest/PNRCreateRQ//Flight[@OriginDestinationID=$originDestId][last()]/Arrival/Date" />
			</xsl:element>
			<xsl:element name="ArrivalTime">
				<xsl:value-of select="//SARequest/PNRCreateRQ//Flight[@OriginDestinationID=$originDestId][last()]/Arrival/Time" />
			</xsl:element>
			<xsl:element name="AirlineCode">
				<xsl:value-of select="//SARequest/PNRCreateRQ//Flight[@OriginDestinationID=$originDestId][last()]/@Source" />
			</xsl:element>
			<!-- TODO: Farebasis code PAX wise -->
			<xsl:element name="FareBasisCode">
				<xsl:value-of select="//SPResponse//AirSellRQResponse//FareBasisCode" />
			</xsl:element>
			
			
			<!-- RelatedSegment -->
			<xsl:for-each
				select="//SARequest/PNRCreateRQ//Flight[@OriginDestinationID=$originDestId]">
				<xsl:variable name="ClassOfService" select="Fares/Fare/ClassOfService" />
				<xsl:call-template name="SegmentInfo" />
			</xsl:for-each>
			
		</xsl:element>

	</xsl:template>
	
	<!-- FareRules - Related Segment -->
	<xsl:template name="SegmentInfo">
		<xsl:element name="RelatedSegment">
				<xsl:element name="SegmentIDRef">
					<xsl:variable name="counterforSegment" select="intinc:getNext($counter_segmentObject)" />
					<xsl:attribute name="FlightRefKey">
					<xsl:value-of select="concat('F', $counterforSegment)" />
				</xsl:attribute>
					<xsl:value-of select="$counterforSegment" />
				</xsl:element>

				<xsl:element name="DepartureCode">
					<xsl:value-of select="Departure/AirportCode" />
				</xsl:element>
				<xsl:element name="ArrivalCode">
					<xsl:value-of select="Arrival/AirportCode" />
				</xsl:element>

				<xsl:element name="DepartureDate">
					<xsl:value-of select="Departure/Date" />
				</xsl:element>
				<xsl:element name="ArrivalDate">
					<xsl:value-of select="Arrival/Date" />
				</xsl:element>
				<xsl:element name="ArrivalTime">
					<xsl:value-of select="Arrival/Time" />
				</xsl:element>
				<xsl:element name="DepartureTime">
					<xsl:value-of select="Departure/Time" />
				</xsl:element>
				<xsl:element name="ClassOfService">
					<xsl:value-of select="ClassOfService" />
				</xsl:element>
			</xsl:element>
	</xsl:template>

	<!-- BookingContact -->
	<xsl:template match="BookingContact">

		<xsl:element name="AddressGroup">
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Address">
				<xsl:value-of select="AddressLine1" />
			</xsl:element>
			<xsl:element name="City">
				<xsl:value-of select="City" />
			</xsl:element>
			<xsl:element name="PostalCode">
				<xsl:value-of select="PostalCode" />
			</xsl:element>
			<xsl:element name="CountryCode">
				<xsl:value-of select="CountryCode" />
			</xsl:element>
			<xsl:element name="AddressText">
				<xsl:value-of select="AddressLine1" />
			</xsl:element>
		</xsl:element>
	</xsl:template>

	
	<!-- INTA-60 (ref.GWIA-29) GeneralRemark -->
	<xsl:template match="GeneralRemark">
		<xsl:element name="GeneralRemark">
			<xsl:attribute name="Source"><xsl:value-of select="$SOURCE"/></xsl:attribute>
			<xsl:attribute name="SourceRef"><xsl:value-of select="//OTA_AirBookRQResponse/OTA_AirBookRS/AirReservation//BookingReferenceID/@ID"/></xsl:attribute>
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Text">
				<xsl:value-of select="Text" />
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>