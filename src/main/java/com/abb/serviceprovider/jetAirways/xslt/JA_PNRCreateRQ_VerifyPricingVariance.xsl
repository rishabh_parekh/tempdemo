<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exslt="http://exslt.org/common">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="JA_Constants.xsl" />
	<xsl:strip-space elements="*" />
	<!-- INTA-30 -->
	<xsl:template match="ABBService">
		
		<!-- Fetch Tolerance value (%) from SARequest -->
		<xsl:variable name="ToleranceValue">
			<xsl:value-of select="//SARequest//CustomItineraryData/ToleranceValue"/>
		</xsl:variable>
		
		<!-- Fetch TotalCost value from CustomItineraryData -->
		<xsl:variable name="TotalCost">
			<xsl:value-of select="//SARequest//CustomItineraryData//BookingSum/TotalCost"/>
		</xsl:variable>
		
		<!-- Calculate allowable Lower limit of Pricing variance value -->
		<xsl:variable name="allowableLowerPricingValue">
			<xsl:value-of select="$TotalCost - (($TotalCost *  $ToleranceValue) div 100)"></xsl:value-of>
		</xsl:variable>
		
		<!-- Calculate allowable Higher limit of Pricing variance value -->
		<xsl:variable name="allowableHigherPricingValue">
			<xsl:value-of select="$TotalCost + (($TotalCost *  $ToleranceValue) div 100)"></xsl:value-of>
		</xsl:variable>
		
		
		<!-- Fetch and sum cost of each PAX and bag(if any) received from SellResponse of Gateway -->
	    <xsl:variable name="AllPaxTotalPrice">
			<xsl:for-each
				select="//SPResponse//AirItineraryPricingInfo/PTC_FareBreakdowns/PTC_FareBreakdown/PassengerFare/TotalFare">
				<PriceForSum>
					<xsl:value-of select="@Amount" />
				</PriceForSum>
			</xsl:for-each>
		</xsl:variable>
    	
    	<xsl:variable name="Final_Total"
				select="(sum(exslt:node-set($AllPaxTotalPrice)/PriceForSum))" />
				
		<xsl:variable name="TotalCostFromSellResponse">
			<xsl:value-of select="$Final_Total"/>
		</xsl:variable>
		
		<!-- TotalCost received in response should be in range of higher/lower allowable pricing value
			1. true = IF TOTAL COST FROM SELL RESPONSE FALLS IN-BETWEEN ALLLOWABLE RANGE
			2. false = IF TOTAL COST FROM SELL RESPONSE DO NOT FALL IN-BETWEEN ALLLOWABLE RANGE
		-->
		<xsl:element name="PricingVerfication">
		  <xsl:choose>                            
		    <xsl:when test="($TotalCostFromSellResponse &gt;= $allowableLowerPricingValue) and ($TotalCostFromSellResponse &lt;= $allowableHigherPricingValue)">
		    	<xsl:element name="varianceFound">
		    		<xsl:value-of select="'false'"></xsl:value-of>
		    	</xsl:element>
		    	
		    	<!-- IF CALLSELLRQ TAG FOUND IN PNRCREATERQ AND IF ITS VALUE = 'true' CONSTRUCT RESPONSE MESSAGE FOR OAPI AND TERMINIATE BOOKING PROCESS-->
		    	<xsl:if test="(//SARequest//CustomItineraryData//CallSellRQ) and (//SARequest//CustomItineraryData//CallSellRQ = 'true')">
		    		<xsl:element name="error">
			    		<xsl:element name="error_code">
			    			<xsl:value-of select="$PRICING_VARIANCE_ERROR_CODE"/>
			    		</xsl:element>
			    		<xsl:element name="error_msg">
			    			<xsl:value-of select="$PRICING_VARIANCE_NOT_FOUND"/>
			    		</xsl:element>
			    	</xsl:element>
		    	</xsl:if>
		    	
		    </xsl:when>                             
		    <xsl:otherwise>
		    	<xsl:element name="varianceFound">
		    		<xsl:value-of select="'true'"></xsl:value-of>
		    	</xsl:element>
		    	<xsl:element name="error">
		    		<xsl:element name="error_code">
		    			<xsl:value-of select="$PRICING_VARIANCE_ERROR_CODE"/>
		    		</xsl:element>
		    		<xsl:element name="error_msg">
		    			<xsl:value-of select="$PRICING_VARIANCE_FOUND"/>
		    		</xsl:element>
		    		<!-- Sent revised price returned from SellResponse of Gateway to OAPI for displaying @ IBE-->
		    		<xsl:element name="error_msg">
		    			<xsl:value-of select="format-number($TotalCostFromSellResponse,'#.00')"/>
		    		</xsl:element>
		    	</xsl:element>
		    </xsl:otherwise>
		  </xsl:choose>
		</xsl:element>
	
	</xsl:template>
	
</xsl:stylesheet>