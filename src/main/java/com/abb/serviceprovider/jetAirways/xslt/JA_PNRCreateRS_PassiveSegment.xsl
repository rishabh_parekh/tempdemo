<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" xmlns:exslt="http://exslt.org/common" xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:strip-space elements="*" />
	<xsl:import href="JA_Constants.xsl" />
	<xsl:import href="../../common/xslt/Lookups.xsl" />
	<xsl:import href="../../common/xslt/FormatDate.xsl" />
	<xsl:import href="JA_ErrorHandling.xsl" />
	<xsl:variable name="Cabin_Lookup"
		select="document('lookupXMLs/Cabin_Classes_Lookup.xml')//Cabin" />
	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />

	<xsl:output method="xml" indent="yes" />
	<xsl:variable name="CRSID" select="'F1'" />
	<xsl:variable name="TicketIssued" select="'Y'" />
	<xsl:variable name="QueueRetrieved" select="'Y'" />
	<xsl:variable name="CCCode" select="'K'" />
	<xsl:variable name="ActionCode" select="'K'" />
	<xsl:variable name="FareType" select="'PUBL'" />
	<xsl:variable name="FareNumber" select="'1'" />
	<xsl:variable name="FarePriced" select="'AUTO'" />
	<xsl:variable name="NumberOfDecimals" select="'2'" />
	<xsl:variable name="CommissionType" select="'A'" />
	<xsl:variable name="BaseAmountType" select="'A'" />
	<xsl:variable name="TaxAmountType" select="'A'" />
	<xsl:variable name="InfantElementNumber" select="'.1'" />
	<xsl:variable name="SourceName" select="'9W'" />
	<xsl:variable name="Title" select="'JetAirways'" />
	<xsl:variable name="TaxDescription" select="'Tax'" />
	<xsl:variable name="FormOfPayment" select="'CreditCard'" />
	<xsl:variable name="DefaultEquipment" select="'737'" />
	<xsl:variable name="RecordLocatorInRequest" select="//PNRCreateRQ/RecordLocator" />
	<xsl:variable name="serviceFeeNotification" select="'Service Fee is applied.'" />
	<xsl:variable name="cnt" select="'1'" />
	<xsl:variable name="counter_global" select="intinc:new(30)" />
	<xsl:variable name="counter_segmentObject" select="intinc:new(1)" />
	
	<xsl:variable name="recordLocator"
			select="//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary/ItineraryRef/@ID" />

	<xsl:template match="/">
		<xsl:element name="PNRViewRS">
			<xsl:call-template name="errorHandling" />
			<xsl:apply-templates select="//SPResponse" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="SPResponse">

		<xsl:element name="PNRIdentification">
			<xsl:attribute name="TicketIssued">
				<xsl:value-of select="$TicketIssued" />
			</xsl:attribute>
			<xsl:attribute name="QueueRetrieved">
				<xsl:value-of select="$QueueRetrieved" />
			</xsl:attribute>
			<xsl:attribute name="FareDataExists">
				<xsl:value-of select="'Y'" />
			</xsl:attribute>
			<xsl:element name="RecordLocator">
				<xsl:value-of select="$recordLocator" />
			</xsl:element>
			<xsl:element name="CreationDate">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr"
						select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/@TimeStamp" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="CreationTime">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr"
						select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/@TimeStamp" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="CreationAgent">
				<xsl:value-of select="//tc/iden/@agt" />
			</xsl:element>
			<xsl:element name="Ownership">
				<xsl:element name="CRSID">
					<xsl:value-of select="$CRSID" />
				</xsl:element>
				<xsl:element name="Agency">
					<xsl:value-of select="//tc/iden/@agy" />
				</xsl:element>
				<xsl:element name="PseudoCityCode">
					<xsl:value-of select="//tc/iden/@pseudocity" />
				</xsl:element>
			</xsl:element>
			<xsl:element name="CurrentPseudoCityCode">
				<xsl:value-of select="//tc/iden/@pseudocity" />
			</xsl:element>
			<xsl:element name="ReceivedFrom">
				<xsl:value-of select="//SARequest//PNRRetrieveRQ/ReceivedFrom" />
			</xsl:element>
		</xsl:element>

		<xsl:variable name="CurrentAirBooking"
			select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary" />

		<!-- FareGroup -->
		<xsl:element name="FareGroup">
			<xsl:attribute name="FareType">
			 		<xsl:value-of select="$FareType" />
				</xsl:attribute>

			<xsl:attribute name="FareNumber">
				 	<xsl:value-of select="$FareNumber" /> 
				</xsl:attribute>

			<xsl:attribute name="TravelerCount">
					<xsl:value-of
				select="count($CurrentAirBooking/CustomerInfos/CustomerInfo)" />
				</xsl:attribute>

			<!-- <xsl:variable name="Final_Total"
				select="//SPResponse//AirTicketRQResponse//AirReservation/PriceInfo/ItinTotalFare/TotalFare/@Amount" /> -->
				
			<xsl:variable name="AllPaxTotalPrice">
				<xsl:for-each select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS//ItineraryPricing">
					<PriceForSum>
						<xsl:value-of select="Cost/@AmountAfterTax" />
					</PriceForSum>
				</xsl:for-each>
			</xsl:variable>
			
			<xsl:variable name="Final_Total"
				select="(sum(exslt:node-set($AllPaxTotalPrice)/PriceForSum))" />

			<xsl:attribute name="TotalPrice">
					<xsl:value-of select="round(format-number($Final_Total,'#.00') * 100)" />						
				</xsl:attribute>

			<xsl:attribute name="Source">
					<xsl:value-of select="$SourceName" />
				</xsl:attribute>

			<!-- CurrencyCode -->
			<xsl:element name="CurrencyCode">
				<xsl:attribute name="NumberOfDecimals">
					<xsl:value-of select="$CURRENCY_NUMBER_OF_DECIMAL" />
				</xsl:attribute>
				<xsl:value-of select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS//ItineraryPricing/Cost/@CurrencyCode" />
			</xsl:element>
			<xsl:apply-templates
				select="$CurrentAirBooking/ItineraryInfo/ItineraryPricing" />
		</xsl:element>

		<!-- Traveler -->
		<xsl:apply-templates select="$CurrentAirBooking/CustomerInfos/CustomerInfo" />

		<!-- Telephone -->
		<xsl:apply-templates
			select="$CurrentAirBooking/ItineraryInfo/ContactInfo/Telephone" />

		<!-- AirGroup -->
		<xsl:element name="AirGroup">
			<xsl:apply-templates
			select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item" />
		</xsl:element>

		<xsl:element name="Ticketing">
			<xsl:attribute name="Source"> 
					<xsl:value-of select="$SourceName" />
			</xsl:attribute>
			<xsl:attribute name="SourceRef"> 
					<xsl:value-of select="$recordLocator" />
				 </xsl:attribute>
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Date">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr"
						select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/@TimeStamp" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="Time">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr"
						select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/@TimeStamp" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>
		</xsl:element>

		<xsl:element name="BillingAndDeliveryData">
			<xsl:element name="FormOfPayment">
				<xsl:attribute name="Source">
					<xsl:value-of select="$SourceName" />
				</xsl:attribute>
				<xsl:attribute name="SourceRef">
					<xsl:value-of select="$recordLocator" />
				</xsl:attribute>
				<xsl:element name="ElementNumber">
					<xsl:value-of select="intinc:getNext($counter_global)" />
				</xsl:element>
				<xsl:element name="Other">
					<xsl:value-of select="$FormOfPayment" />
				</xsl:element>
			</xsl:element>

			<!-- BookingContact-->
			 <xsl:apply-templates select="BookingContact" /> 
			
		</xsl:element>

		<!-- GeneralRemark -->
		<xsl:apply-templates
			select="//SARequest//PNRCreateRQ/OtherPNRElements/GeneralRemark">
		</xsl:apply-templates>
		<!-- AccountingLine <xsl:apply-templates select="" /> -->
	</xsl:template>

	<!-- Telephone -->
	<xsl:template match="Telephone">
		<xsl:element name="Telephone">
			<xsl:element name="ElementNumber">
				<xsl:value-of select="position()" />
			</xsl:element>
			<xsl:element name="TelephoneNumber">
				<xsl:value-of select="@PhoneNumber" />
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- TravelerGroup -->
	<xsl:template match="ItineraryPricing">

		<xsl:variable name="CurrentAirBooking"
			select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary" />

		<xsl:variable name="CurrentPaxType" select="@PassengerTypeCode" />


		<xsl:variable name="PaxTotal_BasePrice">
			<xsl:value-of select="Cost/@AmountBeforeTax" />
		</xsl:variable>

		<xsl:variable name="Taxes">
			<xsl:for-each select="Cost/Taxes/Tax">
				<Tax_Amount>
					<xsl:value-of select="@Amount" />
				</Tax_Amount>
				<Tax_Description>
					<xsl:value-of select="@Code" />
				</Tax_Description>
			</xsl:for-each>
		</xsl:variable>

		<xsl:variable name="PaxTotalPrice">
			<xsl:value-of select="Cost/@AmountAfterTax" />
		</xsl:variable>

		<xsl:element name="TravelerGroup">

			<xsl:variable name="Counter" select="intinc:new()" />
			<xsl:variable name="counterforSegment_reset" select="intinc:resetCounter($counter_segmentObject)" />
			<xsl:if test="$counterforSegment_reset"></xsl:if>
			
			<xsl:variable name="TotalTravelerType">
				<xsl:value-of
					select="count($CurrentAirBooking/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType=$CurrentPaxType])" />
			</xsl:variable>

			<xsl:attribute name="TypeRequested"> 
							<xsl:value-of select="$CurrentPaxType" />
						 </xsl:attribute>
			<xsl:attribute name="TypePriced"> 
							<xsl:value-of select="$CurrentPaxType" />
						 </xsl:attribute>

			<xsl:attribute name="TypeTotalPrice">
							<xsl:value-of
				select="round(format-number($PaxTotalPrice,'#.00') * 100)" />
						</xsl:attribute>

			<xsl:attribute name="TypeCount"> 
							<xsl:value-of select="$TotalTravelerType" /> 
						</xsl:attribute>
			<xsl:element name="Price">

				<xsl:attribute name="Total">
								<xsl:value-of
					select="round(format-number($PaxTotalPrice div $TotalTravelerType,'#.00') * 100)" />
							</xsl:attribute>

				<xsl:element name="BaseFare">
					<xsl:attribute name="Amount"> 
					       			<xsl:value-of
						select="round(format-number($PaxTotal_BasePrice div $TotalTravelerType,'#.00') * 100)" />
								</xsl:attribute>
				</xsl:element>

				<xsl:element name="Taxes">
					<xsl:if test="count(exslt:node-set($Taxes)/*) &gt; 0">
						<xsl:attribute name="Amount">
									<xsl:value-of
							select="round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount),'#.00') * 100)" />
								</xsl:attribute>
						<xsl:for-each select="exslt:node-set($Taxes)/Tax_Amount">
							<xsl:variable name="TaxNode_Count" select="intinc:getNext($Counter)" />
							<xsl:element name="Tax">
								<xsl:attribute name="Amount">
											<xsl:value-of
									select="round(format-number(exslt:node-set($Taxes)/Tax_Amount[position()=$TaxNode_Count],'#.00') * 100)" />
										</xsl:attribute>
								<xsl:element name="Description">
									<xsl:value-of
										select="exslt:node-set($Taxes)/Tax_Description[position()=$TaxNode_Count]" />
								</xsl:element>
							</xsl:element>
						</xsl:for-each>
					</xsl:if>
				</xsl:element>
			</xsl:element>
		</xsl:element>

	</xsl:template>
	
	<!-- TODO: FareRules Creation ommited as per Timothy email due to missing attribute in response from 9W -->
	<!-- BookingContact -->
	<xsl:template match="BookingContact">

		<xsl:element name="AddressGroup">
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Address">
				<xsl:value-of select="'NA'" />
			</xsl:element>
			<xsl:element name="City">
				<xsl:value-of select="'NA'" />
			</xsl:element>
			<xsl:element name="PostalCode">
				<xsl:value-of select="'NA'" />
			</xsl:element>
			<xsl:element name="CountryCode">
				<xsl:value-of select="'NA'" />
			</xsl:element>
			<xsl:element name="AddressText">
				<xsl:value-of select="'NA'" />
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Traveler -->
	<xsl:template match="CustomerInfo">
		<xsl:if test="not(Customer/PersonName/@NameType='INF')">
			<xsl:element name="Traveler">
				<xsl:attribute name="Source">
				<xsl:value-of select="$SourceName" />
			</xsl:attribute>
				<xsl:attribute name="Type">
				<xsl:value-of select="Customer/PersonName/@NameType" />
			</xsl:attribute>
				<xsl:attribute name="SourceRef">
				<xsl:value-of select="$recordLocator" />
			</xsl:attribute>
				<xsl:element name="ElementNumber">
					<xsl:value-of select="(position() +10)" />
				</xsl:element>
				<xsl:element name="TravelerName">
					<xsl:element name="Surname">
						<xsl:value-of select="Customer/PersonName/Surname" />
					</xsl:element>
					<xsl:element name="GivenName">
						<xsl:value-of select="Customer/PersonName/GivenName" />
					</xsl:element>
					<xsl:element name="NativeGivenName">
						<xsl:value-of select="Customer/PersonName/GivenName" />
					</xsl:element>
					<!-- <xsl:element name="DateOfBirth"> <xsl:value-of select="$adults[$counterOutBOundAdultForInfant]/BirthDate" 
						/> </xsl:element> -->
					<xsl:element name="Gender">
						<xsl:choose>
							<xsl:when test="Customer/PersonName/NameTitle ='MR'">
								<xsl:value-of select="'M'" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'F'" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
				</xsl:element>
				<xsl:if
					test="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName/@NameType='INF' and position()='1'">
					<xsl:element name="Infant">
						<xsl:element name="ElementNumber">
							<xsl:value-of select="(position() +10) + $InfantElementNumber" />
						</xsl:element>
						<xsl:element name="Surname">
							<xsl:value-of
								select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType='INF']/Surname" />
						</xsl:element>
						<xsl:element name="GivenName">
							<xsl:value-of
								select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType='INF']/GivenName" />
						</xsl:element>
						<xsl:element name="NativeGivenName">
							<xsl:value-of
								select="//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType='INF']/GivenName" />
						</xsl:element>
						<!-- <xsl:element name="DateOfBirth"> <xsl:value-of select="BirthDate" 
							/> </xsl:element> -->
						<xsl:element name="Gender">
							<xsl:choose>
								<xsl:when test="Customer/PersonName/NameTitle ='Master'">
									<xsl:value-of select="'M'" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'F'" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:if>

			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<!-- AirGroup:: Flight :: For Every Segment -->
	<xsl:template match="Item">
		
		<xsl:element name="Flight">
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Departure">
				<xsl:variable name="OriginCode" select="Air/DepartureAirport/@LocationCode" />
				<xsl:element name="AirportCode">
					<xsl:value-of select="$OriginCode" />
				</xsl:element>
				<xsl:element name="AirportName">
					<xsl:value-of select="concat($Airport_Lookup[AirportCode=$OriginCode]/AirportName,', ',$Airport_Lookup[AirportCode=$OriginCode]/CountryCode)" />
				</xsl:element>
				<xsl:element name="Date">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'date'" />
						<xsl:with-param name="datestr" select="Air/@DepartureDateTime" />  <!-- Date -->
						<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
						<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
					</xsl:call-template>
				</xsl:element>
				
				<xsl:element name="Time">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="datestr" select="Air/@DepartureDateTime" />
						<xsl:with-param name="outformat" select="string('hh-mm')" />
						<xsl:with-param name="informat" select="string('datetime')" />
						<xsl:with-param name="format" select="'time'" />
					</xsl:call-template>
				</xsl:element>
				
			</xsl:element>
			<xsl:element name="Arrival">
				<xsl:variable name="DestinationCode" select="Air/ArrivalAirport/@LocationCode" />
				<xsl:element name="AirportCode">
					<xsl:value-of select="$DestinationCode" />
				</xsl:element>
				<xsl:element name="AirportName">
					<xsl:value-of select="concat($Airport_Lookup[AirportCode=$DestinationCode]/AirportName,', ',$Airport_Lookup[AirportCode=$DestinationCode]/CountryCode)" />
				</xsl:element>
				<xsl:element name="Date">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'date'" />
						<xsl:with-param name="datestr" select="Air/@ArrivalDateTime" />  <!-- Date -->
						<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
						<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
					</xsl:call-template>
				</xsl:element>
				
				<xsl:element name="Time">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="datestr" select="Air/@ArrivalDateTime" />
						<xsl:with-param name="outformat" select="string('hh-mm')" />
						<xsl:with-param name="informat" select="string('datetime')" />
						<xsl:with-param name="format" select="'time'" />
					</xsl:call-template>
				</xsl:element>
			</xsl:element>
			<xsl:element name="Carrier">
				<xsl:variable name="MarketingAirline" select="Air/MarketingAirline/@Code" />
				<xsl:element name="AirlineCode">
						<xsl:value-of select="$MarketingAirline"/>
				</xsl:element>
				<xsl:element name="AirlineName">
					<xsl:value-of select="$Airline_Lookup[AirLineCode=$MarketingAirline]/AirLineName" />
				</xsl:element>
				<xsl:element name="FlightNumber">
					<xsl:attribute name="Suffix">
							<xsl:value-of select="$MarketingAirline" />
					</xsl:attribute>
					<xsl:value-of select="Air/@FlightNumber" />
				</xsl:element>
			</xsl:element>
			<!-- No Mapping Found. Optional Tag. Not used in PNRCreateRS -->
			<!-- <xsl:element name="Equipment">
				<xsl:element name="Code">
					<xsl:value-of select="$DefaultEquipment" />
				</xsl:element>
				<xsl:element name="Name">
					<xsl:value-of select="$Equipment_Lookup[EquipmentCode=$DefaultEquipment]/EquipmentName" />
				</xsl:element>
			</xsl:element> -->
			<xsl:element name="ClassOfService">
				<xsl:value-of select="Air/@ResBookDesigCode" />
			</xsl:element>
			<xsl:element name="NumberInParty">
				<xsl:value-of select="count(//SPResponse//ProcessPassiveSegmentsResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo)" />
			</xsl:element>
			<xsl:element name="ActionCode">
				<xsl:attribute name="Status">
					 <xsl:value-of select="$ActionCode" />
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- INTA-60 (ref.GWIA-29) GeneralRemark -->
	<xsl:template match="GeneralRemark">
		<xsl:element name="GeneralRemark">
			<xsl:attribute name="Source"><xsl:value-of select="$SOURCE"/></xsl:attribute>
			<xsl:attribute name="SourceRef"><xsl:value-of select="//OTA_AirBookRQResponse/OTA_AirBookRS/AirReservation//BookingReferenceID/@ID"/></xsl:attribute>
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Text">
				<xsl:value-of select="Text" />
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>