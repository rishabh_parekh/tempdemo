<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns="http://www.vedaleon.com/webservices" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="JA_Constants.xsl" />

	<xsl:variable name="intInc" select="intinc:new()" />
	
	<xsl:template match="/">
		<!-- AirTicketRQ -->
	   
		<xsl:element name="AirTicketRQ">
			<!-- OTA_AirBookRQ -->
			<xsl:element name="OTA_AirBookRQ">
				<!-- Ticketing -->
				<xsl:element name="Ticketing">
					<xsl:attribute name="TicketType">
						<xsl:value-of select="$TICKET_TYPE" />
					</xsl:attribute>
					</xsl:element>
					<!-- BookingReferenceID -->
					<xsl:element name="BookingReferenceID">
						<xsl:attribute name="ID">
							<xsl:value-of select="//TicketIssueRQ//RecordLocator" />
						</xsl:attribute>
					</xsl:element>
				

			</xsl:element>
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>