<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns="http://www.vedaleon.com/webservices" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="JA_Constants.xsl" />

	<xsl:variable name="intInc" select="intinc:new()" />
	
	<xsl:template match="/">
		<!-- AirSellRQ -->
		<xsl:element name="AirSellRQ">
			<!-- OTA_AirPriceRQ -->
			<xsl:element name="OTA_AirPriceRQ">
				<!-- AirItinerary -->
				<xsl:element name="AirItinerary">
					<xsl:attribute name="DirectionInd">
						<xsl:choose>
							<xsl:when
						test="count(//SARequest//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]) > 1">
								<xsl:value-of select="$ROUND_TRIP" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$ONE_WAY" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>

					<!-- OriginDestinationOptions -->
					<xsl:element name="OriginDestinationOptions">
						<!-- Create 'OriginDestinationOption' node for first inbound and first 
							outbound flight -->
						<xsl:apply-templates
							select="//SARequest//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]"
							mode="uniqueFlightTag" />
					</xsl:element>
				</xsl:element>

				<!-- TravelerInfoSummary -->
				<xsl:element name="TravelerInfoSummary">
					<!-- AirTravelerAvail -->
					<xsl:apply-templates select="//SARequest//Traveler" />
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- OriginDestinationOption -->
	<xsl:template match="Flight" mode="uniqueFlightTag">
		<xsl:variable name="originDestId" select="current()/@OriginDestinationID" />

		<xsl:element name="OriginDestinationOption">
			<xsl:apply-templates
				select="//SARequest//Flight[@OriginDestinationID=$originDestId]" />
		</xsl:element>
	</xsl:template>

	<!-- FlightSegment -->
	<xsl:template match="Flight">
		<xsl:element name="FlightSegment">
			<xsl:attribute name="DepartureDateTime">
				<xsl:value-of select="concat(Departure/Date,'T',Departure/Time,':00')" />
			</xsl:attribute>
			<xsl:attribute name="ArrivalDateTime">
				<xsl:value-of select="concat(Arrival/Date,'T',Arrival/Time,':00')" />
			</xsl:attribute>
			<xsl:attribute name="RPH">
				<xsl:value-of select="intinc:getNext($intInc)" />
			</xsl:attribute>
			<xsl:attribute name="FlightNumber">
				<xsl:value-of select="Carrier/FlightNumber" />
			</xsl:attribute>
			<xsl:attribute name="ResBookDesigCode">
				<xsl:value-of select="ClassOfService" />
			</xsl:attribute>
			<xsl:attribute name="NumberInParty">
				<xsl:value-of select="NumberInParty" />
			</xsl:attribute>
			<xsl:attribute name="Status">
				<xsl:value-of select="$ORIGIN_DEST_OPTION_STATUS"></xsl:value-of>
			</xsl:attribute>

			<xsl:element name="DepartureAirport">
				<xsl:attribute name="LocationCode">
					<xsl:value-of select="Departure/AirportCode" />
				</xsl:attribute>
				<!-- <xsl:attribute name="CodeContext">
					 <xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute> -->
			</xsl:element>
			<xsl:element name="ArrivalAirport">
				<xsl:attribute name="LocationCode">
					<xsl:value-of select="Arrival/AirportCode" />
				</xsl:attribute>
				<!-- <xsl:attribute name="CodeContext">
					<xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute> -->
			</xsl:element>
			<xsl:element name="OperatingAirline">
				<xsl:attribute name="Code">
					<xsl:value-of select="Carrier/AirlineCode" />
				</xsl:attribute>
				<xsl:attribute name="CodeContext">
					<xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute>
				<xsl:attribute name="FlightNumber">
					<xsl:value-of select="Carrier/FlightNumber" />
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="MarketingAirline">
				<xsl:attribute name="Code">
					<xsl:value-of select="Carrier/AirlineCode" />
				</xsl:attribute>
				<xsl:attribute name="CodeContext">
					<xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute>
			</xsl:element>
			
		</xsl:element>
	</xsl:template>

	<!-- PassengerTypeQuantity -->
	<xsl:template match="Traveler">
		<xsl:if test="not(@Type=preceding-sibling::Traveler/@Type)">
			<xsl:element name="AirTravelerAvail">
				<xsl:element name="AirTraveler">
					<xsl:element name="PassengerTypeQuantity">
						<xsl:attribute name="Code">
							<xsl:choose>
								<xsl:when test="@Type='CHD'">
									<xsl:value-of select="'CNN'" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@Type" />
								</xsl:otherwise>
							</xsl:choose>
							</xsl:attribute>
						<xsl:attribute name="Quantity">
					<xsl:value-of
							select="count(//SARequest//Traveler[@Type=current()/@Type])" />
				</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>

		<!-- Count Quantity for Infant -->
		<xsl:if
			test="current()/Infant and not(Infant/@Type=preceding-sibling::Traveler/Infant/@Type)">
			<xsl:element name="AirTravelerAvail">
				<xsl:element name="AirTraveler">
					<xsl:element name="PassengerTypeQuantity">
						<xsl:attribute name="Code">
					<xsl:value-of select="Infant/@Type" />
				</xsl:attribute>
						<xsl:attribute name="Quantity">
					<xsl:value-of
							select="count(//SARequest//Traveler/Infant[@Type=current()/Infant/@Type])" />
				</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>