<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:a="http://www.vedaleon.com/webservices">
		
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	
	<xsl:template match="/">
	<xsl:apply-templates
				select="//SARequest//PNRRetrieveRQ" />
	</xsl:template>	
	
	<xsl:template match="PNRRetrieveRQ">
		<xsl:element name="a:OTA_TravelItineraryReadRQ">
			<xsl:element name="a:OTA_TravelItineraryReadRQ">
				<xsl:element name="a:UniqueID">
					<xsl:attribute name="ID">
						<xsl:value-of select="RecordLocator"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>