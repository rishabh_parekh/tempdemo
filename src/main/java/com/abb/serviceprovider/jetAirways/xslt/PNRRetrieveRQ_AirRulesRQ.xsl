<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.vedaleon.com/webservices">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="JA_Constants.xsl" />

	<!-- <xsl:param name="flights" /> -->

	<xsl:template match="/">
		<!-- OTA_AirRulesRQ -->
		<xsl:element name="OTA_AirRulesRQ">
			<!-- OTA_AirRulesRQ -->
			<xsl:element name="OTA_AirRulesRQ">
				<!-- RuleReqInfo -->
				<xsl:element name="RuleReqInfo">
					<xsl:attribute name="NegotiatedFare">
						<xsl:value-of select="'true'" />
					</xsl:attribute>
					<xsl:attribute name="NegotiatedFareCode">
						<xsl:value-of select="$FARE_BASIS_CODE" />
					</xsl:attribute>
					<!-- <xsl:apply-templates select="//SARequest/PNRCreateRQ//Flight[contains($flights,Carrier/FlightNumber)][1]"/> -->
					<!-- <xsl:apply-templates select="//SARequest/PNRCreateRQ//Flight" /> -->
					<xsl:apply-templates select="//SPResponse//OTA_TravelItineraryReadRQResponse//TravelItinerary/ItineraryInfo/ReservationItems//Air" />
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="Air">
		<xsl:element name="DepartureDate">
				<xsl:value-of select="@ArrivalDateTime" />
		</xsl:element>
		<xsl:element name="DepartureAirport">
			<xsl:attribute name="LocationCode">
				<xsl:value-of select="DepartureAirport/@LocationCode" />
			</xsl:attribute>
			<xsl:attribute name="CodeContext">
					 <xsl:value-of select="$CODE_CONTEXT" />
			</xsl:attribute>
		</xsl:element>
		<xsl:element name="ArrivalAirport">
			<xsl:attribute name="LocationCode">
				<xsl:value-of select="ArrivalAirport/@LocationCode" />
			</xsl:attribute>
			<xsl:attribute name="CodeContext">
					 <xsl:value-of select="$CODE_CONTEXT" />
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>