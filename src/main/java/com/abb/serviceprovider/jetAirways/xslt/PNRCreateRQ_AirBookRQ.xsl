<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns="http://www.vedaleon.com/webservices" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="JA_Constants.xsl" />
	<xsl:variable name="intInc" select="intinc:new()" />

	<xsl:template match="/">
		<!-- OTA_AirBookRQ -->
		<xsl:element name="OTA_AirBookRQ">
			<!-- OTA_AirBookRQ -->
			<xsl:element name="OTA_AirBookRQ">
				<xsl:attribute name="Target">
				<xsl:value-of select="'Test'" />
			</xsl:attribute>
				<xsl:attribute name="Version">
				<xsl:value-of select="'1.0'" />
			</xsl:attribute>
				<xsl:attribute name="TransactionStatusCode">
				<xsl:value-of select="'End'" />
			</xsl:attribute>
				<xsl:element name="FormOfPayment">
					<xsl:value-of select="'CreditCard'" />
				</xsl:element>

				<!-- AirItinerary <xsl:element name="AirItinerary"> <xsl:attribute 
					name="DirectionInd"> <xsl:choose> <xsl:when test="count(//SARequest//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]) 
					> 1"> <xsl:value-of select="$ROUND_TRIP"/> </xsl:when> <xsl:otherwise> <xsl:value-of 
					select="$ONE_WAY"/> </xsl:otherwise> </xsl:choose> </xsl:attribute> <- OriginDestinationOptions 
					-> <xsl:element name="OriginDestinationOptions"> <- Create OriginDestinationOption 
					node if inbound and outbound flights -> <xsl:apply-templates select="//SARequest//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]" 
					mode="uniqueFlightTag"/> </xsl:element> </xsl:element> -->

				<!-- TravelerInfo -->
				<xsl:element name="TravelerInfo">
					<xsl:apply-templates select="//SARequest//Traveler" />
				</xsl:element>

				<!-- SpecialReqDetails <xsl:element name="SpecialReqDetails"> <xsl:element 
					name="SpecialServiceRequests"> <xsl:apply-templates select="//SARequest/PNRCreateRQ//SpecialServiceRequest"/> 
					</xsl:element> </xsl:element> -->
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- OriginDestinationOption -->
	<xsl:template match="Flight" mode="uniqueFlightTag">
		<xsl:variable name="originDestId" select="current()/@OriginDestinationID" />

		<xsl:element name="OriginDestinationOption">
			<xsl:apply-templates
				select="//SARequest//Flight[@OriginDestinationID=$originDestId]" />
		</xsl:element>
	</xsl:template>

	<!-- FlightSegment -->
	<xsl:template match="Flight">
		<xsl:element name="FlightSegment">
			<xsl:attribute name="DepartureDateTime">
				<xsl:value-of select="concat(Departure/Date,'T',Departure/Time)" />
			</xsl:attribute>
			<xsl:attribute name="ArrivalDateTime">
				<xsl:value-of select="concat(Arrival/Date,'T',Arrival/Time)" />
			</xsl:attribute>
			<xsl:attribute name="RPH">
				<xsl:value-of select="intinc:getNext($intInc)" />
			</xsl:attribute>
			<xsl:attribute name="FlightNumber">
				<xsl:value-of select="Carrier/FlightNumber" />
			</xsl:attribute>
			<xsl:attribute name="ResBookDesigCode">
				<xsl:value-of select="ClassOfService" />
			</xsl:attribute>
			<xsl:attribute name="NumberInParty">
				<xsl:value-of select="NumberInParty" />
			</xsl:attribute>
			<xsl:attribute name="Status">
				<xsl:value-of select="$ORIGIN_DEST_OPTION_STATUS" />
			</xsl:attribute>

			<xsl:element name="DepartureAirport">
				<xsl:attribute name="LocationCode">
					<xsl:value-of select="Departure/AirportCode" />
				</xsl:attribute>
				<xsl:attribute name="CodeContext">
					 <xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="ArrivalAirport">
				<xsl:attribute name="LocationCode">
					<xsl:value-of select="Arrival/AirportCode" />
				</xsl:attribute>
				<xsl:attribute name="CodeContext">
					<xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute>
			</xsl:element>
			<xsl:element name="OperatingAirline">
				<xsl:attribute name="Code">
					<xsl:value-of select="Carrier/AirlineCode" />
				</xsl:attribute>
				<xsl:attribute name="CodeContext">
					<xsl:value-of select="$CODE_CONTEXT" />
				</xsl:attribute>
				<xsl:attribute name="FlightNumber">
					<xsl:value-of select="Carrier/FlightNumber" />
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- AirTraveler -->
	<xsl:template match="Traveler">
		<xsl:call-template name="createAirTraveler">
			<xsl:with-param name="traveler" select="descendant-or-self::*" />
		</xsl:call-template>

		<xsl:if test="current()/Infant">
			<xsl:call-template name="createAirTraveler">
				<xsl:with-param name="traveler" select="current()/Infant" />
				<xsl:with-param name="isInfant" select="'true'" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!-- AirTraveler Template -->
	<xsl:template name="createAirTraveler">
		<xsl:param name="isInfant" select="'false'" />
		<xsl:param name="traveler" />

		<xsl:variable name="RPHValue" select="intinc:getNext($intInc)" />

		<xsl:element name="AirTraveler">
			<xsl:attribute name="PassengerTypeCode">
				<xsl:choose>
					<xsl:when test="$traveler/@Type='CHD'">
						<xsl:value-of select="'CNN'" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$traveler/@Type" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<!-- If Infant tag exists then set true. -->
			<xsl:if test="$traveler/Infant">
				<xsl:attribute name="AccompaniedByInfant">
					<xsl:value-of select="'true'" />
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="not($traveler/@Type='ADT')">
				<xsl:attribute name="BirthDate">
				<xsl:choose>
					<xsl:when test="boolean($isInfant)">
						<xsl:value-of select="$traveler/DateOfBirth" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="$traveler/TravelerName/DateOfBirth" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			</xsl:if>
			<!-- PersonName -->
			<xsl:element name="PersonName">
				<xsl:element name="NamePrefix">
					<xsl:choose>
						<xsl:when test="$traveler/@Type='ADT'">
							<xsl:value-of select="$traveler/TravelerName/Title" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'Master'" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
				<xsl:element name="GivenName">
					<xsl:choose>
						<xsl:when test="boolean($isInfant)">
							<xsl:value-of select="$traveler/GivenName" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$traveler/TravelerName/GivenName" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
				<xsl:element name="Surname">
					<xsl:choose>
						<xsl:when test="boolean($isInfant)">
							<xsl:value-of select="$traveler/Surname" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="$traveler/TravelerName/Surname" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>

			<!-- Telephone -->
			<xsl:if test="$traveler/@Type='ADT'">
				
					
				<xsl:if test="//SARequest/PNRCreateRQ//Telephone">
					<xsl:for-each select="//SARequest/PNRCreateRQ//CompletePNRElements/Telephone">
						
						<xsl:if test="TravelerIDRef=$traveler/@AssociationID">
							<xsl:element name="Telephone">
								<xsl:attribute name="PhoneLocationType">
								<xsl:value-of select="@Type" />
							</xsl:attribute>
								<xsl:attribute name="CountryAccessCode">
								<xsl:value-of select="CountryCode" />
							</xsl:attribute>
								<xsl:attribute name="AreaCityCode">
								<xsl:value-of select="CityCode" />
							</xsl:attribute>
								<xsl:attribute name="PhoneNumber">
								<xsl:value-of select="TelephoneNumber" />
							</xsl:attribute>
								<xsl:attribute name="RPH">
								<xsl:value-of select="$RPHValue" />
							</xsl:attribute>
								<xsl:attribute name="Operation">
								<xsl:value-of select="$ADD_OPERATION" />
							</xsl:attribute>
							</xsl:element>
						</xsl:if>
					</xsl:for-each>
				</xsl:if>
			</xsl:if>
			<!-- Email -->
			<xsl:if test="$traveler/@Type='ADT'">
				<xsl:variable name="emailElement"
					select="//SARequest/PNRCreateRQ//EmailAddress[TravelerIDRef=$traveler/@AssociationID]" />
				<xsl:if test="//SARequest/PNRCreateRQ//EmailAddress[TravelerIDRef=$traveler/@AssociationID]">
				<xsl:element name="Email">
					<!--  <xsl:attribute name="EmailType">
					<xsl:choose>
						<xsl:when test="$emailElement/@Type">
							<xsl:value-of select="$emailElement/@Type" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="'Unspecified'" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>-->
					<xsl:attribute name="RPH">
					<xsl:value-of select="$RPHValue" />
				</xsl:attribute>
					<xsl:attribute name="Operation">
					<xsl:value-of select="$ADD_OPERATION" />
				</xsl:attribute>
					<xsl:value-of select="$emailElement/Email" />
				</xsl:element>
				</xsl:if>
			</xsl:if>
			<!-- TravelerRefNumber -->
			<xsl:element name="TravelerRefNumber">
				<xsl:attribute name="RPH">
					<xsl:value-of select="$RPHValue" />
				</xsl:attribute>
			</xsl:element>
		</xsl:element>
		
		<!-- SpecialReqDetails --> 
		<xsl:element name="SpecialReqDetails"> 
			<xsl:element name="SpecialServiceRequests"> 
				<xsl:apply-templates select="//SARequest/PNRCreateRQ//SpecialServiceRequest"/> 
			</xsl:element> 
		</xsl:element>
		
	</xsl:template>

	<!-- SpecialServiceRequest -->
	<xsl:template match="SpecialServiceRequest">
		<xsl:element name="SpecialServiceRequest">
			<xsl:attribute name="SSRCode">
				<xsl:value-of select="current()/SSRCode" />
			</xsl:attribute>
			<!-- To get value for this attribute below logic is applied 1) Get Traveler 
				node using TravelerIDRef 2) If node not found then it's infant 2) Get position 
				of that node 3) Check if Infant is there then also add it -->
			<xsl:attribute name="TravelerRefNumberRPHList">
				<xsl:variable name="refNode"
				select="//Traveler[@AssociationID=current()/TravelerIDRef]" />
				<xsl:choose>
					<xsl:when test="$refNode">
						<xsl:value-of
				select="count($refNode/preceding-sibling::Traveler)+count($refNode/preceding-sibling::Traveler/Infant)+1" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of
				select="count(//Traveler/Infant[@AssociationID=current()/TravelerIDRef]/preceding-sibling::*)+1" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<xsl:element name="Airline">
				<xsl:attribute name="Code">
					<xsl:value-of select="current()/AirlineCode" />
				</xsl:attribute>
			</xsl:element>
			<xsl:copy-of select="current()/Text" />
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>