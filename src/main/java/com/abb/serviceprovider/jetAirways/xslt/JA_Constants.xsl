<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:variable name="ONE_WAY" select="'OneWay'"/>
	<xsl:variable name="ROUND_TRIP" select="'Return'"/>
	<xsl:variable name="ORIGIN_DEST_OPTION_STATUS" select="'NN'"/>
	<xsl:variable name="CODE_CONTEXT" select="'IATA'"/>
	<xsl:variable name="ADD_OPERATION" select="'Add'"/>
	<xsl:variable name="CAREER_CODE_FOR_JA" select="'9W'"/>
	<xsl:variable name="TICKET_TYPE" select="'eTicket'"/>
		
	<xsl:variable name="FARE_QUERY_RCODE" select="'FARE'" />
	<xsl:variable name="BOOKING_RETRIEVAL_RCODE" select="'RETRIEVE'" />
	<xsl:variable name="FARE_VERIFICATION_RCODE" select="'VERIFY'" />
	<xsl:variable name="FARE_RULES_RCODE" select="'RULES'" />
	<xsl:variable name="BOOKING_CANCELLATION_RCODE" select="'CANCEL'" />
	<xsl:variable name="BIDT_RCODE" select="'BIDT'"/>
	<xsl:variable name="CHANGE_OF_AIRPORT" select="'N'" />
	<xsl:variable name="CLASS_OF_SERVICE_STATUS" select="'9'" />
	<xsl:variable name="DEFAULT_EQUIPMENT" select="'737'" />
	<xsl:variable name="CURRENCY_NUMBER_OF_DECIMAL" select="'2'" />
	<xsl:variable name="SOURCE" select="'9W'" />
	<xsl:variable name="NUMBER_OF_STOPS" select="'0'" />
	<xsl:variable name="DEFAULT_FARETYPE" select="'PUBL'" />
	<xsl:variable name="FARE_BASIS_CODE" select="'NA'" />
	<xsl:variable name="TICKET_ISSUED" select="'N'" />
	<xsl:variable name="QUEUE_RETRIEVED" select="'N'" />
	<xsl:variable name="TITLE" select="'JetAirways'" />
	
	<xsl:variable name="FARE_NUMBER" select="'1'" />
	<xsl:variable name="FARE_PRICED" select="'AUTO'" />
	<xsl:variable name="CRSID" select="'F1'" />
	<xsl:variable name="TAX_DESCRIPTION" select="'Tax'" />
	<xsl:variable name="ACTIONCODE" select="'U'" />
	<xsl:variable name="BILLING_INFORMATION" select="'INV'" />

	<xsl:variable name="DEFAULT_PASSENGER_ID" select="'NA'" />
	<xsl:variable name="SOURCE_ASTERISK" select="'*'" />
	<xsl:variable name="TITLE_FOR_MALE" select="'MR'" />
	<xsl:variable name="TITLE_FOR_FEMALE" select="'MS'" />
	<xsl:variable name="FARE_DATA_EXISTS" select="'Y'"/>
	<xsl:variable name="SSR_CODE" select="'DOCS'"/>
	<xsl:variable name="ACTION_CODE" select="'HK'"/>
	<xsl:variable name="PRICING_VARIANCE_ERROR_CODE" select="'OGW_9W01'" />
	<xsl:variable name="PRICING_VARIANCE_FOUND" select="'Pricing variance found'" />
	<xsl:variable name="PRICING_VARIANCE_NOT_FOUND" select="'Pricing variance not found'" />

</xsl:stylesheet>