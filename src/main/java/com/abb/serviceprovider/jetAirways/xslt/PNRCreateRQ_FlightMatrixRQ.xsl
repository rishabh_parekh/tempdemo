<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="../../common/xslt/Lookups.xsl" />
	<xsl:import href="../../common/xslt/FormatDate.xsl" />
	<xsl:variable name="Cabin_Lookup"
		select="document('lookupXMLs/Cabin_Classes_Lookup.xml')//Cabin" />
	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />
	<xsl:import href="JA_Constants.xsl" />

	<xsl:variable name="intInc" select="intinc:new()" />

	<xsl:template match="/">

		<xsl:element name="FlightMatrixRequest">
			<xsl:element name="FlightMatrixRQ">
				<xsl:element name="AirItinerary">
					<xsl:attribute name="DirectionInd">
						<xsl:choose>
							<xsl:when
						test="count(//SARequest//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]) > 1">
								<xsl:value-of select="$ROUND_TRIP" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$ONE_WAY" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>

					<xsl:element name="OriginDestinationOptions">
						<!-- Create OriginDestinationOption node if inbound and outbound flights -->

						<xsl:element name="OriginDestinationOption">
							<xsl:element name="FlightSegment">
								<xsl:attribute name="DepartureDateTime">
				                   <xsl:value-of
									select="concat(//SARequest//Flight[1]/Departure/Date,'T',//SARequest//Flight[1]/Departure/Time,':00')" />
			                    </xsl:attribute>

								<xsl:attribute name="ArrivalDateTime">
			                     <xsl:variable name="returnFlight"
									select="//SARequest//Flight[position() > 1 and not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]" />
			  			             <xsl:choose>
							          <xsl:when test="$returnFlight">
								         <xsl:value-of
									select="concat($returnFlight/Departure/Date,'T',$returnFlight/Departure/Time,':00')" />
									  </xsl:when>
							          <xsl:otherwise>
								         <xsl:value-of
									select="concat(//SARequest//Flight[1]/Arrival/Date,'T',//SARequest//Flight[1]/Arrival/Time,':00')" />
							          </xsl:otherwise>
						            </xsl:choose>
						  		
			                    </xsl:attribute>

								<xsl:attribute name="RPH">
				                    <xsl:value-of select="intinc:getNext($intInc)" />
			                    </xsl:attribute>
								<!-- <xsl:attribute name="FlightNumber"> <xsl:value-of select="Carrier/FlightNumber"/> 
									</xsl:attribute> <xsl:attribute name="ResBookDesigCode"> <xsl:value-of select="ClassOfService"/> 
									</xsl:attribute> <xsl:attribute name="NumberInParty"> <xsl:value-of select="NumberInParty"/> 
									</xsl:attribute> <xsl:attribute name="Status"> <xsl:value-of select="$ORIGIN_DEST_OPTION_STATUS"/> 
									</xsl:attribute> -->

								<xsl:element name="DepartureAirport">
									<xsl:attribute name="LocationCode">
					                     <xsl:value-of
										select="//SARequest//Flight[1]/Departure/AirportCode" />
				                </xsl:attribute>

								</xsl:element>
								<xsl:element name="ArrivalAirport">
									<xsl:attribute name="LocationCode">
										<xsl:variable name="returnFlight"
										select="//SARequest//Flight[position() > 1 and not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]" />
			  			            	<xsl:choose>
							               <xsl:when test="$returnFlight">
								              <xsl:value-of
										select="$returnFlight/Departure/AirportCode" />
									       </xsl:when>
							               <xsl:otherwise>
								           	  <xsl:value-of
										select="//SARequest//Flight[last()]/Arrival/AirportCode" />
							               </xsl:otherwise>
						                </xsl:choose>
					                 </xsl:attribute>
								</xsl:element>
								<xsl:element name="MarketingAirline">
									<xsl:attribute name="Code">
					                     <xsl:value-of
										select="//SARequest//Flight[1]/Carrier/AirlineCode" />
				                    </xsl:attribute>
								</xsl:element>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<!-- TravelerInfoSummary -->
				<xsl:element name="TravelerInfoSummary">
					<xsl:element name="AirTravelerAvail">
						<xsl:element name="AirTraveler">
							<xsl:apply-templates select="//SARequest//Traveler" />
						</xsl:element>
					</xsl:element>
				</xsl:element>

				<!-- PriceRequestInformation -->
				<xsl:element name="PriceRequestInformation">
					<xsl:variable name="getCabin"
						select="$ClassOfService_Lookup[ClassOfServiceCode=//SARequest/PNRCreateRQ//Flight/ClassOfService]/CabinCode" />
					<xsl:attribute name="CabinType">
				                <xsl:value-of
						select="$Cabin_Lookup[FLXCode=$getCabin]/JACode" />
				            </xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>

	</xsl:template>

	<!-- AirTraveler -->
	<xsl:template match="Traveler">
		<xsl:if test="not(@Type=preceding-sibling::Traveler/@Type)">
			<xsl:element name="PassengerTypeQuantity">
				<xsl:attribute name="Code">
					<xsl:value-of select="@Type" />
				</xsl:attribute>
				<xsl:attribute name="Quantity">
					<xsl:value-of
					select="count(//SARequest//Traveler[@Type=current()/@Type])" />
				</xsl:attribute>
			</xsl:element>
		</xsl:if>

		<!-- Count Quantity for Infant -->
		<xsl:if
			test="current()/Infant and not(Infant/@Type=preceding-sibling::Traveler/Infant/@Type)">
			<xsl:element name="PassengerTypeQuantity">
				<xsl:attribute name="Code">
					<xsl:value-of select="Infant/@Type" />
				</xsl:attribute>
				<xsl:attribute name="Quantity">
					<xsl:value-of
					select="count(//SARequest//Traveler/Infant[@Type=current()/Infant/@Type])" />
				</xsl:attribute>
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>