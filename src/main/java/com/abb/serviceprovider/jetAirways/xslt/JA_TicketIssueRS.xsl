<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:exslt="http://exslt.org/common" version="1.0"
	xmlns:intinc="com.abb.framework.util.IntIncrement">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:import href="JA_Constants.xsl" />
	<xsl:import href="../../common/xslt/Lookups.xsl" />
	<xsl:import href="../../common/xslt/FormatDate.xsl" />
	<xsl:import href="JA_ErrorHandling.xsl" />

	<xsl:variable name="intInc" select="intinc:new()" />
	<xsl:variable name="intIncAdult" select="intinc:new()" />
	<xsl:variable name="Cabin_lookup"
		select="document('Cabin_Classes_Lookup.xml')//FlightClass" />
	<xsl:variable name="PaxType_Lookup"
		select="document('PaxType_Lookup.xml')//PaxItem" />
	<xsl:variable name="Source" select="'9W'" />
	<xsl:variable name="DefaultEquipment" select="'737'" />
	<xsl:variable name="DocNameType" select="'TKTT'" />
	<xsl:variable name="Status" select="'TICKETED'" />
	<xsl:variable name="FareIndicator" select="'F'" />
	<xsl:variable name="Code" select="'E-TKT'" />
	<xsl:variable name="DutyCode" select="'SU'" />
	<xsl:variable name="infant" select="'.1'" />
	<xsl:variable name="InfantElementNumber" select="'.1'" />

	<xsl:variable name="BookingCode"
		select="//SPResponse//OTA_TravelItineraryReadRQResponse//TravelItinerary/ItineraryRef/@ID" />
	<xsl:variable name="FormOfPayment" select="'CreditCard'" />
	<xsl:variable name="FareNumber" select="'1'" />
	<xsl:variable name="NumberOfDecimals" select="'2'" />
	<xsl:variable name="TaxDescription" select="'Tax'" />
	<xsl:variable name="ServiceFeeDescription" select="'Service Fee'" />
	<xsl:variable name="FareType" select="'PUBL'" />

	
	<!-- Stop Creation of PNRViewRS other tags, when error message received -->
	<xsl:template match="/">
		<!-- PNRVewRS -->
		<xsl:element name="TicketIssueRS">
			<xsl:choose>
				<!-- SoapFaultExcepptionHandling -->
				<xsl:when test="//Fault/* or //error/* or //Warning/*">
					<xsl:call-template name="errorHandling" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="//SPResponse//AirTicketRQResponse"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>

	<!-- In JetAirways, Ticket will be generated for every traveler each for 
		outbound and inbound segments, therefore TicketImage tag is created for every 
		ticket number -->

	<xsl:template match="AirTicketRQResponse">
		<xsl:apply-templates
			select="OTA_AirBookRS/AirReservation/Ticketing/TicketAdvisory" />
	</xsl:template>

	<xsl:template match="TicketAdvisory">

		<xsl:variable name="CurrentAirBooking"
			select="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/TravelItinerary" />
		<xsl:variable name="CurrentTicket" select="." />
		<xsl:variable name="TravelerPost" select="position()" />
		<xsl:variable name="Total_Travellers"
			select="count($CurrentAirBooking/CustomerInfos/CustomerInfo)" />
		<xsl:variable name="CurrentPaxType"
			select="$CurrentAirBooking/CustomerInfos/CustomerInfo[position()]/Customer/PersonName/@NameType" />
		<xsl:variable name="CurrentTraveler"
			select="$CurrentAirBooking/CustomerInfos/CustomerInfo[position()]" />

		<!-- Added Zero as last check-sum digit to complete 14 digit ticket number 
			accepted by SPRK -->
		<xsl:variable name="Zero" select="'0'" />
		<!-- TicketImage -->
		<xsl:element name="TicketImage">
		
			<xsl:variable name="counter_global" select="intinc:new(20)" />
			<xsl:attribute name="Source">
				<xsl:value-of select="$Source" />
			</xsl:attribute>
			<xsl:attribute name="DocType">
				<xsl:value-of select="$DocNameType" />
			</xsl:attribute>
			<xsl:attribute name="DocName">
				<xsl:value-of select="$DocNameType" />
			</xsl:attribute>
			<xsl:attribute name="Status">
				<xsl:value-of select="$Status" />
			</xsl:attribute>
			<xsl:attribute name="Origin">
				<xsl:value-of
				select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/DepartureAirport/@LocationCode" />
			</xsl:attribute>
			<xsl:attribute name="Destination">
				<xsl:value-of
				select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/ArrivalAirport/@LocationCode" />
			</xsl:attribute>
			<xsl:attribute name="ValidatingCarrier">
				<xsl:value-of
				select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/OperatingAirline/@Code" />
			</xsl:attribute>
			<xsl:attribute name="TicketNumber">
				<xsl:value-of select="concat(current(),$Zero)" />
			</xsl:attribute>

			<!-- PNRIdentification -->
			<xsl:element name="PNRIdentification">
				<xsl:attribute name="TicketIssued">
					<xsl:value-of select="'Y'" />
				</xsl:attribute>
				<xsl:attribute name="QueueRetrieved">
					<xsl:value-of select="'N'" />
				</xsl:attribute>
				<xsl:attribute name="FareDataExists">
					<xsl:value-of select="'Y'" />
				</xsl:attribute>
				<xsl:attribute name="Source">
					<xsl:value-of select="$Source" />
				</xsl:attribute>
				<xsl:attribute name="SourceRef">
					<xsl:value-of select="$BookingCode" />
				</xsl:attribute>

				<xsl:element name="RecordLocator">
					<xsl:value-of select="$BookingCode" />
				</xsl:element>
				<xsl:element name="CreationDate">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'date'" />
						<xsl:with-param name="datestr"
							select="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/@TimeStamp" />
						<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
						<xsl:with-param name="informat" select="string('datetime')" />
					</xsl:call-template>
				</xsl:element>
				<xsl:element name="CreationTime">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="datestr"
							select="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/@TimeStamp" />
						<xsl:with-param name="outformat" select="string('hh-mm')" />
						<xsl:with-param name="informat" select="string('datetime')" />
						<xsl:with-param name="format" select="'time'" />
					</xsl:call-template>
				</xsl:element>
				<xsl:element name="CreationAgent">
					<xsl:value-of select="//tc/iden/@agt" />
				</xsl:element>
				<xsl:element name="Ownership">
					<xsl:element name="CRSID">
						<xsl:value-of select="'F1'" />
					</xsl:element>
					<xsl:element name="Agency">
						<xsl:value-of select="//tc/iden/@agy" />
					</xsl:element>
					<xsl:element name="PseudoCityCode">
						<xsl:value-of select="//tc/iden/@pseudocity" />
					</xsl:element>
				</xsl:element>
				<xsl:element name="CurrentPseudoCityCode">
					<xsl:value-of select="//tc/iden/@pseudocity" />
				</xsl:element>
			</xsl:element>

			<!-- TktType -->
			<xsl:element name="TktType">
				<xsl:attribute name="FareIndicator">
					<xsl:value-of select="$FareIndicator" />
				</xsl:attribute>
				<xsl:attribute name="Code">
					<xsl:value-of select="$Code" />					
				</xsl:attribute>
			</xsl:element>

			<!-- TicketIdentification -->
			<xsl:element name="TicketIdentification">
				<xsl:element name="TicketingAgent">
					<xsl:value-of select="//tc/iden/@agt" />
				</xsl:element>
				<xsl:element name="TktIssueDate">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'date'" />
						<xsl:with-param name="datestr"
							select="//SPResponse//AirTicketRQResponse/OTA_AirBookRS/@TimeStamp" />  <!-- Date -->
						<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
						<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
					</xsl:call-template>
				</xsl:element>
				<xsl:element name="TktIssueTime">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="datestr"
							select="//SPResponse//AirTicketRQResponse/OTA_AirBookRS/@TimeStamp" />
						<xsl:with-param name="outformat" select="string('hh-mm')" />
						<xsl:with-param name="informat" select="string('datetime')" />
						<xsl:with-param name="format" select="'time'" />
					</xsl:call-template>
				</xsl:element>
				<xsl:element name="TktIssuePlace">
					<xsl:value-of select="'BOM'" />
				</xsl:element>
				<xsl:element name="TktIssueCountry">
					<xsl:value-of select="'IN'" />
				</xsl:element>
				<xsl:element name="RecordLocator">
					<xsl:value-of select="$BookingCode" />
				</xsl:element>
				<xsl:element name="AgencyData">
					<xsl:element name="IATA">
						<xsl:value-of select="//tc/iden/@agy" />
					</xsl:element>
					<xsl:element name="ISOC">
						<xsl:value-of select="//BillingInformation/CountryCode" />
					</xsl:element>
					<xsl:element name="PosCityCode">
						<xsl:value-of select="'BOM'" />
					</xsl:element>
				</xsl:element>
				<xsl:element name="Ownership">
					<xsl:element name="CRSID">
						<xsl:value-of select="'F1'" />
					</xsl:element>
					<xsl:element name="Agency">
						<xsl:value-of select="//tc/iden/@agy" />
					</xsl:element>
					<xsl:element name="PseudoCityCode">
						<xsl:value-of select="//tc/iden/@pseudocity" />
					</xsl:element>
				</xsl:element>
			</xsl:element>

			<!-- TravelerElementNumber Removed as Element number already present under Traveler -->
			<xsl:variable name="NoOfPassenger" select="$Total_Travellers" />
			
			<!-- Traveler -->
			<xsl:apply-templates select="$CurrentAirBooking/CustomerInfos/CustomerInfo" />

			<xsl:element name="SegmentElementNumber">
				<xsl:attribute name="TicketNumber">
						<xsl:value-of select="concat(current(),$Zero)" />
					</xsl:attribute>
				<xsl:attribute name="Coupon">
						<xsl:value-of select="'1'" />
					</xsl:attribute>
				<xsl:value-of select="'1'" />
			</xsl:element>

			<!-- Itinerary -->
			<xsl:element name="Itinerary">
				<xsl:attribute name="Source">
					<xsl:value-of select="$Source" />
				</xsl:attribute>
				<xsl:attribute name="SourceRef">
					<xsl:value-of select="$BookingCode" />
				</xsl:attribute>
				<!-- TicketCoupon will be created under each TicketImage for each segment 
					in that ticket. -->
				<xsl:element name="TicketCoupon">
					<xsl:attribute name="CouponNumber">
					<xsl:value-of select="position()" />
				</xsl:attribute>
					<xsl:attribute name="CouponStatus">
					<xsl:value-of select="'O'" />
				</xsl:attribute>

					<xsl:element name="Flight">
						<xsl:attribute name="Source">
						<xsl:value-of select="$Source" />
					</xsl:attribute>
						<xsl:attribute name="SourceRef">
						<xsl:value-of select="$BookingCode" />
					</xsl:attribute>

						<xsl:element name="ElementNumber">
							<xsl:value-of select="intinc:getNext($counter_global)" />
						</xsl:element>
						<xsl:element name="Departure">
							<xsl:variable name="OriginCode"
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/DepartureAirport/@LocationCode" />
							<xsl:element name="AirportCode">
								<xsl:value-of select="$OriginCode" />
							</xsl:element>
							<xsl:element name="AirportName">
								<xsl:value-of
									select="concat($Airport_Lookup[AirportCode=$OriginCode]/AirportName,', ',$Airport_Lookup[AirportCode=$OriginCode]/CountryCode)" />
							</xsl:element>
							<xsl:element name="Date">
								<xsl:call-template name="dateformat">
									<xsl:with-param name="format" select="'date'" />
									<xsl:with-param name="datestr"
										select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@DepartureDateTime" />
									<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
									<xsl:with-param name="informat" select="string('datetime')" />
								</xsl:call-template>
							</xsl:element>
							<xsl:element name="Time">
								<xsl:call-template name="dateformat">
									<xsl:with-param name="datestr"
										select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@DepartureDateTime" />
									<xsl:with-param name="outformat" select="string('hh-mm')" />
									<xsl:with-param name="informat" select="string('datetime')" />
									<xsl:with-param name="format" select="'time'" />
								</xsl:call-template>
							</xsl:element>
						</xsl:element>
						<xsl:element name="Arrival">
							<xsl:variable name="DestinationCode"
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/ArrivalAirport/@LocationCode" />
							<xsl:element name="AirportCode">
								<xsl:value-of select="$DestinationCode" />
							</xsl:element>
							<xsl:element name="AirportName">
								<xsl:value-of
									select="concat($Airport_Lookup[AirportCode=$DestinationCode]/AirportName,', ',$Airport_Lookup[AirportCode=$DestinationCode]/CountryCode)" />
							</xsl:element>
							<xsl:element name="Date">
								<xsl:call-template name="dateformat">
									<xsl:with-param name="format" select="'date'" />
									<xsl:with-param name="datestr"
										select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ArrivalDateTime" />
									<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
									<xsl:with-param name="informat" select="string('datetime')" />
								</xsl:call-template>
							</xsl:element>
							<xsl:element name="Time">
								<xsl:call-template name="dateformat">
									<xsl:with-param name="datestr"
										select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ArrivalDateTime" />
									<xsl:with-param name="outformat" select="string('hh-mm')" />
									<xsl:with-param name="informat" select="string('datetime')" />
									<xsl:with-param name="format" select="'time'" />
								</xsl:call-template>

							</xsl:element>
						</xsl:element>
						<xsl:element name="Carrier">
							<xsl:variable name="MarketingAirline"
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/MarketingAirline/@Code" />
							<xsl:element name="AirlineCode">
								<xsl:value-of select="$MarketingAirline" />
							</xsl:element>
							<xsl:element name="AirlineName">
								<xsl:value-of
									select="$Airline_Lookup[AirLineCode=$MarketingAirline]/AirLineName" />
							</xsl:element>
							<xsl:element name="FlightNumber">
								<xsl:variable name="FlightNumber"
									select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@FlightNumber" />
								<xsl:attribute name="Suffix">
								<xsl:value-of select="$MarketingAirline" />
							</xsl:attribute>
								<xsl:value-of
									select="translate($FlightNumber, translate($FlightNumber,'0123456789', ''), '')" />
							</xsl:element>
						</xsl:element>
						<xsl:element name="Equipment">
							<xsl:element name="Code">
								<xsl:value-of select="$DefaultEquipment" />
							</xsl:element>
							<xsl:element name="Name">
								<xsl:value-of
									select="$Equipment_Lookup[EquipmentCode=$DefaultEquipment]/EquipmentName" />
							</xsl:element>
						</xsl:element>
						<xsl:element name="ClassOfService">
							<xsl:value-of
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ResBookDesigCode" />
						</xsl:element>
						<xsl:element name="ActionCode">
							<xsl:value-of select="'HK'" />
						</xsl:element>

					</xsl:element>
				</xsl:element>

			</xsl:element>

			<!-- BillingAndDeliveryData -->
			<xsl:element name="BillingAndDeliveryData">
				<xsl:element name="FormOfPayment">
					<xsl:attribute name="Source">
						<xsl:value-of select="$Source" />
					</xsl:attribute>
					<xsl:attribute name="SourceRef">
						<xsl:value-of select="$BookingCode" />
					</xsl:attribute>
					<xsl:element name="ElementNumber">
						<xsl:value-of select="intinc:getNext($counter_global)" />
					</xsl:element>
					<xsl:element name="Other">
						<xsl:value-of select="$FormOfPayment" />
					</xsl:element>
				</xsl:element>
			</xsl:element>

			<!-- FareGroup -->
			<xsl:element name="FareGroup">
				<xsl:attribute name="FareType">
			 		<xsl:value-of select="$FareType" />
				</xsl:attribute>

				<xsl:attribute name="FareNumber">
				 	<xsl:value-of select="$FareNumber" /> 
				</xsl:attribute>

				<xsl:attribute name="TravelerCount">
					<xsl:value-of
					select="count($CurrentAirBooking/CustomerInfos/CustomerInfo)" />
				</xsl:attribute>

				<xsl:variable name="Final_Total"
					select="//SPResponse//AirTicketRQResponse//AirReservation/PriceInfo/ItinTotalFare/TotalFare/@Amount" />

				<xsl:attribute name="TotalPrice">
					<xsl:value-of select="round(format-number($Final_Total,'#.00') * 100)" />						
				</xsl:attribute>

				<xsl:attribute name="Source">
					<xsl:value-of select="$Source" />
				</xsl:attribute>

				<!-- CurrencyCode -->
				<xsl:element name="CurrencyCode">
					<xsl:attribute name="NumberOfDecimals">
					<xsl:value-of select="$CURRENCY_NUMBER_OF_DECIMAL" />
				</xsl:attribute>
					<xsl:value-of select="Cost/@CurrencyCode" />
				</xsl:element>
				<xsl:apply-templates
					select="$CurrentAirBooking/ItineraryInfo/ItineraryPricing" />
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- TravelerGroup -->
	<xsl:template match="ItineraryPricing">

		<xsl:variable name="CurrentAirBooking"
			select="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/TravelItinerary" />

		<xsl:variable name="CurrentPaxType" select="@PassengerTypeCode" />

		
		<xsl:variable name="PaxTotal_BasePrice">
			<xsl:value-of
				select="Cost/@AmountBeforeTax" />
		</xsl:variable>

		<xsl:variable name="Taxes">
			<xsl:for-each
				select="Cost/Taxes/Tax">
				<Tax_Amount>
					<xsl:value-of select="@Amount" />
				</Tax_Amount>
				<Tax_Description>
					<xsl:value-of select="@Code" />
				</Tax_Description>
			</xsl:for-each>
		</xsl:variable>

		<xsl:variable name="PaxTotalPrice">
			<xsl:value-of
				select="Cost/@AmountAfterTax" />
		</xsl:variable>

		<xsl:element name="TravelerGroup">

			<xsl:variable name="Counter" select="intinc:new()" />

			<xsl:variable name="TotalTravelerType">
				<xsl:value-of
					select="count($CurrentAirBooking/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType=$CurrentPaxType])" />
			</xsl:variable>

			<xsl:attribute name="TypeRequested"> 
							<xsl:value-of select="$CurrentPaxType" />
						 </xsl:attribute>
			<xsl:attribute name="TypePriced"> 
							<xsl:value-of select="$CurrentPaxType" />
						 </xsl:attribute>

			<xsl:attribute name="TypeTotalPrice">
							<xsl:value-of
				select="round(format-number($PaxTotalPrice,'#.00') * 100)" />
						</xsl:attribute>

			<xsl:attribute name="TypeCount"> 
							<xsl:value-of select="$TotalTravelerType" /> 
						</xsl:attribute>
			<xsl:element name="Price">

				<xsl:attribute name="Total">
								<xsl:value-of
					select="round(format-number($PaxTotalPrice div $TotalTravelerType,'#.00') * 100)" />
							</xsl:attribute>

				<xsl:element name="BaseFare">
					<xsl:attribute name="Amount"> 
					       			<xsl:value-of
						select="round(format-number($PaxTotal_BasePrice div $TotalTravelerType,'#.00') * 100)" />
								</xsl:attribute>
				</xsl:element>

				<xsl:element name="Taxes">
					<xsl:if test="count(exslt:node-set($Taxes)/*) &gt; 0">
						<xsl:attribute name="Amount">
									<xsl:value-of
							select="round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount),'#.00') * 100)" />
								</xsl:attribute>
						<xsl:for-each select="exslt:node-set($Taxes)/Tax_Amount">
							<xsl:variable name="TaxNode_Count" select="intinc:getNext($Counter)" />
							<xsl:element name="Tax">
								<xsl:attribute name="Amount">
											<xsl:value-of
									select="round(format-number(exslt:node-set($Taxes)/Tax_Amount[position()=$TaxNode_Count],'#.00') * 100)" />
										</xsl:attribute>
								<xsl:element name="Description">
									<xsl:value-of
										select="exslt:node-set($Taxes)/Tax_Description[position()=$TaxNode_Count]" />
								</xsl:element>
							</xsl:element>
						</xsl:for-each>
					</xsl:if>
				</xsl:element>
			</xsl:element>

			<!-- FareRules -->
			<xsl:element name="FareRules">

				<xsl:element name="FareInfo">
					<xsl:element name="DepartureCode">
						<xsl:value-of
							select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/DepartureAirport/@LocationCode" />
					</xsl:element>

					<xsl:element name="DepartureDate">
						<xsl:call-template name="dateformat">
							<xsl:with-param name="format" select="'date'" />
							<xsl:with-param name="datestr"
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@DepartureDateTime" />
							<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
							<xsl:with-param name="informat" select="string('datetime')" />
						</xsl:call-template>
					</xsl:element>
					<xsl:element name="DepartureTime">
						<xsl:call-template name="dateformat">
							<xsl:with-param name="datestr"
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@DepartureDateTime" />
							<xsl:with-param name="outformat" select="string('hh-mm')" />
							<xsl:with-param name="informat" select="string('datetime')" />
							<xsl:with-param name="format" select="'time'" />
						</xsl:call-template>
					</xsl:element>
					<xsl:element name="ArrivalCode">
						<xsl:value-of
							select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/ArrivalAirport/@LocationCode" />
					</xsl:element>
					<xsl:element name="ArrivalDate">
						<xsl:call-template name="dateformat">
							<xsl:with-param name="format" select="'date'" />
							<xsl:with-param name="datestr"
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ArrivalDateTime" />
							<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
							<xsl:with-param name="informat" select="string('datetime')" />
						</xsl:call-template>
					</xsl:element>
					<xsl:element name="ArrivalTime">
						<xsl:call-template name="dateformat">
							<xsl:with-param name="datestr"
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ArrivalDateTime" />
							<xsl:with-param name="outformat" select="string('hh-mm')" />
							<xsl:with-param name="informat" select="string('datetime')" />
							<xsl:with-param name="format" select="'time'" />
						</xsl:call-template>

					</xsl:element>
					<xsl:element name="AirlineCode">
						<xsl:value-of
							select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/MarketingAirline/@Code" />
					</xsl:element>
					<!-- <xsl:element name="FareBasisCode"> <xsl:value-of select="//SPResponse//AirSellRQResponse//FareBasisCode" 
						/> </xsl:element> -->

					<xsl:element name="RelatedSegment">
						<xsl:element name="SegmentIDRef">
							<xsl:attribute name="FlightRefKey">
											<xsl:value-of select="'F1'" />
										</xsl:attribute>
							<xsl:value-of select="1" />
						</xsl:element>

						<xsl:element name="DepartureCode">
							<xsl:value-of
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/DepartureAirport/@LocationCode" />
						</xsl:element>
						<xsl:element name="ArrivalCode">
							<xsl:value-of
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/ArrivalAirport/@LocationCode" />
						</xsl:element>

						<xsl:element name="DepartureDate">
							<xsl:call-template name="dateformat">
								<xsl:with-param name="format" select="'date'" />
								<xsl:with-param name="datestr"
									select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@DepartureDateTime" />
								<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
								<xsl:with-param name="informat" select="string('datetime')" />
							</xsl:call-template>
						</xsl:element>
						<xsl:element name="ArrivalDate">
							<xsl:call-template name="dateformat">
								<xsl:with-param name="format" select="'date'" />
								<xsl:with-param name="datestr"
									select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ArrivalDateTime" />
								<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
								<xsl:with-param name="informat" select="string('datetime')" />
							</xsl:call-template>
						</xsl:element>
						<xsl:element name="ArrivalTime">
							<xsl:call-template name="dateformat">
								<xsl:with-param name="datestr"
									select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ArrivalDateTime" />
								<xsl:with-param name="outformat" select="string('hh-mm')" />
								<xsl:with-param name="informat" select="string('datetime')" />
								<xsl:with-param name="format" select="'time'" />
							</xsl:call-template>
						</xsl:element>
						<xsl:element name="DepartureTime">
							<xsl:call-template name="dateformat">
								<xsl:with-param name="datestr"
									select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@DepartureDateTime" />
								<xsl:with-param name="outformat" select="string('hh-mm')" />
								<xsl:with-param name="informat" select="string('datetime')" />
								<xsl:with-param name="format" select="'time'" />
							</xsl:call-template>

						</xsl:element>
						<xsl:element name="ClassOfService">
							<xsl:value-of
								select="$CurrentAirBooking/ItineraryInfo/ReservationItems/Item/Air/@ResBookDesigCode" />
						</xsl:element>
					</xsl:element>
				</xsl:element>

				<!-- Rules -->
				<xsl:for-each select="//SPResponse//OTA_AirRulesRQResponse//Text">
					<xsl:element name="Remark">
						<xsl:value-of select="current()" />
					</xsl:element>
				</xsl:for-each>

			</xsl:element>
		</xsl:element>

	</xsl:template>

	<!-- Traveler -->
	<xsl:template match="CustomerInfo">
		<xsl:if test="not(Customer/PersonName/@NameType='INF')">
			<xsl:element name="Traveler">
				<xsl:attribute name="Source">
				<xsl:value-of select="$Source" />
			</xsl:attribute>
				<xsl:attribute name="Type">
				<xsl:value-of select="Customer/PersonName/@NameType" />
			</xsl:attribute>
				<xsl:attribute name="SourceRef">
				<xsl:value-of select="$BookingCode" />
			</xsl:attribute>
				<xsl:element name="ElementNumber">
					<xsl:value-of select="(position() +10)" />
				</xsl:element>
				<xsl:element name="TravelerName">
					<xsl:element name="Surname">
						<xsl:value-of select="Customer/PersonName/Surname" />
					</xsl:element>
					<xsl:element name="GivenName">
						<xsl:value-of select="Customer/PersonName/GivenName" />
					</xsl:element>
					<xsl:element name="NativeGivenName">
						<xsl:value-of select="Customer/PersonName/GivenName" />
					</xsl:element>
					<!-- <xsl:element name="DateOfBirth"> <xsl:value-of select="$adults[$counterOutBOundAdultForInfant]/BirthDate" 
						/> </xsl:element> -->
					<xsl:element name="Gender">
						<xsl:choose>
							<xsl:when test="Customer/PersonName/NameTitle ='MR'">
								<xsl:value-of select="'M'" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="'F'" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
				</xsl:element>
				<xsl:if
					test="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName/@NameType='INF' and position()='1'">
					<xsl:element name="Infant">
						<xsl:element name="ElementNumber">
							<xsl:value-of select="(position() +10) + $InfantElementNumber" />
						</xsl:element>
						<xsl:element name="Surname">
							<xsl:value-of select="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType='INF']/Surname" />
						</xsl:element>
						<xsl:element name="GivenName">
							<xsl:value-of select="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType='INF']/GivenName" />
						</xsl:element>
						<xsl:element name="NativeGivenName">
							<xsl:value-of select="//SPResponse//OTA_TravelItineraryReadRQResponse/OTA_TravelItineraryRS/TravelItinerary/CustomerInfos/CustomerInfo/Customer/PersonName[@NameType='INF']/GivenName" />
						</xsl:element>
						<!-- <xsl:element name="DateOfBirth"> <xsl:value-of select="BirthDate" 
							/> </xsl:element> -->
						<xsl:element name="Gender">
							<xsl:choose>
								<xsl:when test="Customer/PersonName/NameTitle ='Master'">
									<xsl:value-of select="'M'" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="'F'" />
								</xsl:otherwise>
							</xsl:choose>
						</xsl:element>
					</xsl:element>
				</xsl:if>

			</xsl:element>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>