package com.abb.serviceprovider.jetAirways.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.abb.framework.serviceprovider.SPRequest;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.transformation.OpenAirTransformation;
import com.abb.framework.util.Utils;
import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.common.CommonConstant;
import com.abb.serviceprovider.jetAirways.JARequest;

/**
 * The Class JATicketIssueService.
 */
@Service("jaTicketIssueService")
public class JATicketIssueService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JAPNRCreateService.class);

	/** The soap header. */
	private JASOAPHeader soapHeader;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {
		String token = null;
		// INTA-57
		SOAPWebServiceTemplate jaWebServiceClient = ApplicationContextUtil.getBean("jaWebServiceClient", SOAPWebServiceTemplate.class);
		JALoginService jaLoginService = ApplicationContextUtil.getBean("jaLoginService", JALoginService.class);
		JALogoutService jaLogoutService = ApplicationContextUtil.getBean("jaLogoutService", JALogoutService.class);
		OpenAirTransformation xmlTransformer = ApplicationContextUtil.getBean("XMLTransformation", OpenAirTransformation.class);

		try {
			String spRequestXML = "";
			String spResponseXML = null;

			String loginresponse = jaLoginService.executeLogin();
			LOG.debug("\n loginresponse=={}", loginresponse);
			token = StringUtils.substringBetween(loginresponse, "<BinarySecurityToken>", "</BinarySecurityToken>");
			for (SPRequest spRequest : getSpRequests()) {

				LOG.debug("Index=={}", getSpRequests().indexOf(spRequest));
				LOG.debug("Token=={}", token);

				// Get Session Response and add to Service Envelop
				/*
				 * if (getSpRequests().indexOf(spRequest) > 0 && token != null) { LOG.info( "JATicketIssueService - jaSession before token set to " + token); jaSession.setSessionToken(token); LOG.info(
				 * "JATicketIssueService - jaSession after token set to " + token); } else { LOG.info("JATicketIssueService - before borrow session "); jaSession = spSessionManager.openSession(ServiceProvider.JET_AIRWAYS .getSessionKey());
				 * LOG.info("JATicketIssueService - after borrow session "); LOG.info("JATicketIssueService - borrowed session " + jaSession.getSessionToken()); }
				 */
				getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(loginresponse);

				// Get SOAP Header and add to Service Envelop
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestHeaderXML(
						soapHeader.getJASoapHeader(getOpenAirlinesServiceEnvelopXML().getServiceXML(), ((JARequest) spRequest).getService(), ((JARequest) spRequest).getAction(), token));

				LOG.info("before transormation *******{}", getOpenAirlinesServiceEnvelopXML().getServiceXML());
				// Transform to Service Provide request and add to Service
				// Envelop
				spRequestXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), spRequestXML, spRequest.getRequestXSLTAbsolutePath(), null);
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestBodyXML(spRequestXML);

				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPAction(spRequest.getSoapAction());
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPServiceURL(spRequest.getServiceURL());
				LOG.info("after transormation *******{}", getOpenAirlinesServiceEnvelopXML().getServiceXML());

				// Send request to web service client
				spResponseXML = jaWebServiceClient.sendAndReceive(getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML());

				// Extract Security Token from response
				token = StringUtils.substringBetween(spResponseXML, "<BinarySecurityToken>", "</BinarySecurityToken>");

				// To check for error in response and come out of loop
				if (this.getOpenAirlinesServiceEnvelopXML().getServiceXML().contains("Fault")) {
					break;
				}

				LOG.info("\n spResponseXML in JATicketIssueService==={}", spResponseXML);
				getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spResponseXML);
			}

		} catch (SoapFaultClientException e) {
			LOG.info("SoapFault Exception Occured");
			LOG.error(CommonConstant.ERROR_MSG, e);
			String response = Utils.getErrorResponseForFaultWithMessage(e);
			getOpenAirlinesServiceEnvelopXML().setSpResponseXML(response);
		} catch (Exception e) {
			LOG.error(CommonConstant.ERROR_MSG, e);
			throw e;
		} finally {
			if (StringUtils.isNotEmpty(token)) {
				jaLogoutService.setBinarySecurityToken(token);
				try {
					jaLogoutService.executeLogout();
					LOG.info("JAPNRRetrieveService logout completed for token - {}", token);
				} catch (Exception e) {
					LOG.error("JAPNRRetrieveService error occured during Logout for session token - {}", token, e);
				}
			}
		}

		String saResponseXML = "";
		saResponseXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), saResponseXML, spResponse.getResponseXSLTAbsolutePath(), null);
		getOpenAirlinesServiceEnvelopXML().setSaResponseXML(saResponseXML);
		LOG.info("JATicketIssueService ****** saResponseXML == {}", saResponseXML);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JATicketIssueService";
	}

	/**
	 * Gets the soap header.
	 * 
	 * @return the soap header
	 */
	public JASOAPHeader getSoapHeader() {
		return soapHeader;
	}

	/**
	 * Sets the soap header.
	 * 
	 * @param soapHeader
	 *            the new soap header
	 */
	public void setSoapHeader(JASOAPHeader soapHeader) {
		this.soapHeader = soapHeader;
	}
}
