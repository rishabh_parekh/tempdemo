package com.abb.serviceprovider.jetAirways.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.abb.framework.entity.SOAPServiceEnvelopXML;
import com.abb.framework.serviceprovider.SPService;
import com.abb.serviceprovider.jetAirways.JARequest;
import com.abb.serviceprovider.jetAirways.JAWebServiceClient;


/**
 * This class calls login service of JetAirways.
 * 
 * @author irfanak
 * 
 */
@Service("jaLoginService")
public class JALoginService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JALoginService.class);

	/** The ja web service client. */
	@Autowired
	private JAWebServiceClient jaWebServiceClient;

	/** The context. */
	@Autowired
	private ApplicationContext context;

	/** The username. */
	private String username;
	
	/** The password. */
	private String password;
	
	/** The company id. */
	private String companyId;

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {
		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		soapEnvelopXML.setSpSOAPRequestBodyXML(getLoginXMLBody());
		soapEnvelopXML.setSpSOAPRequestHeaderXML(getLoginXMLHeader());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());

		String spLoginResponse = jaWebServiceClient.sendAndReceive(soapEnvelopXML);
		LOG.info("spLoginResponse = {}", spLoginResponse);
		getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(spLoginResponse);
	}

	/**
	 * Execute login.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String executeLogin() throws Exception {
		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		soapEnvelopXML.setSpSOAPRequestBodyXML(getLoginXMLBody());
		soapEnvelopXML.setSpSOAPRequestHeaderXML(getLoginXMLHeader());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());

		String spLoginResponse = jaWebServiceClient.sendAndReceive(soapEnvelopXML);
		LOG.info("spLoginResponse = {}", spLoginResponse);
		return spLoginResponse;

	}

	/**
	 * Create and return header XML of Login Service.
	 *
	 * @return the login xml header
	 */
	private String getLoginXMLHeader() {
		StringBuilder headerStringBuilder = new StringBuilder();

		headerStringBuilder.append("<OpenAirSOAPHeader>");
		// Security Header
		headerStringBuilder.append("<sec:Security xmlns:sec=\"http://schemas.xmlsoap.org/ws/2002/12/secext\">");
		headerStringBuilder.append("<sec:UsernameToken>");
		headerStringBuilder.append("<sec:Username>");
		headerStringBuilder.append(username);
		headerStringBuilder.append("</sec:Username>");
		headerStringBuilder.append("<sec:Password>");
		headerStringBuilder.append(password);
		headerStringBuilder.append("</sec:Password>");
		headerStringBuilder.append("<Organization>");
		headerStringBuilder.append(companyId);
		headerStringBuilder.append("</Organization>");
		headerStringBuilder.append("</sec:UsernameToken>");
		headerStringBuilder.append("</sec:Security>");

		// Message Header
		headerStringBuilder.append("<mes:MessageHeader xmlns:mes=\"http://www.ebxml.org/namespaces/messageHeader\">");
		headerStringBuilder.append("<mes:CPAId>");
		headerStringBuilder.append(companyId);
		headerStringBuilder.append("</mes:CPAId>");
		headerStringBuilder.append("<mes:ConversationId>");
		headerStringBuilder.append(JAUtils.getConversationId());
		headerStringBuilder.append("</mes:ConversationId>");
		headerStringBuilder.append("<mes:Service>" + ((JARequest) spRequests.get(0)).getService() + "</mes:Service>");
		headerStringBuilder.append("<mes:Action>" + ((JARequest) spRequests.get(0)).getAction() + "</mes:Action>");
		headerStringBuilder.append("<mes:MessageData>");
		headerStringBuilder.append("<mes:MessageId>");
		headerStringBuilder.append(JAUtils.getMessageId());
		headerStringBuilder.append("</mes:MessageId>");
		headerStringBuilder.append("<mes:Timestamp>");
		headerStringBuilder.append(JAUtils.getCurrentUTCTime());
		headerStringBuilder.append("</mes:Timestamp>");
		headerStringBuilder.append("</mes:MessageData>");
		headerStringBuilder.append("</mes:MessageHeader>");

		headerStringBuilder.append("</OpenAirSOAPHeader>");

		return headerStringBuilder.toString();
	}

	/**
	 * Create and return body XML for login service.
	 *
	 * @return the login xml body
	 */
	private String getLoginXMLBody() {
		return "<web:Logon xmlns:web=\"http://www.vedaleon.com/webservices\"/>";
	}

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JALoginService";
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
}
