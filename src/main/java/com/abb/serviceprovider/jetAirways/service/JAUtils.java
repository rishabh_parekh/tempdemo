package com.abb.serviceprovider.jetAirways.service;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


/**
 * The Class JAUtils.
 */
public class JAUtils {

	/** The date time format. */
	public static String dateTimeFormat = "YYYY-MM-dd'T'hh:mm:ss.sss";

	/**
	 * Gets the conversation id.
	 *
	 * @return the conversation id
	 */
	public static String getConversationId() {
		String currentTimeStamp = new DateTime(DateTimeZone.UTC).toString(dateTimeFormat);
		return "session:" + currentTimeStamp + "@airblackbox.com";
	}

	/**
	 * Gets the message id.
	 *
	 * @return the message id
	 */
	public static String getMessageId() {
		String currentTimeStamp = new DateTime(DateTimeZone.UTC).toString(dateTimeFormat);
		return "session:" + currentTimeStamp + "@airblackbox.com.session.msg-1";
	}

	/**
	 * Gets the current utc time.
	 *
	 * @return the current utc time
	 */
	public static String getCurrentUTCTime() {
		return new DateTime(DateTimeZone.UTC).toString("YYYY-MM-dd'T'hh:mm:ss");
	}
}
