package com.abb.serviceprovider.jetAirways.service;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.abb.framework.entity.SOAPServiceEnvelopXML;
import com.abb.framework.serviceprovider.SPService;
import com.abb.serviceprovider.jetAirways.JARequest;
import com.abb.serviceprovider.jetAirways.JAWebServiceClient;


/**
 * The Class JALogoutService.
 */
@Service("jaLogoutService")
public class JALogoutService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JALogoutService.class);

	/** The ja web service client. */
	@Autowired
	private JAWebServiceClient jaWebServiceClient;

	/** The context. */
	@Autowired
	private ApplicationContext context;

	/** The username. */
	private String username;
	
	/** The password. */
	private String password;
	
	/** The company id. */
	private String companyId;
	
	/** The binary security token. */
	private String binarySecurityToken;

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {
		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		soapEnvelopXML.setSpSOAPRequestBodyXML(getLogoutXMLBody());
		soapEnvelopXML.setSpSOAPRequestHeaderXML(getLogoutXMLHeader());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());

		String spLogoutResponse = jaWebServiceClient.sendAndReceive(soapEnvelopXML);
		LOG.info("spLogoutResponse = {}", spLogoutResponse);
		// getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(spLoginResponse);
	}

	/**
	 * This method can be removed once execute() method is adjusted/verified.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String executeLogout() throws Exception {
		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		soapEnvelopXML.setSpSOAPRequestBodyXML(getLogoutXMLBody());
		soapEnvelopXML.setSpSOAPRequestHeaderXML(getLogoutXMLHeader());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());

		String spLogoutResponse = jaWebServiceClient.sendAndReceive(soapEnvelopXML);
		LOG.info("spLogoutResponse = {}", spLogoutResponse);
		return spLogoutResponse;
	}

	/**
	 * Create and return header XML of Logout Service.
	 *
	 * @return the logout xml header
	 */
	private String getLogoutXMLHeader() {
		StringBuilder headerStringBuilder = new StringBuilder();
		String openAirlineEmail = "@openairline.com";
		String currentUTCTime = new DateTime(DateTimeZone.UTC).toString("YYYY-MM-dd'T'hh:mm:ss'Z'");
		String currentTimeStamp = new DateTime(DateTimeZone.UTC).toString("YYYY-MM-dd'T'hh:mm:ss.sss");
		String conversationId = "cid:" + currentTimeStamp + openAirlineEmail;
		String messageId = "mid:" + currentTimeStamp + openAirlineEmail;

		headerStringBuilder.append("<OpenAirSOAPHeader>");
		// Security Header
		headerStringBuilder.append("<sec:Security xmlns:sec=\"http://schemas.xmlsoap.org/ws/2002/12/secext\">");
		headerStringBuilder.append("<sec:UsernameToken>");
		headerStringBuilder.append("<sec:Username>");
		headerStringBuilder.append(username);
		headerStringBuilder.append("</sec:Username>");
		headerStringBuilder.append("<sec:Password>");
		headerStringBuilder.append(password);
		headerStringBuilder.append("</sec:Password>");
		headerStringBuilder.append("<Organization>");
		headerStringBuilder.append(companyId);
		headerStringBuilder.append("</Organization>");
		headerStringBuilder.append("</sec:UsernameToken>");
		headerStringBuilder.append("<sec:BinarySecurityToken>");
		headerStringBuilder.append(binarySecurityToken);
		headerStringBuilder.append("</sec:BinarySecurityToken>");
		headerStringBuilder.append("</sec:Security>");

		// Message Header
		headerStringBuilder.append("<mes:MessageHeader xmlns:mes=\"http://www.ebxml.org/namespaces/messageHeader\">");
		headerStringBuilder.append("<mes:CPAId>");
		headerStringBuilder.append(companyId);
		headerStringBuilder.append("</mes:CPAId>");
		headerStringBuilder.append("<mes:ConversationId>");
		headerStringBuilder.append(conversationId);
		headerStringBuilder.append("</mes:ConversationId>");
		headerStringBuilder.append("<mes:Service>" + ((JARequest) spRequests.get(0)).getService() + "</mes:Service>");
		headerStringBuilder.append("<mes:Action>" + ((JARequest) spRequests.get(0)).getAction() + "</mes:Action>");
		headerStringBuilder.append("<mes:MessageData>");
		headerStringBuilder.append("<mes:MessageId>");
		headerStringBuilder.append(messageId);
		headerStringBuilder.append("</mes:MessageId>");
		headerStringBuilder.append("<mes:Timestamp>");
		headerStringBuilder.append(currentUTCTime);
		headerStringBuilder.append("</mes:Timestamp>");
		headerStringBuilder.append("</mes:MessageData>");
		headerStringBuilder.append("</mes:MessageHeader>");

		headerStringBuilder.append("</OpenAirSOAPHeader>");

		return headerStringBuilder.toString();
	}

	/**
	 * Create and return body XML for Logout service.
	 *
	 * @return the logout xml body
	 */
	private String getLogoutXMLBody() {
		return "<web:Logoff xmlns:web=\"http://www.vedaleon.com/webservices\"/>";
	}

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JALogoutService";
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/**
	 * Gets the binary security token.
	 *
	 * @return the binary security token
	 */
	public String getBinarySecurityToken() {
		return binarySecurityToken;
	}

	/**
	 * Sets the binary security token.
	 *
	 * @param binarySecurityToken the new binary security token
	 */
	public void setBinarySecurityToken(String binarySecurityToken) {
		this.binarySecurityToken = binarySecurityToken;
	}
}
