package com.abb.serviceprovider.jetAirways.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class JASOAPHeader.
 */
@Service
public class JASOAPHeader {

	/** The soap header xslt. */
	private Resource soapHeaderXSLT;
	
	/** The company id. */
	private String companyId;
	
	/** The token from response. */
	private String tokenFromResponse;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JASOAPHeader.class);

	/** The xml transformation. */
	@Autowired
	private OpenAirTransformation xmlTransformation;

	/**
	 * Gets the JA soap header.
	 *
	 * @param serviceEnvelopXML the service envelop xml
	 * @param service the service
	 * @param action the action
	 * @param token the token
	 * @return the JA soap header
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getJASoapHeader(String serviceEnvelopXML, String service, String action, String token) throws OpenAirTransformationException, IOException {

		Map<String, String> paramMap = new HashMap<String, String>();
		tokenFromResponse = token;
		paramMap.put("CPA_id", getCompanyId());
		if (tokenFromResponse != null) {
			paramMap.put("BinarySecurityToken", getTokenFromresponse());
		}
		paramMap.put("service", service);
		paramMap.put("action", action);
		paramMap.put("conversation_id", JAUtils.getConversationId());
		paramMap.put("message_id", JAUtils.getMessageId());
		paramMap.put("time_stamp", JAUtils.getCurrentUTCTime());
		LOG.info(" soapHeaderXSLT = {}", soapHeaderXSLT);
		String resultSOAPHeaderXML = "";

		return xmlTransformation.transform(serviceEnvelopXML, resultSOAPHeaderXML, soapHeaderXSLT.getFile().getAbsolutePath(), paramMap);
	}

	/**
	 * Gets the soap header xslt.
	 *
	 * @return the soap header xslt
	 */
	public Resource getSoapHeaderXSLT() {
		return soapHeaderXSLT;
	}

	/**
	 * Sets the soap header xslt.
	 *
	 * @param soapHeaderXSLT the new soap header xslt
	 */
	public void setSoapHeaderXSLT(Resource soapHeaderXSLT) {
		this.soapHeaderXSLT = soapHeaderXSLT;
	}

	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public String getCompanyId() {
		return companyId;
	}

	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	/**
	 * Gets the token fromresponse.
	 *
	 * @return the token fromresponse
	 */
	public String getTokenFromresponse() {
		return tokenFromResponse;
	}

	/**
	 * Sets the token fromresponse.
	 *
	 * @param tokenFromResponse the new token fromresponse
	 */
	public void setTokenFromresponse(String tokenFromResponse) {
		this.tokenFromResponse = tokenFromResponse;
	}
}
