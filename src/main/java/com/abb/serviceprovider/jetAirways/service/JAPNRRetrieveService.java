package com.abb.serviceprovider.jetAirways.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.abb.framework.serviceprovider.SPRequest;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.transformation.OpenAirTransformation;
import com.abb.framework.util.Utils;
import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.common.CommonConstant;
import com.abb.serviceprovider.jetAirways.JARequest;

/**
 * The Class JAPNRRetrieveService.
 */
@Service("jaPNRRetrieveService")
public class JAPNRRetrieveService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JAPNRRetrieveService.class);

	/** The soap header. */
	private JASOAPHeader soapHeader;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {
		String spRequestXML = "";
		String spResponseXML = null;
		String token = null;
		// INTA-57
		SOAPWebServiceTemplate jaWebServiceClient = ApplicationContextUtil.getBean("jaWebServiceClient", SOAPWebServiceTemplate.class);
		JALoginService jaLoginService = ApplicationContextUtil.getBean("jaLoginService", JALoginService.class);
		JALogoutService jaLogoutService = ApplicationContextUtil.getBean("jaLogoutService", JALogoutService.class);
		OpenAirTransformation xmlTransformer = ApplicationContextUtil.getBean("XMLTransformation", OpenAirTransformation.class);

		try {
			String loginresponse = jaLoginService.executeLogin();
			LOG.debug("\n loginresponse==" + loginresponse);
			token = StringUtils.substringBetween(loginresponse, "<BinarySecurityToken>", "</BinarySecurityToken>");
			for (SPRequest spRequest : getSpRequests()) {

				LOG.debug("Index== {}", getSpRequests().indexOf(spRequest));
				// /LOG.debug("Token==" + token);

				// Get Session Response and add to Service Envelop

				getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(loginresponse);

				// Get SOAP Header and add to Service Envelop
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestHeaderXML(
						soapHeader.getJASoapHeader(getOpenAirlinesServiceEnvelopXML().getServiceXML(), ((JARequest) spRequest).getService(), ((JARequest) spRequest).getAction(), token));

				// Transform to Service Provide request and add to Service
				// Envelop
				spRequestXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), spRequestXML, spRequest.getRequestXSLTAbsolutePath(), null);
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestBodyXML(spRequestXML);

				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPAction(spRequest.getSoapAction());
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPServiceURL(spRequest.getServiceURL());

				// Send request to web service client
				spResponseXML = jaWebServiceClient.sendAndReceive(getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML());

				// Extract Security Token from response
				/*
				 * token = XMLUtils.getCompleteTagAsString(this. getOpenAirlinesServiceEnvelopXML().getServiceXML(), "BinarySecurityToken[getSpRequests().indexOf(spRequest)+2]");
				 */

				token = StringUtils.substringBetween(spResponseXML, "<BinarySecurityToken>", "</BinarySecurityToken>");

				// To check for error in response and come out of loop
				if (this.getOpenAirlinesServiceEnvelopXML().getServiceXML().contains("Fault")) {
					break;
				}

				LOG.info("spResponseXML=={}", spResponseXML);
				getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spResponseXML);
			}
		} catch (SoapFaultClientException e) {
			LOG.info("SoapFault Exception Occured");
			LOG.error(CommonConstant.ERROR_MSG, e);
			String response = Utils.getErrorResponseForFaultWithMessage(e);
			getOpenAirlinesServiceEnvelopXML().setSpResponseXML(response);
		} catch (Exception e) {
			LOG.error(CommonConstant.ERROR_MSG, e);
			throw e;
		} finally {
			if (StringUtils.isNotEmpty(token)) {
				jaLogoutService.setBinarySecurityToken(token);
				try {
					jaLogoutService.executeLogout();
					LOG.info("JAPNRRetrieveService logout completed for token - {}", token);
				} catch (Exception e) {
					LOG.error("JAPNRRetrieveService error occured during Logout for session token - {}", token, e);
				}
			}
		}

		String saResponseXML = "";
		saResponseXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), saResponseXML, spResponse.getResponseXSLTAbsolutePath(), null);
		getOpenAirlinesServiceEnvelopXML().setSaResponseXML(saResponseXML);
		LOG.info("JAPNRRetrieveService ****** saResponseXML == {}", saResponseXML);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JAPNRRetrieveService";
	}

	/**
	 * Gets the soap header.
	 * 
	 * @return the soap header
	 */
	public JASOAPHeader getSoapHeader() {
		return soapHeader;
	}

	/**
	 * Sets the soap header.
	 * 
	 * @param soapHeader
	 *            the new soap header
	 */
	public void setSoapHeader(JASOAPHeader soapHeader) {
		this.soapHeader = soapHeader;
	}
}
