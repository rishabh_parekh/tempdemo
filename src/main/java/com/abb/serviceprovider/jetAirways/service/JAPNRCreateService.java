package com.abb.serviceprovider.jetAirways.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.abb.framework.serviceprovider.SPRequest;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.transformation.OpenAirTransformation;
import com.abb.framework.util.Utils;
import com.abb.framework.util.XMLUtils;
import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.common.CommonConstant;
import com.abb.serviceprovider.jetAirways.JARequest;
import com.abb.serviceprovider.jetAirways.util.JAConstants;

/**
 * The Class JAPNRCreateService.
 */
@Service("jaPNRCreateService")
public class JAPNRCreateService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JAPNRCreateService.class);

	/** The soap header. */
	private JASOAPHeader soapHeader;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {

		String spRequestXML = "";
		String spResponseXML = null;
		String token = null;
		String isPricingVarianceFound = null;
		String isErrorReceivedInRespone = null;
		boolean isPassiveSegmentRQ = false;
		// INTA-57
		SOAPWebServiceTemplate jaWebServiceClient = ApplicationContextUtil.getBean("jaWebServiceClient", SOAPWebServiceTemplate.class);
		JALoginService jaLoginService = ApplicationContextUtil.getBean("jaLoginService", JALoginService.class);
		JALogoutService jaLogoutService = ApplicationContextUtil.getBean("jaLogoutService", JALogoutService.class);
		OpenAirTransformation xmlTransformer = ApplicationContextUtil.getBean("XMLTransformation", OpenAirTransformation.class);

		try {
			Integer i = 1;
			String loginresponse = jaLoginService.executeLogin();
			LOG.debug("\n loginresponse== {}", loginresponse);
			token = StringUtils.substringBetween(loginresponse, "<BinarySecurityToken>", "</BinarySecurityToken>");

			// INTA-44 Passive Segment check
			String requestXML = getOpenAirlinesServiceEnvelopXML().getSaRequestXML();
			String flightType = XMLUtils.getAttributeValue(requestXML, "Flight", "Type");
			if ("P".equalsIgnoreCase(flightType)) {
				// Start Process for Adding Passive Segment of 9W into JQ
				token = processForAddingPassiveSegment(loginresponse, token, jaWebServiceClient, xmlTransformer);
				isPassiveSegmentRQ = true;
			} else {
				for (SPRequest spRequest : getSpRequests()) {

					LOG.debug("Index== {}", getSpRequests().indexOf(spRequest));
					LOG.debug("Token== {}", token);

					// Skip execution of Pricing variance xslt
					if (i == 4) {
						// Complete the execution of For loop
						continue;
					}

					// Get Session Response and add to Service Envelop
					getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(loginresponse);

					// Get SOAP Header and add to Service Envelop
					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestHeaderXML(
							soapHeader.getJASoapHeader(getOpenAirlinesServiceEnvelopXML().getServiceXML(), ((JARequest) spRequest).getService(), ((JARequest) spRequest).getAction(), token));

					// Transform to Service Provide request and add to Service Envelop
					spRequestXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), spRequestXML, spRequest.getRequestXSLTAbsolutePath(), null);
					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestBodyXML(spRequestXML);
					LOG.info("spRequestXML== {} ", spRequestXML);

					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPAction(spRequest.getSoapAction());
					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPServiceURL(spRequest.getServiceURL());

					// Send request to web service client
					spResponseXML = jaWebServiceClient.sendAndReceive(getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML());

					// Extract Security Token from response
					token = StringUtils.substringBetween(spResponseXML, "<BinarySecurityToken>", "</BinarySecurityToken>");

					LOG.info("spResponseXML== {}", spResponseXML);

					getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spResponseXML);

					// BREAK THE LOOP TO AVOID CALLING OF PRICING VARIANCE IF ERROR RETURNED IN SELL RESPONSE
					isErrorReceivedInRespone = StringUtils.substringBetween(spResponseXML, "<Errors>", "</Errors>");
					if (Utils.isNotNull(isErrorReceivedInRespone)) {
						break;
					}

					// INTA-30 Pricing Variance Check only once at time of AirSellRQ
					// hence i=1
					if (i == 1 && JAConstants.enablePricingVarianceCheck) {

						// CHECK IF TOLERANCE VALUE TAG EXIST
						String isTagExisting = XMLUtils.getTagValue(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), "ToleranceValue");
						if (isTagExisting != null) {

							String spRequestXMLForPricingVariance = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), "", spRequests.get(3).getRequestXSLTAbsolutePath(), null);
							LOG.info("spRequestXMLForPricingVariance for Pricing Variance=={}", spRequestXMLForPricingVariance);

							isPricingVarianceFound = StringUtils.substringBetween(spRequestXMLForPricingVariance, "<varianceFound>", "</varianceFound>");

							// CASE 1. PRICING VARIANCE FOUND IN JQ, SEND ONLY SELL RQ FOR 9W , TERMINATE BOOKING PROCESS & REVERT WITH RESPONSE TO OAPI
							// CASE 2. PRICING VARIANCE NOT FOUND IN JQ, GO AHEAD WITH CHECK OF 9W ->
							// IF PRICING VARIANCE FOUND IN 9W, REVERT WITH ERROR RESPONSE,
							// ELSE CONTINUE WITH BOOKING
							String isCallSellRQTagExisting = XMLUtils.getTagValue(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), "CallSellRQ");

							LOG.info("isPricingVarianceFound == {}", isPricingVarianceFound);

							if ("true".equalsIgnoreCase(isPricingVarianceFound) || "true".equalsIgnoreCase(isCallSellRQTagExisting)) {
								getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spRequestXMLForPricingVariance);
								break;
							}
						}
					}

					// To check for error in response and come out of loop
					if (this.getOpenAirlinesServiceEnvelopXML().getServiceXML().contains("Fault")) {
						break;
					}

					i++;
				}
			}

		} catch (SoapFaultClientException e) {
			LOG.info("SoapFault Exception Occured");
			LOG.error(CommonConstant.ERROR_MSG, e);
			String response = Utils.getErrorResponseForFaultWithMessage(e);
			getOpenAirlinesServiceEnvelopXML().setSpResponseXML(response);
		} catch (Exception e) {
			LOG.error(CommonConstant.ERROR_MSG, e);
			throw e;
		} finally {
			if (StringUtils.isNotEmpty(token)) {
				jaLogoutService.setBinarySecurityToken(token);
				try {
					jaLogoutService.executeLogout();
					LOG.info("JAPNRCreateService logout completed for token - {}", token);
				} catch (Exception e) {
					LOG.error("JAPNRCreateService error occured during Logout for session token - {}", token, e);
				}
			}
		}

		String saResponseXML = "";

		// If PassiveSegment RQ then select 1st Response XSL from JetAiways-Config else select 0th
		// getSpResponses().get(1).getResponseXSLTAbsolutePath()
		if (isPassiveSegmentRQ) {
			LOG.debug("Appended Service XML:: ===={}", getOpenAirlinesServiceEnvelopXML().getServiceXML());
			saResponseXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), saResponseXML, getSpResponses().get(1).getResponseXSLTAbsolutePath(), null);
		} else {
			saResponseXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), saResponseXML, getSpResponses().get(0).getResponseXSLTAbsolutePath(), null);
		}

		getOpenAirlinesServiceEnvelopXML().setSaResponseXML(saResponseXML);
		LOG.info("JAPNRCreateService ****** saResponseXML == {}", saResponseXML);
	}

	/**
	 * METHOD TO ADD PASSIVE SEGMENT OF JQ TO 9W - PNR CROSS REFERENCING.
	 * 
	 * @param loginresponse
	 *            the loginresponse
	 * @param token
	 *            the token
	 * @param xmlTransformer
	 * @param jaWebServiceClient
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	private String processForAddingPassiveSegment(String loginresponse, String token, SOAPWebServiceTemplate jaWebServiceClient, OpenAirTransformation xmlTransformer) throws Exception {
		String spRequestXML = "";
		String spResponseXML = null;

		try {

			// Get Session Response and add to Service Envelop
			getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(loginresponse);

			// Get SOAP Header and add to Service Envelop. Used "getSpRequests().get(4)" As XSL files for Passive Segment start from count(starting from 0) 4 in JetAirways-Config.xml
			getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestHeaderXML(
					soapHeader.getJASoapHeader(getOpenAirlinesServiceEnvelopXML().getServiceXML(), ((JARequest) getSpRequests().get(4)).getService(), ((JARequest) getSpRequests().get(4)).getAction(), token));

			// Transform to Service Provide request and add to Service Envelop
			spRequestXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), spRequestXML, getSpRequests().get(4).getRequestXSLTAbsolutePath(), null);
			getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestBodyXML(spRequestXML);
			LOG.info("spRequestXML== {}", spRequestXML);

			getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPAction(getSpRequests().get(4).getSoapAction());
			getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPServiceURL(getSpRequests().get(4).getServiceURL());

			// Send request to web service client
			spResponseXML = jaWebServiceClient.sendAndReceive(getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML());
			LOG.info("spResponseXML=={}", spResponseXML);

			// Extract Security Token from response
			token = StringUtils.substringBetween(spResponseXML, "<BinarySecurityToken>", "</BinarySecurityToken>");

			getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spResponseXML);

		} catch (SoapFaultClientException e) {
			LOG.info("SoapFault Exception Occured");
			LOG.error(CommonConstant.ERROR_MSG, e);
			String response = Utils.getErrorResponseForFaultWithMessage(e);
			getOpenAirlinesServiceEnvelopXML().setSpResponseXML(response);
		} catch (Exception e) {
			LOG.error(CommonConstant.ERROR_MSG, e);
			throw e;
		}
		return token;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JAPNRCreateService";
	}

	/**
	 * Gets the soap header.
	 * 
	 * @return the soap header
	 */
	public JASOAPHeader getSoapHeader() {
		return soapHeader;
	}

	/**
	 * Sets the soap header.
	 * 
	 * @param soapHeader
	 *            the new soap header
	 */
	public void setSoapHeader(JASOAPHeader soapHeader) {
		this.soapHeader = soapHeader;
	}
}
