package com.abb.serviceprovider.jetAirways.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abb.framework.serviceprovider.session.SPSession;
import com.abb.framework.serviceprovider.session.SessionFactory;


/**
 * A factory for creating JASession objects.
 *
 * @author irfanak,ankurr
 */

public class JASessionFactory implements SessionFactory {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JASessionFactory.class);

	/*
	 * @Autowired
	 * 
	 * @Qualifier("jaLoginService") private SPService loginService;
	 * 
	 * @Autowired
	 * 
	 * @Qualifier("jaLogoutService") private SPService logoutService;
	 */

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#makeObject(java.lang.Object)
	 */
	@Override
	public SPSession makeObject(String key) throws Exception {
		/*
		 * LOG.info("Create JetAirways Session"); loginService.execute();
		 * 
		 * SPSession spSession = new SPSession(); spSession.setResponseXML(loginService .getOpenAirlinesServiceEnvelopXML().getSpLoginResponseXML()); spSession .setSessionToken(StringUtils.substringBetween(spSession.getResponseXML (),
		 * "<BinarySecurityToken>", "</BinarySecurityToken>")); return spSession;
		 */
		return null;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#destroyObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void destroyObject(String key, SPSession spSession) throws Exception {
		/*
		 * LOG.info("Destory JetAirways Session"); ((JALogoutService) logoutService).setBinarySecurityToken(spSession.getSessionToken()); logoutService.execute();
		 */
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#validateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean validateObject(String key, SPSession obj) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#activateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void activateObject(String key, SPSession spSession) throws Exception {
		// spSession.setActiveFlag(true);
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#passivateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void passivateObject(String key, SPSession spSession) throws Exception {
		// spSession.setActiveFlag(false);
	}
}
