package com.abb.serviceprovider.jetAirways;

import org.springframework.stereotype.Component;

import com.abb.framework.serviceprovider.SPRequest;


/**
 * The Class JARequest.
 */
@Component
public class JARequest extends SPRequest {

	/** The service. */
	private String service;
	
	/** The action. */
	private String action;

	/**
	 * Gets the service.
	 *
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * Sets the service.
	 *
	 * @param service the new service
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(String action) {
		this.action = action;
	}
}
