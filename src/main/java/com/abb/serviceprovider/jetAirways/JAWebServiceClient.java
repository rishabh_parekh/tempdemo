package com.abb.serviceprovider.jetAirways;

import org.springframework.stereotype.Service;

import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;

/**
 * The Class JAWebServiceClient.
 */
@Service("jaWebServiceClient")
public class JAWebServiceClient extends SOAPWebServiceTemplate {

}
