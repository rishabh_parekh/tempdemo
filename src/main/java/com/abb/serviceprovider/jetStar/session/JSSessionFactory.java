package com.abb.serviceprovider.jetStar.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abb.framework.serviceprovider.session.SPSession;
import com.abb.framework.serviceprovider.session.SessionFactory;


/**
 * A factory for creating JSSession objects.
 *
 * @author irfanak
 */

public class JSSessionFactory implements SessionFactory {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JSSessionFactory.class);

	/*
	 * @Autowired
	 * 
	 * @Qualifier("jsLoginService") private SPService jsLoginService;
	 * 
	 * @Autowired
	 * 
	 * @Qualifier("jsLogoutService") private SPService logoutService;
	 */

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#makeObject(java.lang.Object)
	 */
	@Override
	public SPSession makeObject(String key) throws Exception {
		/*
		 * LOG.info("======== Create JetStar Session ========"); jsLoginService.execute();
		 * 
		 * SPSession spSession = new SPSession(); spSession.setResponseXML(jsLoginService .getOpenAirlinesServiceEnvelopXML().getSpLoginResponseXML()); spSession .setSessionToken(StringUtils.substringBetween(spSession.getResponseXML (),
		 * "<Signature>", "</Signature>")); return spSession;
		 */
		return null;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#destroyObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void destroyObject(String key, SPSession spSession) throws Exception {
		/*
		 * LOG.info("========= Destory JetStar Session ========="); ((JSLogoutService) logoutService).setSignature(spSession.getSessionToken()); logoutService.execute();
		 */
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#validateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean validateObject(String key, SPSession obj) {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#activateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void activateObject(String key, SPSession obj) throws Exception {
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#passivateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void passivateObject(String key, SPSession obj) throws Exception {

	}
}
