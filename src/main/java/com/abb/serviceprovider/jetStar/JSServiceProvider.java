package com.abb.serviceprovider.jetStar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.abb.framework.exception.OpenAirServiceNotFoundForProviderException;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.serviceprovider.ServiceProvider;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.jetStar.service.JSPNRCancelService;
import com.abb.serviceprovider.jetStar.service.JSPNRCreateService;
import com.abb.serviceprovider.jetStar.service.JSPNRProcessPaymentService;
import com.abb.serviceprovider.jetStar.service.JSPNRRetrieveService;

/**
 * The Class JSServiceProvider.
 */
@Service("jsServiceProvider")
public class JSServiceProvider implements ServiceProvider {

	private static final Logger LOG = LoggerFactory.getLogger(JSServiceProvider.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getFareSearchService()
	 */
	@Override
	public SPService getFareSearchService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getFlightPriceService()
	 */
	@Override
	public SPService getFlightPriceService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getPNRCreateService()
	 */
	@Override
	public SPService getPNRCreateService() throws OpenAirServiceNotFoundForProviderException {
		SPService jsPNRCreateService = ApplicationContextUtil.getBean("jsPNRCreateService", JSPNRCreateService.class);
		LOG.debug("JSPNRCreateService Hashcode:{}", jsPNRCreateService.hashCode());
		return jsPNRCreateService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getPNRRetrieveService()
	 */
	@Override
	public SPService getPNRRetrieveService() throws OpenAirServiceNotFoundForProviderException {
		SPService jsPNRRetrieveService = ApplicationContextUtil.getBean("jsPNRRetrieveService", JSPNRRetrieveService.class);
		LOG.debug("JSPNRRetrieveService Hashcode:{}", jsPNRRetrieveService.hashCode());
		return jsPNRRetrieveService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getPNRCancelService()
	 */
	@Override
	public SPService getPNRCancelService() throws OpenAirServiceNotFoundForProviderException {
		SPService jsPNRCancelService = ApplicationContextUtil.getBean("jsPNRCancelService", JSPNRCancelService.class);
		LOG.debug("JSPNRCancelService Hashcode:{}", jsPNRCancelService.hashCode());
		return jsPNRCancelService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getFareRulesService()
	 */
	@Override
	public SPService getFareRulesService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getAddPaymentsInJetStar()
	 */
	@Override
	public SPService getAddPaymentsInJetStar() throws OpenAirServiceNotFoundForProviderException {
		SPService jsPNRProcessPaymentService = ApplicationContextUtil.getBean("jsPNRProcessPaymentService", JSPNRProcessPaymentService.class);
		LOG.debug("JSPNRProcessPaymentService Hashcode:{}", jsPNRProcessPaymentService.hashCode());
		return jsPNRProcessPaymentService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getServiceProviderName()
	 */
	@Override
	public String getServiceProviderName() {
		return "JSServiceProvider";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.ServiceProvider#getTicketIssueService()
	 */
	@Override
	public SPService getTicketIssueService() throws OpenAirServiceNotFoundForProviderException {
		return null;
	}

}
