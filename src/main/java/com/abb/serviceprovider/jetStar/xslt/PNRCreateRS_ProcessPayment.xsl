<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:strip-space elements="*" />
	<xsl:output method="xml" indent="yes" />
	<xsl:import href="JS_Constants.xsl" />
	<xsl:import href="../../common/xslt/FormatDate.xsl" />
	<xsl:import href="JS_ErrorHandling.xsl" />
	<xsl:variable name="TicketIssued" select="'N'" />
	<xsl:variable name="QueueRetrieved" select="'N'" />
	<xsl:variable name="CRSID" select="'F1'" />

	<xsl:variable name="recordLocator"
		select="//BookingCommitResponse/BookingUpdateResponseData/Success/RecordLocator" />

	<xsl:template match="/">
		<!-- PNRVewRS -->
		<xsl:element name="PNRViewRS">
			<xsl:call-template name="errorHandling" />
			<!-- PNRIdentification -->
			<xsl:element name="PNRIdentification">
				<xsl:attribute name="TicketIssued">
				<xsl:value-of select="$TicketIssued" />
			</xsl:attribute>
				<xsl:attribute name="QueueRetrieved">
				<xsl:value-of select="$QueueRetrieved" />
			</xsl:attribute>
				<xsl:attribute name="FareDataExists">
				<xsl:value-of select="'Y'" />
			</xsl:attribute>
				<xsl:element name="RecordLocator">
					<xsl:value-of select="$recordLocator" />
				</xsl:element>
				<xsl:element name="Sources">
					<xsl:element name="Source">
						<xsl:attribute name="name">
						<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
						<xsl:attribute name="title">
						<xsl:value-of select="$SOURCE_NAME" />
						</xsl:attribute>

						<xsl:element name="PNRIdentification">
							<xsl:attribute name="Source">
								<xsl:value-of select="$SOURCE" />
							</xsl:attribute>
							<xsl:element name="RecordLocator">
								<xsl:value-of select="$recordLocator" />
							</xsl:element>
							<xsl:element name="CreationAgent">
								<xsl:value-of
									select="//SARequest/CustomItineraryData/POS/AgentCode" />
							</xsl:element>

							<xsl:element name="CreationDate">
								<xsl:call-template name="dateformat">
									<xsl:with-param name="format" select="'date'" />
									<xsl:with-param name="datestr"
										select="//SARequest/CustomItineraryData/BookingInfo/BookingDate" />  <!-- Date -->
									<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
									<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
								</xsl:call-template>
							</xsl:element>
							<xsl:element name="CreationTime">
								<xsl:call-template name="dateformat">
									<xsl:with-param name="format" select="'time'" />
									<xsl:with-param name="datestr"
										select="//SARequest/CustomItineraryData/BookingInfo/BookingDate" />
									<xsl:with-param name="outformat" select="string('hh-mm')" />
									<xsl:with-param name="informat" select="string('datetime')" />
								</xsl:call-template>
							</xsl:element>
							<xsl:element name="ReceivedFrom">
								<xsl:value-of select="//PNRCreateRQ/CompletePNRElements/ReceivedFrom" />
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>

			</xsl:element>

			<!-- FareGroup -->
			<xsl:element name="FareGroup"></xsl:element>

			<!-- Traveler -->
			<xsl:copy-of select="//SARequest//PNRCreateRQ/CompletePNRElements/Traveler" />

			<!-- AirGroup -->
			<xsl:element name="AirGroup">

				<xsl:copy-of
					select="//SARequest/PNRCreateRQ/CompletePNRElements/Itinerary/Flight" />
			</xsl:element>

			<!-- Telephone -->
			<xsl:copy-of select="//SARequest/PNRCreateRQ/CompletePNRElements/Telephone" />

			<!-- EmailAddress -->
			<xsl:copy-of select="//SARequest/PNRCreateRQ/OtherPNRElements/EmailAddress" />

		</xsl:element>
	</xsl:template>
</xsl:stylesheet>