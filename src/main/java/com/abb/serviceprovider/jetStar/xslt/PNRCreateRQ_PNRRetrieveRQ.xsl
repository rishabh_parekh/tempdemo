<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common"
	xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">

	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/">
		<xsl:element name="a:GetBookingRequest">			
			<xsl:apply-templates select="//CommitResponse/BookingUpdateResponseData" />		
		</xsl:element>
	</xsl:template>

	<xsl:template match="BookingUpdateResponseData">
		<xsl:element name="a:GetBookingReqData">
			<xsl:element name="GetBookingBy">
				<xsl:value-of select="'RecordLocator'" />
			</xsl:element>
			<xsl:element name="GetByRecordLocator">
				<xsl:element name="RecordLocator">
					<xsl:value-of select="Success/RecordLocator" />
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>