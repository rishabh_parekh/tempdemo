<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">

	<xsl:import href="JS_Constants.xsl" />
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

<xsl:template match="/">
	<xsl:element name="a:AddPaymentToBookingRequest">
		<xsl:element name="addPaymentToBookingReqData">
			
			<xsl:element name="MessageState">
				<xsl:value-of select="$STATE"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="WaiveFee">
				<xsl:value-of select="$WAIVE_PENALTY_FEE"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="ReferenceType">
				<xsl:value-of select="$REFERENCETYPE"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="PaymentMethodType">
				<xsl:value-of select="$PAYMENT_METHOD_TYPE"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="PaymentMethodCode">
				<xsl:value-of select="//SARequest//ProcessPaymentRQ/FormOfPayment/CreditCard/CCCode"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="QuotedCurrencyCode">
				<xsl:value-of select="//SARequest//ProcessPaymentRQ/CurrencyCode"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="QuotedAmount">
				<xsl:value-of select="//SARequest//ProcessPaymentRQ/FormOfPayment/FOPTotal"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="Status">
				<xsl:value-of select="$STATUS"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="AccountNumberID">
				<xsl:value-of select="$ACCOUNT_NUM_ID"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="AccountNumber">
				<!-- <xsl:value-of select="$ACCOUNT_NUMBER"></xsl:value-of> -->
				<xsl:value-of select="//SARequest//ProcessPaymentRQ/FormOfPayment/CreditCard/CCNumber"></xsl:value-of>
			</xsl:element>
			
			<!-- <xsl:element name="Expiration">
				<xsl:value-of select="$EXPIRATION"></xsl:value-of>
			</xsl:element> -->
			
			<xsl:variable name="CCMonth" select="//SARequest//ProcessPaymentRQ/FormOfPayment/CreditCard/CCExpiration/Month" />
			<xsl:variable name="CCYear" select="//SARequest//ProcessPaymentRQ/FormOfPayment/CreditCard/CCExpiration/Year" />
			<xsl:element name="Expiration">
				<xsl:value-of select="concat('20',$CCYear,'-',$CCMonth, '-' ,'28','T00:00:00')" />
			</xsl:element>
			
			<xsl:element name="ParentPaymentID">
				<xsl:value-of select="$PARENT_PAYMENT_ID"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="Installments">
				<xsl:value-of select="$INSTALLMENTS"></xsl:value-of>
			</xsl:element>
			
			<xsl:element name="Deposit">
				<xsl:value-of select="$DEPOSIT"></xsl:value-of>
			</xsl:element>
			
			 <xsl:element name="PaymentFields">
				<xsl:element name="PaymentField">
					<xsl:element name="FieldName">
						<xsl:value-of select="$PAYMENT_FIELD_NAME"></xsl:value-of>
					</xsl:element>
					<xsl:element name="FieldValue">
						<xsl:value-of select="$PAYMENT_FIELD_VALUE"></xsl:value-of>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			
		</xsl:element>
	</xsl:element>
</xsl:template>

	
</xsl:stylesheet>