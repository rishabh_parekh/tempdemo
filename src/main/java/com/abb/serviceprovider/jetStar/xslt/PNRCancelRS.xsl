<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	
	<xsl:import href="../../common/xslt/FormatDate.xsl" />
	<xsl:import href="JS_Constants.xsl" />
	<xsl:import href="JS_ErrorHandling.xsl" />
	<xsl:output method="xml" indent="yes" />

	<xsl:variable name="SourceName" select="$CARRIER_CODE_FOR_JS" />
	
	<xsl:template match="/">	
		<xsl:element name="PNRCancelRS">
			<xsl:call-template name="errorHandling" />
			<xsl:apply-templates select="//BookingUpdateResponseData" />			
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="BookingUpdateResponseData">		
		<xsl:element name="Status">
			<xsl:attribute name="Source">
				<xsl:value-of select="$SourceName"/>
			</xsl:attribute>
			<xsl:choose>
				<!-- 0 = Successful PNR cancellation
				1 = error, PNR not cancelled -->
				<xsl:when test="Error = 'true'">
					<xsl:value-of select="'1'"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'0'"/>
				</xsl:otherwise>				
			</xsl:choose>
		</xsl:element>
	</xsl:template>	
</xsl:stylesheet>