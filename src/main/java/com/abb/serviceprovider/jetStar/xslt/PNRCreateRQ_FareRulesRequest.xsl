<xsl:stylesheet version="1.0"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices"
	xmlns:con="http://schemas.navitaire.com/WebServices/ServiceContracts/ContentService"
	xmlns:con1="http://schemas.navitaire.com/WebServices/DataContracts/Content"
	xmlns:book="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan">

	<xsl:import href="JS_Constants.xsl" />
	<xsl:import href="../../common/xslt/StringUtils.xsl" />

	<xsl:output method="xml" indent="yes" />

	<xsl:variable name="Cabin_Lookup"
		select="document('lookupXMLs/Cabin_Classes_Lookup.xml')//Cabin" />
	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />

	<xsl:template match="/">
		<xsl:element name="con:FareRuleRequest">
			<xsl:apply-templates select="SALogin" />
			<xsl:apply-templates select="//PNRCreateRQ" />
		</xsl:element>
	</xsl:template>

	<!-- Auth -->

	<!-- Fields -->
	<xsl:template match="PNRCreateRQ">

		<xsl:element name="con1:fareRuleReqData">
			<xsl:element name="book:FareBasisCode" />
			<xsl:element name="book:ClassOfService">
				<xsl:value-of select="//SARequest/PNRCreateRQ//Flight/ClassOfService" />
			</xsl:element>
			<xsl:element name="book:CarrierCode">
				<xsl:value-of select="//SARequest/PNRCreateRQ//Flight/Carrier/AirlineCode" />
			</xsl:element>
			<xsl:element name="book:RuleNumber" />
			<xsl:element name="book:RuleTariff" />
			<xsl:element name="book:CultureCode" />
		</xsl:element>

	</xsl:template>

</xsl:stylesheet>