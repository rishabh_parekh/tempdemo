<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:strip-space elements="*" />
	<xsl:import href="JS_Constants.xsl" />
	
	<xsl:template match="/">
		<!-- SellRequest -->
		<xsl:element name="BookingCommitRequest" xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
			<!-- SellRequestData -->
			<xsl:element name="BookingCommitRequestData" xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking">
				
				<xsl:element name="State">
					<xsl:value-of select="'New'" />
				</xsl:element>
				
				<xsl:element name="RecordLocator">
					<xsl:value-of select="//SARequest//PNRCreateRQ/RecordLocator"></xsl:value-of>
				</xsl:element>
				
				<xsl:element name="PaxCount">
					<xsl:value-of select="//SARequest/PNRCreateRQ//NumberInParty" />
				</xsl:element>
				
				<!-- GDS code that requested the booking -->
				<xsl:element name="SystemCode">
					<xsl:value-of select="//SARequest//PNRCreateRQ//Flight/@Source" />
				</xsl:element>
				
				<!-- unique identifier of a booking -->
				<xsl:element name="BookingID">
					<xsl:value-of select="'0'" />
				</xsl:element>
				
				<!-- unique identifier of the booking where the current booking resulted from. -->
				<xsl:element name="BookingParentID">
					<xsl:value-of select="'0'" />
				</xsl:element>
				
				<xsl:call-template name="RecordLocators"/>
				
			</xsl:element>

		</xsl:element>

	</xsl:template>
	
	<xsl:template name="RecordLocators">
		
		<xsl:element name="RecordLocators" xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Common">
			<xsl:element name="a:RecordLocator">
				<xsl:element name="a:State">
					<xsl:value-of select="'New'" />
				</xsl:element>
				
				<xsl:element name="a:SystemDomainCode">
					<xsl:value-of select="$JETAIRWAYS_SOURCE" />
				</xsl:element>
				
				<xsl:element name="a:SystemCode">
					<xsl:value-of select="$JETAIRWAYS_SOURCE" />
				</xsl:element>
				
				<!-- 9W PNR -->
				<xsl:element name="a:RecordCode">
					<xsl:value-of select="//SARequest//PNRCreateRQ//Flight/VendorLocator" />
				</xsl:element>
				
				<!-- NO MAPPING FOUND -->
				<xsl:element name="a:InteractionPurpose">
					<xsl:value-of select="'BS'" />
				</xsl:element>
				
				<xsl:element name="a:HostedCarrierCode">
					<xsl:value-of select="$JETAIRWAYS_SOURCE" />
				</xsl:element>
				
			</xsl:element>
		</xsl:element>
				
	</xsl:template>
</xsl:stylesheet>