<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />


<xsl:template match="/">
	<xsl:element name="a:BookingCommitRequest">
		<xsl:element name="BookingCommitRequestData">
			
			<xsl:element name="RecordLocator">
				<xsl:value-of select="//SARequest//ProcessPaymentRQ/RecordLocator"></xsl:value-of>
			</xsl:element>
			
		</xsl:element>
	</xsl:element>
</xsl:template>

	
</xsl:stylesheet>