<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/">
		<xsl:element name="FlightPriceRS">
			<xsl:apply-templates select="//PriceItineraryResponse" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="PriceItineraryResponse">
		<xsl:element name="InfoGroup">
			<xsl:element name="ForInfo">
				<xsl:attribute name="Source">
					<xsl:value-of select="'$SOURCE'"/>
				</xsl:attribute>
				<Text>Entered AirSearch search method.</Text>
				<Text>Service Fee is applied.</Text>
			</xsl:element>
		</xsl:element>

		<xsl:element name="FareGroup">
			<xsl:attribute name="FareType">PUBL</xsl:attribute>
			<xsl:attribute name="Source">GW</xsl:attribute>
			<xsl:attribute name="TravelerCount">
					<xsl:value-of select="Booking/PaxCount" />
			</xsl:attribute>
			<xsl:attribute name="TotalPrice">
				<xsl:value-of select="Booking//BookingSum/TotalCost"/>
			</xsl:attribute>

			<xsl:element name="CurrencyCode">
				<xsl:attribute name="NumberOfDecimals">
						<xsl:value-of select="'2'" />
					</xsl:attribute>
				<xsl:value-of
					select="Booking/Journeys/Journey/Segments/Segment/Fares/Fare/PaxFares/PaxFare/ServiceCharges/BookingServiceCharge/CurrencyCode" />
			</xsl:element>
			<xsl:apply-templates
				select="Booking/Journeys/Journey/Segments/Segment/Fares/Fare/PaxFares/PaxFare" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="PaxFare">
		<xsl:variable name="BaseFareTotal"
			select="sum(ServiceCharges/BookingServiceCharge[ChargeType='FarePrice']/Amount)" />
		<xsl:variable name="TaxTotal"
			select="sum(ServiceCharges/BookingServiceCharge[ChargeType!='FarePrice']/Amount)" />

		<xsl:variable name="PaxTypeValue" select="PaxType" />
		<xsl:if test="//FlightPriceRQ/TravelerInfo/@Type = $PaxTypeValue">
			<xsl:variable name="PaxCount" select="//FlightPriceRQ/TravelerInfo" />

			<xsl:if test="not(preceding-sibling::PaxType=$PaxTypeValue)">
				<xsl:element name="TravelerGroup">
					<xsl:attribute name="TypeRequested">
						<xsl:value-of select="PaxType" />
					</xsl:attribute>
					<xsl:attribute name="TypePriced">
						<xsl:value-of select="PaxType" />							
					</xsl:attribute>
					<xsl:attribute name="TypeCount">
						<xsl:value-of select="$PaxCount" />
					</xsl:attribute>
					<xsl:attribute name="TypeTotalPrice">
						<xsl:value-of select="($PaxCount)*($BaseFareTotal + $TaxTotal)" />
					</xsl:attribute>

					<xsl:element name="Price">
						<xsl:attribute name="Total">
							<xsl:value-of select="($BaseFareTotal + $TaxTotal)" />
						</xsl:attribute>
						
						<xsl:element name="BaseFare">
							<xsl:attribute name="Amount">
							<xsl:value-of select="$BaseFareTotal" />				
						</xsl:attribute>
						</xsl:element>

						<xsl:element name="Taxes">
							<xsl:attribute name="Amount">				
							<xsl:value-of select="$TaxTotal" />				
						</xsl:attribute>
							<xsl:apply-templates select="ServiceCharges/BookingServiceCharge" />
						</xsl:element>
					</xsl:element>
					
					<xsl:element name="FareRules">
						<xsl:apply-templates select="//Journeys/Journey" />
					</xsl:element>
				</xsl:element>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="BookingServiceCharge">
		<xsl:if test="not(ChargeType='FarePrice')">
			<xsl:element name="Tax">
				<xsl:value-of select="Amount" />
			</xsl:element>
			<xsl:element name="Description">
				<xsl:value-of select="ChargeType" />
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template match="Journey">
		<xsl:element name="FareInfo">
			<xsl:element name="DepartureCode">
				<xsl:value-of
					select="//FlightPriceRQ/OriginDestination/Flight[position()]/Departure/AirportCode" />
			</xsl:element>
			<xsl:element name="ArrivalCode">
				<xsl:value-of
					select="//FlightPriceRQ/OriginDestination/Flight[position()]/Arrival/AirportCode" />
			</xsl:element>
			<xsl:element name="DepartureDate">
				<xsl:value-of
					select="//FlightPriceRQ/OriginDestination/Flight[position()]/Departure/Date" />
			</xsl:element>
			<xsl:element name="ArrivalDate">
				<xsl:value-of
					select="//FlightPriceRQ/OriginDestination/Flight[position()]/Arrival/Date" />
			</xsl:element>
			<xsl:element name="ArrivalTime">
				<xsl:value-of
					select="//FlightPriceRQ/OriginDestination/Flight[position()]/Arrival/Date" />
			</xsl:element>
			<xsl:element name="DepartureTime">
				<xsl:value-of
					select="//FlightPriceRQ/OriginDestination/Flight[position()]/Departure/Date" />
			</xsl:element>
			<xsl:element name="AirlineCode">
				<xsl:value-of select="//Fares/Fare/CarrierCode" />
			</xsl:element>
			<xsl:apply-templates select="Segments/Segment" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="Segment">
		<xsl:element name="RelatedSegment">
			<xsl:element name="SegmentIDRef">
				<xsl:attribute name="FlightRefKey">
					<xsl:value-of select="concat('F', position())" />
					</xsl:attribute>
			</xsl:element>
			<xsl:element name="DepartureCode">
				<xsl:value-of select="DepartureStation" />
			</xsl:element>
			<xsl:element name="ArrivalCode">
				<xsl:value-of select="ArrivalStation" />
			</xsl:element>
			<xsl:element name="DepartureDate">
				<xsl:value-of select="DepartureStation" />
			</xsl:element>
			<xsl:element name="ArrivalDate">
				<xsl:value-of select="STA" />
			</xsl:element>
			<xsl:element name="ArrivalTime">
				<xsl:value-of select="STA" />
			</xsl:element>
			<xsl:element name="DepartureTime">
				<xsl:value-of select="STD" />
			</xsl:element>
			<xsl:element name="ClassOfService">
				<xsl:value-of select="//Fares/Fare/ClassOfService" />
			</xsl:element>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>