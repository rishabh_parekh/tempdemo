<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" 
	xmlns:web="http://schemas.navitaire.com/WebServices">
	
	<xsl:output method="xml" indent="yes" />
	<xsl:param name="SessionTocken"/>
	<xsl:template match="/">
		<xsl:element name="OpenAirSOAPHeader">
			<xsl:element name="web:Signature"><xsl:value-of select="$SessionTocken"/></xsl:element>
			<xsl:element name="web:ContractVersion"><xsl:value-of select="'0'"/></xsl:element>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>