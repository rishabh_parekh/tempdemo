<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	>
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />


<xsl:template match="/">
	<xsl:element name="a:GetBookingRequest">
		<xsl:element name="a:GetBookingReqData">
			
			<xsl:element name="GetBookingBy">
				<xsl:value-of select="'RecordLocator'"/>
			</xsl:element>
			
			<xsl:element name="GetByRecordLocator">
				<xsl:element name="RecordLocator">
					 <xsl:value-of select="//SARequest//PNRCreateRQ/RecordLocator"></xsl:value-of>
				</xsl:element>        
			</xsl:element>
			
		</xsl:element>
	</xsl:element>
</xsl:template>

	
</xsl:stylesheet>