<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common"
	xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />
	<xsl:variable name="Gender_Lookup"
		select="document('lookupXMLs/Gender_Lookup.xml')//GenderType" />
	<xsl:strip-space elements="*" />

	<xsl:template match="/">
		<!-- SellRequest -->
		<xsl:element name="a:SellRequest">
			<!-- SellRequestData -->
			<xsl:element name="a:SellRequestData">
				<xsl:element name="SellBy">
					<xsl:value-of select="'Journey'" />
				</xsl:element>
				<!-- SellJourney -->
				<xsl:element name="SellJourneyRequest">
					<xsl:element name="SellJourneyRequestData">

						<!-- Journeys -->
						<xsl:element name="Journeys">
							<xsl:apply-templates
								select="//SARequest//PNRCreateRQ//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]"
								mode="uniqueFlightTag" />

						</xsl:element>

						<xsl:element name="PaxCount">
							<xsl:value-of select="count(//SARequest/PNRCreateRQ//Traveler)" />
						</xsl:element>
						<xsl:element name="CurrencyCode">
							<xsl:value-of select="//SARequest/CustomItineraryData/Flight/Fare/PaxFares/PaxFare//BookingServiceCharge/CurrencyCode" />
						</xsl:element>
						<!-- Passengers -->
						<xsl:element name="Passengers">
							<xsl:apply-templates select="//CompletePNRElements/Traveler" />
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>

		</xsl:element>

	</xsl:template>
	<!-- SellJourney -->
	<xsl:template match="Flight" mode="uniqueFlightTag">
		<xsl:variable name="originDestId" select="current()/@OriginDestinationID" />
		<xsl:element name="SellJourney">

			<xsl:element name="com:State">
				<xsl:value-of select="'New'" />
			</xsl:element>
			<xsl:element name="Segments">

				<xsl:for-each
					select="//SARequest/PNRCreateRQ//Flight[@OriginDestinationID=$originDestId]">

					<xsl:variable name="departDate" select="current()/Departure/Date" />
					<xsl:variable name="flightNumber" select="current()/Carrier/FlightNumber" />
					<xsl:variable name="classOfService" select="current()/ClassOfService" />
					<xsl:variable name="filteredJourney"
						select="//SARequest/CustomItineraryData/Flight[translate(FlightDesignator/FlightNumber, ' ', '')=$flightNumber]" />

					<xsl:element name="SellSegment">

						<xsl:element name="com:State">
							<xsl:value-of select="'New'" />
						</xsl:element>
						<xsl:element name="ActionStatusCode">
							<xsl:value-of select="'NN'" />
						</xsl:element>
						<xsl:element name="ArrivalStation">
							<xsl:value-of select="current()/Arrival/AirportCode" />
						</xsl:element>
						<xsl:element name="DepartureStation">
							<xsl:value-of select="current()/Departure/AirportCode" />
						</xsl:element>
						<xsl:element name="STA">
								<xsl:value-of select="concat(current()/Arrival/Date,'T',current()/Arrival/Time , ':00')" />
						</xsl:element>
						<xsl:element name="STD">
								<xsl:value-of select="concat(current()/Departure/Date,'T',current()/Departure/Time , ':00')" />
						</xsl:element>
						<xsl:element name="International">
							<xsl:value-of select="'true'" />
						</xsl:element>
						<xsl:element name="FlightDesignator">
							<xsl:element name="com:CarrierCode">
								<xsl:value-of select="$filteredJourney/FlightDesignator/CarrierCode" />
							</xsl:element>
							<xsl:element name="com:FlightNumber">
								<xsl:value-of select="$filteredJourney/FlightDesignator/FlightNumber" />
							</xsl:element>
						</xsl:element>
						<xsl:apply-templates
							select="$filteredJourney/Fare[ClassOfService=$classOfService]" />

					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<!-- Passengers -->
	<xsl:template match="Traveler">

		<xsl:element name="Passenger">
			<xsl:element name="PassengerNumber">
				<xsl:value-of select="position()-1" />
			</xsl:element>

			<xsl:element name="Names">
				<xsl:element name="BookingName">
					<xsl:element name="State">
						<xsl:value-of select="'New'" />
					</xsl:element>
					<xsl:element name="FirstName">
						<xsl:value-of select="TravelerName/GivenName" />
					</xsl:element>
					<xsl:element name="LastName">
						<xsl:value-of select="TravelerName/Surname" />
					</xsl:element>
					<xsl:if test="TravelerName/Title">
						<xsl:element name="Title">
							<xsl:value-of select="TravelerName/Title" />
						</xsl:element>
					</xsl:if>
					<xsl:if test="TravelerName/Gender">
						<xsl:element name="Gender">
							<xsl:variable name="currentGender" select="TravelerName/Gender" />
							<xsl:value-of select="$Gender_Lookup[FLXCode=$currentGender]/JSName" />
						</xsl:element>
					</xsl:if>
				</xsl:element>
			</xsl:element>

			<!-- For Infant create passenger start -->
			<xsl:if test="current()/Infant/*">
				<xsl:element name="Infant">

					<xsl:element name="State">
						<xsl:value-of select="'New'" />
					</xsl:element>
					<xsl:element name="DOB">
						<xsl:value-of select="Infant/DateOfBirth" />
					</xsl:element>
					<xsl:element name="Gender">
						<!-- <xsl:value-of select="Infant/Gender" /> -->
						<xsl:variable name="currentInfantGender" select="Infant/Gender" />
						<xsl:value-of select="$Gender_Lookup[FLXCode=$currentInfantGender]/JSName" />
					</xsl:element>
					<xsl:element name="Names">
						<xsl:element name="BookingName">
							<xsl:element name="State">
								<xsl:value-of select="'New'" />
							</xsl:element>
							<xsl:element name="FirstName">
								<xsl:value-of select="Infant/GivenName" />
							</xsl:element>
							<xsl:element name="LastName">
								<xsl:value-of select="Infant/Surname" />
							</xsl:element>

							<xsl:element name="Title">
								<xsl:choose>
									<xsl:when test="Infant/Title">
										<xsl:value-of select="Infant/Title" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'NA'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>

						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<!-- For Infant create passenger end -->
			<xsl:element name="PassengerTypeInfos">
				<xsl:element name="PassengerTypeInfo">
					<xsl:element name="State">
						<xsl:value-of select="'New'" />
					</xsl:element>
					<xsl:element name="DOB">
						<xsl:value-of select="TravelerName/DateOfBirth" />
					</xsl:element>
					<xsl:element name="PaxType">
						<xsl:value-of select="$PaxType_Lookup[FLXCode=current()/@Type]/JSCode" />
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:element name="PseudoPassenger">
				<xsl:value-of select="'false'" />
			</xsl:element>

			<!-- For Infant create passenger -->
			<xsl:if test="current()/Infant/*">
				<xsl:element name="PassengerInfants">
					<xsl:element name="PassengerInfant">
						<xsl:element name="State">
							<xsl:value-of select="'New'" />
						</xsl:element>
						<xsl:element name="DOB">
							<xsl:value-of select="Infant/DateOfBirth" />
						</xsl:element>
						<xsl:element name="Gender">
							<!-- <xsl:value-of select="Infant/Gender" /> -->
							<xsl:variable name="currentInfantGender" select="Infant/Gender" />
							<xsl:value-of select="$Gender_Lookup[FLXCode=$currentInfantGender]/JSName" />
						</xsl:element>
					</xsl:element>

				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<xsl:template match="ActionStatusCode">
		<xsl:element name="ActionStatusCode">
			<xsl:value-of select="'NN'" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="State">
		<xsl:element name="com:State">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="Legs">
		<xsl:element name="PriceLegs">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="Leg">
		<xsl:element name="PriceLeg">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="CarrierCode[parent::FlightDesignator]">
		<xsl:element name="com:CarrierCode">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="FlightNumber">
		<xsl:element name="com:FlightNumber">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" />
		</xsl:copy>
	</xsl:template>

	<!-- This empty template skips all elements matched -->
	<xsl:template
		match="Fares|Legs|PaxBags|PaxSeats|PaxSSRs|PaxSegments|PaxTickets|PaxSeatPreferences|SalesDate|SegmentSellKey|PaxScores|ChannelType|PaxFares|LegInfo|
			CabinOfService|ChangeReasonCode|PriorityCode|SegmentType|OpSuffix|XrefFlightDesignator" />

</xsl:stylesheet>