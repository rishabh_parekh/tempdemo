<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:strip-space elements="*" />

	<xsl:template match="/">
		<!-- SellRequest -->
		<xsl:element name="a:SellRequest">
			<!-- SellRequestData -->
			<xsl:element name="a:SellRequestData">
				<xsl:element name="SellBy">
					<xsl:value-of select="'Journey'" />
				</xsl:element>
				<!-- SellJourney -->
				<xsl:element name="SellJourneyRequest">
					<xsl:element name="SellJourneyRequestData">

						<!-- Journeys -->
						<xsl:element name="Journeys">
							<xsl:apply-templates
								select="//SARequest//PNRCreateRQ//Flight[not(@OriginDestinationID=preceding-sibling::Flight/@OriginDestinationID)]"
								mode="uniqueFlightTag" />
						</xsl:element>

						<xsl:element name="PaxCount">
							<xsl:value-of select="//SARequest/PNRCreateRQ//NumberInParty" />
						</xsl:element>
						<xsl:element name="CurrencyCode">
							<xsl:value-of select="//SARequest/CustomItineraryData/Flight/Fare/PaxFares/PaxFare//BookingServiceCharge/CurrencyCode" />
						</xsl:element>
						
					</xsl:element>
				</xsl:element>
				<!-- Blank tags for now. As baggage info is not to be shared. -->
				<xsl:element name="SellSSR"/>
				<xsl:element name="SellFee"/>
			</xsl:element>

		</xsl:element>

	</xsl:template>
	<!-- SellJourney -->
	<xsl:template match="Flight" mode="uniqueFlightTag">
		<xsl:variable name="originDestId" select="current()/@OriginDestinationID" />
		<xsl:element name="SellJourney">

			<xsl:element name="com:State">
				<xsl:value-of select="'New'" />
			</xsl:element>
			<xsl:element name="NotForGeneralUse">
				<xsl:value-of select="'false'" />
			</xsl:element>
			<xsl:element name="Segments">

				<xsl:for-each
					select="//SARequest/PNRCreateRQ//Flight[@OriginDestinationID=$originDestId]">

					<xsl:variable name="departDate" select="current()/Departure/Date" />
					<xsl:variable name="flightNumber" select="current()/Carrier/FlightNumber" />
					<xsl:variable name="classOfService" select="current()/ClassOfService" />
					<xsl:variable name="filteredJourney"
						select="//SARequest/CustomItineraryData/Flight[translate(FlightDesignator/FlightNumber, ' ', '')=$flightNumber]" />

					<xsl:element name="SellSegment">

						<xsl:element name="com:State">
							<xsl:value-of select="'New'" />
						</xsl:element>
						<xsl:element name="ActionStatusCode">
							<xsl:value-of select="'NN'" />
						</xsl:element>
						<xsl:element name="ArrivalStation">
							<xsl:value-of select="current()/Arrival/AirportCode" />
						</xsl:element>
						<xsl:element name="DepartureStation">
							<xsl:value-of select="current()/Departure/AirportCode" />
						</xsl:element>
						<xsl:element name="SegmentType">
							<!-- for passive segment segmentType should be kept 'P' as per peiru's email  -->
							<xsl:value-of select="'P'"></xsl:value-of>
						</xsl:element>
						<xsl:element name="STA">
							<xsl:value-of select="concat(current()/Arrival/Date,'T',current()/Arrival/Time , ':00')" />
						</xsl:element>
						<xsl:element name="STD">
							<xsl:value-of select="concat(current()/Departure/Date,'T',current()/Departure/Time , ':00')" />
						</xsl:element>
						<xsl:element name="FlightDesignator">
							<xsl:element name="com:CarrierCode">
								<xsl:value-of select="current()/Carrier/AirlineCode" />
							</xsl:element>
							<xsl:element name="com:FlightNumber">
								<xsl:value-of select="current()/Carrier/FlightNumber" />
							</xsl:element>
						</xsl:element>
						<xsl:element name="Fare">
							<xsl:element name="com:State">
								<xsl:value-of select="'New'" />
							</xsl:element>
							<xsl:element name="ClassOfService">
								<xsl:value-of select="current()/ClassOfService"></xsl:value-of>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>