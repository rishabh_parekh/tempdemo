<xsl:stylesheet version="1.0"
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices"
	xmlns:con="http://schemas.navitaire.com/WebServices/ServiceContracts/ContentService"
	xmlns:con1="http://schemas.navitaire.com/WebServices/DataContracts/Content"
	xmlns:book="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:exslt="http://exslt.org/common" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="http://xml.apache.org/xalan">

	<xsl:import href="GW_Constants.xsl" />
	<xsl:import href="../../common/xslt/StringUtils.xsl" />

	<xsl:output method="xml" indent="yes" />

	<xsl:template match="LuteService">
		
		<xsl:choose>
			<xsl:when test="//FareRulesRQ">
				<xsl:call-template name="FareRules_FareInfo" />
			</xsl:when>
			<xsl:otherwise>
			<xsl:element name="FareRulesRequestsData">
			<xsl:call-template name="Flight_FareInfo" />				
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="Flight_FareInfo">
		<xsl:choose>
			<xsl:when test="//PriceItineraryResponse">
				<xsl:apply-templates select="//PriceItineraryResponse" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="//GetBookingResponse" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="//GetBookingResponse">
		<xsl:for-each select="//GetBookingResponse//Booking//Fare">
					<xsl:call-template name="FareInfo" />
				</xsl:for-each>
	</xsl:template>
	<xsl:template match="//PriceItineraryResponse">
	
		<xsl:for-each select="//PriceItineraryResponse//Booking//Fare"> 
			<xsl:call-template name="FareInfo_PriceItineraryResponse" />
		</xsl:for-each>
	</xsl:template>

	<xsl:template name="FareInfo">	
		<xsl:variable name="Current_FareBasisCode" select="FareBasisCode" />
		
		<xsl:if
			test="not($Current_FareBasisCode=preceding::*//FareBasisCode)">
			
			<xsl:element name="con:FareRuleRequest">
				<xsl:element name="con1:fareRuleReqData">
					<xsl:element name="book:FareBasisCode">
						<xsl:value-of select="FareBasisCode" />
					</xsl:element>
					<xsl:element name="book:ClassOfService">
						<xsl:value-of select="ClassOfService" />
					</xsl:element>
					<xsl:element name="book:CarrierCode">
						<xsl:value-of select="CarrierCode" />
					</xsl:element>
					<xsl:element name="book:RuleNumber">
						<xsl:value-of select="RuleNumber" />
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="FareInfo_PriceItineraryResponse">	
		<xsl:variable name="Current_FareBasisCode" select="FareBasisCode" />
		
		<xsl:if
			test="not($Current_FareBasisCode=preceding::*//PriceItineraryResponse//FareBasisCode)">
			
			<xsl:element name="con:FareRuleRequest">
				<xsl:element name="con1:fareRuleReqData">
					<xsl:element name="book:FareBasisCode">
						<xsl:value-of select="FareBasisCode" />
					</xsl:element>
					<xsl:element name="book:ClassOfService">
						<xsl:value-of select="ClassOfService" />
					</xsl:element>
					<xsl:element name="book:CarrierCode">
						<xsl:value-of select="CarrierCode" />
					</xsl:element>
					<xsl:element name="book:RuleNumber">
						<xsl:value-of select="RuleNumber" />
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<!-- FareRules_FareInfo -->
	<xsl:template name="FareRules_FareInfo">
		<xsl:element name="con1:FareRuleRequest">
			<xsl:element name="con1:fareRuleReqData">
				<xsl:element name="book:FareBasisCode">
					<xsl:value-of select="//FareRulesRQ/FareBasisCode" />
				</xsl:element>
				<xsl:element name="book:CarrierCode">
					<xsl:value-of select="'4U'" />
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>