<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:ns2="http://schemas.microsoft.com/2003/10/Serialization/Arrays"
	xmlns:ns3="http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations"
	xmlns:ns4="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:ns5="http://schemas.navitaire.com/WebServices/DataContracts/Common"
	xmlns:ns6="http://schemas.datacontract.org/2004/07/System.Collections.Generic"
	xmlns:ns7="http://schemas.navitaire.com/WebServices" xmlns:ns8="http://schemas.microsoft.com/2003/10/Serialization/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xalan="http://xml.apache.org/xalan">

	<xsl:import href="JS_Constants.xsl" />
	<xsl:strip-space elements="*" />
	
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:variable name="Cabin_Lookup"
		select="document('lookupXMLs/Cabin_Classes_Lookup.xml')//Cabin" />
	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />

	<xsl:template match="/">
		<xsl:element name="ns4:GetAvailabilityRequest">
			<xsl:apply-templates select="//FlightPriceRQ" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="FlightPriceRQ">
		<xsl:element name="TripAvailabilityRequest">
			<xsl:element name="AvailabilityRequests">
				<xsl:apply-templates select="OriginDestination" />
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="Flight">
		<xsl:element name="AvailabilityRequest">
			<xsl:element name="DepartureStation">
				<xsl:value-of select="Departure/AirportCode" />
			</xsl:element>
			<xsl:element name="ArrivalStation">
				<xsl:value-of select="Arrival/AirportCode" />
			</xsl:element>
			<xsl:element name="BeginDate">
				<xsl:variable name="time" select="concat(format-number(substring-before(Departure/Time,':'),'00'),':', format-number(substring-after(Departure/Time,':'),'00'))"/>
					<xsl:value-of
						select="concat(Departure/Date,'T','00:00:00')" />
			</xsl:element>
			<!-- Check this -->
			<xsl:element name="EndDate">
					<xsl:variable name="time" select="concat(format-number(substring-before(Arrival/Time,':'),'00'),':', format-number(substring-after(Arrival/Time,':'),'00'))"/>
					<xsl:value-of
						select="concat(Departure/Date,'T','00:00:00')" />
			</xsl:element>
			<xsl:element name="FlightType">
				<xsl:value-of select="'All'" />
			</xsl:element>
			<xsl:element name="PaxCount">
				<xsl:value-of select="count(//TravelerIDs)" />
			</xsl:element>
			<xsl:element name="Dow">
				<xsl:value-of select="$DOW_DAILY" />
			</xsl:element>
			<xsl:element name="CurrencyCode">
				<xsl:value-of select="$CURRENCY_CODE" />
			</xsl:element>
			<xsl:element name="AvailabilityType">
				<xsl:value-of select="$AVAILABILITY_TYPE" />
			</xsl:element>
			<xsl:element name="MaximumConnectingFlights">
				<xsl:value-of select="$MAXIMUM_CONNECTINT_FLIGHTS" />
			</xsl:element>
			<xsl:element name="AvailabilityFilter">
				<xsl:value-of select="$AVAILAIBLIY_FILTER" />
			</xsl:element>
			<xsl:element name="FareClassControl">
				<xsl:value-of select="$FARE_CLASS_CONTROL" />
			</xsl:element>
			<xsl:for-each select="//SARequest//Traveler">
				<xsl:variable name="paxTypeCode"
					select="$PaxType_Lookup[FLXCode=current()/@Type]/JSCode" />
				<xsl:variable name="FLXpaxTypeCode"
					select="$PaxType_Lookup[JSCode=$paxTypeCode]/FLXCode" />
				<xsl:variable name="paxCount"
					select="count(//Traveler[@Type=$FLXpaxTypeCode])" />
				<xsl:if test="not(preceding-sibling::Traveler[@Type=$FLXpaxTypeCode])">
					<xsl:element name="ArrayOfPaxPriceType">
						<xsl:element name="PaxPriceType">
							<xsl:element name="PaxType">
								<xsl:value-of select="$paxTypeCode" />
							</xsl:element>
							<xsl:element name="PaxDiscountCode" />
						</xsl:element>
					</xsl:element>
				</xsl:if>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>