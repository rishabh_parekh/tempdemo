<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:import href="JS_Constants.xsl" />
	<xsl:output method="xml" />
	<xsl:template name="errorHandling">
		<xsl:if test="//Fault or //error">
			<xsl:element name="InfoGroup">
				<!-- Error sample response -->
				<!-- Gateway gives error in below format,when we have multiple errors in request it gives only one error fault in response skipping others <s:Fault> 
					<faultcode xmlns:a="http://schemas.microsoft.com/net/2005/12/windowscommunicationfoundation/dispatcher">a:InternalServiceFault</faultcode> <faultstring 
					xml:lang="en-US">Station does not exist. StationCode = TXW</faultstring> -->

				<xsl:if test="//Fault">
					<xsl:element name="Error">
						<xsl:attribute name="Source">
							<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
						<xsl:attribute name="ErrorType">
    						<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
						<xsl:element name="Code">
							<xsl:value-of select="//Fault/faultcode" />
						</xsl:element>
						<xsl:element name="Text">
							<xsl:value-of select="//Fault/faultstring" />
						</xsl:element>
					</xsl:element>
				</xsl:if>
				<!-- Error sample response -->
				<!-- <error> <error_msg>System reported error. No signature returned. Please check username / password.</error_msg> </error> -->
				<xsl:if test="//error">
					<xsl:element name="Error">
						<xsl:attribute name="Source">
							<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
						
						<xsl:attribute name="ErrorType">
    						<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
					
						<xsl:if test="//error/error_code">
							<xsl:element name="Code">
								<xsl:value-of select="//error/error_code" />
							</xsl:element>
						</xsl:if>
					
						<xsl:for-each select="//error/error_msg" >
							<xsl:element name="Text">
								<xsl:value-of select="." />
							</xsl:element>
						</xsl:for-each>
					</xsl:element>
				</xsl:if>
			</xsl:element>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>