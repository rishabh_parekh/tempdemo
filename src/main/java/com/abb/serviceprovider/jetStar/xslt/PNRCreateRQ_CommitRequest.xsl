<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common"
	xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />
	<xsl:strip-space elements="*" />

	<xsl:template match="/">
		<!-- CommitRequest -->
		<xsl:element name="a:CommitRequest">
			<!-- "BookingRequest" -->
			<xsl:element name="BookingRequest">
				<xsl:element name="Booking">
					<xsl:element name="CurrencyCode">
						<xsl:value-of select="'USD'" />
					</xsl:element>
					<xsl:element name="PaxCount">
						<xsl:value-of select="count(//SARequest/PNRCreateRQ//Traveler)" />
					</xsl:element>

					<xsl:element name="BookingInfo">
						<xsl:element name="BookingType">
							<!-- TODO -->
						</xsl:element>
						<xsl:element name="CreatedDate">
							<xsl:value-of select="'9999-12-31T00:00:00'" />
						</xsl:element>
						<xsl:element name="ExpiredDate">
							<xsl:value-of select="'2016-05-31T00:00:00'" />
						</xsl:element>
						<xsl:element name="ModifiedDate">
							<xsl:value-of select="'9999-12-31T00:00:00'" />
						</xsl:element>
						<xsl:element name="BookingDate">
							<xsl:value-of select="'9999-12-31T00:00:00'" />
						</xsl:element>
					</xsl:element>

					<xsl:element name="ReceivedBy">
						<xsl:element name="ReceivedBy">
							<xsl:value-of select="'pol'" />
						</xsl:element>
						<xsl:element name="ReceivedReference">
							<!-- TODO -->
						</xsl:element>
						<xsl:element name="ReferralCode">
							<!-- TODO -->
						</xsl:element>
					</xsl:element>

					<xsl:element name="BookingContacts">
						<xsl:element name="BookingContact">
							<xsl:element name="TypeCode">
								<xsl:value-of select="'P'" />
							</xsl:element>

							<xsl:element name="Names">
								<xsl:element name="BookingName">

									<xsl:element name="FirstName">
										<xsl:value-of
											select="//SARequest/PNRCreateRQ/CompletePNRElements/Traveler[1]/TravelerName/GivenName" />
									</xsl:element>
									<xsl:element name="LastName">
										<xsl:value-of
											select="//SARequest/PNRCreateRQ/CompletePNRElements/Traveler[1]/TravelerName//Surname" />
									</xsl:element>
									<xsl:element name="Title">
									<xsl:choose>
										<xsl:when
											test="//SARequest/PNRCreateRQ/CompletePNRElements/Traveler[1]/TravelerName//Title">
											<xsl:value-of
												select="//SARequest/PNRCreateRQ/CompletePNRElements/Traveler[1]/TravelerName//Title" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:value-of select="'MR'" />
										</xsl:otherwise>
									</xsl:choose>
									</xsl:element>
								</xsl:element>
							</xsl:element>
							<xsl:element name="EmailAddress">
								<xsl:value-of select= "//SARequest/PNRCreateRQ/OtherPNRElements/EmailAddress/Email" />
							</xsl:element>
							<xsl:element name="HomePhone">
								<xsl:value-of select="concat('+', //SARequest/PNRCreateRQ/CompletePNRElements/Telephone/TelephoneNumber[Type='H'])" />
							</xsl:element>
							<!-- Mobile Number -->
							<xsl:element name="OtherPhone">
								<xsl:value-of select="concat('+', //SARequest/PNRCreateRQ/CompletePNRElements/Telephone/TelephoneNumber[Type='M'])" />
							</xsl:element>
							<xsl:variable name="travelerIdRef"
								select="//SARequest/PNRCreateRQ/OtherPNRElements/APIS//TravelerIDRef" />
							<xsl:variable name="firstTravelerIdRef"
								select="//SARequest/PNRCreateRQ/CompletePNRElements/Traveler[1]/@AssociationID" />

							<xsl:variable name="addressGroup"
								select="//SARequest/PNRCreateRQ/OtherPNRElements/BillingAndDeliveryData/AddressGroup[TravelerIDRef = $firstTravelerIdRef]" />
							<xsl:element name="AddressLine1">
								<xsl:choose>
									<xsl:when test="$addressGroup">
										<xsl:value-of select="$addressGroup/Address" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'NA'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>

							<xsl:element name="City">
								<xsl:choose>
									<xsl:when test="$addressGroup">
										<xsl:value-of select="$addressGroup/City" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'NA'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>

							<xsl:element name="ProvinceState">
								<xsl:choose>
									<xsl:when test="$addressGroup">
										<xsl:value-of select="$addressGroup/StateCode" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'NA'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>
							<xsl:element name="PostalCode">
								<xsl:choose>
									<xsl:when test="$addressGroup">
										<xsl:value-of select="$addressGroup/PostalCode" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'123456'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>

							<xsl:element name="CountryCode">
								<xsl:choose>
									<xsl:when test="$addressGroup">
										<xsl:value-of select="$addressGroup/CountryCode" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'NA'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:element>

							<xsl:element name="DistributionOption">
								<xsl:value-of select="'Email'" />
							</xsl:element>

						</xsl:element>
					</xsl:element>

				</xsl:element>

				<!-- <xsl:element name="DistributeToContacts">
					<xsl:value-of select="'true'" />
				</xsl:element> -->
			</xsl:element>

		</xsl:element>
	</xsl:template>

	<xsl:template match="ActionStatusCode">
		<xsl:element name="ActionStatusCode">
			<xsl:value-of select="'NN'" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="State">
		<xsl:element name="com:State">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="Legs">
		<xsl:element name="PriceLegs">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="Leg">
		<xsl:element name="PriceLeg">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="CarrierCode[parent::FlightDesignator]">
		<xsl:element name="com:CarrierCode">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="FlightNumber">
		<xsl:element name="com:FlightNumber">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" />
		</xsl:copy>
	</xsl:template>

	<!-- This empty template skips all elements matched -->
	<xsl:template
		match="Fares|Legs|PaxBags|PaxSeats|PaxSSRs|PaxSegments|PaxTickets|PaxSeatPreferences|SalesDate|SegmentSellKey|PaxScores|ChannelType|PaxFares|LegInfo|
			CabinOfService|ChangeReasonCode|PriorityCode|SegmentType|OpSuffix|XrefFlightDesignator|ClassType|RuleTariff" />

</xsl:stylesheet>