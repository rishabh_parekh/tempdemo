<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common"
	xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">

	<xsl:import href="JS_Constants.xsl" />
	
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />

	<xsl:variable name="PaxType_Lookup"
		select="document('lookupXMLs/PaxType_Lookup.xml')//PaxType" />
	<xsl:strip-space elements="*" />

	<xsl:template match="/">
		<!-- PriceItineraryRequest -->
		<xsl:element name="a:PriceItineraryRequest">
			<!-- ItineraryPriceRequest -->
			<xsl:element name="a:ItineraryPriceRequest">
				<!-- PriceItineraryBy -->
				<xsl:element name="PriceItineraryBy">

					<xsl:value-of select="'JourneyWithLegs'" />

				</xsl:element>
				<!-- TypeOfSale -->
				<xsl:element name="TypeOfSale">
					<xsl:element name="com:State">
						<xsl:value-of select="'New'" />
					</xsl:element>
					<xsl:element name="PaxResidentCountry"/>
					<xsl:element name="PromotionCode"/>					
					<xsl:element name="FareTypes">
						<xsl:element name="arr:string">
							<xsl:value-of select="'RE'" />
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="SellByKeyRequest"/>

				<!-- PriceJourneyWithLegsRequest -->
				<xsl:element name="PriceJourneyWithLegsRequest">

					<!-- PriceJourneys -->
					<xsl:element name="PriceJourneys">
						<xsl:apply-templates
							select="//SARequest//FlightPriceRQ/OriginDestination"/>
					</xsl:element>

					<xsl:element name="PaxCount">
						<xsl:value-of select="count(//SARequest/FlightPriceRQ//TravelerIDs)" />
					</xsl:element>
					<xsl:element name="CurrencyCode">
						<xsl:value-of select="$CURRENCY_CODE" />
					</xsl:element>
					
					<!-- <xsl:element name="SourcePOS">
						<xsl:element name="com:State">
							<xsl:value-of select="$STATE" />
						</xsl:element>
						<xsl:element name="com:AgentCode">
							<xsl:value-of select="$AGENT_CODE"/>
						</xsl:element>
						<xsl:element name="com:OrganizationCode">
							<xsl:value-of select="$ORGANIZATION_CODE"/>
						</xsl:element>
						<xsl:element name="com:DomainCode">
							<xsl:value-of select="$DOMAIN_CODE"/>
						</xsl:element>
						<xsl:element name="com:LocationCode">
							<xsl:value-of select="$LOCATION_CODE"/>
						</xsl:element>
					</xsl:element> -->
						
					<!-- Passengers -->
					<xsl:element name="Passengers">
						<xsl:apply-templates select="//SARequest/FlightPriceRQ//TravelerIDs" />
					</xsl:element>

				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- PriceJourney -->
	<xsl:template match="OriginDestination">
		<xsl:element name="PriceJourney">			
			<xsl:element name="com:State">
				<xsl:value-of select="'New'" />
			</xsl:element>
			<xsl:element name="Segments">
				<xsl:variable name="departDate" select="current()/Flight/Departure/Date" />
					<xsl:variable name="departTime" select="current()/Flight/Departure/Time" />
					<xsl:variable name="date">
						<xsl:value-of select="concat($departDate,'T',$departTime)" />
					</xsl:variable>				
					
					<xsl:variable name="flightNumber" select="current()/Flight/FlightNumber" />
					<xsl:variable name="classOfService" select="current()/Flight/ClassOfService" />
					<xsl:variable name="filteredJourney"
						select="//SPResponse//GetAvailabilityByTripResponse//JourneyDateMarket[contains(DepartureDate,$date)]/Journeys/Journey/Segments/Segment[FlightDesignator/FlightNumber=$flightNumber]" />

				<xsl:element name="PriceSegment">
					<xsl:apply-templates select="$filteredJourney/*" />
					<xsl:apply-templates
						select="$filteredJourney/Fares/Fare[ClassOfService=$classOfService]" />
				</xsl:element>			
			</xsl:element>
		</xsl:element>
	</xsl:template>


	<!-- Passengers -->
	<xsl:template match="TravelerIDs">
		<xsl:element name="Passenger">
			<xsl:element name="PassengerNumber">
				<xsl:value-of select="position()-1" />
			</xsl:element>

			<xsl:element name="Names">
				<xsl:element name="BookingName">
					<xsl:element name="State">
						<xsl:value-of select="'New'" />
					</xsl:element>
					<xsl:element name="FirstName">
						<xsl:value-of select="TravelerName/GivenName" />
					</xsl:element>
					<xsl:element name="LastName">
						<xsl:value-of select="TravelerName/Surname" />
					</xsl:element>
					<xsl:if test="TravelerName/Title">
						<xsl:element name="Title">
							<xsl:value-of select="TravelerName/Title" />
						</xsl:element>
					</xsl:if>
				</xsl:element>
			</xsl:element>


			<xsl:element name="PassengerTypeInfo">

				<xsl:element name="State">
					<xsl:value-of select="'New'" />
				</xsl:element>
				<xsl:element name="DOB">
					<xsl:value-of select="TravelerName/DateOfBirth" />
				</xsl:element>
				<xsl:element name="PaxType">
					<xsl:value-of select="$PaxType_Lookup[FLXCode=current()/@Type]/JSCode" />
				</xsl:element>
			</xsl:element>
			<xsl:element name="PseudoPassenger">
				<xsl:value-of select="'false'" />
			</xsl:element>

			<!-- For Infant create passenger -->
			<xsl:if test="current()/Infant/*">
				<xsl:element name="PassengerInfants">
					<xsl:element name="PassengerInfant">
						<xsl:element name="State">
							<xsl:value-of select="'New'" />
						</xsl:element>
						<xsl:element name="DOB">
							<xsl:value-of select="Infant/DateOfBirth" />
						</xsl:element>
						<xsl:element name="Gender">
							<xsl:value-of select="Infant/Gender" />
						</xsl:element>
					</xsl:element>

				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<xsl:template match="ActionStatusCode">
		<xsl:element name="ActionStatusCode">
			<xsl:value-of select="'NN'" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="State">
		<xsl:element name="com:State">
			<xsl:value-of select="'New'" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="Legs">
		<xsl:element name="PriceLegs">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="Leg">
		<xsl:element name="PriceLeg">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="CarrierCode[parent::FlightDesignator]">
		<xsl:element name="com:CarrierCode">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="FlightNumber">
		<xsl:element name="com:FlightNumber">
			<xsl:value-of select="current()/text()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" />
		</xsl:copy>
	</xsl:template>

	<!-- This empty template skips all elements matched -->
	<xsl:template
		match="ChangeReasonCode|CabinOfService|PriorityCode|SegmentType|OpSuffix|XrefFlightDesignator|Legs|PaxBags|PaxSeats|PaxSSRs|PaxSegments|PaxTickets|PaxSeatPreferences|SalesDate|SegmentSellKey|PaxScores|ChannelType|PaxFares|LegInfo" />


</xsl:stylesheet>