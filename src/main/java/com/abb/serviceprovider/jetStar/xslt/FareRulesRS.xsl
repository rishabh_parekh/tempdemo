<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:import href="../../common/xslt/Lookups.xsl" />

	<xsl:import href="GW_ErrorHandling.xsl" />
	<xsl:output method="xml" indent="yes" />

	<xsl:template match="LuteService">
		<xsl:element name="FareRulesRS">
			<xsl:apply-templates select="//GetFareRuleInfoResponse" />
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="GetFareRuleInfoResponse">
		<xsl:element name="DepartureCity">
			<xsl:value-of select="//FareRulesRQ/DepartureCity" />
		</xsl:element>
		<xsl:element name="ArrivalCity">
			<xsl:value-of select="//FareRulesRQ/ArrivalCity" />
		</xsl:element>
		<xsl:element name="DepartureDate">
			<xsl:value-of select="//FareRulesRQ/DepartureDate" />
		</xsl:element>
		<xsl:element name="AirlineCode">
			<xsl:value-of select="//FareRulesRQ/AirlineCode" />
		</xsl:element>
		<xsl:element name="FareBasisCode">
			<xsl:value-of select="//FareRulesRQ/FareBasisCode" />
		</xsl:element>
		<xsl:element name="FareType">
			<xsl:value-of select="//FareRulesRQ/FareType" />
		</xsl:element>
		<xsl:element name="Rules">		
			<!-- Rules -->					
			<xsl:apply-templates select="//Decodedrules" />			
		</xsl:element>		
	</xsl:template>
	
	<xsl:template match="Decodedrules">
		<xsl:copy-of select="node()"/>
	</xsl:template>
</xsl:stylesheet>