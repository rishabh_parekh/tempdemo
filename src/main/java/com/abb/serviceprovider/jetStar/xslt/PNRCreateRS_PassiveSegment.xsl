<xsl:stylesheet version="1.0" xmlns:exslt="http://exslt.org/common"
	xmlns:intinc="com.abb.framework.util.IntIncrement" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:strip-space elements="*" />

	<xsl:import href="JS_Constants.xsl" />
	<xsl:import href="../../common/xslt/Lookups.xsl" />
	<xsl:import href="../../common/xslt/FormatDate.xsl" />
	<xsl:import href="JS_ErrorHandling.xsl" />	
	<xsl:variable name="TicketIssued" select="'N'" />
	<xsl:variable name="QueueRetrieved" select="'N'" />
	<xsl:variable name="CRSID" select="'F1'" />
	<!-- <xsl:variable name="ActionCode" select="'K'" /> -->
	<xsl:variable name="FormOfPayment" select="'INV'" />
	<xsl:variable name="InfantElementNumber" select="'.1'" />
	<xsl:variable name="cnt" select="'0'" />
	<xsl:variable name="counter_global" select="intinc:new(30)" />
	<xsl:variable name="counter_flight" select="intinc:new(20)" />
	<xsl:variable name="counter_Segment_Element_Flight" select="intinc:new(20)" />
	<xsl:variable name="counter_segmentObject" select="intinc:new(1)" />
	<xsl:variable name="counter_JourneyObj" select="intinc:new(1)" />
	<xsl:variable name="baggage_counter_for_infant_passnege" select="intinc:new(1)" />
	<xsl:variable name="baggage_counter" select="intinc:new(1)" />
	<xsl:variable name="Baggage_Lookup"
	select="document('lookupXMLs/Baggage_Lookup.xml')//BagType" />
	<xsl:variable name="Gender_Lookup"
		select="document('lookupXMLs/Gender_Lookup.xml')//GenderType" />
	<xsl:output method="xml" indent="yes" />
	
	<!-- Stop Creation of PNRViewRS other tags, when error message received -->
	<xsl:template match="/">
		<!-- PNRVewRS -->
		<xsl:element name="PNRViewRS">
			<xsl:choose>
				<!-- SoapFaultExcepptionHandling -->
				<xsl:when test="//Fault/* or //error/* or //Warning/*">
					<xsl:call-template name="errorHandling" />
				</xsl:when>
				<xsl:otherwise>
					<!-- used last() TO AVOID MULTIPLE GETBOOKING RESPONSE appended in spResponse, 
					     GET VALUES FROM RETRIEVE RESPONSE after addition of passive segment -->
					<xsl:apply-templates select="(//GetBookingResponse/Booking)[last()]"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>

	<xsl:template match="//GetBookingResponse/Booking">		

			<!-- INTA-65 Called only in case of Cancelled PNR Retrieve-->
			<xsl:if test="//GetBookingResponse/Booking/BookingInfo/BookingStatus = 'Closed'">
				<xsl:element name="InfoGroup">
					<xsl:element name="Error">
						<xsl:attribute name="Source">
							<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
						<xsl:attribute name="ErrorType">
    						<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
						<xsl:element name="Code">
							<xsl:value-of select="$PNR_CANCELLED_ERROR_CODE" />
						</xsl:element>
						<xsl:element name="Text">
							<xsl:value-of select="$PNR_CANCELLED_ERROR_MSG" />
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			
			<!-- PNRIdentification -->
			<xsl:element name="PNRIdentification">
				<xsl:attribute name="TicketIssued">
					<xsl:value-of select="$TicketIssued" />
			</xsl:attribute>
				<xsl:attribute name="QueueRetrieved">
				<xsl:value-of select="$QueueRetrieved" />
			</xsl:attribute>
				<xsl:attribute name="FareDataExists">
				<xsl:value-of select="'Y'" />
			</xsl:attribute>
				<xsl:element name="RecordLocator">
					<xsl:value-of select="RecordLocator" />
				</xsl:element>
				<xsl:element name="Sources">
					<xsl:element name="Source">
						<xsl:attribute name="name">
							<xsl:value-of select="$SOURCE" />
						</xsl:attribute>
						<xsl:attribute name="title">
							<xsl:value-of select="$SOURCE_NAME" />
						</xsl:attribute>

						<xsl:element name="PNRIdentification">
							<xsl:attribute name="Source">
								<xsl:value-of select="$SOURCE" />
							</xsl:attribute>
							<xsl:attribute name="SourceRef">
								<xsl:value-of select="RecordLocator" />
							</xsl:attribute>

							<xsl:element name="RecordLocator">
								<xsl:value-of select="RecordLocator" />
							</xsl:element>
							
							<!-- Common Source Info -->
							<xsl:call-template name="SourceInfo"/>
							
						</xsl:element>
					</xsl:element>
					
					<!-- Source of 9W: Create only if Passive Segment added -->
					<xsl:if test="//GetBookingResponse/Booking/RecordLocators/*">
						<xsl:variable name="RecordLocator" select="//GetBookingResponse/Booking/RecordLocators/RecordLocator/RecordCode"/>
						<xsl:element name="Source">
							<xsl:attribute name="name">
								<xsl:value-of select="$JETAIRWAYS_SOURCE" />
							</xsl:attribute>
							<xsl:attribute name="title">
								<xsl:value-of select="$JETAIRWAYS_SOURCENAME" />
							</xsl:attribute>
							<xsl:element name="PNRIdentification">
								<xsl:attribute name="Source">
									<xsl:value-of select="$JETAIRWAYS_SOURCE" />
								</xsl:attribute>
								<xsl:attribute name="SourceRef">
									<xsl:value-of select="$RecordLocator" />
								</xsl:attribute>
								
								<xsl:element name="RecordLocator">
									<xsl:value-of select="$RecordLocator" />
								</xsl:element>
							
								<!-- Common Source Info -->
								<xsl:call-template name="SourceInfo"/>
							</xsl:element>
						</xsl:element>
					</xsl:if>
				</xsl:element>
				
				<xsl:element name="CreationAgent">
					<xsl:value-of select="BookingInfo/CreatedAgentID" />
				</xsl:element>
				
				<xsl:element name="ReceivedFrom">
					<xsl:choose>
						<xsl:when test="//PNRCreateRQ//ReceivedFrom">
							<xsl:value-of select="//PNRCreateRQ//ReceivedFrom" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="//PNRRetrieveRQ/ReceivedFrom" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
				
				<xsl:element name="Ownership">
					<xsl:element name="CRSID">
						<xsl:value-of select="$CRSID" />
					</xsl:element>
					<xsl:element name="Agency">
						<xsl:value-of select="//tc/iden/@agy" />
					</xsl:element>
					<xsl:element name="PseudoCityCode">
						<xsl:value-of select="//tc/iden/@pseudocity" />
					</xsl:element>
				</xsl:element>	
							
				<xsl:element name="CurrentPseudoCityCode">
					<xsl:value-of select="//tc/iden/@pseudocity" />
				</xsl:element>
			</xsl:element>

			<!-- FareGroup -->
			<!-- Added for INF Fares from PassengerFees tag -->
			<xsl:variable name="AllPaxTotalPrice_INF">
				<xsl:for-each select="//GetBookingResponse//Passenger">
					<xsl:for-each select="PassengerFees/PassengerFee[SSRCode='INFT']/ServiceCharges/BookingServiceCharge">
						<PriceForSum>
							<xsl:value-of select="Amount" />
						</PriceForSum>
					</xsl:for-each>
				</xsl:for-each>
			</xsl:variable>
			<xsl:variable name="One_Total_INF" select="(sum(exslt:node-set($AllPaxTotalPrice_INF)/PriceForSum))" />
			
			<!--Fare Group -->
			<xsl:for-each select="(//GetBookingResponse//Journeys)[last()]">
				<xsl:call-template name="Journeys_Main">
					<xsl:with-param name="INF_Charges" select="$One_Total_INF" />
				</xsl:call-template>
			</xsl:for-each>

			<!--Passenger Group -->
			<xsl:apply-templates select="Passengers/Passenger" />

			<xsl:element name="AirGroup">
				<xsl:apply-templates select="(//GetBookingResponse//Journeys)[last()]/Journey" />			
			</xsl:element>

			<!-- We are not receiving any passenger telephone no/email id so it is not created ,contact details are of booking agency contact details -->
			<!-- <xsl:element name="Telephone"/> -->			

			<xsl:element name="Ticketing">
				<xsl:attribute name="Source"> 
					<xsl:value-of select="$SOURCE" />
					 </xsl:attribute>
				<xsl:attribute name="SourceRef"> 
					<xsl:value-of select="RecordLocator" />
				</xsl:attribute>
				<xsl:element name="ElementNumber">
					<xsl:value-of select="intinc:getNext($counter_global)" />
				</xsl:element>
				<xsl:element name="Date">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="BookingInfo/CreatedDate" />  <!-- Date -->
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
					<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="Time">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'time'" />
					<xsl:with-param name="datestr" select="BookingInfo/CreatedDate" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			</xsl:element>
						
			<!-- We are not receiving any passenger telephone no/email id so it is not created ,contact details are of booking agency contact details -->
			<xsl:if test="//GetBookingResponse//Booking//EmailAddress">
				<xsl:element name="EmailAddress">
					<xsl:element name="Email">
						<xsl:value-of select="//GetBookingResponse//Booking//EmailAddress"/>
					</xsl:element>			
					<xsl:element name="ElementNumber">
						<xsl:value-of select="//TravelerIDRef"/>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			
			<!-- Inhibit Creation of Payment Tag at Book&Hold request -->
			<xsl:if test="//GetBookingPaymentsResponse/getBookingPaymentRespData/Payments/Payment">
				<xsl:element name="BillingAndDeliveryData">
					<xsl:element name="FormOfPayment">
					<xsl:attribute name="Source">
						<xsl:value-of select="$SOURCE" />
					</xsl:attribute>
					<xsl:attribute name="SourceRef">
						<xsl:value-of select="//GetBookingResponse/Booking/RecordLocator" />
					</xsl:attribute>
					<xsl:element name="ElementNumber">
						<xsl:value-of select="intinc:getNext($counter_global)" />
					</xsl:element>
					<xsl:element name="NativeElementText">
						<xsl:value-of select="PaymentText" />
					</xsl:element>
					<xsl:element name="Other">
						<xsl:value-of select="$FormOfPayment" />
					</xsl:element>
					<xsl:apply-templates select="//GetBookingPaymentsResponse/getBookingPaymentRespData/Payments/Payment" />
					</xsl:element>
					<xsl:element name="AddressGroup">
						<xsl:apply-templates select="BookingContacts/BookingContact"/>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			
			
			<!--INTA-60 (ref.GWIA-29) GeneralRemark for PNRCreateRS -->
			<xsl:apply-templates select="//SARequest//PNRCreateRQ/OtherPNRElements/GeneralRemark"/>
			
			<!--INTA-60 (ref.GWIA-29) GeneralRemark for PNRRetrieveRS-->
			<xsl:apply-templates select="//SARequest//PNRRetrieveRQ/PNRViewRS/GeneralRemark"/>
			

			<!--TODO SSR is not being handled in phase-1  -->
			<!-- <xsl:element name="SpecialServiceRequest">
				<xsl:attribute name="Source">
				<xsl:value-of select="$SOURCE" />
			</xsl:attribute>
				<xsl:attribute name="SourceRef">
				<xsl:value-of select="RecordLocator" />
			</xsl:attribute>
				<xsl:apply-templates
					select="Passengers/Passenger/PassengerFees/PassengerFee" />
			</xsl:element>		 -->
	</xsl:template>

	<!-- Common Source Info of Sources tag above -->
	<xsl:template name="SourceInfo">
		<xsl:element name="CreationDate">
			<xsl:call-template name="dateformat">
				<xsl:with-param name="format" select="'date'" />
				<xsl:with-param name="datestr" select="BookingInfo/CreatedDate" />  <!-- Date -->
				<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
				<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
			</xsl:call-template>
		</xsl:element>
		<xsl:element name="CreationTime">
			<xsl:call-template name="dateformat">
				<xsl:with-param name="format" select="'time'" />
				<xsl:with-param name="datestr" select="BookingInfo/CreatedDate" />
				<xsl:with-param name="outformat" select="string('hh-mm')" />
				<xsl:with-param name="informat" select="string('datetime')" />
			</xsl:call-template>
		</xsl:element>
		<xsl:element name="CreationAgent">
			<xsl:value-of select="BookingInfo/CreatedAgentID" />
		</xsl:element>							
		<xsl:element name="Ownership">
			<xsl:element name="CRSID">
				<xsl:value-of select="$CRSID" />
			</xsl:element>
			<xsl:element name="Agency">
				<xsl:value-of select="//tc/iden/@agy" />
			</xsl:element>
			<xsl:element name="PseudoCityCode">
				<xsl:value-of select="//tc/iden/@pseudocity" />
			</xsl:element>
		</xsl:element>		
		
		<!-- TODO:Not received in request -->
		<xsl:element name="ReceivedFrom">
			<xsl:value-of select="//PNRRetrieveRQ/ReceivedFrom" />
		</xsl:element>	
	</xsl:template>
	
	<!--No Mapping available for Telephone details of passengers so tags are commented  -->
	<!-- Telephone BookingContacts 
	<xsl:template match="BookingContact" mode="telephone">
		<xsl:element name="ElementNumber">
			<xsl:value-of select="position()" />
		</xsl:element>
		<xsl:element name="TravelerElementNumber">
			<xsl:value-of select="position()" />
		</xsl:element>
		<xsl:element name="TelephoneNumber">
			<xsl:value-of select="HomePhone" />
		</xsl:element>
		<xsl:element name="CityCode">
			<xsl:value-of select="City" />
		</xsl:element>
		<xsl:element name="StateCode">
			<xsl:value-of select="ProvinceState" />
		</xsl:element>
		<xsl:element name="CountryCode">
			<xsl:value-of select="CountryCode" />
		</xsl:element>
	</xsl:template> -->

	<!--SSR Phase-2 -->
	<!-- <xsl:template match="PassengerFee">
		<xsl:element name="ElementNumber">
			<xsl:value-of select="position()" />
		</xsl:element>
		<xsl:element name="SSRCode">
			<xsl:value-of select="SSRCode" />
		</xsl:element>
		<xsl:element name="ActionCode">
			<xsl:value-of select="ActionStatusCode" />
		</xsl:element>
	</xsl:template> -->

	<!--Payment Group -->
	<xsl:template match="Payment">
	
		<xsl:variable name="expirationDate" select="Expiration"/>
		<xsl:variable name="expirationMonth" select="substring-after($expirationDate,'-')"/>
		<xsl:variable name="expMonth" select="substring-before($expirationMonth,'-' )"/>
		<xsl:variable name="expirationYear" select="substring-before($expirationDate,'-')"/>	
		<xsl:variable name="expYear" select="substring-after($expirationYear, 20)"/>
	
		<xsl:element name="CreditCard">
			<xsl:element name="CCCode">
				<xsl:value-of select="PaymentMethodCode" />
			</xsl:element>
			<xsl:element name="CCNumber">
				<xsl:value-of select="AccountNumber" />
			</xsl:element>
			<xsl:element name="CCExpiration">
				<xsl:element name="Month">
					<xsl:value-of select="$expMonth"/>
				</xsl:element>
				<xsl:element name="Year">
					<xsl:value-of select="$expYear"/>
				</xsl:element>
			</xsl:element>	
			<xsl:element name="CardholderLastName">
				<xsl:value-of select="AccountName" />
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<!-- Traveler :: For Every Passenger -->
	<xsl:template match="Passenger">
		<xsl:element name="Traveler">
			<xsl:attribute name="Type">
				<xsl:value-of select="current()/PassengerTypeInfos/PassengerTypeInfo/PaxType" />		
			</xsl:attribute>
			<xsl:element name="ElementNumber">
				<xsl:value-of select="$cnt+ position()" />
			</xsl:element>
			<xsl:element name="TravelerName">
				<xsl:element name="Surname">
					<xsl:value-of select="Names/BookingName/LastName" />
				</xsl:element>
				<xsl:element name="GivenName">
					<xsl:value-of select="Names/BookingName/FirstName" />
				</xsl:element>
				<xsl:element name="NativeGivenName">
					<xsl:value-of select="Names/BookingName/FirstName" />
				</xsl:element>
				<!-- if Title is returned in response it should be returned in response -->
				<xsl:if test="Names/BookingName/Title">
					<xsl:element name="Title">
						<xsl:value-of select="Names/BookingName/Title" />
					</xsl:element>
				</xsl:if>
				<xsl:element name="DateOfBirth">
					<xsl:value-of select="current()/PassengerTypeInfos/PassengerTypeInfo/DOB" />
				</xsl:element>
				<!-- Changed Gender according to FLX Schema Converted from 'Male' to 'M' & 'Female' to 'F' -->
				<xsl:element name="Gender">
					<xsl:variable name="currentGender" select="current()/PassengerInfo/Gender" />
					<xsl:value-of select="$Gender_Lookup[JSName=$currentGender]/FLXName" />
				</xsl:element>
			</xsl:element>
			<!-- Infant -->
			<xsl:if test="Infant/*">
				<xsl:element name="Infant">
					<xsl:element name="ElementNumber">
						<xsl:value-of select="number($cnt+ position()) + $InfantElementNumber" />
					</xsl:element>
					<xsl:element name="Surname">
						<xsl:value-of select="Infant/Names/BookingName/LastName" />
					</xsl:element>
					<xsl:element name="GivenName">
						<xsl:value-of select="Infant/Names/BookingName/FirstName" />
					</xsl:element>
					<xsl:element name="NativeGivenName">
						<xsl:value-of select="Infant/Names/BookingName/FirstName" />
					</xsl:element>
					<xsl:element name="DateOfBirth">
						<xsl:value-of select="Infant/DOB" />
					</xsl:element>
					<!-- Changed Gender according to FLX Schema Converted from 'Male' to 'M' & 'Female' to 'F' -->
					<xsl:element name="Gender">
						<xsl:variable name="currentInfantGender" select="Infant/Gender" />
					<xsl:value-of select="$Gender_Lookup[JSName=$currentInfantGender]/FLXName" />
					</xsl:element>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>

	<!-- AirGroup : Flight :: For Every Segment -->
	<xsl:template match="Journey">
		<xsl:apply-templates select="Segments/Segment" />
	</xsl:template>

	<xsl:template match="Segment">
		<xsl:variable name="currentSegmentType" select="current()/SegmentType"/>
		<xsl:apply-templates select="Legs/Leg">
			<xsl:with-param name="currentSegmentType" select="$currentSegmentType"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<!--Flight Group -->
	<xsl:template match="Leg">
	
		<!--Variable for Passive Segment  -->
		<xsl:param name="currentSegmentType"/>
		
		<xsl:element name="Flight">
			<xsl:attribute name="BookingDate">			
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="//CreatedDate" />  <!-- Date -->
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
					<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
				</xsl:call-template>
			</xsl:attribute>
			<xsl:attribute name="BookingTime">				
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'time'" />
					<xsl:with-param name="datestr" select="//CreatedDate" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:attribute>
			
			<!-- Create this attribute only in case of Passive Flight Data -->
			<xsl:if test="$currentSegmentType = 'P'">
				<xsl:attribute name="Type">
					<xsl:value-of select="$currentSegmentType"/>
				</xsl:attribute>
			</xsl:if>

			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_flight)" />
			</xsl:element>
			<xsl:element name="Departure">
				<xsl:variable name="AirportCode" select="DepartureStation" />
				<xsl:element name="AirportCode">
					<xsl:value-of select="$AirportCode" />
				</xsl:element>
				<xsl:element name="AirportName">
					<xsl:value-of
						select="concat($Airport_Lookup[AirportCode=$AirportCode]/AirportName,', ',$Airport_Lookup[AirportCode=$AirportCode]/CountryCode)" />
				</xsl:element>
				<xsl:element name="Date">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'date'" />
						<xsl:with-param name="datestr" select="STD" />  <!-- Date -->
						<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
						<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
					</xsl:call-template>
				</xsl:element>
				<xsl:element name="Time">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'time'" />
						<xsl:with-param name="datestr" select="STD" />
						<xsl:with-param name="outformat" select="string('hh-mm')" />
						<xsl:with-param name="informat" select="string('datetime')" />
					</xsl:call-template>
				</xsl:element>
			</xsl:element>
			<xsl:element name="Arrival">
				<xsl:variable name="AirportCode" select="ArrivalStation" />
				<xsl:element name="AirportCode">
					<xsl:value-of select="ArrivalStation" />
				</xsl:element>
				<xsl:element name="AirportName">
					<xsl:value-of
						select="concat($Airport_Lookup[AirportCode=$AirportCode]/AirportName,', ',$Airport_Lookup[AirportCode=$AirportCode]/CountryCode)" />
				</xsl:element>
				<xsl:element name="Date">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'date'" />
						<xsl:with-param name="datestr" select="STA" />  <!-- Date -->
						<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />  <!-- Output Date Format -->
						<xsl:with-param name="informat" select="string('datetime')" />  <!-- Input Date Format -->
					</xsl:call-template>
				</xsl:element>
				<xsl:element name="Time">
					<xsl:call-template name="dateformat">
						<xsl:with-param name="format" select="'time'" />
						<xsl:with-param name="datestr" select="STA" />
						<xsl:with-param name="outformat" select="string('hh-mm')" />
						<xsl:with-param name="informat" select="string('datetime')" />
					</xsl:call-template>
				</xsl:element>
			</xsl:element>
			<xsl:element name="Carrier">
			<xsl:variable name="MarketingAirline" select="FlightDesignator/CarrierCode" />
				<xsl:element name="AirlineCode">
					<xsl:value-of select="$MarketingAirline" />
				</xsl:element>
				<xsl:element name="AirlineName">
					<xsl:value-of select="$Airline_Lookup[AirLineCode=$MarketingAirline]/AirLineName" />
				</xsl:element>
				<xsl:element name="FlightNumber">
					<xsl:attribute name="Suffix">
						<!-- <xsl:value-of select="$CARRIER_CODE_FOR_JS" /> -->
						<xsl:value-of select="$MarketingAirline" />
					</xsl:attribute>
					<xsl:value-of select="FlightDesignator/FlightNumber" />
				</xsl:element>
			</xsl:element>
				
			<xsl:element name="ClassOfService">
				<xsl:value-of select="../../Fares/Fare/ClassOfService" />
			</xsl:element>
			<xsl:element name="ActionCode">
				<!-- <xsl:attribute name="Status">
					<xsl:value-of select="$ActionCode" />
				</xsl:attribute> -->
			<xsl:value-of select="//ActionStatusCode"></xsl:value-of>
			</xsl:element>
			<xsl:element name="NumberInParty">
				<xsl:value-of select="//PaxCount" />
			</xsl:element>		
					
		</xsl:element>
			
	</xsl:template>	
	
	<!-- BillingAndDeliveryData -->
	<xsl:template match="BookingContact">
		<xsl:variable name="AddressLine1" select="AddressLine1" />
		<xsl:variable name="AddressLine2" select="AddressLine2" />
		<xsl:variable name="AddressLine3" select="AddressLine3" />
		<xsl:element name="ElementNumber">
			<xsl:value-of select="intinc:getNext($counter_global)" />
		</xsl:element>
		<xsl:element name="Address">			
			<xsl:value-of
				select="concat($AddressLine1,',' ,$AddressLine2, $AddressLine3)" />
		</xsl:element>
		<xsl:element name="City">
			<xsl:value-of select="City" />
		</xsl:element>
		<xsl:element name="PostalCode">
			<xsl:value-of select="PostalCode" />
		</xsl:element>
		<xsl:element name="CountryCode">
			<xsl:value-of select="CountryCode" />
		</xsl:element>
		<xsl:element name="AddressText">
				<xsl:value-of select="concat($AddressLine1,',' ,$AddressLine2, $AddressLine3)" />
			</xsl:element>
	</xsl:template>
	
	<!--Fare Group -->
	
	<xsl:template name="Journeys_Main">
		<xsl:param name="INF_Charges" />
		<xsl:variable name="Amount_Charged_INF" select="$INF_Charges" />
		
		<xsl:element name="FareGroup">
			<xsl:attribute name="FareNumber">
				 <xsl:value-of select="$FARE_NUMBER" /> 
			</xsl:attribute>
			<xsl:attribute name="FareType">
				<!-- For Non-supported  FareType=”?” -->
				<xsl:value-of select="$DEFAULT_FARETYPE_NONPREBOOKING" />
			</xsl:attribute>
			<xsl:attribute name="FarePriced">
				<xsl:value-of select="$FARE_PRICED" />
			</xsl:attribute>
			<xsl:attribute name="TravelerCount">
				<xsl:value-of select="//PaxCount" />
			</xsl:attribute>
			<xsl:attribute name="TotalPrice">
				<xsl:value-of select="round(format-number(//GetBookingResponse//Booking//BookingSum/TotalCost,'#.00') * 100)" />		
			</xsl:attribute>
			<xsl:element name="CurrencyCode">
				<xsl:attribute name="NumberOfDecimals">
					<xsl:value-of select="$CURRENCY_NUMBER_OF_DECIMAL" />
				</xsl:attribute>
				<xsl:value-of select="Journey//PaxFare//CurrencyCode" />
			</xsl:element>
			
			
			<xsl:variable name="AllPaxTotalPrice_ADT">
				<xsl:for-each select="Journey">
					<xsl:choose>
						<xsl:when test="//PassengerTypeInfos/PassengerTypeInfo[PaxType='ADT']">
							<xsl:for-each select="Segments/Segment[1]/Fares/Fare/PaxFares/PaxFare[PaxType='ADT']/ServiceCharges/BookingServiceCharge[ChargeType='FarePrice']">
								<PriceForSum>
									<xsl:value-of select="Amount" />
								</PriceForSum>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<PriceForSum>
								<xsl:value-of select="'0'" />
							</PriceForSum>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:variable>
			
			<xsl:variable name="AllPaxTotalPrice_CHD">
				<xsl:for-each select="Journey">
					<xsl:choose>
						<xsl:when test="//PassengerTypeInfos/PassengerTypeInfo[PaxType='CHD']">
							<xsl:for-each select="Segments/Segment[1]/Fares/Fare/PaxFares/PaxFare[PaxType='CHD']/ServiceCharges/BookingServiceCharge[ChargeType='FarePrice']">
								<PriceForSum>
									<xsl:value-of select="Amount" />
								</PriceForSum>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<PriceForSum>
								<xsl:value-of select="'0'" />
							</PriceForSum>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:variable>
			
			<xsl:variable name="NoOfCHD" select="count(//PassengerTypeInfos/PassengerTypeInfo[PaxType='CHD'])" />

			<xsl:variable name="NoOfADT" select="count(//PassengerTypeInfos/PassengerTypeInfo[PaxType='ADT'])" />

			<xsl:variable name="One_Total_CHD" select="(sum(exslt:node-set($AllPaxTotalPrice_CHD)/PriceForSum))" />

			<xsl:variable name="One_Total_ADT" select="(sum(exslt:node-set($AllPaxTotalPrice_ADT)/PriceForSum))" />
			
			<xsl:variable name="FinalTotalPrice">
				<xsl:choose>
					<xsl:when test="//GetBookingResponse//Passengers//PassengerInfants/PassengerInfant/*">
						<xsl:value-of select="($One_Total_ADT)*$NoOfADT+$Amount_Charged_INF" />
					</xsl:when>
					<xsl:when test="not(//PassengerTypeInfos/PassengerTypeInfo[PaxType='ADT'])">
						<xsl:value-of select="'0'" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="($One_Total_ADT)*$NoOfADT" />
					</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>

			<!-- TravelerGroup :: For Each Traveler -->
			<xsl:for-each select="../Passengers/Passenger">				
			<xsl:variable name="CurrentPaxType" select="PassengerTypeInfos/PassengerTypeInfo/PaxType"/>				
			<xsl:if test="not($CurrentPaxType=preceding-sibling::node()/PassengerTypeInfos/PassengerTypeInfo/PaxType)">					
		
				<xsl:variable name="Taxes">
					<xsl:for-each select="../../Journeys/Journey">
						<xsl:for-each select="Segments/Segment">
							<xsl:for-each
								select="Fares/Fare/PaxFares/PaxFare[PaxType=$CurrentPaxType]/ServiceCharges/BookingServiceCharge[not((ChargeType='Discount') or (ChargeType='FarePrice') or (ChargeType='IncludedTax') or (ChargeType='IncludedTravelFee'))]">
								<Tax_Amount>
									<xsl:value-of select="Amount" />
								</Tax_Amount>
								<Tax_Description>
									<xsl:value-of select="ChargeType" />
								</Tax_Description>
							</xsl:for-each>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:variable>

				<xsl:element name="TravelerGroup">
				
					<!--GWIA-52 Issue Implementation Changes are done to a unique segment ID (for outbound and inbound flight) under Fare Info -->
					<!-- below xsl:if line is used to call the function if we do not use then counter is not reset,it works as hibernate lazy loading -->
					<xsl:variable name="counterforSegment_reset" select="intinc:resetCounter($counter_segmentObject)" />
					<xsl:if test="$counterforSegment_reset"></xsl:if>
					<!-- End GWIA-52 Issue Implementation -->
					<xsl:variable name="Counter" select="intinc:new()" />
					<xsl:variable name="TotalTravelerType" select="count((//GetBookingResponse/Booking)[last()]//PassengerTypeInfos/PassengerTypeInfo[PaxType=$CurrentPaxType])" />

					<xsl:attribute name="TypeRequested"> 
						<xsl:value-of select="$CurrentPaxType" />
					 </xsl:attribute>
					<xsl:attribute name="TypePriced"> 
						<xsl:value-of select="$CurrentPaxType" />
					</xsl:attribute>
					
					<!-- SIMILAR TO REF. GWIA-47 (GWIA-21) TST Related info -->
					 <xsl:variable name="PassengerBaggageFee">
					 	<xsl:for-each select="(//GetBookingResponse/Booking)[last()]/Passengers/Passenger[PassengerTypeInfos/PassengerTypeInfo/PaxType=$CurrentPaxType]">
								 
							<xsl:variable name="BookingRequest" select="current()"/>
							<xsl:if test="$CurrentPaxType=current()/PassengerTypeInfos/PassengerTypeInfo/PaxType">
								 <xsl:variable name="SumOfBaggage">
									<xsl:for-each select="current()/PassengerFees/PassengerFee">
										<xsl:if test="contains(current()/FeeCode,'BG')">
										
											 <xsl:for-each select="current()/ServiceCharges/BookingServiceCharge">
												 <totalPrice>
													<xsl:value-of select="Amount" />
												</totalPrice>
											 </xsl:for-each>
										 </xsl:if>
									</xsl:for-each>
								 </xsl:variable>
								 <totalPassengerPrice>
								 	<xsl:value-of select="sum(exslt:node-set($SumOfBaggage)/totalPrice)"/>
								 </totalPassengerPrice>
							</xsl:if>
						</xsl:for-each> 
					</xsl:variable>
					
					<xsl:variable name="TotalBaggageFees" 
						select="round(format-number(sum(exslt:node-set($PassengerBaggageFee)/totalPassengerPrice),'#.##')*100)"/>
					
					<!-- END -->
					<xsl:attribute name="TypeTotalPrice"> 
	       	 			<xsl:choose>
							<xsl:when test="$CurrentPaxType='CHD' ">
								<xsl:value-of select="round(format-number(($One_Total_CHD)*$TotalTravelerType,'#.00') * 100) + round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount)*$TotalTravelerType,'#.00') * 100) + $TotalBaggageFees" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="round(format-number(($One_Total_ADT)*$TotalTravelerType,'#.00') * 100) + round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount)*$TotalTravelerType,'#.00') * 100) + $TotalBaggageFees" />
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				
					<xsl:attribute name="TypeCount"> 
						<xsl:value-of select="$TotalTravelerType" /> 
					</xsl:attribute>
				
					<!--Price Start  -->
					<xsl:element name="Price">
						<xsl:attribute name="Total"> 
		 		        	<xsl:choose>
								<xsl:when test="$CurrentPaxType='CHD'">
									<xsl:value-of select="round(format-number($One_Total_CHD,'#.00') * 100) + round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount),'#.00') * 100)" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="round(format-number($One_Total_ADT,'#.00') * 100) + round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount),'#.00') * 100)" />
								</xsl:otherwise>
							</xsl:choose>
						 </xsl:attribute>
						<xsl:element name="BaseFare">
							<xsl:attribute name="Amount"> 
				       		<xsl:choose>
								<xsl:when test="$CurrentPaxType='CHD'">
									<xsl:value-of select="round(format-number($One_Total_CHD,'#.00') * 100)" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="round(format-number($One_Total_ADT,'#.00') * 100)" />
								</xsl:otherwise>
							</xsl:choose>
							</xsl:attribute>
						</xsl:element>

						<xsl:element name="Taxes">
							<xsl:if test="count(exslt:node-set($Taxes)/*) &gt; 0">
								<xsl:attribute name="Amount">
									<xsl:value-of select="round(format-number(sum(exslt:node-set($Taxes)/Tax_Amount),'#.00') * 100)" />
								</xsl:attribute>
								<xsl:for-each select="exslt:node-set($Taxes)/Tax_Amount">
									<xsl:variable name="TaxNode_Count" select="intinc:getNext($Counter)" />
									<xsl:element name="Tax">
										<xsl:attribute name="Amount">
										<xsl:value-of select="round(format-number(exslt:node-set($Taxes)/Tax_Amount[position()=$TaxNode_Count],'#.00') * 100)" />
									</xsl:attribute>
										<xsl:element name="Description">
											<xsl:value-of select="exslt:node-set($Taxes)/Tax_Description[position()=$TaxNode_Count]" />
										</xsl:element>
									</xsl:element>
								</xsl:for-each>
							</xsl:if>
						</xsl:element>
					</xsl:element>
					<!--Price END  -->
						
					<!-- FareRules -->
					<xsl:element name="FareRules">
						<!-- Skip Creation of FareInfo Tags for PassiveSegment 9W journey -->
						<xsl:for-each select="../../../Booking/Journeys/Journey[Segments/Segment/SegmentType != 'P']">
							<xsl:variable name="thisPaxType" select="$CurrentPaxType" />
							<xsl:call-template name="JourneyInfo">
								<xsl:with-param name="CurrentPaxType" select="$thisPaxType" />
							</xsl:call-template>
						</xsl:for-each>
						<!-- Rules -->
						<xsl:apply-templates select="//Decodedrules" />
					</xsl:element>					
				</xsl:element>
			</xsl:if>					
			</xsl:for-each>		

			<xsl:if test="(//GetBookingResponse/Booking)[last()]//Passengers//PassengerInfants/PassengerInfant/*">
				<xsl:element name="TravelerGroup">
					<!--GWIA-52 Issue Implementation Changes are done to a unique segment ID (for outbound and inbound flight) under Fare Info -->
					<!-- below xsl:if line is used to call the function if we do not use then counter is not reset,it works as hibernate lazy loading -->
					<xsl:variable name="counterforSegment_reset" select="intinc:resetCounter($counter_segmentObject)" />
					<xsl:if test="$counterforSegment_reset"></xsl:if>
					<!-- End GWIA-52 Issue Implementation -->
					<xsl:call-template name="TravelerGroupInfo_INF">
						<xsl:with-param name="INF_Fees" select="$Amount_Charged_INF" />
					</xsl:call-template>
					<!-- FareRules -->
					<xsl:element name="FareRules">
						<xsl:for-each select="Journey">
							<xsl:variable name="thisPaxType" select="'INF'" />
							<xsl:call-template name="JourneyInfo">
								<xsl:with-param name="CurrentPaxType" select="$thisPaxType" />
							</xsl:call-template>
						</xsl:for-each>
						<!-- Rules -->
						<xsl:apply-templates select="//Decodedrules" />
					</xsl:element>
				</xsl:element>
			</xsl:if>
			
			<xsl:element name="ValidatingCarrier">
				<xsl:value-of select="//CarrierCode" />
			</xsl:element>
			
			<xsl:for-each select="//Leg">
				<xsl:element name="SegmentElementNumber">
					<xsl:attribute name="Source">
						<xsl:value-of select="$SOURCE" />
					</xsl:attribute>
					<xsl:value-of select="intinc:getNext($counter_Segment_Element_Flight)" />
				</xsl:element>
			</xsl:for-each>
			
			<!-- bag -->
			<xsl:apply-templates select="Journey" mode="Baggage" />
			
		</xsl:element>
	</xsl:template>
	
	<!-- Added for including Flight Rules in TravelerGroup -->
	<!-- Rules -->
	<xsl:template match="Decodedrules">
		<xsl:copy-of select="node()" />
	</xsl:template>
	
	<!-- TravelerGroupInfo_INF -->
	<xsl:template name="TravelerGroupInfo_INF">
		<xsl:param name="INF_Fees" />

		<xsl:attribute name="TypeRequested">
				<xsl:value-of select="'INF'" />
			</xsl:attribute>
		<xsl:attribute name="TypePriced">
					<xsl:value-of select="'INF'" />
			</xsl:attribute>
		<xsl:variable name="TotalPax" select="count((//GetBookingResponse/Booking)[last()]//Passengers//PassengerInfants/PassengerInfant)" />
		<xsl:attribute name="TypeCount">
				<xsl:value-of select="$TotalPax" />
		</xsl:attribute>
		
		<!-- GWIA-47 (GWIA-21) TST Related info -->
		 <xsl:variable name="PassengerBaggageFee">
			 <xsl:for-each select="(//GetBookingResponse/Booking)[last()]/Passengers/Passenger">
		  		 <xsl:variable name="BookingRequest" select="current()"/>
					 <xsl:variable name="SumOfBaggage">
						<xsl:for-each select="current()/PassengerFees/PassengerFee">
							<xsl:if test="contains(current()/FeeCode,'BG')">
								 <xsl:for-each select="current()/ServiceCharges/BookingServiceCharge">
									 <totalPrice>
										<xsl:value-of select="Amount" />
									</totalPrice>
								 </xsl:for-each>
							 </xsl:if>
						</xsl:for-each>
					 </xsl:variable>
					 <totalPassengerPrice>
					 	<xsl:value-of select="sum(exslt:node-set($SumOfBaggage)/totalPrice)"/>
					 </totalPassengerPrice>
			</xsl:for-each> 
		</xsl:variable>
		<xsl:variable name="TotalBaggageFees" 
			select="round(format-number(sum(exslt:node-set($PassengerBaggageFee)/totalPassengerPrice),'#.##')*100)"/>
			
		<!-- -->
		<xsl:attribute name="TypeTotalPrice">
			<xsl:value-of select="round(format-number(($INF_Fees),'#.00') * 100) + $TotalBaggageFees" />
		</xsl:attribute>

		<xsl:element name="Price">
			<xsl:attribute name="Total">
				<xsl:value-of select="round(format-number($INF_Fees div $TotalPax,'#.00') * 100)" />
			</xsl:attribute>

			<xsl:element name="BaseFare">
				<xsl:attribute name="Amount">
						<xsl:value-of select="round(format-number('00','#.00') * 100)" />
					</xsl:attribute>
			</xsl:element>
			<xsl:element name="Taxes">
				<xsl:attribute name="Amount">
						<xsl:value-of select="round(format-number($INF_Fees div $TotalPax ,'#.00') * 100)" />
					</xsl:attribute>

				<xsl:element name="Tax">
					<xsl:attribute name="Amount"> 
							<xsl:value-of select="round(format-number($INF_Fees div $TotalPax,'#.00') * 100)" />
						</xsl:attribute>
					<xsl:element name="Description">
						<xsl:value-of select="'Service Fee (INF)'" />
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- FareRules -->
	<xsl:template name="JourneyInfo">
		<xsl:param name="CurrentPaxType" />
		<xsl:element name="FareInfo">
			<xsl:element name="DepartureCode">
				<xsl:value-of select="Segments/Segment[1]/Legs/Leg[1]/DepartureStation" />
			</xsl:element>
			<xsl:element name="DepartureDate">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="Segments/Segment/Legs/Leg[1]/STD" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="DepartureTime">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr" select="Segments/Segment/Legs/Leg[1]/STD" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="ArrivalCode">
				<xsl:value-of select="Segments/Segment[last()]/Legs/Leg[last()]/ArrivalStation" />
			</xsl:element>
			<xsl:element name="ArrivalDate">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="Segments/Segment/Legs/Leg[last()]/STA" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="ArrivalTime">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr" select="Segments/Segment/Legs/Leg[last()]/STA" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="AirlineCode">
				<xsl:value-of select="Segments/Segment/Legs/Leg[1]/FlightDesignator/CarrierCode" />
			</xsl:element>
			<xsl:element name="FareBasisCode">
				<xsl:value-of select="Segments/Segment/Fares/Fare[1]/FareBasisCode" />
			</xsl:element>
			
			<xsl:for-each select="Segments/Segment">
				<xsl:variable name="ClassOfService" select="Fares/Fare/ClassOfService" />

				<xsl:call-template name="SegmentInfo">
					<xsl:with-param name="Class" select="$ClassOfService" />
				</xsl:call-template>
			</xsl:for-each>

		</xsl:element>
	</xsl:template>
	
	<!-- FareRules - Related Segment -->
	<xsl:template name="SegmentInfo">
		<xsl:param name="Class" />
		<xsl:element name="RelatedSegment">
			
			<xsl:element name="SegmentIDRef">
				<!-- GWIA-52 Issue Implementation -->
				<xsl:variable name="counterforSegment" select="intinc:getNext($counter_segmentObject)" />
				<xsl:attribute name="FlightRefKey">
					<xsl:value-of select="concat('F', $counterforSegment)" />
				</xsl:attribute>
				<xsl:value-of select="$counterforSegment" />
				<!-- End GWIA-52 Issue Implementation -->
			</xsl:element>

			<xsl:element name="DepartureCode">
				<xsl:value-of select="DepartureStation" />
			</xsl:element>
			<xsl:element name="ArrivalCode">
				<xsl:value-of select="ArrivalStation" />
			</xsl:element>

			<xsl:element name="DepartureDate">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="STD" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="ArrivalDate">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="STA" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="ArrivalTime">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr" select="STA" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>

			<xsl:element name="DepartureTime">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="datestr" select="STD" />
					<xsl:with-param name="outformat" select="string('hh-mm')" />
					<xsl:with-param name="informat" select="string('datetime')" />
					<xsl:with-param name="format" select="'time'" />
				</xsl:call-template>
			</xsl:element>

			<xsl:element name="ClassOfService">
				<xsl:value-of select="Fares/Fare/FareClassOfService" />
			</xsl:element>

			<!-- <xsl:element name="BaggageAllowance">
					
			</xsl:element> -->
			<!--GWIA - 21  -->
			<xsl:element name="NVB">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="STD" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="NVA">
				<xsl:call-template name="dateformat">
					<xsl:with-param name="format" select="'date'" />
					<xsl:with-param name="datestr" select="STD" />
					<xsl:with-param name="outformat" select="string('yyyy-mm-dd')" />
					<xsl:with-param name="informat" select="string('datetime')" />
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="AirlineCode">
					<xsl:value-of select="//Segments/Segment/Legs/Leg[1]/FlightDesignator/CarrierCode"/>
			</xsl:element> 
		</xsl:element>
	</xsl:template>

	<!-- Tag for Flight Rules Ends -->
	
	
	<!--Baggage Implementation Starts -->
	<xsl:template match="//GetBookingResponse//Journeys/Journey" mode="Baggage">
	
		<xsl:variable name="count_Journey" select="count(//Journeys/Journey)" />
		<xsl:variable name="Current_Journey" select="current()" />
		<xsl:variable name="Counter_Journey" select="intinc:getNext($counter_JourneyObj)" />
		
		<xsl:variable name="baggage_counter_reset" select="intinc:resetCounter($baggage_counter)" />
		<xsl:if test="$baggage_counter_reset"/>
		
		 <xsl:variable name="baggage_counter_infant_reset" select="intinc:resetCounter($baggage_counter_for_infant_passnege)" />
 		 <xsl:if test="$baggage_counter_infant_reset"/>
 		 
 		 <xsl:variable name="currentJounreySellKey" select="current()/JourneySellKey" />
		
		<xsl:if test="(//GetBookingResponse/Booking)[last()]//Passengers//Passenger//PassengerFees//PassengerFee[SSRCode !='INFT' and SSRCode != ''] ">
			
			<xsl:element name="BaggageData">
				<xsl:element name="Allowance">
					<xsl:variable name="PassengerFeeCount" select="count(PassengerFees/PassengerFee[SSRCode !='INFT'])" />

					<xsl:call-template name="TravelerElementNumber" />
					
					<xsl:call-template name="SegmentIDRef">
						<xsl:with-param name="currentJourney" select="$Current_Journey" />
						<xsl:with-param name="counterJourney" select="$Counter_Journey" />
					</xsl:call-template>


					<xsl:element name="MSCarrier">
						<xsl:value-of select="$CARRIER_CODE_FOR_JS" />
					</xsl:element>

				</xsl:element>
				
				<xsl:element name="CarryOnAllowance">
					<xsl:call-template name="TravelerElementNumber" />

					<xsl:call-template name="SegmentIDRef">
						<xsl:with-param name="currentJourney" select="$Current_Journey" />
						<xsl:with-param name="counterJourney" select="$Counter_Journey" />
					</xsl:call-template>

					<xsl:element name="MSCarrier">
						<xsl:value-of select="$CARRIER_CODE_FOR_JS" />
					</xsl:element>

				</xsl:element>
				
				<xsl:element name="Bags">
					<xsl:for-each select="(//GetBookingResponse//Booking)[last()]//Passengers/Passenger">
						
						<xsl:variable name="PassengerFeeCount" select="count(PassengerFees/PassengerFee[SSRCode !='INF'])" />
						<xsl:variable name="PassengerFeeCountWithInf" select="count(PassengerFees/PassengerFee)" />
						<xsl:variable name="baggage_counter_infant_reset" select="intinc:resetCounter($baggage_counter_for_infant_passnege)" />
 						<xsl:if test="$baggage_counter_infant_reset"></xsl:if>
						
						
						<!-- create bag tags only for pax having bags -->
						<xsl:if test="contains(current()/PassengerFees/PassengerFee/SSRCode, 'BG')">
							
							<xsl:call-template name="BaggageTemplate">
								<xsl:with-param name="from" select="1" />
								<xsl:with-param name="to" select="$PassengerFeeCountWithInf" />
								<xsl:with-param name="currentPassengerFees" select="current()/PassengerFees" />
								<xsl:with-param name="currentJounreySellKey" select="$currentJounreySellKey" />
								<xsl:with-param name="currentJourney" select="$Current_Journey" />
								<xsl:with-param name="counterJourney" select="$Counter_Journey" />
							</xsl:call-template>
						</xsl:if>
						
					</xsl:for-each>
				</xsl:element>
			</xsl:element>
			
		</xsl:if>
	</xsl:template>
	
	<!--TravelerInfo for Baggage -->
	<xsl:template name="TravelerElementNumber">

		<xsl:for-each select="(//GetBookingResponse/Booking)[last()]//Passengers/Passenger">

			<xsl:variable name="currentPassengerNumber" select="PassengerNumber" />
			<xsl:if test="count(PassengerFees/PassengerFee[SSRCode !='INF'])>0">
				<xsl:element name="TravelerElementNumber">
					<!-- <xsl:value-of select="$counter_passenger" /> -->
					<xsl:value-of select="$currentPassengerNumber + 1" />
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	
	<!--SegmentInfo for Baggage -->
	<xsl:template name="SegmentIDRef">

		<xsl:param name="currentJourney" />
		<xsl:param name="counterJourney" />

		<xsl:variable name="counter_SegmentObj" select="intinc:new(1)" />

		<xsl:for-each select="$currentJourney/Segments/Segment">

			<xsl:variable name="counter_Segment" select="intinc:getNext($counter_SegmentObj)" />
			<xsl:variable name="count_segment" select="count(//Booking/Journeys/Journey[1]/Segments/Segment)" />
			<xsl:choose>
				<xsl:when test="$counterJourney > 1">
					<xsl:element name="SegmentElementNumber">
						<xsl:value-of select="($counter_Segment + $count_segment)" />
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="SegmentElementNumber">
						<xsl:value-of select="$counter_Segment" />
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>

	</xsl:template>
	
	<!-- Template for Bags -->
	<xsl:template name="BaggageTemplate">
		<xsl:param name="currentPassengerFees" />
		<xsl:param name="from" />
		<xsl:param name="to" />
		<xsl:param name="currentJounreySellKey" />
		<xsl:param name="currentJourney" />
		<xsl:param name="counterJourney" />
		
		<xsl:variable name="currentJounreySellKeyaftreremoval" select="normalize-space(translate($currentJounreySellKey,'~', ''))" />
	
		<xsl:if test="$from &lt;= $to">
			<xsl:if test="not($currentPassengerFees/PassengerFee[$from]/SSRCode = 'INF') ">
			
				<xsl:variable name="currentFlightRefKey" select="normalize-space($currentPassengerFees/PassengerFee[$from]/FlightReference)" />
				<xsl:variable name="subStringofFlightRefKey" select="substring($currentFlightRefKey,10,((string-length($currentFlightRefKey) - 3)-9))" />

				<xsl:if test="contains($currentJounreySellKeyaftreremoval,$subStringofFlightRefKey)">
				
				
					<xsl:element name="Bag">
					
						<xsl:variable name="counterforBag" >
							<xsl:value-of select="intinc:getNext($baggage_counter)" />
						</xsl:variable>
						
						<xsl:attribute name="Type">
							<xsl:value-of select="concat('B',$counterforBag)" />
						</xsl:attribute>
						
						<xsl:element name="Description">
							<xsl:variable name="currentBagDesc" select="$currentPassengerFees/PassengerFee[$from]/SSRCode" />
							<xsl:value-of select="$Baggage_Lookup[FLXCode=$currentBagDesc]/JSCode" />
						</xsl:element>
						
						<!-- serviceprice -->
						<xsl:element name="ServicePrice">
							
							<!-- calculate basePrice -->
							<xsl:variable name="BasePriceAmount">
								<xsl:choose>
									<xsl:when test="(count($currentPassengerFees/PassengerFee[$from]/ServiceCharges/BookingServiceCharge[ChargeType='ServiceCharge'])>0) ">
										<xsl:value-of
											select="round(format-number(($currentPassengerFees/PassengerFee[$from]/ServiceCharges/BookingServiceCharge[ChargeType='ServiceCharge']/Amount),'#.00') * 100)" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'0'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							
							<!-- calculate TaxAmount -->
							<xsl:variable name="TaxAmount">
								<xsl:choose>
									<xsl:when test="(count($currentPassengerFees/PassengerFee[$from]/ServiceCharges/BookingServiceCharge[ChargeType!='ServiceCharge'])>0) ">
										<xsl:value-of
											select="round(format-number(($currentPassengerFees/PassengerFee[$from]/ServiceCharges/BookingServiceCharge[ChargeType !='ServiceCharge']/Amount),'#.00') * 100)" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="'0'" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:variable>
							
							<xsl:attribute name="Total">
								<xsl:value-of select="$BasePriceAmount + $TaxAmount" />
							</xsl:attribute>
							
							<xsl:element name="BasePrice">
								<xsl:attribute name="Amount">
									<xsl:value-of select="$BasePriceAmount"/>
								</xsl:attribute>
							</xsl:element>
							
							<xsl:element name="Taxes">
								<xsl:attribute name="Amount">
									<xsl:value-of select="$TaxAmount" />
								</xsl:attribute>
								<xsl:element name="Tax">
									<xsl:attribute name="Amount">
										<xsl:value-of select="$TaxAmount" />
									</xsl:attribute>
								</xsl:element>
							</xsl:element>
							
							
						</xsl:element>
						
						<!-- CurrencyCode -->
						<xsl:element name="CurrencyCode">
							<xsl:attribute name="NumberOfDecimals">
								<xsl:value-of select="$CURRENCY_NUMBER_OF_DECIMAL" />
							</xsl:attribute>
							<xsl:value-of select="$currentPassengerFees/PassengerFee/ServiceCharges/BookingServiceCharge/CurrencyCode" />
						</xsl:element>
											
						<!-- TravelElementNumber for baggage -->
						<xsl:element name="TravelerElementNumber">
							<xsl:value-of select="position()" />
						</xsl:element>
						
						<xsl:call-template name="SegmentIDRef">
							<xsl:with-param name="currentJourney" select="$currentJourney" />
							<xsl:with-param name="counterJourney" select="$counterJourney" />
						</xsl:call-template>
						
					</xsl:element>
				</xsl:if>
			</xsl:if><!-- loop of not infant -->
			
			<xsl:call-template name="BaggageTemplate">
				<xsl:with-param name="from" select="$from + 1" />
				<xsl:with-param name="to" select="$to" />
				<xsl:with-param name="currentPassengerFees" select="$currentPassengerFees" />
				<xsl:with-param name="currentJounreySellKey" select="$currentJounreySellKey" />
				<xsl:with-param name="currentJourney" select="$currentJourney" />
				<xsl:with-param name="counterJourney" select="$counterJourney" />
			</xsl:call-template>
		</xsl:if><!-- loop of from to -->
		
	</xsl:template>
	
	<!-- INTA-60 (ref.GWIA-29) GeneralRemark -->
	<xsl:template match="GeneralRemark">

		<xsl:element name="GeneralRemark">
			<!-- GWIA-29 -->
			<xsl:attribute name="Source"><xsl:value-of select="$SOURCE"/></xsl:attribute>
			<xsl:attribute name="SourceRef"><xsl:value-of select="//GetBookingResponse/Booking/RecordLocator"/></xsl:attribute>
			<xsl:element name="ElementNumber">
				<xsl:value-of select="intinc:getNext($counter_global)" />
			</xsl:element>
			<xsl:element name="Text">
				<xsl:value-of select="Text" />
			</xsl:element>

		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>