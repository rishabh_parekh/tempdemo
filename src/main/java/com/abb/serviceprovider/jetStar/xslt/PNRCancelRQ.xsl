<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">

	<xsl:output method="xml" indent="yes" />

	<xsl:template match="ABBService">
		<xsl:element name="a:CancelRequest">
			<xsl:apply-templates select="//PNRCancelRQ" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="//PNRCancelRQ">
		<xsl:element name="CancelRequestData">
			<xsl:element name="CancelBy">
				<xsl:value-of select="'All'" />
			</xsl:element>
			
			<!-- REMOVED THIS TAG: AS PER JQ SCHEMA THIS IS TO BE USED ONLY IN CASE OF CANCELBY:=FEE -->
			<!-- <xsl:element name="CancelFee">
				<xsl:element name="FeeRequest">
					<xsl:element name="PassengerNumber">
						<xsl:value-of select="'0'" />
					</xsl:element>
					<xsl:element name="FeeNumber">
						<xsl:value-of select="'0'" />
					</xsl:element>
				</xsl:element>
			</xsl:element> -->

		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
