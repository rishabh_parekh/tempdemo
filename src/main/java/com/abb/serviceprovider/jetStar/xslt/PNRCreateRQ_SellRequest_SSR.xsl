<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:intinc="com.abb.framework.util.IntIncrement" 
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common">

	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />
	<xsl:strip-space elements="*" />
	<xsl:variable name="counter_segmentObject" select="intinc:new(1)" />

	<xsl:template match="/">
		<!-- SellRequest -->
		<xsl:element name="a:SellRequest">
			<!-- SellRequestData -->
			<xsl:element name="a:SellRequestData">
				<xsl:element name="SellBy">
					<xsl:value-of select="'SSR'" />
				</xsl:element>
				<!-- SellJourney -->
				<xsl:element name="SellSSR">
					<xsl:element name="SSRRequest">
						<!-- Journeys -->
						<xsl:element name="SegmentSSRRequests">
							<xsl:apply-templates select="//SARequest/CustomItineraryData/Journeys/Journey/Segments/Segment" />
						</xsl:element>
						
						<xsl:element name="CurrencyCode">
							<xsl:value-of select="//SARequest/CustomItineraryData/Flight/Fare/PaxFares/PaxFare//BookingServiceCharge/CurrencyCode" />
						</xsl:element>
						<xsl:element name="CancelFirstSSR">
							<xsl:value-of select="'false'" />
						</xsl:element>
						<xsl:element name="SSRFeeForceWaiveOnSell">
							<xsl:value-of select="'false'" />
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>

	</xsl:template>
	
	
	<!-- SegmentSSRRequest -->
	<xsl:template match="Journeys/Journey/Segments/Segment">
		<xsl:variable name="counterforSegment" select="intinc:getNext($counter_segmentObject)" />
		<xsl:variable name="currentFlight" select="//SARequest//PNRCreateRQ//Flight[$counterforSegment]"/>
		
		<xsl:variable name="departDate" select="$currentFlight/Departure/Date"/>
		<xsl:variable name="departTime" select="$currentFlight/Departure/Time"/>
		<xsl:variable name="departDateTime" select="concat($departDate, 'T',$departTime , ':00')"/>
		
		
		<xsl:element name="SegmentSSRRequest">

			<xsl:element name="FlightDesignator">
				<xsl:element name="com:CarrierCode">
					<xsl:value-of select="$currentFlight/Carrier/AirlineCode"/>
				</xsl:element>
				<xsl:element name="com:FlightNumber">
					<xsl:value-of select="$currentFlight/Carrier/FlightNumber"/>
				</xsl:element>
			</xsl:element>

			<xsl:element name="STD">
				<xsl:value-of select="$departDateTime"/>
			</xsl:element>
			
			<xsl:variable name="departureStation" select="$currentFlight/Departure/AirportCode"/>
			<xsl:variable name="arrivalStation" select="$currentFlight/Arrival/AirportCode"/>
			<xsl:element name="DepartureStation">
				<xsl:value-of select="$currentFlight/Departure/AirportCode"/>
			</xsl:element>
			<xsl:element name="ArrivalStation">
				<xsl:value-of select="$currentFlight/Arrival/AirportCode"/>
			</xsl:element>
	
			<!-- PAXSSRs template -->
			<xsl:element name="PaxSSRs">
				<xsl:apply-templates select="PaxSSRs/PaxSSR"/>
				
				<!-- INFANT PaxSSR -->
				<!-- <xsl:for-each select="//Traveler//Infant">
					<xsl:call-template name="InfantPaxSSR">
						<xsl:with-param name="departureStation">
							<xsl:value-of select="$departureStation"/>
						</xsl:with-param>
						<xsl:with-param name="arrivalStation">
							<xsl:value-of select="$arrivalStation"/>
						</xsl:with-param>
					</xsl:call-template>
				</xsl:for-each> -->
				
			</xsl:element>

			<xsl:element name="CancelFirstSSR">
				<xsl:value-of select="'false'" />
			</xsl:element>
			<xsl:element name="SSRFeeForceWaiveOnSell">
				<xsl:value-of select="'false'" />
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- PAXSSRs template -->
	<xsl:template match="PaxSSRs/PaxSSR">
		<xsl:element name="PaxSSR">
			<xsl:element name="State">
				<xsl:value-of select="State"/>
			</xsl:element>
			<xsl:element name="ActionStatusCode">
				<xsl:value-of select="ActionStatusCode"/>
			</xsl:element>
			<xsl:element name="ArrivalStation">
				<xsl:value-of select="ArrivalStation"/>
			</xsl:element>
			<xsl:element name="DepartureStation">
				<xsl:value-of select="DepartureStation"/>
			</xsl:element>
			<xsl:element name="PassengerNumber">
				<xsl:value-of select="PassengerNumber"/>
			</xsl:element>
			<xsl:element name="SSRCode">
				<xsl:value-of select="SSRCode"/>
			</xsl:element>
			<xsl:element name="SSRNumber">
				<xsl:value-of select="SSRNumber"/>
			</xsl:element>
			<xsl:element name="SSRDetail">
				<xsl:value-of select="SSRDetail"/>
			</xsl:element>
			<xsl:element name="FeeCode">
				<xsl:value-of select="FeeCode"/>
			</xsl:element>
			<xsl:element name="Note">
				<xsl:value-of select="Note"/>
			</xsl:element>
			<xsl:element name="SSRValue">
				<xsl:value-of select="SSRValue"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	
	<!-- Infant PaxSSR -->
	<!-- <xsl:template name="InfantPaxSSR">
		<xsl:param name="departureStation"/>
		<xsl:param name="arrivalStation"/>
		<xsl:element name="PaxSSR">
			<xsl:element name="com:State">
				<xsl:value-of select="'New'" />
			</xsl:element>
			<xsl:element name="ActionStatusCode">
				<xsl:value-of select="'SS'" />
			</xsl:element>
			<xsl:element name="ArrivalStation">
				<xsl:value-of select="$arrivalStation"/>
			</xsl:element>
			<xsl:element name="DepartureStation">
				<xsl:value-of select="$departureStation"/>
			</xsl:element>
			<xsl:element name="PassengerNumber">
				<xsl:value-of select="0" />
			</xsl:element>
			<xsl:element name="SSRCode">
				<xsl:value-of select="'INFT'" />
			</xsl:element>						
			<xsl:element name="SSRNumber">
				<xsl:value-of select="'0'" />
			</xsl:element>
			<xsl:element name="SSRValue">
				<xsl:value-of select="'0'" />
			</xsl:element>
		</xsl:element>
	</xsl:template> -->
	
</xsl:stylesheet>