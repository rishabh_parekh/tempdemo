<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking"
	xmlns:a="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService"
	xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common"
	xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">


	<xsl:output method="xml" indent="yes" />

	<xsl:template match="ABBService">
		<xsl:element name="a:CommitRequest">
			<xsl:apply-templates select="//PNRCancelRQ" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="PNRCancelRQ">
		<xsl:element name="BookingRequest">

			<xsl:variable name="ReceivedBy" select="'AERTicket Conso'" />
			<xsl:element name="Booking">
				<xsl:element name="RecordLocator">
					<xsl:value-of select="RecordLocator" />
				</xsl:element>

				<xsl:element name="ReceivedBy">
					<xsl:element name="ReceivedBy">
						<xsl:value-of select="$ReceivedBy" />
					</xsl:element>
				</xsl:element>
			</xsl:element>

			<xsl:element name="RestrictionOverride">
				<xsl:value-of select="'false'" />
			</xsl:element>

			<xsl:element name="ChangeHoldDateTime">
				<xsl:value-of select="'false'" />
			</xsl:element>
			<xsl:element name="WaiveNameChangeFee">
				<xsl:value-of select="'false'" />
			</xsl:element>
			<xsl:element name="WaivePenaltyFee">
				<xsl:value-of select="'false'" />
			</xsl:element>
			<xsl:element name="WaiveSpoilageFee">
				<xsl:value-of select="'false'" />
			</xsl:element>
			<xsl:element name="DistributeToContacts">
				<xsl:value-of select="'false'" />
			</xsl:element>

		</xsl:element>
	</xsl:template>

</xsl:stylesheet>