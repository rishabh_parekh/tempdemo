<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:variable name="CURRENCY_CODE" select="'USD'" />
	<xsl:variable name="CURRENCY_NUMBER_OF_DECIMAL" select="'2'" />
	<xsl:variable name="DEFAULT_FARETYPE" select="'PUBL'" />
	<xsl:variable name="DEFAULT_FARETYPE_NONPREBOOKING" select="'NFR'" />
	<xsl:variable name="AVAILABILITY_TYPE" select="'Default'" />
	<xsl:variable name="SOURCE" select="'JQ'" />
	<xsl:variable name="SOURCE_NAME" select="'Jetstar'" />
	<xsl:variable name="DOW_DAILY" select="'Daily'" />
	<xsl:variable name="CARRIER_CODE_FOR_JS" select="'JQ'" />
	<xsl:variable name="FLIGHT_TYPE_ALL" select="'All'" />	
	<xsl:variable name="MAXIMUM_CONNECTINT_FLIGHTS" select="'4'" />
	<xsl:variable name="AVAILAIBLIY_FILTER" select="'ExcludeUnavailable'" />
	<xsl:variable name="FARE_CLASS_CONTROL" select="'Default'" />
	<xsl:variable name="MINUMM_FARE_PRICE" select="'0'" />
	<xsl:variable name="MAXIMUM_FARE_PRICE" select="'0'" />
	<xsl:variable name="SSR_COLLECTION_MODE" select="'All'" />
	<xsl:variable name="INBOUND_AND_OUTBOUND" select="'Both'" />
	<xsl:variable name="NIGHTSTAY" select="'0'" />
	<xsl:variable name="INCLUDEALLOTMENTS" select="'false'" />
	<xsl:variable name="FARETYPE" select="'R'" />
	<xsl:variable name="JOURNEY_SORT_KEY" select="'EarliestDeparture'" />
	<xsl:variable name="INCLUDE_TAX_AND_FEE" select="'true'" />
	<xsl:variable name="STATE" select="'New'"/>
	<xsl:variable name="ACTION_STATUS_CODE" select="'NN'"/>
	<xsl:variable name="SSRCODE" select="'INF'"/>
	<xsl:variable name="LOYALTY_FILTER" select="'MonetaryOnly'"/>
	<xsl:variable name="FARETYPE_STRING" select="'R'"/>
	<xsl:variable name="PAX_DISCOUNT_CODE" select="'50'"/>
	<xsl:variable name="SELLBY" select="'SSR'"/>
	<xsl:variable name="CANCEL_FIRST_SSR" select="'false'"/>
	<xsl:variable name="SSR_FEE_FORCE_WAIVE_ON_SELL" select="'false'"/>
	<xsl:variable name="RESTRICTI_ON_OVERRIDE" select="'false'"/>	
	<xsl:variable name="CHANGE_HOLD_DATE_TIME" select="'false'"/>	
	<xsl:variable name="WAIVE_NAME_CHANGE_FEE" select="'false'"/>	
	<xsl:variable name="WAIVE_PENALTY_FEE" select="'false'"/>	
	<xsl:variable name="WAIVE_SPOILAGE_FEE" select="'false'"/>	
	<xsl:variable name="DISTRIBUTE_TO_CONTACTS" select="'true'"/>
	<xsl:variable name="NOT_FOR_GENERAL_USE" select="'false'"/>	
	<xsl:variable name="TYPE_CODE" select="'P'"/>	
	<xsl:variable name="FARE_NUMBER" select="'1'" />
	<xsl:variable name="FARE_PRICED" select="'AUTO'" />
	<xsl:variable name="REFERENCETYPE" select="'Default'"/>
	<xsl:variable name="PAYMENT_METHOD_TYPE" select="'ExternalAccount'"/>
	<xsl:variable name="STATUS" select="'New'"/>
	<xsl:variable name= "ACCOUNT_NUM_ID" select="'0'"/>
	<xsl:variable name="ACCOUNT_NUMBER" select="'5555555555000026'"/>
	<xsl:variable name="EXPIRATION" select="'2015-05-20T00:00:00'"/>
	<xsl:variable name="PARENT_PAYMENT_ID" select="'0'"/>
	<xsl:variable name="INSTALLMENTS" select="'0'"/>
	<xsl:variable name="DEPOSIT" select="'false'"/>
	<xsl:variable name="PAYMENT_FIELD_NAME" select="'CC::AccountHolderName'"/>
	<xsl:variable name="PAYMENT_FIELD_VALUE" select="'Account Name Test'"/>
	<xsl:variable name="KG15" select="'Baggage 15kg'" />
	<xsl:variable name="KG20" select="'Baggage 20kg'" />
	<xsl:variable name="KG25" select="'Baggage 25kg'" />
	<xsl:variable name="KG30" select="'Baggage 30kg'" />
	<xsl:variable name="PRICING_VARIANCE_ERROR_CODE" select="'OGW_JQ01'" />
	<xsl:variable name="PRICING_VARIANCE_FOUND" select="'Pricing variance found'" />
	<xsl:variable name="PRICING_VARIANCE_NOT_FOUND" select="'Pricing variance not found'" />
	<xsl:variable name="JETAIRWAYS_SOURCE" select="'9W'" />
	<xsl:variable name="JETAIRWAYS_SOURCENAME" select="'JetAirways'" />
	<xsl:variable name="PNR_CANCELLED_ERROR_CODE" select="'OGW_JQ02'"/>
	<xsl:variable name="PNR_CANCELLED_ERROR_MSG" select="'This PNR has been Cancelled'"/>
</xsl:stylesheet>