package com.abb.serviceprovider.jetStar.util;


/**
 * The Class JSConstants.
 */
public class JSConstants {
	/**
	 * 10 minutes.
	 */
	public static final long SESSION_TIME_TO_LIVE = 600000;
	// INTA-30 true=enable false=disable
	/** The enable pricing variance check. */
	public static boolean enablePricingVarianceCheck = true;
}
