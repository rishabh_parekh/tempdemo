package com.abb.serviceprovider.jetStar;

import org.springframework.stereotype.Service;

import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;

/**
 * The Class JSWebServiceClient.
 */
@Service("jsWebServiceClient")
public class JSWebServiceClient extends SOAPWebServiceTemplate {

}