package com.abb.serviceprovider.jetStar.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.abb.framework.entity.SOAPServiceEnvelopXML;
import com.abb.framework.serviceprovider.SPService;
import com.abb.serviceprovider.jetStar.JSWebServiceClient;


/**
 * This class calls logout service of JetStar.
 *
 * @author irfanak
 */
@Service("jsLogoutService")
public class JSLogoutService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JSLogoutService.class);

	/** The js web service client. */
	@Autowired
	private JSWebServiceClient jsWebServiceClient;

	/** The context. */
	@Autowired
	private ApplicationContext context;

	/** The signature. */
	private String signature;

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {

		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		soapEnvelopXML.setSpSOAPRequestBodyXML("<ses:LogoutRequest xmlns:ses=\"http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService\"></ses:LogoutRequest>");
		soapEnvelopXML.setSpSOAPRequestHeaderXML(getLogoutXMLHeader());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());

		String spLogoutResponse = jsWebServiceClient.sendAndReceive(soapEnvelopXML);
		LOG.info("spLogoutResponse = {} ", spLogoutResponse);

	}

	/**
	 * This method can be removed once execute() method is adjusted/verified.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String executeLogOut() throws Exception {

		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		soapEnvelopXML.setSpSOAPRequestBodyXML("<ses:LogoutRequest xmlns:ses=\"http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService\"></ses:LogoutRequest>");
		soapEnvelopXML.setSpSOAPRequestHeaderXML(getLogoutXMLHeader());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());
		String spLogoutResponse = jsWebServiceClient.sendAndReceive(soapEnvelopXML);

		LOG.info("spLogoutResponse = {}", spLogoutResponse);
		return spLogoutResponse;
	}

	/**
	 * Create and return header XML of Logout Service.
	 *
	 * @return the logout xml header
	 */
	private String getLogoutXMLHeader() {
		StringBuilder headerStringBuilder = new StringBuilder();

		headerStringBuilder.append("<web:Signature xmlns:web=\"http://schemas.navitaire.com/WebServices\">");
		headerStringBuilder.append(signature);
		headerStringBuilder.append("</web:Signature>");

		return headerStringBuilder.toString();
	}

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JSLogoutService";
	}

	/**
	 * Gets the signature.
	 *
	 * @return the signature
	 */
	public String getSignature() {
		return signature;
	}

	/**
	 * Sets the signature.
	 *
	 * @param signature the new signature
	 */
	public void setSignature(String signature) {
		this.signature = signature;
	}

}
