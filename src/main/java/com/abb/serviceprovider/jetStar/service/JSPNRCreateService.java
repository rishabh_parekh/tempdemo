package com.abb.serviceprovider.jetStar.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.abb.framework.serviceprovider.SPRequest;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.transformation.OpenAirTransformation;
import com.abb.framework.util.Utils;
import com.abb.framework.util.XMLUtils;
import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.common.CommonConstant;
import com.abb.serviceprovider.jetStar.util.JSConstants;

/**
 * The Class JSPNRCreateService.
 */
@Service("jsPNRCreateService")
public class JSPNRCreateService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JSPNRCreateService.class);

	/** The soap header. */
	private JSSOAPHeader soapHeader;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {
		LOG.debug("JSPNRCreateService.execute() method started.");
		String spRequestXML = "";
		String spRequestXMLForPricingVariance = "";
		String spResponseXML = null;
		String token = null;
		int i = 1;
		String isPricingVarianceFound = null;
		boolean isPassiveSegmentRQ = false;
		String isINFTRequested;
		String isSSRRequested;

		// INTA-57
		SOAPWebServiceTemplate jsWebServiceClient = ApplicationContextUtil.getBean("jsWebServiceClient", SOAPWebServiceTemplate.class);
		JSLoginService jsLoginService = ApplicationContextUtil.getBean("jsLoginService", JSLoginService.class);
		JSLogoutService jsLogoutService = ApplicationContextUtil.getBean("jsLogoutService", JSLogoutService.class);
		OpenAirTransformation xmlTransformer = ApplicationContextUtil.getBean("XMLTransformation", OpenAirTransformation.class);

		try {

			String loginresponse = jsLoginService.executeLogin();
			LOG.debug("\n JSPNRCreateService loginresponse=={}", loginresponse);
			token = StringUtils.substringBetween(loginresponse, "<Signature>", "</Signature>");
			LOG.info("JSPNRCreateService - got session - {}", token);

			// INTA-36 Passive Segment check
			String requestXML = getOpenAirlinesServiceEnvelopXML().getSaRequestXML();
			String flightType = XMLUtils.getAttributeValue(requestXML, "Flight", "Type");
			if ("P".equalsIgnoreCase(flightType)) {
				// Start Process for Adding Passive Segment of 9W into JQ
				processForAddingPassiveSegment(loginresponse, token, jsWebServiceClient, xmlTransformer);
				isPassiveSegmentRQ = true;
			} else {
				// Check if Infant passenger exists in PNRCreateRQ
				isINFTRequested = StringUtils.substringBetween(requestXML, "<Infant", "</Infant>");
				// Check if SSR/Baggage exists in PNRCreateRQ
				isSSRRequested = StringUtils.substringBetween(requestXML, "<SpecialServiceRequest>", "</SpecialServiceRequest>");

				for (SPRequest spRequest : getSpRequests()) {

					// Get Session Response and add to Service Envelop
					getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(loginresponse);

					// Get SOAP Header and add to Service Envelop
					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestHeaderXML(soapHeader.getJSSOAPHeader(getOpenAirlinesServiceEnvelopXML().getServiceXML(), token));

					// Skip execution of Pricing variance xslt
					if (i == 5) {
						// Complete the execution of For loop
						continue;
					}

					if (i == 2) {
						// IF EITHER OF THEM DOESN'T EXIST INCREMENT i AND COMPLETE LOOP
						if (Utils.isNull(isINFTRequested) && Utils.isNull(isSSRRequested)) {
							i++;
							continue;
						}
					}

					// Transform to Service Provide request and add to Service
					// Envelop
					LOG.info("JSPNRCreateService ****** Before transformatoin == ");
					spRequestXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), spRequestXML, spRequest.getRequestXSLTAbsolutePath(), null);
					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestBodyXML(spRequestXML);
					LOG.info("spRequestXML =={}", spRequestXML);

					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPAction(spRequest.getSoapAction());
					getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPServiceURL(spRequest.getServiceURL());

					// Send request to web service client
					spResponseXML = jsWebServiceClient.sendAndReceive(getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML());
					LOG.info("spResponseXML =={}", spResponseXML);

					// changed to appendSpResponseXML : 16Jun
					getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spResponseXML);

					// INTA-30 Pricing Variance Check only once at time of SellRequest hence i=1.
					// Flag defined in constants to enable disable check of pricing variance
					if (JSConstants.enablePricingVarianceCheck) {

						// CASE 1 : i=1 (SELL RQ), CHECK IF SSR (bag/inft) IS NOT REQUESTED, IF REQUESTED CHECK PRICE VARIANCE FROM SELLSSR RESPONSE, HENCE CHECK CASE :2
						// CASE 2 : i=2 (SELLSSR RQ), CHECK IF EITHER INF AND SSR (BAGGAGE) ARE REQUESTED, IF YES THEN COMPARE TOTALCOST FROM SellSSR response

						if ((i == 1 && (Utils.isNull(isINFTRequested) && Utils.isNull(isSSRRequested))) || (i == 2 && (Utils.isNotNull(isINFTRequested) || Utils.isNotNull(isSSRRequested)))) {
							// CHECK IF TOLERANCE VALUE TAG EXIST
							String isTagExisting = XMLUtils.getTagValue(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), "ToleranceValue");
							if (isTagExisting != null) {

								spRequestXMLForPricingVariance = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), "", spRequests.get(4).getRequestXSLTAbsolutePath(), null);
								LOG.info("spRequestXMLForPricingVariance for Pricing Variance=={}", spRequestXMLForPricingVariance);

								isPricingVarianceFound = StringUtils.substringBetween(spRequestXMLForPricingVariance, "<varianceFound>", "</varianceFound>");
								LOG.info("isPricingVarianceFound == {}", isPricingVarianceFound);

								// CASE 1. PRICING VARIANCE FOUND IN JQ, TERMINATE BOOKING PROCESS & REVERT WITH RESPONSE TO OAPI
								// CASE 2. <CallSellRQ>true</CallSellRQ> tag FOUND IN PNRCREATERQ, CALL ONLY SELLRQ AND REVERT WITH PRICING RESPONSE AND TERMINATE BOOKING
								String isCallSellRQTagExisting = XMLUtils.getTagValue(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), "CallSellRQ");

								if ("true".equalsIgnoreCase(isPricingVarianceFound) || "true".equalsIgnoreCase(isCallSellRQTagExisting)) {
									getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spRequestXMLForPricingVariance);
									break;
								}
							}
						}
					}
					i++;
				}
			}

		} catch (SoapFaultClientException e) {
			LOG.info("SoapFault Exception Occured");
			LOG.error(CommonConstant.ERROR_MSG, e);
			String response = Utils.getErrorResponseForFaultWithMessage(e);
			getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(response);
		} catch (Exception e) {
			LOG.error("Main Exception in JSPNRCreateService : {}", e.getMessage());
			LOG.error(CommonConstant.ERROR_MSG, e);
			throw e;
		} finally {
			if (StringUtils.isNotEmpty(token)) {
				jsLogoutService.setSignature(token);
				try {
					jsLogoutService.executeLogOut();
					LOG.info("Logout completed for token -  {}", token);
				} catch (Exception e) {
					LOG.error("Error occured during Logout for session token - {}", token, e);
				}

			}
		}

		String saResponseXML = "";
		// If PassiveSegment RQ then select 1st Response XSL from JetStar-Config else select 0th
		// getSpResponses().get(1).getResponseXSLTAbsolutePath()
		if (isPassiveSegmentRQ) {
			LOG.debug("Appended Service XML:: ==== {}", getOpenAirlinesServiceEnvelopXML().getServiceXML());
			saResponseXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), saResponseXML, getSpResponses().get(1).getResponseXSLTAbsolutePath(), null);
		} else {
			saResponseXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), saResponseXML, getSpResponses().get(0).getResponseXSLTAbsolutePath(), null);
		}

		getOpenAirlinesServiceEnvelopXML().setSaResponseXML(saResponseXML);
		LOG.info("JSPNRCreateService ****** saResponseXML == {}", saResponseXML);
		LOG.debug("JSPNRCreateService.execute() method completed.");
	}

	/**
	 * METHOD TO ADD PASSIVE SEGMENT OF 9W TO JQ - PNR CROSS REFERENCING.
	 * 
	 * @param loginresponse
	 *            the loginresponse
	 * @param token
	 *            the token
	 * @param xmlTransformer
	 * @param jsWebServiceClient
	 * @throws Exception
	 *             the exception
	 */
	private void processForAddingPassiveSegment(String loginresponse, String token, SOAPWebServiceTemplate jsWebServiceClient, OpenAirTransformation xmlTransformer) throws Exception {

		String spRequestXML = "";
		String spResponseXML = null;

		try {
			// Starting with K=5; As XSL files for Passive Segment start from count(starting from 0) 5 in JetStar-Config.xml
			int k = 5;
			for (SPRequest spRequest : getSpRequests()) {
				// k=9 Terminate loop when reached last count of XSL i.e. 9
				if (k == 9) {
					// Complete the execution of For loop
					continue;
				}
				// Get Session Response and add to Service Envelop
				getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(loginresponse);

				// Get SOAP Header and add to Service Envelop
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestHeaderXML(soapHeader.getJSSOAPHeader(getOpenAirlinesServiceEnvelopXML().getServiceXML(), token));

				// Transform to Service Provide request and add to Service Envelop
				spRequestXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), spRequestXML, getSpRequests().get(k).getRequestXSLTAbsolutePath(), null);
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestBodyXML(spRequestXML);
				LOG.info("spRequestXML =={}", spRequestXML);

				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPAction(getSpRequests().get(k).getSoapAction());
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPServiceURL(getSpRequests().get(k).getServiceURL());

				// Send request to web service client
				LOG.info("JSPNRCreateService ****** before calling send and receive == ");
				spResponseXML = jsWebServiceClient.sendAndReceive(getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML());

				LOG.info("spResponseXML =={}", spResponseXML);

				getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spResponseXML);

				k++;

			}
		} catch (SoapFaultClientException e) {
			LOG.info("SoapFault Exception Occured");
			LOG.error(CommonConstant.ERROR_MSG, e);
			String response = Utils.getErrorResponseForFaultWithMessage(e);
			getOpenAirlinesServiceEnvelopXML().setSpResponseXML(response);
		} catch (Exception e) {
			LOG.error(CommonConstant.ERROR_MSG, e);
			throw e;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JSPNRCreateService";
	}

	/**
	 * Gets the soap header.
	 * 
	 * @return the soap header
	 */
	public JSSOAPHeader getSoapHeader() {
		return soapHeader;
	}

	/**
	 * Sets the soap header.
	 * 
	 * @param soapHeader
	 *            the new soap header
	 */
	public void setSoapHeader(JSSOAPHeader soapHeader) {
		this.soapHeader = soapHeader;
	}

}
