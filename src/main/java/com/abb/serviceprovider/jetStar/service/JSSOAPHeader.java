package com.abb.serviceprovider.jetStar.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.abb.framework.exception.OpenAirTransformationException;
import com.abb.framework.transformation.OpenAirTransformation;


/**
 * The Class JSSOAPHeader.
 */
@Service("jsSOAPHeaer")
public class JSSOAPHeader {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JSSOAPHeader.class);
	
	/** The soap header xslt. */
	private Resource soapHeaderXSLT;

	/** The xml transformation. */
	@Autowired
	private OpenAirTransformation xmlTransformation;

	/**
	 * Gets the JSSOAP header.
	 *
	 * @param serviceEnvelopXML the service envelop xml
	 * @param sessionTocken the session tocken
	 * @return the JSSOAP header
	 * @throws OpenAirTransformationException the open air transformation exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getJSSOAPHeader(String serviceEnvelopXML, String sessionTocken) throws OpenAirTransformationException, IOException {

		LOG.info(" soapHeaderXSLT ={}", soapHeaderXSLT);
		LOG.info(" sessionTocken ={}", sessionTocken);
		String resultSOAPHeaderXML = "";
		Map<String, String> transformerParams = new HashMap<String, String>();
		transformerParams.put("SessionTocken", sessionTocken);
		return xmlTransformation.transform(serviceEnvelopXML, resultSOAPHeaderXML, soapHeaderXSLT.getFile().getAbsolutePath(), transformerParams);

	}

	/**
	 * Gets the soap header xslt.
	 *
	 * @return the soap header xslt
	 */
	public Resource getSoapHeaderXSLT() {
		return soapHeaderXSLT;
	}

	/**
	 * Sets the soap header xslt.
	 *
	 * @param soapHeaderXSLT the new soap header xslt
	 */
	public void setSoapHeaderXSLT(Resource soapHeaderXSLT) {
		this.soapHeaderXSLT = soapHeaderXSLT;
	}

}
