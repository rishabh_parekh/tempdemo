package com.abb.serviceprovider.jetStar.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.abb.framework.serviceprovider.SPRequest;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.transformation.OpenAirTransformation;
import com.abb.framework.util.Utils;
import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.common.CommonConstant;

/**
 * The Class JSPNRProcessPaymentService.
 */
@Service("jsPNRProcessPaymentService")
public class JSPNRProcessPaymentService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JSPNRProcessPaymentService.class);

	/** The soap header. */
	private JSSOAPHeader soapHeader;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {

		String spRequestXML = "";
		String spResponseXML = null;
		String token = null;
		int i = 1;
		// INTA-57
		SOAPWebServiceTemplate jsWebServiceClient = ApplicationContextUtil.getBean("jsWebServiceClient", SOAPWebServiceTemplate.class);
		JSLoginService jsLoginService = ApplicationContextUtil.getBean("jsLoginService", JSLoginService.class);
		JSLogoutService jsLogoutService = ApplicationContextUtil.getBean("jsLogoutService", JSLogoutService.class);
		OpenAirTransformation xmlTransformer = ApplicationContextUtil.getBean("XMLTransformation", OpenAirTransformation.class);

		try {

			// Get Session Response and add to Service Envelop
			String loginresponse = jsLoginService.executeLogin();
			LOG.debug("\n JSPNRCreateService loginresponse=={}", loginresponse);
			token = StringUtils.substringBetween(loginresponse, "<Signature>", "</Signature>");
			LOG.info("JSPNRCreateService - got session - {}", token);
			for (SPRequest spRequest : getSpRequests()) {
				getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(loginresponse);

				// Get SOAP Header and add to Service Envelop
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestHeaderXML(soapHeader.getJSSOAPHeader(getOpenAirlinesServiceEnvelopXML().getServiceXML(), token));

				// Transform to Service Provide request and add to Service
				// Envelop
				spRequestXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), spRequestXML, spRequest.getRequestXSLTAbsolutePath(), null);
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPRequestBodyXML(spRequestXML);

				if (i == 2) {
					String creditCardNumber = StringUtils.substringBetween(spRequestXML, "<AccountNumber>", "</AccountNumber>");
					String expirationDate = StringUtils.substringBetween(spRequestXML, "<Expiration>", "</Expiration>");
					if ("".equalsIgnoreCase(creditCardNumber)) {
						String maskedCCNumber = StringUtils.leftPad(creditCardNumber.substring(creditCardNumber.length() - 4), creditCardNumber.length(), 'X');
						spRequestXML = spRequestXML.replace(creditCardNumber, maskedCCNumber);

						String maskedExpirationDate = expirationDate.replaceAll("\\d", "X");
						spRequestXML = spRequestXML.replace(expirationDate, maskedExpirationDate);
					}

				}
				LOG.info("spRequestXML =={}", spRequestXML);

				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPAction(spRequest.getSoapAction());
				getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML().setSpSOAPServiceURL(spRequest.getServiceURL());

				// Send request to web service client
				spResponseXML = jsWebServiceClient.sendAndReceive(getOpenAirlinesServiceEnvelopXML().getSoapServiceEnvelopXML());

				LOG.info("spResponseXML =={}", spResponseXML);
				getOpenAirlinesServiceEnvelopXML().appendSpResponseXML(spResponseXML);

				i++;
			}
		} catch (SoapFaultClientException e) {
			LOG.info("SoapFault Exception Occured");
			LOG.error(CommonConstant.ERROR_MSG, e);
			String response = Utils.getErrorResponseForFaultWithMessage(e);
			getOpenAirlinesServiceEnvelopXML().setSpResponseXML(response);
		} catch (Exception e) {
			LOG.error(CommonConstant.ERROR_MSG, e);
			throw e;
		} finally {
			if (StringUtils.isNotEmpty(token)) {
				jsLogoutService.setSignature(token);
				try {
					jsLogoutService.executeLogOut();
					LOG.info("Logout completed for token - {} ", token);
				} catch (Exception e) {
					LOG.error("Error occured during Logout for session token - {} ", token, e);
				}

			}
		}

		String saResponseXML = "";
		saResponseXML = xmlTransformer.transform(this.getOpenAirlinesServiceEnvelopXML().getServiceXML(), saResponseXML, spResponse.getResponseXSLTAbsolutePath(), null);
		getOpenAirlinesServiceEnvelopXML().setSaResponseXML(saResponseXML);
		LOG.info("JSPNRProcessPaymentService ****** saResponseXML == {}", saResponseXML);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JSPNRProcessPaymentService";
	}

	/**
	 * Gets the soap header.
	 * 
	 * @return the soap header
	 */
	public JSSOAPHeader getSoapHeader() {
		return soapHeader;
	}

	/**
	 * Sets the soap header.
	 * 
	 * @param soapHeader
	 *            the new soap header
	 */
	public void setSoapHeader(JSSOAPHeader soapHeader) {
		this.soapHeader = soapHeader;
	}

}
