package com.abb.serviceprovider.jetStar.service;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.abb.framework.entity.SOAPServiceEnvelopXML;
import com.abb.framework.serviceprovider.SPService;
import com.abb.serviceprovider.jetStar.JSWebServiceClient;


/**
 * This class calls login service of JetStar.
 *
 * @author irfanak
 */
@Service("jsLoginService")
public class JSLoginService extends SPService {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JSLoginService.class);

	/** The js web service client. */
	@Autowired
	private JSWebServiceClient jsWebServiceClient;

	/** The context. */
	@Autowired
	private ApplicationContext context;

	/** The domain code. */
	private String domainCode;
	
	/** The agent name. */
	private String agentName;
	
	/** The password. */
	private String password;

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#execute()
	 */
	@Override
	public void execute() throws Exception {

		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		final StringBuilder loginRequestStringBuilder = new StringBuilder();
		loginRequestStringBuilder
				.append("<ses:LogonRequest xmlns:ses=\"http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService\" xmlns:ses1=\"http://schemas.navitaire.com/WebServices/DataContracts/Session\"><ses:logonRequestData><ses1:DomainCode>");
		loginRequestStringBuilder.append(domainCode);
		loginRequestStringBuilder.append("</ses1:DomainCode><ses1:AgentName>");
		loginRequestStringBuilder.append(agentName);
		loginRequestStringBuilder.append("</ses1:AgentName><ses1:Password>");
		loginRequestStringBuilder.append(password);
		loginRequestStringBuilder.append("</ses1:Password> </ses:logonRequestData></ses:LogonRequest>");

		LOG.info("Login Request== {}", loginRequestStringBuilder.toString());
		soapEnvelopXML.setSpSOAPRequestBodyXML(loginRequestStringBuilder.toString());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());
		LOG.info("JSLoginService get service xml before sending= {}", soapEnvelopXML.getServiceXML());
		String spLoginResponse = jsWebServiceClient.sendAndReceive(soapEnvelopXML);
		LOG.info("spLoginResponse = {}", spLoginResponse);
		getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(spLoginResponse);

	}

	/**
	 * This method returns spLoginResponse and can be removed once execute method is adjusted.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	public String executeLogin() throws Exception {
		LOG.debug("JSLoginService.executeLogin method started");
		SOAPServiceEnvelopXML soapEnvelopXML = context.getBean(SOAPServiceEnvelopXML.class);

		final StringBuilder loginRequestStringBuilder = new StringBuilder();
		loginRequestStringBuilder
				.append("<ses:LogonRequest xmlns:ses=\"http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService\" xmlns:ses1=\"http://schemas.navitaire.com/WebServices/DataContracts/Session\"><ses:logonRequestData><ses1:DomainCode>");
		loginRequestStringBuilder.append(domainCode);
		loginRequestStringBuilder.append("</ses1:DomainCode><ses1:AgentName>");
		loginRequestStringBuilder.append(agentName);
		loginRequestStringBuilder.append("</ses1:AgentName><ses1:Password>");
		loginRequestStringBuilder.append(password);
		loginRequestStringBuilder.append("</ses1:Password> </ses:logonRequestData></ses:LogonRequest>");

		LOG.info("Login Request=={}", loginRequestStringBuilder.toString());
		soapEnvelopXML.setSpSOAPRequestBodyXML(loginRequestStringBuilder.toString());
		soapEnvelopXML.setSpSOAPServiceURL(spRequests.get(0).getServiceURL());
		soapEnvelopXML.setSpSOAPAction(spRequests.get(0).getSoapAction());
		LOG.info("JSLoginService get service xml before sending= {}", soapEnvelopXML.getServiceXML());
		String spLoginResponse = jsWebServiceClient.sendAndReceive(soapEnvelopXML);
		LOG.info("spLoginResponse = {}", spLoginResponse);
		getOpenAirlinesServiceEnvelopXML().setSpLoginResponseXML(spLoginResponse);
		LOG.debug("JSLoginService.executeLogin method completed");
		return spLoginResponse;
	}

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceprovider.SPService#getSPServiceName()
	 */
	@Override
	public String getSPServiceName() {
		return "JSLoginService";
	}

	/**
	 * Gets the domain code.
	 *
	 * @return the domain code
	 */
	public String getDomainCode() {
		return domainCode;
	}

	/**
	 * Gets the agent name.
	 *
	 * @return the agent name
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the domain code.
	 *
	 * @param domainCode the new domain code
	 */
	public void setDomainCode(String domainCode) {
		this.domainCode = domainCode;
	}

	/**
	 * Sets the agent name.
	 *
	 * @param agentName the new agent name
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
