package com.abb.serviceprovider.airBB;

import org.springframework.stereotype.Service;

import com.abb.framework.webserviceclient.SOAPWebServiceTemplate;

/**
 * The Class ABBWebServiceClient.
 */
@Service("abbWebServiceClient")
public class ABBWebServiceClient extends SOAPWebServiceTemplate {

}
