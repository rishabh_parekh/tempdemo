package com.abb.serviceprovider.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * The Class ApplicationContextUtil.
 */
public class ApplicationContextUtil {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationContextUtil.class);

	/** The context. */
	private static ApplicationContext context;

	/**
	 * Instantiates a new application context util.
	 */
	private ApplicationContextUtil() {

	}

	/**
	 * Sets the application context.
	 * 
	 * @param ctx
	 *            the new application context
	 */
	public static void setApplicationContext(ApplicationContext ctx) {
		context = ctx;
	}

	/**
	 * Gets the bean.
	 * 
	 * @param <T>
	 *            the generic type
	 * @param name
	 *            the name
	 * @param requiredType
	 *            the required type
	 * @return the bean
	 */
	public static <T> T getBean(final String name, final Class<T> requiredType) {
		if (requiredType == null) {

			throw new IllegalArgumentException("requiredType is null");
		}
		return context.getBean(name, requiredType);
	}

	/**
	 * Gets the bean.
	 * 
	 * @param name
	 *            the name
	 * @return the bean
	 */
	public static Object getBean(final String name) {
		LOG.info("start ApplicationContextUtil get bean");
		if (name == null) {
			throw new IllegalArgumentException("name is null");
		}
		LOG.info("context.getBean(name)=== {} ", context.getBean(name));
		return context.getBean(name);
	}

}
