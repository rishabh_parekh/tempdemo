package com.abb.serviceprovider.common;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * The Class CommonConstant.
 */
public class CommonConstant {

	/** The app context. */
	@Autowired
	private ApplicationContext appContext;
	/** The Constant ERROR_MSG for Common Error Message. */
	public static final String ERROR_MSG = "OOPS! Error Occured:";

	/**
	 * INTA-69 Constant values for replacing accent chars with english
	 */

	// NOTE : while changing below two variable values ensure that if
	// any value is inserted or removed from any 2 of below variables than
	// their corresponding position value should be inserted or deleted in another variable

	public static final String accentChars = "�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�";
	public static final String englishChars = "a,A,a,A,ae,AE,a,A,a,A,a,A,ae,AE,c,C,e,E,e,E,e,E,e,E,i,I,i,I,i,I,i,I,n,N,o,O,o,O,o,o,O,o,O,oe,OE,oe,OE,u,U,u,U,u,U,ue,UE,y,Y,ss";

	// XSL FILE PATH USED FOR REMOVING SPACE := src\main\java\com\abb\serviceprovider\common\xslt\
	public static final String removeSpaceXSLFilePath = "com" + File.separator + "abb" + File.separator + "serviceprovider" + File.separator + "common" + File.separator + "xslt" + File.separator + "RemoveSpace.xsl";

}
