<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:variable name="Airport_Lookup" select="document('lookupXMLs/Airports.xml')//Airport"/>
	<xsl:variable name="Airline_Lookup" select="document('lookupXMLs/Airlines.xml')//Airline"/>
	<xsl:variable name="City_Lookup" select="document('lookupXMLs/Cities.xml')//City"/>
	<xsl:variable name="Country_Lookup" select="document('lookupXMLs/Countries.xml')//Country"/>
	<xsl:variable name="ClassOfService_Lookup" select="document('lookupXMLs/ClassOfServices.xml')//ClassOfService"/>
	<xsl:variable name="Equipment_Lookup" select="document('lookupXMLs/Equipments.xml')//Equipment"/>
</xsl:stylesheet>