<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" />

	<xsl:template name="dateformat">
		<xsl:param name="datestr" />
		<xsl:param name="outformat" />
		<xsl:param name="informat" />
		<xsl:param name="format" />
		
		<xsl:if test="$format='date'">
			<xsl:if test="$outformat = 'yyyy-mm-dd' and $informat= 'datetime'">	
				<xsl:call-template name="divdate">
					<xsl:with-param name="datestr" select="$datestr" />
					<xsl:with-param name="subdate" select="'date'" />
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		
		<xsl:if test="$format='time'">
			<xsl:if test="$outformat = 'hh-mm' and $informat= 'datetime'">
				<xsl:call-template name="divdate">
					<xsl:with-param name="datestr" select="$datestr" />
					<xsl:with-param name="subdate" select="'time'" />
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		
	</xsl:template>
	<xsl:template name="timeformat">
		<xsl:param name="informat" />
		<xsl:param name="outformat" />
		<xsl:param name="time" />
		<xsl:if test="$outformat='hh-mm' and $informat='PTHM'">
			<xsl:call-template name="hh-mm">
				<xsl:with-param name="time" select="$time" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="divdate">
		<xsl:param name="datestr" />
		<xsl:param name="subdate" />

		<!-- input format yyyy-mm-dd hh:mm -->     <!-- output format yyyy-mm-dd or hh:mm -->
		<xsl:if test="$subdate='date'">
			<xsl:value-of select="substring($datestr,1,10)" />
		</xsl:if>
		<xsl:if test="$subdate='time'">
			<xsl:variable name="hh">
				<xsl:value-of select="substring($datestr,12,2)" />
			</xsl:variable>
			<xsl:variable name="mm">
				<xsl:value-of select="substring($datestr,15,2)" />
			</xsl:variable>
			<xsl:value-of select="$hh" />
			<xsl:value-of select="':'" />
			<xsl:value-of select="$mm" />
		</xsl:if>
	</xsl:template>

	<xsl:template name="hh-mm">
		<xsl:param name="time" />
		<xsl:variable name="hours">
			<xsl:variable name="hrs" select="substring-before(translate($time,'PTM',''),'H')"/>
			<xsl:choose>
				<xsl:when test="$hrs = ''">
					<xsl:value-of select="'0'"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$hrs"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="minutes">
			<xsl:variable name="min" select="substring-after(translate($time,'PTM',''),'H')"/>
			<xsl:choose>
				<xsl:when test="$min = ''">
					<xsl:value-of select="'0'"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="$min"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:value-of select="concat(format-number($hours,'00'), ':' ,format-number($minutes,'00'))"/>
	</xsl:template>

</xsl:stylesheet>