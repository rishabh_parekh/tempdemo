<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:variable name="vLower" select="'abcdefghijklmnopqrstuvwxyz'"/>
	<xsl:variable name="vUpper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>

	<xsl:template name="ToTitleCase">
		<xsl:param name="inputStr"/>
	
		<xsl:variable name="firstChar" select="substring($inputStr,1,1)"/>
		<xsl:value-of select="translate($firstChar,$vLower,$vUpper)"/>
		<xsl:value-of select="translate(substring-after($inputStr,$firstChar),$vUpper,$vLower)"/>
	</xsl:template>

	<xsl:template name="ToLowerCase">
		<xsl:param name="inputStr"/>
		<xsl:value-of select="translate($inputStr,$vUpper,$vLower)"/>
	</xsl:template>

	<xsl:template name="ToUpperCase">
		<xsl:param name="inputStr"/>
		<xsl:value-of select="translate($inputStr,$vLower,$vUpper)"/>
	</xsl:template>
</xsl:stylesheet>