<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes" />
	<xsl:variable name="allowedSymbols" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'"/>
	<!-- Copies Entire XML -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<!--Perform enhancement/logic on certain part of XML  -->
	<xsl:template match="Traveler/TravelerName">
		<xsl:copy>
			<!-- Remove space from LastName -->
			<xsl:element name="Surname">
				<xsl:value-of select="translate(Surname, translate(Surname, $allowedSymbols, ''), '')" />
			</xsl:element>
			<!-- Remove space from FirstName -->
			<xsl:element name="GivenName">
				<xsl:value-of select="translate(GivenName, translate(GivenName, $allowedSymbols, ''), '')" />
			</xsl:element>
			<xsl:copy-of select="Initial" />
			<xsl:copy-of select="Title" />
			<xsl:copy-of select="DateOfBirth" />
			<xsl:copy-of select="Gender" />
			<xsl:copy-of select="NameRemark" />
		</xsl:copy>
	</xsl:template>
	
	<!-- For Infant's Name -->
	<xsl:template match="Traveler/Infant">
		<xsl:copy>
			<!-- copying attributes of Infant element -->
			<xsl:copy-of select="@*" />
			<!-- Remove space from LastName -->
			<xsl:element name="Surname">
				<xsl:value-of select="translate(Surname, translate(Surname, $allowedSymbols, ''), '')" />
			</xsl:element>
			<!-- Remove space from FirstName -->
			<xsl:element name="GivenName">
				<xsl:value-of select="translate(GivenName, translate(GivenName, $allowedSymbols, ''), '')" />
			</xsl:element>
			<xsl:copy-of select="Initial" />
			<xsl:copy-of select="Title" />
			<xsl:copy-of select="DateOfBirth" />
			<xsl:copy-of select="Gender" />
			<xsl:copy-of select="NameRemark" />
		</xsl:copy>
	</xsl:template>
	
	<!-- For APIs Name -->
	<xsl:template match="PictureID">
		<xsl:copy>
			<xsl:copy-of select="DocType" />
			<xsl:copy-of select="DateOfBirth" />
			<xsl:copy-of select="Gender" />
			<!-- Remove space from LastName -->
			<xsl:element name="Surname">
				<xsl:value-of select="translate(Surname, translate(Surname, $allowedSymbols, ''), '')" />
			</xsl:element>
			<!-- Remove space from FirstName -->
			<xsl:element name="GivenName">
				<xsl:value-of select="translate(GivenName, translate(GivenName, $allowedSymbols, ''), '')" />
			</xsl:element>
			<xsl:copy-of select="Initial" />
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>