package com.abb.serviceprovider.common;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The Class ApplicationContextProvider.
 */
public class ApplicationContextProvider implements ApplicationContextAware {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext (org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(final ApplicationContext ctx) throws BeansException {
		ApplicationContextUtil.setApplicationContext(ctx);
	}

}
