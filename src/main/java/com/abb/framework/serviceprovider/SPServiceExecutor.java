package com.abb.framework.serviceprovider;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.abb.framework.exception.OpenAirException;
import com.abb.framework.merge.OpenAirMerger;


/**
 * This class is intended to execute the services of service providers and merger their transformed response.
 * 
 * @author ankurr
 * 
 */
@Service
public class SPServiceExecutor {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SPServiceExecutor.class);

	/** The xml merger. */
	@Autowired
	@Qualifier("XMLMerger")
	private OpenAirMerger xmlMerger;

	/**
	 * Create threads to execute service of each service provider and return merged response.
	 *
	 * @param spCompositeList the sp composite list
	 * @return the string
	 * @throws OpenAirException the open air exception
	 */
	public String execute(List<SPComposite> spCompositeList) throws OpenAirException {
		SPServiceThread[] spServiceThreads = new SPServiceThread[spCompositeList.size()];
		List<String> saResponseXMLs = new ArrayList<String>();

		// Start all Threads
		LOG.info("Start all the threads.");
		int i = 0;
		for (SPComposite spComposite : spCompositeList) {
			spServiceThreads[i] = new SPServiceThread();
			// Set Service Name
			spServiceThreads[i].setName(spComposite.getCurrentSPService().getSPServiceName());
			// Set SPService
			spServiceThreads[i].setSpService(spComposite.getCurrentSPService());
			// Start the thread
			spServiceThreads[i].start();
			LOG.info("Thread started for: {}", spServiceThreads[i].getName());
			i++;
		}

		// Join all Threads, wait for a set of threads to complete
		LOG.info("Join All Thread. ");
		for (i = 0; i < spServiceThreads.length; i++) {
			try {
				LOG.info("Thread before join for: {}", spServiceThreads[i].getName());
				spServiceThreads[i].join();
				LOG.info("Thread completed for: {}", spServiceThreads[i].getName());

				if (spServiceThreads[i].getException() != null) {
					// TODO: If error or exception is received from any of the
					// ServiceProvider then call cancel service as appropriate.
					throw spServiceThreads[i].getException();
				} else {
					saResponseXMLs.add(spServiceThreads[i].getSpService().getOpenAirlinesServiceEnvelopXML().getSaResponseXML());
				}
			} catch (InterruptedException e) {
				throw new OpenAirException(e);
			}
		}

		// TODO: Merging of responses to be carried out.

		LOG.info("Return merged response.");
		return saResponseXMLs.get(0);
	}
}
