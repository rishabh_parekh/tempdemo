package com.abb.framework.serviceprovider;

import com.abb.framework.exception.OpenAirServiceNotFoundForProviderException;


/**
 * This interface defines service for each method mapped and implemented for different Service Providers.
 */
public interface ServiceProvider {

	/**
	 * Gets the fare search service.
	 *
	 * @return the fare search service
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getFareSearchService() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the flight price service.
	 *
	 * @return the flight price service
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getFlightPriceService() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the PNR create service.
	 *
	 * @return the PNR create service
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getPNRCreateService() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the PNR retrieve service.
	 *
	 * @return the PNR retrieve service
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getPNRRetrieveService() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the PNR cancel service.
	 *
	 * @return the PNR cancel service
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getPNRCancelService() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the fare rules service.
	 *
	 * @return the fare rules service
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getFareRulesService() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the adds the payments in jet star.
	 *
	 * @return the adds the payments in jet star
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getAddPaymentsInJetStar() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the ticket issue service.
	 *
	 * @return the ticket issue service
	 * @throws OpenAirServiceNotFoundForProviderException the open air service not found for provider exception
	 */
	SPService getTicketIssueService() throws OpenAirServiceNotFoundForProviderException;

	/**
	 * Gets the service provider name.
	 *
	 * @return the service provider name
	 */
	String getServiceProviderName();
}