package com.abb.framework.serviceprovider.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.abb.framework.dao.HibernateUtils;
import com.abb.framework.entity.OpenAirSuperPNR;
import com.abb.framework.util.GeneratePNR;


/**
 * The Class ServiceProviderDAO.
 */
public class ServiceProviderDAO extends HibernateUtils {

	/**
	 * This method generates 6 digit unique super PNR, it checks for uniqueness and inserts it into database.
	 * 
	 * @return ABBSuperPNR
	 */
	public OpenAirSuperPNR getUniqueSuperPNR() {
		String superPNR = null;
		int resultNo = 1;

		while (resultNo > 0) {
			superPNR = GeneratePNR.getPNR();

			DetachedCriteria superPNRCriteria = DetachedCriteria.forClass(OpenAirSuperPNR.class);
			superPNRCriteria.setProjection(Projections.rowCount());
			superPNRCriteria.add(Restrictions.eq("superPNR", superPNR));

			List<Integer> queryResult = hibernateTemplate.findByCriteria(superPNRCriteria);
			resultNo = queryResult.get(0);
		}

		OpenAirSuperPNR luteSuperPNR = new OpenAirSuperPNR();
		luteSuperPNR.setSuperPNR(superPNR);

		hibernateTemplate.save(luteSuperPNR);

		return luteSuperPNR;
	}
}
