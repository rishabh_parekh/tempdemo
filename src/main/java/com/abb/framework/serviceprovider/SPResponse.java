package com.abb.framework.serviceprovider;

import java.io.IOException;

import org.springframework.core.io.Resource;


/**
 * The Class SPResponse.
 */
public class SPResponse {
	
	/** The response xslt. */
	Resource responseXSLT;

	/**
	 * Gets the response xslt.
	 *
	 * @return the response xslt
	 */
	public Resource getResponseXSLT() {
		return responseXSLT;
	}

	/**
	 * Sets the response xslt.
	 *
	 * @param responseXSLT the new response xslt
	 */
	public void setResponseXSLT(Resource responseXSLT) {
		this.responseXSLT = responseXSLT;
	}

	/**
	 * Gets the response xslt absolute path.
	 *
	 * @return the response xslt absolute path
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getResponseXSLTAbsolutePath() throws IOException {
		return responseXSLT.getFile().getAbsolutePath();
	}

}
