package com.abb.framework.serviceprovider;

import java.rmi.RemoteException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.abb.framework.entity.OpenAirlinesServiceEnvelopXML;


/**
 * Service class of Service Provider.
 *
 * @author ankurr
 */
public abstract class SPService {

	/** The app context. */
	@Autowired
	private ApplicationContext appContext;

	/** The open airlines service envelop xml. */
	protected OpenAirlinesServiceEnvelopXML openAirlinesServiceEnvelopXML;
	
	/** The sp requests. */
	protected List<SPRequest> spRequests;
	
	/** The sp response. */
	protected SPResponse spResponse;
	// This variable added to handle multiple XSL in response. Previous implementation was handling only one XSL in response.
	/** The sp responses. */
	protected List<SPResponse> spResponses;

	/**
	 * Call the service provider method.
	 *
	 * @throws Exception the exception
	 */
	public abstract void execute() throws Exception;

	/**
	 * Get the service name.
	 *
	 * @return the SP service name
	 */
	public abstract String getSPServiceName();

	/**
	 * Gets the open airlines service envelop xml.
	 *
	 * @return the open airlines service envelop xml
	 */
	public OpenAirlinesServiceEnvelopXML getOpenAirlinesServiceEnvelopXML() {
		if (openAirlinesServiceEnvelopXML == null)
			openAirlinesServiceEnvelopXML = appContext.getBean(OpenAirlinesServiceEnvelopXML.class);
		return openAirlinesServiceEnvelopXML;
	}

	/**
	 * Sets the open airlines service envelop xml.
	 *
	 * @param openAirlinesServiceEnvelopXML the new open airlines service envelop xml
	 */
	public void setOpenAirlinesServiceEnvelopXML(OpenAirlinesServiceEnvelopXML openAirlinesServiceEnvelopXML) {
		this.openAirlinesServiceEnvelopXML = openAirlinesServiceEnvelopXML;
	}

	/**
	 * Gets the sp requests.
	 *
	 * @return the sp requests
	 */
	public List<SPRequest> getSpRequests() {
		return spRequests;
	}

	/**
	 * Sets the sp requests.
	 *
	 * @param spRequests the new sp requests
	 */
	public void setSpRequests(List<SPRequest> spRequests) {
		this.spRequests = spRequests;
	}

	/**
	 * Gets the sp response.
	 *
	 * @return the sp response
	 */
	public SPResponse getSpResponse() {
		return spResponse;
	}

	/**
	 * Gets the sp responses.
	 *
	 * @return the sp responses
	 */
	public List<SPResponse> getSpResponses() {
		return spResponses;
	}

	/**
	 * Sets the sp responses.
	 *
	 * @param spResponses the new sp responses
	 */
	public void setSpResponses(List<SPResponse> spResponses) {
		this.spResponses = spResponses;
	}

	/**
	 * Sets the sp response.
	 *
	 * @param spResponse the new sp response
	 */
	public void setSpResponse(SPResponse spResponse) {
		this.spResponse = spResponse;
	}
}
