package com.abb.framework.serviceprovider;

import java.io.IOException;

import org.springframework.core.io.Resource;


/**
 * The Class SPRequest.
 */
public class SPRequest {
	
	/** The request xslt. */
	Resource requestXSLT;
	
	/** The service url. */
	String serviceURL;
	
	/** The soap action. */
	String soapAction;

	/**
	 * Gets the service url.
	 *
	 * @return the service url
	 */
	public String getServiceURL() {
		return serviceURL;
	}

	/**
	 * Sets the service url.
	 *
	 * @param serviceURL the new service url
	 */
	public void setServiceURL(String serviceURL) {
		this.serviceURL = serviceURL;
	}

	/**
	 * Gets the soap action.
	 *
	 * @return the soap action
	 */
	public String getSoapAction() {
		return soapAction;
	}

	/**
	 * Sets the soap action.
	 *
	 * @param soapAction the new soap action
	 */
	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	/**
	 * Gets the request xslt absolute path.
	 *
	 * @return the request xslt absolute path
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String getRequestXSLTAbsolutePath() throws IOException {
		return requestXSLT.getFile().getAbsolutePath();
	}

	/**
	 * Gets the request xslt.
	 *
	 * @return the request xslt
	 */
	public Resource getRequestXSLT() {
		return requestXSLT;
	}

	/**
	 * Sets the request xslt.
	 *
	 * @param requestXSLT the new request xslt
	 */
	public void setRequestXSLT(Resource requestXSLT) {
		this.requestXSLT = requestXSLT;
	}

}
