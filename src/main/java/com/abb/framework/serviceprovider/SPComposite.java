package com.abb.framework.serviceprovider;


/**
 * This class composites the servicePropvider and its current service which needs to be execute.
 * 
 * @author ankurr
 * 
 */
public class SPComposite {

	/** The service provider. */
	private ServiceProvider serviceProvider;
	
	/** The current sp service. */
	private SPService currentSPService;

	/**
	 * Instantiates a new SP composite.
	 *
	 * @param serviceProvider the service provider
	 * @param currentSPService the current sp service
	 */
	public SPComposite(ServiceProvider serviceProvider, SPService currentSPService) {
		this.serviceProvider = serviceProvider;
		this.currentSPService = currentSPService;
	}

	/**
	 * Gets the service provider.
	 *
	 * @return the service provider
	 */
	public ServiceProvider getServiceProvider() {
		return serviceProvider;
	}

	/**
	 * Gets the current sp service.
	 *
	 * @return the current sp service
	 */
	public SPService getCurrentSPService() {
		return currentSPService;
	}

}
