package com.abb.framework.serviceprovider;

import java.rmi.RemoteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;

import com.abb.framework.exception.OpenAirException;
import com.abb.framework.exception.OpenAirRemoteException;
import com.abb.framework.exception.OpenAirSPLoginException;
import com.abb.framework.exception.OpenAirSPServiceException;
import com.abb.serviceprovider.common.CommonConstant;


/**
 * This class define and create thread for a service to execute.
 * 
 * @author ankurr
 * 
 */
public class SPServiceThread extends Thread {

	/** The sp service. */
	private SPService spService;

	/** The exception. */
	private OpenAirException exception;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SPServiceThread.class);

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		try {
			spService.execute();
		} catch (RemoteException e) {
			exception = new OpenAirRemoteException((Exception) e.getCause());
			LOG.error(CommonConstant.ERROR_MSG, e);
		} catch (RestClientException e) {
			exception = new OpenAirRemoteException(e);
			LOG.error(CommonConstant.ERROR_MSG, e);
		} catch (OpenAirSPLoginException e) {
			exception = e;
			LOG.error(CommonConstant.ERROR_MSG, e);
		} catch (Exception e) {
			exception = new OpenAirSPServiceException(e);
			LOG.error(CommonConstant.ERROR_MSG, e);
		}
		LOG.debug(spService.getSPServiceName() + " is completed");
	}

	/**
	 * Gets the sp service.
	 *
	 * @return the sp service
	 */
	public SPService getSpService() {
		return spService;
	}

	/**
	 * Sets the sp service.
	 *
	 * @param spService the new sp service
	 */
	public void setSpService(SPService spService) {
		this.spService = spService;
	}

	/**
	 * Gets the exception.
	 *
	 * @return the exception
	 */
	public OpenAirException getException() {
		return exception;
	}
}
