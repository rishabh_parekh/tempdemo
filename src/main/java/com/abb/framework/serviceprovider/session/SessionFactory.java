package com.abb.framework.serviceprovider.session;

import org.apache.commons.pool.KeyedPoolableObjectFactory;

/**
 * A factory for creating Session objects.
 */
public interface SessionFactory extends KeyedPoolableObjectFactory<String, SPSession> {

}
