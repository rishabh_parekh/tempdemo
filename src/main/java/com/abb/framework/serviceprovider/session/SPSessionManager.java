package com.abb.framework.serviceprovider.session;

import org.apache.commons.pool.impl.GenericKeyedObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abb.framework.exception.OpenAirSPLoginException;


/**
 * Session Manager to manage session pool for service providers.
 * 
 * @author ankurr
 * 
 */
public class SPSessionManager {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SPSessionManager.class);

	// private GenericKeyedObjectPool<String, SPSession> spSessionPool;

	/**
	 * Initialize pool.
	 *
	 * @throws Exception the exception
	 */
	public void initializePool() throws Exception {
		/*
		 * // Create a session object for each service provider for (ServiceProvider serviceProvider : ServiceProvider.values()) { LOG. info("SPSessionManager - before initializePool for provider - " + serviceProvider.getSessionKey());
		 * spSessionPool.preparePool(serviceProvider.getSessionKey(), true); LOG.info("SPSessionManager - after initializePool for provider - " + serviceProvider.getSessionKey()); }
		 */
	}

	/**
	 * Open session.
	 *
	 * @param sessionKey the session key
	 * @return the SP session
	 * @throws OpenAirSPLoginException the open air sp login exception
	 */
	public SPSession openSession(String sessionKey) throws OpenAirSPLoginException {
		/*
		 * try { LOG.info("SPSessionManager - before openSession for provider - " + sessionKey); SPSession spSession = spSessionPool.borrowObject(sessionKey); LOG.info("SPSessionManager - after openSession for provider - " + sessionKey); return
		 * spSession; } catch (Exception e) { LOG.error("SPSessionManager - Error in openSession", e); throw new OpenAirSPLoginException(e); }
		 */
		return null;
	}

	/**
	 * Close session.
	 *
	 * @param spSession the sp session
	 * @throws OpenAirSPLoginException the open air sp login exception
	 */
	public void closeSession(SPSession spSession) throws OpenAirSPLoginException {
		/*
		 * try { LOG.info("SPSessionManager - before closeSession for provider - " + spSession.getSessionKey() + "with token - " + spSession.getSessionToken()); spSessionPool.returnObject(spSession.getSessionKey(), spSession);
		 * LOG.info("SPSessionManager - after closeSession for provider - " + spSession.getSessionKey() + "with token - " + spSession.getSessionToken()); } catch (Exception e) { LOG.error("SPSessionManager - Error in closeSession", e); throw new
		 * OpenAirSPLoginException(e); }
		 */
	}

	/**
	 * Gets the sp session pool.
	 *
	 * @return the sp session pool
	 */
	public GenericKeyedObjectPool<String, SPSession> getSpSessionPool() {
		// return spSessionPool;
		return null;
	}

	/**
	 * Sets the sp session pool.
	 *
	 * @param spSessionPool the sp session pool
	 */
	public void setSpSessionPool(GenericKeyedObjectPool<String, SPSession> spSessionPool) {
		// this.spSessionPool = spSessionPool;
	}
}
