package com.abb.framework.serviceprovider.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.abb.serviceprovider.common.CommonConstant;


/**
 * The Class SessionManagerLoader.
 */
@Service
public class SessionManagerLoader implements ApplicationListener<ContextRefreshedEvent> {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SessionManagerLoader.class);

	/*
	 * @Autowired private SPSessionManager spSessionManager;
	 */

	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		try {
			LOG.info("SessionManagerLoader.onApplicationEvent session pool deactivated for now.");
			/*
			 * LOG.info("Start Creating Session Pool"); spSessionManager.initializePool(); LOG.info("Session Pool Created");
			 */
		} catch (Exception e) {
			LOG.error(CommonConstant.ERROR_MSG, e);
		}
	}

}
