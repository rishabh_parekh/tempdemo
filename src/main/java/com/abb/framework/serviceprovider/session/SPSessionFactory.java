package com.abb.framework.serviceprovider.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.abb.framework.enums.ServiceProvider;


/**
 * A factory for creating SPSession objects.
 */
public class SPSessionFactory implements SessionFactory {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SPSessionFactory.class);

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#makeObject(java.lang.Object)
	 */
	@Override
	public SPSession makeObject(String key) throws Exception {
		/*
		 * ServiceProvider serviceProvider = getServiceProvider(key); if (serviceProvider != null) { SPSession sessionObject = serviceProvider.getSessionFactory().makeObject(key); sessionObject.setSessionKey(key); return sessionObject; } else throw
		 * new OpenAirSPLoginException( "Can't create object for specific provider with key " + key);
		 */
		return null;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#destroyObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void destroyObject(String key, SPSession obj) throws Exception {
		/*
		 * ServiceProvider serviceProvider = getServiceProvider(key); if (serviceProvider != null) { serviceProvider.getSessionFactory().destroyObject(key, obj); } else throw new OpenAirSPLoginException(
		 * "Can't create object for specific provider with key " + key);
		 */

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#validateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public boolean validateObject(String key, SPSession obj) {
		/*
		 * LOG.info("Validating Session token -" + obj.getSessionToken() + " for provider -" + key); boolean result = false; if (JSConstants.SESSION_TIME_TO_LIVE > (System.currentTimeMillis() - obj.getExpiryTime())) { result = true; }
		 * LOG.info("validateObject for Session token -" + obj.getSessionToken() + " for provider -" + key + " ExpiryTime - " + obj.getExpiryTime()); LOG.info("validateObject for Session token -" + obj.getSessionToken() + " for provider -" + key +
		 * " returns - " + result); return result;
		 */
		return false;
	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#activateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void activateObject(String key, SPSession obj) throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see org.apache.commons.pool.KeyedPoolableObjectFactory#passivateObject(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void passivateObject(String key, SPSession obj) throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * Get object of service provider based on given session key.
	 *
	 * @param key the key
	 * @return the service provider
	 */
	private ServiceProvider getServiceProvider(String key) {
		/*
		 * for (ServiceProvider serviceProvider : ServiceProvider.values()) if (serviceProvider.getSessionKey().equals(key)) return serviceProvider;
		 */
		return null;
	}
}
