package com.abb.framework.serviceprovider.session;


/**
 * Session information of Service Provider.
 *
 * @author ankurr
 */
public class SPSession {

	/** The user name. */
	private String userName;
	
	/** The password. */
	private String password;
	
	/** The session id. */
	private String sessionId;
	
	/** The session token. */
	private String sessionToken;
	
	/** The expiry time. */
	private long expiryTime;
	
	/** The response xml. */
	private String responseXML;
	
	/** The session key. */
	private String sessionKey;
	
	/** The active flag. */
	private boolean activeFlag;

	/**
	 * Instantiates a new SP session.
	 */
	public SPSession() {
		expiryTime = System.currentTimeMillis();
	}

	/**
	 * Gets the user name.
	 *
	 * @return the user name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the user name.
	 *
	 * @param userName the new user name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the session id.
	 *
	 * @return the session id
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the session id.
	 *
	 * @param sessionId the new session id
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * Gets the session token.
	 *
	 * @return the session token
	 */
	public String getSessionToken() {
		return sessionToken;
	}

	/**
	 * Sets the session token.
	 *
	 * @param sessionToken the new session token
	 */
	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}

	/**
	 * Gets the expiry time.
	 *
	 * @return the expiry time
	 */
	public long getExpiryTime() {
		return expiryTime;
	}

	/**
	 * Sets the expiry time.
	 *
	 * @param expiryTime the new expiry time
	 */
	public void setExpiryTime(long expiryTime) {
		this.expiryTime = expiryTime;
	}

	/**
	 * Gets the response xml.
	 *
	 * @return the response xml
	 */
	public String getResponseXML() {
		return responseXML;
	}

	/**
	 * Sets the response xml.
	 *
	 * @param responseXML the new response xml
	 */
	public void setResponseXML(String responseXML) {
		this.responseXML = responseXML;
	}

	/**
	 * Gets the session key.
	 *
	 * @return the session key
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * Sets the session key.
	 *
	 * @param sessionKey the new session key
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	/**
	 * Checks if is active flag.
	 *
	 * @return true, if is active flag
	 */
	public boolean isActiveFlag() {
		return activeFlag;
	}

	/**
	 * Sets the active flag.
	 *
	 * @param activeFlag the new active flag
	 */
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
}
