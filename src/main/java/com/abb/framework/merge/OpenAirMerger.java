package com.abb.framework.merge;

import com.abb.framework.exception.OpenAirMergingException;


/**
 * Class which deals with any kind of merging implements this interface.
 * 
 * @author ankurr
 * 
 */
public interface OpenAirMerger {

	/**
	 * Merge.
	 *
	 * @param <T1> the generic type
	 * @param <T2> the generic type
	 * @param xPaths the x paths
	 * @param mergePath the merge path
	 * @param sourceObjects the source objects
	 * @return the t2
	 * @throws OpenAirMergingException the open air merging exception
	 */
	<T1, T2> T2 merge(String xPaths, String mergePath, T1... sourceObjects) throws OpenAirMergingException;
}
