package com.abb.framework.merge;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.abb.framework.exception.OpenAirMergingException;


/**
 * Merge XMLs based on given XPaths and return the merged XML.
 * 
 * @author ankurr
 * 
 */
@Service("XMLMerger")
public class XMLMerger implements OpenAirMerger {

	/* (non-Javadoc)
	 * @see com.abb.framework.merge.OpenAirMerger#merge(java.lang.String, java.lang.String, T1[])
	 */
	@Override
	public <T1, T2> T2 merge(String xPath, String mergingRootNode, T1... sourceObjects) throws OpenAirMergingException {

		try {
			XPath xpath = XPathFactory.newInstance().newXPath();
			String[] XPaths = xPath.split(",");
			String[] mergingRootNodes = mergingRootNode.split(",");
			Document masterDoc = null;

			NodeList listOfNodes;
			for (T1 sourceStr : sourceObjects) {
				if (masterDoc == null) {
					masterDoc = (Document) xpath.evaluate("/", new InputSource(new StringReader((String) sourceObjects[0])), XPathConstants.NODE);
				} else {
					for (int j = 0; j < XPaths.length; j++) {
						listOfNodes = (NodeList) xpath.evaluate(XPaths[j], new InputSource(new StringReader((String) sourceStr)), XPathConstants.NODESET);

						// merge
						Element element = (Element) xpath.evaluate(mergingRootNodes[j], masterDoc, XPathConstants.NODE);
						if (element != null) {
							for (int i = 0; i < listOfNodes.getLength(); i++) {
								Node listPost = listOfNodes.item(i);
								element.appendChild(masterDoc.importNode(listPost, true));
							}
						}
					}
				}
			}

			// print
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(masterDoc), new StreamResult(writer));
			return (T2) writer.getBuffer().toString().replaceAll("\n|\r", "");

		} catch (XPathExpressionException e) {
			throw new OpenAirMergingException(e);
		} catch (TransformerConfigurationException e) {
			throw new OpenAirMergingException(e);
		} catch (TransformerException e) {
			throw new OpenAirMergingException(e);
		}
	}
}
