package com.abb.framework.transformation;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.abb.framework.exception.OpenAirTransformationException;


/**
 * This class applies XSLT on XML and transform into required XML format.
 * 
 * @author ankurr, irfanak
 * 
 */
@Service("XMLTransformation")
public class XMLTransformation implements OpenAirTransformation {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(XMLTransformation.class);
	
	/** The transformer variable. */
	private Map<String, String> transformerVariable = new HashMap<String, String>();

	/*
	 * Transform source XML into destination XML using given XSLT.
	 */
	/** The transformer factory. */
	@Autowired
	private TransformerFactory transformerFactory;

	/** The remove namespace xslt. */
	private Resource removeNamespaceXSLT;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.abb.framework.transformation.OpenAirTransformation#transform(java .lang .Object, java.lang.Object, java.lang.Object)
	 */
	@Override
	public <T1, T2, T3, T4> T2 transform(T1 sourceObject, T2 destinationObject, T3 xslFile, T4 transformParam) throws OpenAirTransformationException {

		ByteArrayOutputStream outputStream = null;

		try {

			sourceObject = (T1) removeNamespace((String) sourceObject);

			String xsltFilePath = (String) xslFile;
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			File stylesheet = new File(xsltFilePath);

			/**
			 * Actual Transformation code starts here
			 */

			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new ByteArrayInputStream(String.valueOf(sourceObject).getBytes("UTF8")));

			// Use a Transformer for output
			StreamSource stylesource = new StreamSource(stylesheet);

			Transformer transformer = transformerFactory.newTransformer(stylesource);
			if (transformParam != null) {
				this.setParameter(transformer, (Map<String, String>) transformParam);
			}

			DOMSource source = new DOMSource(document);

			/**
			 * Create output XML file of resultant content
			 */
			// fileOut=new FileOutputStream("updatedFSR.xml");
			outputStream = new ByteArrayOutputStream();
			StreamResult result = new StreamResult(outputStream);
			transformer.transform(source, result);
			destinationObject = (T2) outputStream.toString();

		} catch (Exception e) {
			throw new OpenAirTransformationException(e);
		}
		return destinationObject;
	}

	/**
	 * Removes the namespace.
	 *
	 * @param sourceObject the source object
	 * @return the string
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private String removeNamespace(String sourceObject) throws TransformerException, ParserConfigurationException, SAXException, IOException {
		LOG.info("Remove NameSpace XSLT = {}", removeNamespaceXSLT);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		/**
		 * Actual Transformation code starts here
		 */

		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new ByteArrayInputStream(String.valueOf(sourceObject).getBytes("UTF8")));

		// Use a Transformer for output
		StreamSource stylesource = new StreamSource(removeNamespaceXSLT.getFile());

		Transformer transformer = transformerFactory.newTransformer(stylesource);

		DOMSource source = new DOMSource(document);

		/**
		 * Create output XML file of resultant content
		 */
		// fileOut=new FileOutputStream("updatedFSR.xml");
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(outputStream);
		transformer.transform(source, result);
		return outputStream.toString();
	}

	/**
	 * Sets the parameter.
	 *
	 * @param transformer the transformer
	 * @param transformerVariable the transformer variable
	 */
	private void setParameter(Transformer transformer, Map<String, String> transformerVariable) {
		for (String key : transformerVariable.keySet()) {
			transformer.setParameter(key, transformerVariable.get(key));
		}
	}

	/**
	 * Gets the transformer factory.
	 *
	 * @return the transformer factory
	 */
	public TransformerFactory getTransformerFactory() {
		return transformerFactory;
	}

	/**
	 * Sets the transformer factory.
	 *
	 * @param transformerFactory the new transformer factory
	 */
	public void setTransformerFactory(TransformerFactory transformerFactory) {
		this.transformerFactory = transformerFactory;
	}

	/**
	 * Sets the transformer variable.
	 *
	 * @param name the name
	 * @param value the value
	 */
	public void setTransformerVariable(String name, String value) {
		this.transformerVariable.put(name, value);
	}

	/**
	 * Gets the transformer variable.
	 *
	 * @param name the name
	 * @return the transformer variable
	 */
	public String getTransformerVariable(String name) {
		return this.transformerVariable.get(name);
	}

	/**
	 * Gets the removes the namespace xslt.
	 *
	 * @return the removes the namespace xslt
	 */
	public Resource getRemoveNamespaceXSLT() {
		return removeNamespaceXSLT;
	}

	/**
	 * Sets the removes the namespace xslt.
	 *
	 * @param removeNamespaceXSLT the new removes the namespace xslt
	 */
	public void setRemoveNamespaceXSLT(Resource removeNamespaceXSLT) {
		this.removeNamespaceXSLT = removeNamespaceXSLT;
	}
}
