package com.abb.framework.transformation;

import com.abb.framework.exception.OpenAirTransformationException;


/**
 * Class which deals with any kind of transformation implements this interface.
 * 
 * @author ankurr
 * 
 */
public interface OpenAirTransformation {

	/**
	 * Transform the source object into destination object.
	 *
	 * @param <T1> the generic type
	 * @param <T2> the generic type
	 * @param <T3> the generic type
	 * @param <T4> the generic type
	 * @param sourceObject the source object
	 * @param destinationObject the destination object
	 * @param param the param
	 * @param parameter the parameter
	 * @return the t2
	 * @throws OpenAirTransformationException the open air transformation exception
	 */
	<T1, T2, T3, T4> T2 transform(T1 sourceObject, T2 destinationObject, T3 param, T4 parameter) throws OpenAirTransformationException;

}
