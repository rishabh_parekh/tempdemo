package com.abb.framework.authentication.service;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.abb.framework.authentication.dao.AuthenticationDAO;
import com.abb.framework.exception.OpenAirException;
import com.abb.framework.util.XMLUtils;

/**
 * The Class AuthenticationService.
 */
@Service
public class AuthenticationService {

	/** The authenticate dao. */
	@Autowired
	private AuthenticationDAO authenticateDao;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationService.class);

	/**
	 * Accepts the SOAP request XML and extract the authentication information to validate the request.
	 * 
	 * @param soapRequestXML
	 *            the soap request xml
	 * @return authentication result.
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public boolean authenticateRequest(String soapRequestXML) throws OpenAirException {

		LOG.info(" Start Authentication Service. ");

		boolean isAuthenticat = false;

		// Extract authentication information from SOAP request XML String
		String agencyPassword = "";
		String agencyUsername = "";
		String agentUsername = "";
		String agentPassword = "";

		Document doc = null;
		LOG.debug(" Get Authentication Information from TC Header String.");
		doc = XMLUtils.loadXMLFromString(soapRequestXML);
		Element idenNode = (Element) doc.getElementsByTagName("iden").item(0);
		if (idenNode != null) {
			agencyUsername = StringUtils.defaultString(idenNode.getAttribute("u"));
			agencyPassword = StringUtils.defaultString(idenNode.getAttribute("p"));
			agentUsername = StringUtils.defaultString(idenNode.getAttribute("agt"));
			agentPassword = StringUtils.defaultString(idenNode.getAttribute("agtpwd"));

			LOG.info(" Extracted authentication inforamtion and now authenticate it. ");
			isAuthenticat = authenticateDao.authenticateUser(agencyUsername, agencyPassword, agentUsername, agentPassword);
		} else {
			LOG.info("Missing <tc> or <iden> tag in given SOAP Request XML.");
			LOG.info("Given soapRequestXML = " + soapRequestXML);
		}

		LOG.info("Authentication Result = {}", isAuthenticat);
		return isAuthenticat;

	}
}
