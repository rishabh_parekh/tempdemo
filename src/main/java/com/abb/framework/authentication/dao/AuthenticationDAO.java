package com.abb.framework.authentication.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.abb.framework.dao.HibernateUtils;
import com.abb.framework.entity.OpenAirFarelogixUser;
import com.abb.framework.entity.OpenAirUser;

/**
 * The Class AuthenticationDAO.
 */
@Repository
public class AuthenticationDAO extends HibernateUtils {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationDAO.class);

	/**
	 * Authenticate user based on given authentication information with Database.
	 * 
	 * @param username
	 *            the username
	 * @param password
	 *            the password
	 * @return authentication result.
	 */
	public boolean authenticateUser(String username, String password) {

		LOG.info(" Start User Authentication at DAO. ");

		DetachedCriteria authenticationCriteria = DetachedCriteria.forClass(OpenAirUser.class);
		authenticationCriteria.setProjection(Projections.rowCount());
		authenticationCriteria.add(Restrictions.eq("username", username)).add(Restrictions.eq("password", password)).add(Restrictions.eq("activeFlag", true));

		List<Long> queryResult = hibernateTemplate.findByCriteria(authenticationCriteria);

		long resultNo = queryResult.get(0);

		LOG.info("Query executed successfully and Result = {}", ((resultNo == 1) ? true : false));
		return (resultNo == 1) ? true : false;
	}

	/**
	 * Authenticate user based on given authentication information with Database.
	 * 
	 * @param agencyUsername
	 *            the agency username
	 * @param agencyPassword
	 *            the agency password
	 * @param agentUsername
	 *            the agent username
	 * @param agentPassword
	 *            the agent password
	 * @return authentication result.
	 */
	public boolean authenticateUser(String agencyUsername, String agencyPassword, String agentUsername, String agentPassword) {

		LOG.info(" Start User Authentication at DAO. ");

		DetachedCriteria authenticationCriteria = DetachedCriteria.forClass(OpenAirFarelogixUser.class);
		authenticationCriteria.setProjection(Projections.rowCount());
		authenticationCriteria.add(Restrictions.eq("agencyUserName", agencyUsername)).add(Restrictions.eq("agencyPassword", agencyPassword)).add(Restrictions.eq("agentUserName", agentUsername)).add(Restrictions.eq("agentPassword", agentPassword))
				.add(Restrictions.eq("checkIsActive", true));

		List queryResult = hibernateTemplate.findByCriteria(authenticationCriteria);
		int resultNo = Integer.parseInt(String.valueOf(queryResult.get(0)));

		LOG.info("Query executed successfully and Result = " + ((resultNo == 1) ? true : false));
		return (resultNo == 1) ? true : false;
	}
}
