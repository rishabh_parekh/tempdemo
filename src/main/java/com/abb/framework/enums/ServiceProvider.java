package com.abb.framework.enums;

import org.springframework.core.io.Resource;

import com.abb.framework.serviceprovider.session.SessionFactory;


/**
 * The Enum ServiceProvider.
 *
 * @author ankurr
 */
public enum ServiceProvider {

	/** The jet star. */
	JET_STAR, /** The jet airways. */
 JET_AIRWAYS;

	/** The session key. */
	private String sessionKey;
	
	/** The session config file. */
	private Resource sessionConfigFile;
	
	/** The session factory. */
	private SessionFactory sessionFactory;

	/**
	 * Gets the session key.
	 *
	 * @return the session key
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * Sets the session key.
	 *
	 * @param sessionKey the new session key
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	/**
	 * Gets the session config file.
	 *
	 * @return the session config file
	 */
	public Resource getSessionConfigFile() {
		return sessionConfigFile;
	}

	/**
	 * Sets the session config file.
	 *
	 * @param sessionConfigFile the new session config file
	 */
	public void setSessionConfigFile(Resource sessionConfigFile) {
		this.sessionConfigFile = sessionConfigFile;
	}

	/**
	 * Gets the session factory.
	 *
	 * @return the session factory
	 */
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	/**
	 * Sets the session factory.
	 *
	 * @param sessionFactory the new session factory
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
