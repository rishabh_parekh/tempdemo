package com.abb.framework.enums;

import com.abb.framework.exception.OpenAirInvalidServiceException;

/**
 * The Enum OpenAxisService.
 */
public enum OpenAxisService {

	/** The fare search. */
	FARE_SEARCH("FareSearch", "FareSearchRQ", "FareSearchRS", "OpenAxis_Schema/FareSearchRQ.xsd", "", ""),
	/** The flight price. */
	FLIGHT_PRICE("FlightPrice", "FlightPriceRQ", "FlightPriceRS", "OpenAxis_Schema/FlightPriceRQ.xsd", "", ""),
	/** The pnr create. */
	PNR_CREATE("PNRCreate", "PNRCreateRQ", "PNRViewRS", "OpenAxis_Schema/PNRCreateRQ.xsd", "", ""),
	/** The pnr retrieve. */
	PNR_RETRIEVE("PNRRetrieve", "PNRRetrieveRQ", "PNRViewRS", "OpenAxis_Schema/PNRRetrieveRQ.xsd", "", ""),
	/** The pnr cancel. */
	PNR_CANCEL("PNRCancel", "PNRCancelRQ", "PNRCancelRS", "OpenAxis_Schema/PNRCancelRQ.xsd", "", ""),
	/** The ticket issue. */
	TICKET_ISSUE("TicketIssue", "TicketIssueRQ", "TicketIssueRS", "farelogix_schemas/TicketIssueRQ.xsd", "", ""),
	/** The add payment in jetstar. */
	ADD_PAYMENT_IN_JETSTAR("AddPaymentInJetstar", "AddPaymentInJetstarRQ", "AddPaymentInJetstarRS", "", "", ""),
	/** The fare rules. */
	FARE_RULES("FareRules", "FareRulesRQ", "FareRulesRS", "OpenAxis_Schema/FareRulesRQ.xsd", "", ""),
	/** Process Payment method name in Jetstar changed according to FLX naming standard. */
	PROCESS_PAYMENT_IN_JETSTAR("ProcessPaymentRQ", "AddPaymentInJetstarRQ", "AddPaymentInJetstarRS", "", "", "");

	/** The name. */
	private String name;

	/** The request wrapper. */
	private String requestWrapper;

	/** The response wrapper. */
	private String responseWrapper;

	/** The schema. */
	private String schema;

	/** The x paths to extract. */
	private String xPathsToExtract;

	/** The x paths to merge. */
	private String xPathsToMerge;

	/**
	 * Instantiates a new open axis service.
	 * 
	 * @param name
	 *            the name
	 * @param requestWrapper
	 *            the request wrapper
	 * @param responseWrapper
	 *            the response wrapper
	 * @param schema
	 *            the schema
	 * @param xPathsToExtract
	 *            the x paths to extract
	 * @param xPathsToMerge
	 *            the x paths to merge
	 */
	OpenAxisService(String name, String requestWrapper, String responseWrapper, String schema, String xPathsToExtract, String xPathsToMerge) {
		this.name = name;
		this.requestWrapper = requestWrapper;
		this.responseWrapper = responseWrapper;
		this.schema = schema;
		this.xPathsToExtract = xPathsToExtract;
		this.xPathsToMerge = xPathsToMerge;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the request wrapper.
	 * 
	 * @return the request wrapper
	 */
	public String getRequestWrapper() {
		return requestWrapper;
	}

	/**
	 * Sets the request wrapper.
	 * 
	 * @param requestWrapper
	 *            the new request wrapper
	 */
	public void setRequestWrapper(String requestWrapper) {
		this.requestWrapper = requestWrapper;
	}

	/**
	 * Gets the response wrapper.
	 * 
	 * @return the response wrapper
	 */
	public String getResponseWrapper() {
		return responseWrapper;
	}

	/**
	 * Sets the response wrapper.
	 * 
	 * @param responseWrapper
	 *            the new response wrapper
	 */
	public void setResponseWrapper(String responseWrapper) {
		this.responseWrapper = responseWrapper;
	}

	/**
	 * Gets the schema.
	 * 
	 * @return the schema
	 */
	public String getSchema() {
		return schema;
	}

	/**
	 * Sets the schema.
	 * 
	 * @param schema
	 *            the new schema
	 */
	public void setSchema(String schema) {
		this.schema = schema;
	}

	/**
	 * Gets the x paths to extract.
	 * 
	 * @return the x paths to extract
	 */
	public String getxPathsToExtract() {
		return xPathsToExtract;
	}

	/**
	 * Sets the x paths to extract.
	 * 
	 * @param xPathsToExtract
	 *            the new x paths to extract
	 */
	public void setxPathsToExtract(String xPathsToExtract) {
		this.xPathsToExtract = xPathsToExtract;
	}

	/**
	 * Gets the x paths to merge.
	 * 
	 * @return the x paths to merge
	 */
	public String getxPathsToMerge() {
		return xPathsToMerge;
	}

	/**
	 * Sets the x paths to merge.
	 * 
	 * @param xPathsToMerge
	 *            the new x paths to merge
	 */
	public void setxPathsToMerge(String xPathsToMerge) {
		this.xPathsToMerge = xPathsToMerge;
	}

	/**
	 * Gets the open axis service.
	 * 
	 * @param name
	 *            the name
	 * @return the open axis service
	 * @throws OpenAirInvalidServiceException
	 *             the open air invalid service exception
	 */
	public static OpenAxisService getOpenAxisService(String name) throws OpenAirInvalidServiceException {
		if (name.equals(OpenAxisService.FARE_SEARCH.getName())) {
			return OpenAxisService.FARE_SEARCH;
		} else if (name.equals(OpenAxisService.FLIGHT_PRICE.getName())) {
			return OpenAxisService.FLIGHT_PRICE;
		} else if (name.equals(OpenAxisService.PNR_CREATE.getName())) {
			return OpenAxisService.PNR_CREATE;
		} else if (name.equals(OpenAxisService.PNR_RETRIEVE.getName())) {
			return OpenAxisService.PNR_RETRIEVE;
		} else if (name.equals(OpenAxisService.PNR_CANCEL.getName())) {
			return OpenAxisService.PNR_CANCEL;
		} else if (name.equals(OpenAxisService.FARE_RULES.getName())) {
			return OpenAxisService.FARE_RULES;
		} else if (name.equals(OpenAxisService.ADD_PAYMENT_IN_JETSTAR.getName())) {
			return OpenAxisService.ADD_PAYMENT_IN_JETSTAR;
		} else if (name.equals(OpenAxisService.TICKET_ISSUE.getName())) {
			return OpenAxisService.TICKET_ISSUE;
		} else if (name.equals(OpenAxisService.PROCESS_PAYMENT_IN_JETSTAR.getName())) {
			return OpenAxisService.PROCESS_PAYMENT_IN_JETSTAR;
		} else {
			throw new OpenAirInvalidServiceException(name);
		}
	}
}
