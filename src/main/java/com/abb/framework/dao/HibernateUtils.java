package com.abb.framework.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;


/**
 * This class has a object of HibernateTemplate which initialize during the startup. Any class which deals with database will extend this class and execute HQL or SQL using HibernateTemplate object.
 * 
 * @author ankurr
 * 
 */
public abstract class HibernateUtils {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(HibernateUtils.class);
	
	/** The hibernate template. */
	protected HibernateTemplate hibernateTemplate;

	/**
	 * This method initialize HibernateTemplate object during startup.
	 *
	 * @param sessionFactory the new session factory
	 */
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		LOG.info("**** Setting Session Factory ****");
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	/**
	 * This method update values for given Object into Database.
	 *
	 * @param obj the obj
	 */
	public void updateObject(Object obj) {

		hibernateTemplate.update(obj);
	}
}
