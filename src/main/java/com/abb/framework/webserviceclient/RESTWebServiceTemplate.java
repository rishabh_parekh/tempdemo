package com.abb.framework.webserviceclient;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


/**
 * The Class RESTWebServiceTemplate.
 */
public class RESTWebServiceTemplate {

	/** The rest service uri. */
	String restServiceURI;
	
	/** The rest template. */
	RestTemplate restTemplate = new RestTemplate();

	/**
	 * Send and receive.
	 *
	 * @param request the request
	 * @return the string
	 */
	public String sendAndReceive(String request) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/x-www-form-urlencoded"));

		HttpEntity<String> entity = new HttpEntity<String>(request, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRESTServiceURI(), HttpMethod.POST, entity, String.class);

		return response.toString();
	}

	/**
	 * Gets the REST service uri.
	 *
	 * @return the REST service uri
	 */
	public String getRESTServiceURI() {
		return restServiceURI;
	}

	/**
	 * Sets the REST service uri.
	 *
	 * @param rESTServiceURI the new REST service uri
	 */
	public void setRESTServiceURI(String rESTServiceURI) {
		restServiceURI = rESTServiceURI;
	}
}
