package com.abb.framework.webserviceclient;

import java.io.IOException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;

import org.springframework.stereotype.Service;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringSource;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * The Class WebServiceMessageCallbackImpl.
 */
@Service
public class WebServiceMessageCallbackImpl implements WebServiceMessageCallback {

	/** The soap action. */
	private String soapAction;
	
	/** The soap header. */
	private String soapHeader;
	
	/** The soap body. */
	private String soapBody;

	/* (non-Javadoc)
	 * @see org.springframework.ws.client.core.WebServiceMessageCallback#doWithMessage(org.springframework.ws.WebServiceMessage)
	 */
	@Override
	public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {

		if (this.getSoapAction() != null) {
			((SoapMessage) message).setSoapAction(this.getSoapAction());
		}

		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.transform(new StringSource(soapBody), ((SoapMessage) message).getSoapBody().getPayloadResult());

		if (this.getSoapHeader() != null && this.getSoapHeader().length() > 0) {

			transformer.transform(new StringSource(soapHeader), ((SoapMessage) message).getSoapHeader().getResult());

			// Delete dummy root element OpenAirSOAPHeader, if exists
			DOMResult header = (DOMResult) ((SoapMessage) message).getSoapHeader().getResult();
			NodeList headerNodeList = header.getNode().getOwnerDocument().getElementsByTagName("OpenAirSOAPHeader");
			if (headerNodeList.getLength() > 0) {
				Element element = (Element) headerNodeList.item(0);
				Node fragment = header.getNode().getOwnerDocument().createDocumentFragment();

				int totalLeng = element.getChildNodes().getLength();
				for (int i = 0; i < totalLeng; i++) {
					fragment.appendChild(element.getChildNodes().item(0));
				}
				Node parent = element.getParentNode();
				parent.appendChild(fragment);
				parent.removeChild(element);
			}
		}
	}

	/**
	 * Gets the soap action.
	 *
	 * @return the soap action
	 */
	public String getSoapAction() {
		return soapAction;
	}

	/**
	 * Sets the soap action.
	 *
	 * @param soapAction the new soap action
	 */
	public void setSoapAction(String soapAction) {
		this.soapAction = soapAction;
	}

	/**
	 * Gets the soap header.
	 *
	 * @return the soap header
	 */
	public String getSoapHeader() {
		return soapHeader;
	}

	/**
	 * Sets the soap header.
	 *
	 * @param soapHeader the new soap header
	 */
	public void setSoapHeader(String soapHeader) {
		this.soapHeader = soapHeader;
	}

	/**
	 * Gets the soap body.
	 *
	 * @return the soap body
	 */
	public String getSoapBody() {
		return soapBody;
	}

	/**
	 * Sets the soap body.
	 *
	 * @param soapBody the new soap body
	 */
	public void setSoapBody(String soapBody) {
		this.soapBody = soapBody;
	}
}
