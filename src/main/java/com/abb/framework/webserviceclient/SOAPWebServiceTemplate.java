package com.abb.framework.webserviceclient;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageExtractor;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;

import com.abb.framework.entity.SOAPServiceEnvelopXML;


/**
 * The Class SOAPWebServiceTemplate.
 */
@Service
public class SOAPWebServiceTemplate {

	/** The web service template. */
	private WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SOAPWebServiceTemplate.class);
	
	/** The webservice message call back. */
	@Autowired
	private WebServiceMessageCallbackImpl webserviceMessageCallBack;

	/**
	 * Sets the default uri.
	 *
	 * @param defaultUri the new default uri
	 */
	public void setDefaultUri(String defaultUri) {
		webServiceTemplate.setDefaultUri(defaultUri);
	}

	/**
	 * Send and receive.
	 *
	 * @param soapEnvelopXML the soap envelop xml
	 * @return the string
	 */
	public String sendAndReceive(SOAPServiceEnvelopXML soapEnvelopXML) {

		final StreamResult result = new StreamResult(new StringWriter());

		webserviceMessageCallBack.setSoapHeader(soapEnvelopXML.getSpSOAPRequestHeaderXML());
		webserviceMessageCallBack.setSoapAction(soapEnvelopXML.getSpSOAPAction());
		webserviceMessageCallBack.setSoapBody(soapEnvelopXML.getSpSOAPRequestBodyXML());

		// If URL is specific to service then use service specific URL else use
		// default URL
		if (StringUtils.defaultString(soapEnvelopXML.getSpSOAPServiceURL()).length() > 0) {
			// LOG.debug("serviceXML==" + soapEnvelopXML.getServiceXML());
			webServiceTemplate.sendAndReceive(soapEnvelopXML.getSpSOAPServiceURL(), webserviceMessageCallBack, new WebServiceMessageExtractor<StreamResult>() {

				@Override
				public StreamResult extractData(WebServiceMessage message) throws IOException, TransformerException {
					Transformer transformer = TransformerFactory.newInstance().newTransformer();
					transformer.transform(((SoapMessage) message).getEnvelope().getSource(), result);
					return result;
				}
			});
		} else {
			webServiceTemplate.sendAndReceive(webserviceMessageCallBack, new WebServiceMessageExtractor<StreamResult>() {

				@Override
				public StreamResult extractData(WebServiceMessage message) throws IOException, TransformerException {
					Transformer transformer = TransformerFactory.newInstance().newTransformer();
					transformer.transform(((SoapMessage) message).getEnvelope().getSource(), result);
					return result;
				}
			});
		}
		return ((StringWriter) result.getWriter()).getBuffer().toString();
	}

	/**
	 * Gets the web service template.
	 *
	 * @return the web service template
	 */
	public WebServiceTemplate getWebServiceTemplate() {
		return webServiceTemplate;
	}

	/**
	 * Sets the web service template.
	 *
	 * @param webServiceTemplate the new web service template
	 */
	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}
}
