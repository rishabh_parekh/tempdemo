package com.abb.framework.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.abb.framework.authentication.service.AuthenticationService;
import com.abb.framework.enums.OpenAxisService;
import com.abb.framework.exception.OpenAirException;
import com.abb.framework.exception.OpenAirInvalidAuthenticationException;
import com.abb.framework.exception.OpenAirRequestXMLNotFoundException;
import com.abb.framework.manager.OpenAxisErrorManager;
import com.abb.framework.manager.OpenAxisManager;
import com.abb.serviceAccessor.farelogix.manager.FarelogixConfiguration;

/**
 * The Class OpenAxisController.
 */
@Controller
public class OpenAxisController {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(OpenAxisController.class);

	/** The error manager. */
	@Autowired
	private OpenAxisErrorManager errorManager;

	/** The authentication service. */
	@Autowired
	private AuthenticationService authenticationService;

	/** The open axis manager. */
	@Autowired
	private OpenAxisManager openAxisManager;

	/** The app context. */
	@Autowired
	private ApplicationContext appContext;

	/**
	 * Service end point.
	 * 
	 * @param serviceName
	 *            the service name
	 * @param requestMessage
	 *            the request message
	 * @param request
	 *            the request
	 * @return the string
	 */
	@RequestMapping(value = "{serviceName}", method = RequestMethod.POST)
	@ResponseBody
	public String serviceEndPoint(@PathVariable String serviceName, @RequestBody String requestMessage, HttpServletRequest request) {
		// Replace extra space between tags
		requestMessage = requestMessage.replaceAll(">\\s+<", "><");
		// Replace extra space between attributes or data with one space
		requestMessage = requestMessage.replaceAll("\\s+", " ").trim();

		// CC Masking-INTA34
		String logMaskedRequestMessage = requestMessage;

		// CC NUMBER TO BE MASKED IN requestMessage
		String creditCardNumberInSaResponse = StringUtils.substringBetween(logMaskedRequestMessage, "<CCNumber>", "</CCNumber>");
		String expirationMonthInSaResponse = StringUtils.substringBetween(logMaskedRequestMessage, "<Month>", "</Month>");
		String expirationYearInSaResponse = StringUtils.substringBetween(logMaskedRequestMessage, "<Year>", "</Year>");

		if (creditCardNumberInSaResponse != null && !"".equalsIgnoreCase(creditCardNumberInSaResponse.trim()) && creditCardNumberInSaResponse.trim().length() > 0) {
			String maskedCCNumber = StringUtils.leftPad(creditCardNumberInSaResponse.substring(creditCardNumberInSaResponse.length() - 4), creditCardNumberInSaResponse.length(), 'X');
			logMaskedRequestMessage = logMaskedRequestMessage.replace(creditCardNumberInSaResponse, maskedCCNumber);
			logMaskedRequestMessage = logMaskedRequestMessage.replace(expirationMonthInSaResponse, expirationMonthInSaResponse.replaceAll("\\d", "X"));
			logMaskedRequestMessage = logMaskedRequestMessage.replace(expirationYearInSaResponse, expirationYearInSaResponse.replaceAll("\\d", "X"));
		}

		LOG.info("Request XML =  {}", logMaskedRequestMessage);

		String responseMessage = null;
		OpenAxisService openAxisService = null;

		try {
			if (requestMessage.length() == 0) {
				throw new OpenAirRequestXMLNotFoundException();
			}

			openAxisService = OpenAxisService.getOpenAxisService(serviceName);

			// Validate XML against its Schema
			// XMLUtils.validateXMLAgainstXSD(requestMessage,
			// getAbsolutePathOfSchema(openAxisService.getSchema()));
			LOG.info("Request XML is valid.");

			// Authentication
			boolean authenticateResult = true;// authenticationService.authenticateRequest(requestMessage);

			// INTA-51 TC Header Security Implementation for FLX
			// AUTHENTICATE_XML flag in Farelogix.Configuration.Properties. TRUE=PERFORM AUTHENTICATION. FALSE=DON'T PERFORM
			if (FarelogixConfiguration.isAuthenticateXML()) {
				authenticateResult = false;
				LOG.info("Authenticate Request.");
				authenticateResult = authenticationService.authenticateRequest(requestMessage);
			}

			if (authenticateResult) {
				responseMessage = openAxisManager.processRequest(requestMessage, openAxisService);
			} else {
				throw new OpenAirInvalidAuthenticationException();
			}
		} catch (OpenAirException ex) {
			LOG.error("Received Error in OpenAxisController: ", ex);
			responseMessage = errorManager.getErrorResponseForException(ex, openAxisService);
		}
		return responseMessage;
	}

	/**
	 * Gets the absolute path of schema.
	 * 
	 * @param schemaFile
	 *            the schema file
	 * @return the absolute path of schema
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public String getAbsolutePathOfSchema(String schemaFile) throws OpenAirException {
		try {
			return appContext.getResource("classpath:" + schemaFile).getFile().getAbsolutePath();
		} catch (IOException ex) {
			throw new OpenAirException(ex);
		}
	}
}
