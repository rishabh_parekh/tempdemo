package com.abb.framework.util;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.abb.framework.exception.OpenAirException;
import com.abb.framework.transformation.OpenAirTransformation;
import com.abb.serviceprovider.common.ApplicationContextUtil;
import com.abb.serviceprovider.common.CommonConstant;

public class CommonValidationUtil {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CommonValidationUtil.class);

	/**
	 * INTA-69 1. Removes the space/Special characters from First and Last name. 2. Replace Accent Characters
	 * 
	 * @param xmlStr
	 *            the xml str
	 * @return the string
	 * @throws OpenAirException
	 *             the open air exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public String removeSpaceAndReplaceAccentChars(String xmlStr) throws OpenAirException {
		// replacing accent characters
		if (!"".equalsIgnoreCase(xmlStr.trim())) {
			xmlStr = StringUtils.replaceEach(xmlStr.trim(), CommonConstant.accentChars.split(","), CommonConstant.englishChars.split(","));
		}
		try {
			// removing space characters from name
			OpenAirTransformation xmlTransformer = ApplicationContextUtil.getBean("XMLTransformation", OpenAirTransformation.class);
			Resource xmlResource = new ClassPathResource(CommonConstant.removeSpaceXSLFilePath);
			LOG.debug("Special Char XSLT path ::=" + xmlResource.getFile().getAbsolutePath());
			xmlStr = xmlTransformer.transform(xmlStr, xmlStr, xmlResource.getFile().getAbsolutePath(), null);
		} catch (IOException e) {
			LOG.error("IOException in getting SpecialChar XSL::= {}", e.getMessage());
		}
		return xmlStr;
	}

}
