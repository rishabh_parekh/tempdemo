package com.abb.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.abb.serviceprovider.common.CommonConstant;

/**
 * The Class Utils.
 */
public class Utils {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(Utils.class);

	/**
	 * This method will get reason for fault from exception and generate response string to be passed in response.
	 * 
	 * @param e
	 *            the e
	 * @return String
	 */
	public static String getErrorResponseForFaultWithMessage(SoapFaultClientException e) {
		String responseString = null;
		try {
			StringBuffer falutResponse = new StringBuffer("<Fault><faultstring>");
			falutResponse.append(e.getSoapFault().getFaultStringOrReason());
			falutResponse.append("</faultstring></Fault>");
			responseString = falutResponse.toString();
		} catch (Exception ex) {
			LOG.error("\n Exception **************", e);
			LOG.error(CommonConstant.ERROR_MSG, ex);
		}

		return responseString;

	}

	/**
	 * Checks if is null. if Null=TRUE else FALSE
	 * 
	 * @param object
	 *            the object
	 * @return true, if is null
	 */
	public static boolean isNull(Object object) {
		return (null == object) ? true : false;
	}

	/**
	 * Checks if is not null. if Null=FALSE else TRUE
	 * 
	 * @param object
	 *            the object
	 * @return true, if is not null
	 */
	public static boolean isNotNull(Object object) {
		return (null == object) ? false : true;
	}
}
