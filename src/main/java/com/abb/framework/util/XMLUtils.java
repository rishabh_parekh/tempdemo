package com.abb.framework.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.abb.framework.exception.OpenAirException;
import com.abb.framework.exception.OpenAirInvalidXMLException;

/**
 * Utility class for XML.
 * 
 * @author irfanak
 * 
 */
public class XMLUtils {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(XMLUtils.class);

	/**
	 * Method to validate XML file against its given XSD or schema.
	 * 
	 * @param sourceXML
	 *            the source xml
	 * @param pathXSD
	 *            the path xsd
	 * @return Message for valid XML and Error message in case of Invalid XML
	 * @throws OpenAirInvalidXMLException
	 *             the open air invalid xml exception
	 */
	public static void validateXMLAgainstXSD(String sourceXML, String pathXSD) throws OpenAirInvalidXMLException {

		LOG.info("Validate XML againsts XSD.");

		LOG.debug("Source XML = {}", sourceXML);
		LOG.debug("Schema Location = {}", pathXSD);

		// Specify factory for W3C XML Schema language
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

		// Load the specific schema you want.
		File schemaLocation = new File(pathXSD);

		// Compile the schema.
		Schema schema;
		try {
			schema = factory.newSchema(schemaLocation);

			// Get a validator from the schema.
			Validator validator = schema.newValidator();

			// Parse the document you want to check.
			Source source = new StreamSource(new StringReader(sourceXML));

			// Check the document
			validator.validate(source);
			LOG.info("Successfully Validate XML againsts XSD.");
		} catch (SAXException e) {
			throw new OpenAirInvalidXMLException(e);
		} catch (IOException e) {
			throw new OpenAirInvalidXMLException(e);
		}
	}

	/**
	 * Return ChildNode with its children as String for given parentTag from given XML string.
	 * 
	 * @param soapRequestXML
	 *            -xml as String
	 * @param parentTagName
	 *            -parent tag name
	 * @return ChildNode as String
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public static String getChildNodeAsStringFromXML(String soapRequestXML, String parentTagName) throws OpenAirException {
		InputStream requestXMLInputStream = new ByteArrayInputStream(soapRequestXML.getBytes());
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		String requestXMLBody = "";
		Document doc = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(requestXMLInputStream);
			NodeList requestNodeList = doc.getElementsByTagName(parentTagName);
			Node node = requestNodeList.item(0);
			DOMImplementationLS domImplLS = (DOMImplementationLS) doc.getImplementation();
			LSSerializer serializer = domImplLS.createLSSerializer();
			if (node != null && node.getFirstChild() != null) {
				requestXMLBody = serializer.writeToString(node.getFirstChild()).trim();
			}
			return requestXMLBody;
		} catch (SAXException e) {
			throw new OpenAirInvalidXMLException(e);
		} catch (IOException e) {
			throw new OpenAirInvalidXMLException(e);
		} catch (ParserConfigurationException e) {
			throw new OpenAirException(e);
		}
	}

	/**
	 * Load the XML and get Document object of given XML as string.
	 * 
	 * @param xml
	 *            - xml as String
	 * @return Document object
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public static Document loadXMLFromString(String xml) throws OpenAirException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();

			InputSource is = new InputSource(new StringReader(xml));
			return builder.parse(is);
		} catch (ParserConfigurationException e) {
			throw new OpenAirException(e);
		} catch (SAXException e) {
			throw new OpenAirException(e);
		} catch (IOException e) {
			throw new OpenAirException(e);
		}
	}

	/**
	 * Get tag as String from given XML.
	 * 
	 * @param xmlStr
	 *            the xml str
	 * @param tagName
	 *            the tag name
	 * @return the complete tag as string
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public static String getCompleteTagAsString(String xmlStr, String tagName) throws OpenAirException {
		Document doc = loadXMLFromString(xmlStr);
		Element rootNode = doc.getDocumentElement();

		NodeList list = rootNode.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			DOMImplementationLS domImplLS = (DOMImplementationLS) doc.getImplementation();
			LSSerializer serializer = domImplLS.createLSSerializer();
			return serializer.writeToString(list.item(0)).trim();

		}
		return null;
	}

	/**
	 * Get the value of the given tag name from given XML.
	 * 
	 * @param xmlStr
	 *            the xml str
	 * @param tagName
	 *            the tag name
	 * @return the tag value
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public static String getTagValue(String xmlStr, String tagName) throws OpenAirException {
		Document doc = loadXMLFromString(xmlStr);
		Element rootNode = doc.getDocumentElement();

		NodeList list = rootNode.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			NodeList subList = list.item(0).getChildNodes();

			if (subList != null && subList.getLength() > 0) {
				return subList.item(0).getNodeValue();
			}
		}
		return null;
	}

	/**
	 * Get the values of the given tag name from given XML.
	 * 
	 * @param xmlStr
	 *            the xml str
	 * @param tagName
	 *            the tag name
	 * @return the tag values
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public static List<String> getTagValues(String xmlStr, String tagName) throws OpenAirException {
		Document doc = loadXMLFromString(xmlStr);
		Element rootNode = doc.getDocumentElement();
		List<String> tagValues = new ArrayList<String>();

		NodeList list = rootNode.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			for (int i = 0; i < list.getLength(); i++) {
				NodeList subList = list.item(i).getChildNodes();

				if (subList != null && subList.getLength() > 0) {
					tagValues.add(subList.item(0).getNodeValue());
				}
			}
		}
		return tagValues;
	}

	/**
	 * Get the value of the given attribute name from given tag.
	 * 
	 * @param xmlStr
	 *            the xml str
	 * @param tagName
	 *            the tag name
	 * @param attributeName
	 *            the attribute name
	 * @return the attribute value
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public static String getAttributeValue(String xmlStr, String tagName, String attributeName) throws OpenAirException {

		Document doc = loadXMLFromString(xmlStr);
		Element rootNode = doc.getDocumentElement();

		if (rootNode.getNodeName().equals(tagName)) {
			return rootNode.getAttributes().getNamedItem(attributeName).getNodeValue();
		} else {
			NodeList list = rootNode.getElementsByTagName(tagName);
			// To check if Attribute is missing added 3rd argument check
			if (list != null && list.getLength() > 0 && list.item(0).getAttributes().getNamedItem(attributeName) != null) {
				return list.item(0).getAttributes().getNamedItem(attributeName).getNodeValue();
			}
		}
		return null;
	}

	/**
	 * Get the value of the given attribute name from given tag.
	 * 
	 * @param xmlStr
	 *            the xml str
	 * @param tagName
	 *            the tag name
	 * @param attributeName
	 *            the attribute name
	 * @return the attribute values
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public static String[] getAttributeValues(String xmlStr, String tagName, String attributeName) throws OpenAirException {
		Document doc = loadXMLFromString(xmlStr);
		Element rootNode = doc.getDocumentElement();
		List<String> attributeValues = new ArrayList<String>();

		NodeList list = rootNode.getElementsByTagName(tagName);
		if (list != null && list.getLength() > 0) {
			for (int i = 0; i < list.getLength(); i++) {
				attributeValues.add(list.item(i).getAttributes().getNamedItem(attributeName).getNodeValue());
			}
			return attributeValues.toArray(new String[attributeValues.size()]);
		}

		return null;
	}

}
