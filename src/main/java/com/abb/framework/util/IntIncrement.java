package com.abb.framework.util;


/**
 * Increment int value.
 *
 * @author ankurr
 */
public class IntIncrement {

	/** The counter. */
	private int counter = 1;
	
	/** The counter static. */
	private static int counterStatic = 0;
	
	/** The new counter. */
	private static int newCounter = 1;

	/**
	 * Instantiates a new int increment.
	 */
	public IntIncrement() {
	}

	/**
	 * Instantiates a new int increment.
	 *
	 * @param startValue the start value
	 */
	public IntIncrement(int startValue) {
		this.counter = startValue;
	}

	/**
	 * Get next int value.
	 *
	 * @return the next
	 */
	public int getNext() {
		return this.counter++;
	}

	/**
	 * *
	 * RESET COUNTER VALUE = 1.
	 *
	 * @return the int
	 */
	public int resetCounter() {
		return this.counter = newCounter;
	}

	/**
	 * Gets the incremented value.
	 *
	 * @param paramValue the param value
	 * @return the incremented value
	 */
	public static int getIncrementedValue(String paramValue) {

		if (paramValue.equalsIgnoreCase("1")) {
			counterStatic++;
		} else {
			counterStatic = 0;
		}

		return counterStatic;
	}
}
