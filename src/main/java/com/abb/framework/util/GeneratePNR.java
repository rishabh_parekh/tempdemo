package com.abb.framework.util;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class generates 6 digit PNR, validate 6 digit PNR.
 * 
 * @author ankurr
 * 
 */
public class GeneratePNR {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(GeneratePNR.class);

	/**
	 * Get PNR number.
	 *
	 * @return the pnr
	 */
	public static String getPNR() {

		String randomAlphaNumeric = RandomStringUtils.randomAlphanumeric(5).toUpperCase();

		int sumOfAlphaNumeric = getSumOfAlphaNumeric(randomAlphaNumeric);
		int getLastDigit = getSumOfDigits(sumOfAlphaNumeric);

		String superPNR = randomAlphaNumeric + getLastDigit;
		return superPNR;
	}

	/**
	 * Sum of ASCII value of chars and number.
	 *
	 * @param randomAlphaNumeric the random alpha numeric
	 * @return the sum of alpha numeric
	 */
	private static int getSumOfAlphaNumeric(String randomAlphaNumeric) {
		int sum = 0;
		for (int i = 0; i < randomAlphaNumeric.length(); i++) {
			Character character = randomAlphaNumeric.charAt(i);
			if (Character.isDigit(character)) {
				sum += Integer.parseInt(character.toString());
			} else {
				sum += (int) character;
			}
		}
		return sum;
	}

	/**
	 * Sums of Digits. If its 2 digit number then calculate sum of digits else return single digit number.
	 *
	 * @param num the num
	 * @return the sum of digits
	 */
	private static int getSumOfDigits(int num) {
		if (num < 10) {
			return num;
		} else {
			int sum = 0;
			while (num > 0) {
				int p = num % 10;
				sum = sum + p;
				num = num / 10;
			}
			return getSumOfDigits(sum);
		}
	}

	/**
	 * Validate PNR.
	 *
	 * @param pnrNumber the pnr number
	 * @return true, if successful
	 */
	public static boolean validatePNR(String pnrNumber) {
		try {
			int lastDigit = Integer.parseInt(StringUtils.right(pnrNumber, 1));
			int sum = getSumOfAlphaNumeric(StringUtils.left(pnrNumber, 5));
			int resultDigit = getSumOfDigits(sum);

			return (lastDigit == resultDigit);
		} catch (Exception ex) {
			LOG.error("", ex);
			return false;
		}
	}
}
