package com.abb.framework.exception;


/**
 * The Class OpenAirMergingException.
 */
public class OpenAirMergingException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3031478342787917132L;

	/**
	 * Instantiates a new open air merging exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirMergingException(Exception ex) {
		super(ex);
	}

}
