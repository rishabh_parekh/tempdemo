package com.abb.framework.exception;


/**
 * The Class OpenAirSPServiceException.
 */
public class OpenAirSPServiceException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air sp service exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirSPServiceException(Exception ex) {
		super(ex);
	}

}
