package com.abb.framework.exception;


/**
 * The Class OpenAirRequestXMLNotFoundException.
 */
public class OpenAirRequestXMLNotFoundException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air request xml not found exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirRequestXMLNotFoundException(Exception ex) {
		super(ex);
	}

	/**
	 * Instantiates a new open air request xml not found exception.
	 */
	public OpenAirRequestXMLNotFoundException() {
		super(new Exception());
	}

}
