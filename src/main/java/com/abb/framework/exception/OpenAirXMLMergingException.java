package com.abb.framework.exception;


/**
 * The Class OpenAirXMLMergingException.
 */
public class OpenAirXMLMergingException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air xml merging exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirXMLMergingException(Exception ex) {
		super(ex);
	}

}
