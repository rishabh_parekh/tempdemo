package com.abb.framework.exception;


/**
 * The Class OpenAirRemoteException.
 */
public class OpenAirRemoteException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air remote exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirRemoteException(Exception ex) {
		super(ex);
	}

}
