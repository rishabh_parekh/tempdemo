package com.abb.framework.exception;


/**
 * The Class OpenAirServiceNotFoundForProviderException.
 */
public class OpenAirServiceNotFoundForProviderException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air service not found for provider exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirServiceNotFoundForProviderException(Exception ex) {
		super(ex);
	}

}
