package com.abb.framework.exception;


/**
 * The Class OpenAirException.
 */
public class OpenAirException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air exception.
	 *
	 * @param message the message
	 */
	public OpenAirException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new open air exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirException(Exception ex) {
		super(ex);
	}
}
