package com.abb.framework.exception;


/**
 * The Class OpenAirInvalidServiceException.
 */
public class OpenAirInvalidServiceException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air invalid service exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirInvalidServiceException(Exception ex) {
		super(ex);
	}

	/**
	 * Instantiates a new open air invalid service exception.
	 *
	 * @param message the message
	 */
	public OpenAirInvalidServiceException(String message) {
		super(message);
	}

}
