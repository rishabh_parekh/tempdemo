package com.abb.framework.exception;


/**
 * The Class OpenAirInActiveServiceProvideException.
 */
public class OpenAirInActiveServiceProvideException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air in active service provide exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirInActiveServiceProvideException(Exception ex) {
		super(ex);
	}

}
