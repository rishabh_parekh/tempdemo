package com.abb.framework.exception;


/**
 * Call to present exception while transformation. This exception will be thrown by transformation method.
 * 
 * @author ankurr
 * 
 */
public class OpenAirTransformationException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air transformation exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirTransformationException(Exception ex) {
		super(ex);
	}

}
