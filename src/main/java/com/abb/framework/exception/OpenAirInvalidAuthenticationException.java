package com.abb.framework.exception;

/**
 * The Class OpenAirInvalidAuthenticationException.
 */
public class OpenAirInvalidAuthenticationException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new lute invalid authentication exception.
	 */
	public OpenAirInvalidAuthenticationException() {
		super(new Exception("Authentication Failed."));
	}

	/**
	 * Instantiates a new open air invalid authentication exception.
	 * 
	 * @param ex
	 *            the ex
	 */
	public OpenAirInvalidAuthenticationException(Exception ex) {
		super(ex);
	}

}
