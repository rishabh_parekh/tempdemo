package com.abb.framework.exception;


/**
 * The Class OpenAirXMLTransformationException.
 */
public class OpenAirXMLTransformationException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air xml transformation exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirXMLTransformationException(Exception ex) {
		super(ex);
	}
}
