package com.abb.framework.exception;


/**
 * The Class OpenAirSPLoginException.
 */
public class OpenAirSPLoginException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air sp login exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirSPLoginException(Exception ex) {
		super(ex);
	}

	/**
	 * Instantiates a new open air sp login exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirSPLoginException(String ex) {
		super(ex);
	}
}
