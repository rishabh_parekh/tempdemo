package com.abb.framework.exception;


/**
 * The Class OpenAirInvalidXMLException.
 */
public class OpenAirInvalidXMLException extends OpenAirException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new open air invalid xml exception.
	 *
	 * @param ex the ex
	 */
	public OpenAirInvalidXMLException(Exception ex) {
		super(ex);
	}
}
