package com.abb.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The Class OpenAirUser.
 */
@Entity
@Table(name = "openair_user")
public class OpenAirUser {

	/** The open air user id. */
	@Id
	@Column(name = "user_id")
	private long openAirUserId;

	/** The username. */
	@Column(name = "username")
	private String username;

	/** The password. */
	@Column(name = "password")
	private String password;

	/** The active flag. */
	@Column(name = "active_flag")
	private boolean activeFlag;

	/**
	 * Gets the open air user id.
	 *
	 * @return the open air user id
	 */
	public long getOpenAirUserId() {
		return openAirUserId;
	}

	/**
	 * Sets the open air user id.
	 *
	 * @param openAirUserId the new open air user id
	 */
	public void setOpenAirUserId(long openAirUserId) {
		this.openAirUserId = openAirUserId;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Checks if is active flag.
	 *
	 * @return true, if is active flag
	 */
	public boolean isActiveFlag() {
		return activeFlag;
	}

	/**
	 * Sets the active flag.
	 *
	 * @param activeFlag the new active flag
	 */
	public void setActiveFlag(boolean activeFlag) {
		this.activeFlag = activeFlag;
	}
}
