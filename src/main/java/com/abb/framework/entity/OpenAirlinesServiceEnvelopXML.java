package com.abb.framework.entity;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * Container class for the ServiceAccessor request, ServiceProvider request and response XMLs.
 * 
 * @author irfanak
 * 
 */

@Component
@Scope("prototype")
public class OpenAirlinesServiceEnvelopXML {

	/** The sa request xml. */
	private String saRequestXML;

	/** The sa response xml. */
	private String saResponseXML;

	/** The sp login response xml. */
	private String spLoginResponseXML;

	/** The rest service envelop xml. */
	private RESTServiceEnvelopXML restServiceEnvelopXML;

	/** The soap service envelop xml. */
	private SOAPServiceEnvelopXML soapServiceEnvelopXML;

	/** The sp response xml. */
	private String spResponseXML;

	/** The service wrapper. */
	private String SERVICE_WRAPPER = "ABBService";
	
	/** The sarequest wrapper. */
	private String SAREQUEST_WRAPPER = "SARequest";
	
	/** The saresponse wrapper. */
	private String SARESPONSE_WRAPPER = "SAResponse";
	
	/** The sprequest wrapper. */
	private String SPREQUEST_WRAPPER = "SPRequest";
	
	/** The spresponse wrapper. */
	private String SPRESPONSE_WRAPPER = "SPResponse";
	
	/** The splogin response wrapper. */
	private String SPLOGIN_RESPONSE_WRAPPER = "SPLoginResponse";

	/** The Constant LESSTHAN_SIGN. */
	private static final char LESSTHAN_SIGN = '<';
	
	/** The Constant CLOSETAG_SIGN. */
	private static final String CLOSETAG_SIGN = "</";
	
	/** The Constant GREATERTHAN_SIGN. */
	private static final char GREATERTHAN_SIGN = '>';

	/**
	 * Gets the service xml.
	 *
	 * @return the service xml
	 */
	public String getServiceXML() {

		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SERVICE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		// SARequest
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SAREQUEST_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);
		strBuilder.append(StringUtils.defaultString(this.saRequestXML).replaceAll("<\\?.*?>", ""));
		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SAREQUEST_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		// SAResponse
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SARESPONSE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);
		strBuilder.append(StringUtils.defaultString(this.saResponseXML).replaceAll("<\\?.*?>", ""));
		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SARESPONSE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		// SPLoginResponse
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SPLOGIN_RESPONSE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);
		strBuilder.append(StringUtils.defaultString(this.spLoginResponseXML).replaceAll("<\\?.*?>", ""));
		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SPLOGIN_RESPONSE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		// SPRequest
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SPREQUEST_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);
		if (restServiceEnvelopXML != null && StringUtils.defaultString(restServiceEnvelopXML.getSPRequest()).length() > 0)
			strBuilder.append(StringUtils.defaultString(this.restServiceEnvelopXML.getSPRequest()).replaceAll("<\\?.*?>", ""));
		else
			strBuilder.append(StringUtils.defaultString(this.soapServiceEnvelopXML.getServiceXML()).replaceAll("<\\?.*?>", ""));
		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SPREQUEST_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		// SPResponse
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SPRESPONSE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);
		strBuilder.append(StringUtils.defaultString(this.spResponseXML).replaceAll("<\\?.*?>", ""));
		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SPRESPONSE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SERVICE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		return strBuilder.toString();
	}

	/**
	 * Gets the sa request xml.
	 *
	 * @return the sa request xml
	 */
	public String getSaRequestXML() {
		return saRequestXML;
	}

	/**
	 * Sets the sa request xml.
	 *
	 * @param saRequestXML the new sa request xml
	 */
	public void setSaRequestXML(String saRequestXML) {
		this.saRequestXML = saRequestXML;
	}

	/**
	 * Gets the sa response xml.
	 *
	 * @return the sa response xml
	 */
	public String getSaResponseXML() {
		return saResponseXML;
	}

	/**
	 * Sets the sa response xml.
	 *
	 * @param saResponseXML the new sa response xml
	 */
	public void setSaResponseXML(String saResponseXML) {
		this.saResponseXML = saResponseXML;
	}

	/**
	 * Gets the sp login response xml.
	 *
	 * @return the sp login response xml
	 */
	public String getSpLoginResponseXML() {
		return spLoginResponseXML;
	}

	/**
	 * Sets the sp login response xml.
	 *
	 * @param spLoginResponseXML the new sp login response xml
	 */
	public void setSpLoginResponseXML(String spLoginResponseXML) {
		this.spLoginResponseXML = spLoginResponseXML;
	}

	/**
	 * Gets the rest service envelop xml.
	 *
	 * @return the rest service envelop xml
	 */
	public RESTServiceEnvelopXML getRestServiceEnvelopXML() {
		return restServiceEnvelopXML;
	}

	/**
	 * Sets the rest service envelop xml.
	 *
	 * @param restServiceEnvelopXML the new rest service envelop xml
	 */
	public void setRestServiceEnvelopXML(RESTServiceEnvelopXML restServiceEnvelopXML) {
		this.restServiceEnvelopXML = restServiceEnvelopXML;
	}

	/**
	 * Gets the soap service envelop xml.
	 *
	 * @return the soap service envelop xml
	 */
	public SOAPServiceEnvelopXML getSoapServiceEnvelopXML() {
		if (soapServiceEnvelopXML == null)
			soapServiceEnvelopXML = new SOAPServiceEnvelopXML();
		return soapServiceEnvelopXML;
	}

	/**
	 * Sets the soap service envelop xml.
	 *
	 * @param soapServiceEnvelopXML the new soap service envelop xml
	 */
	public void setSoapServiceEnvelopXML(SOAPServiceEnvelopXML soapServiceEnvelopXML) {
		this.soapServiceEnvelopXML = soapServiceEnvelopXML;
	}

	/**
	 * Gets the sp response xml.
	 *
	 * @return the sp response xml
	 */
	public String getSpResponseXML() {
		return spResponseXML;
	}

	/**
	 * Sets the sp response xml.
	 *
	 * @param spResponseXML the new sp response xml
	 */
	public void setSpResponseXML(String spResponseXML) {
		this.spResponseXML = spResponseXML;
	}

	/**
	 * Append sp response xml.
	 *
	 * @param spResponseXML the sp response xml
	 */
	public void appendSpResponseXML(String spResponseXML) {
		this.spResponseXML += spResponseXML;
	}
}
