package com.abb.framework.entity;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * Container class for the ServiceAccessor request, ServiceProvider request and its response XMLs.
 * 
 * @author irfanak
 * 
 */
@Component
@Scope("prototype")
public class RESTServiceEnvelopXML {

	/** The sp request xml. */
	private String spRequestXML;

	/**
	 * Sets the SP request.
	 *
	 * @param spRequestXML the new SP request
	 */
	public void setSPRequest(String spRequestXML) {
		this.spRequestXML = spRequestXML;
	}

	/**
	 * Append sp request.
	 *
	 * @param spRequestXML the sp request xml
	 */
	public void appendSPRequest(String spRequestXML) {
		this.spRequestXML += spRequestXML;
	}

	/**
	 * Gets the SP request.
	 *
	 * @return the SP request
	 */
	public String getSPRequest() {
		return this.spRequestXML;
	}
}
