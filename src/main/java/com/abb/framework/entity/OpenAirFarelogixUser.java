package com.abb.framework.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is an entity class for Farelogix users. It is mapped with abb_farelogix_user database table.
 * 
 * @author rishabhp Created on 16/09/14
 * 
 */

@Entity
@Table(name = "abb_farelogix_user")
public class OpenAirFarelogixUser {

	/** The farelogix user id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "flx_user_id")
	private int farelogixUserId;

	/** The agency user name. */
	@Column(name = "agency_username")
	private String agencyUserName;

	/** The agency password. */
	@Column(name = "agency_password")
	private String agencyPassword;

	/** The agent user name. */
	@Column(name = "agent_username")
	private String agentUserName;

	/** The agent password. */
	@Column(name = "agent_password")
	private String agentPassword;

	/** The created date. */
	@Column(name = "created_date")
	private Date createdDate;

	/** The check is active. */
	@Column(name = "check_is_active")
	private boolean checkIsActive;

	/**
	 * Gets the farelogix user id.
	 * 
	 * @return the farelogix user id
	 */
	public int getFarelogixUserId() {
		return farelogixUserId;
	}

	/**
	 * Sets the farelogix user id.
	 * 
	 * @param farelogixUserId
	 *            the new farelogix user id
	 */
	public void setFarelogixUserId(int farelogixUserId) {
		this.farelogixUserId = farelogixUserId;
	}

	/**
	 * Gets the agency user name.
	 * 
	 * @return the agency user name
	 */
	public String getAgencyUserName() {
		return agencyUserName;
	}

	/**
	 * Sets the agency user name.
	 * 
	 * @param agencyUserName
	 *            the new agency user name
	 */
	public void setAgencyUserName(String agencyUserName) {
		this.agencyUserName = agencyUserName;
	}

	/**
	 * Gets the agency password.
	 * 
	 * @return the agency password
	 */
	public String getAgencyPassword() {
		return agencyPassword;
	}

	/**
	 * Sets the agency password.
	 * 
	 * @param agencyPassword
	 *            the new agency password
	 */
	public void setAgencyPassword(String agencyPassword) {
		this.agencyPassword = agencyPassword;
	}

	/**
	 * Gets the agent user name.
	 * 
	 * @return the agent user name
	 */
	public String getAgentUserName() {
		return agentUserName;
	}

	/**
	 * Sets the agent user name.
	 * 
	 * @param agentUserName
	 *            the new agent user name
	 */
	public void setAgentUserName(String agentUserName) {
		this.agentUserName = agentUserName;
	}

	/**
	 * Gets the agent password.
	 * 
	 * @return the agent password
	 */
	public String getAgentPassword() {
		return agentPassword;
	}

	/**
	 * Sets the agent password.
	 * 
	 * @param agentPassword
	 *            the new agent password
	 */
	public void setAgentPassword(String agentPassword) {
		this.agentPassword = agentPassword;
	}

	/**
	 * Gets the created date.
	 * 
	 * @return the created date
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the created date.
	 * 
	 * @param createdDate
	 *            the new created date
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Checks if is check is active.
	 * 
	 * @return true, if is check is active
	 */
	public boolean isCheckIsActive() {
		return checkIsActive;
	}

	/**
	 * Sets the check is active.
	 * 
	 * @param checkIsActive
	 *            the new check is active
	 */
	public void setCheckIsActive(boolean checkIsActive) {
		this.checkIsActive = checkIsActive;
	}

}
