package com.abb.framework.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The Class OpenAirSuperPNR.
 */
@Entity
@Table(name = "openairsuperpnr")
public class OpenAirSuperPNR {

	/** The super pnr id. */
	@Id
	private long superPNRId;

	/** The super pnr. */
	@Column
	private String superPNR;

	/**
	 * Gets the super pnr id.
	 *
	 * @return the super pnr id
	 */
	public long getSuperPNRId() {
		return superPNRId;
	}

	/**
	 * Sets the super pnr id.
	 *
	 * @param superPNRId the new super pnr id
	 */
	public void setSuperPNRId(long superPNRId) {
		this.superPNRId = superPNRId;
	}

	/**
	 * Gets the super pnr.
	 *
	 * @return the super pnr
	 */
	public String getSuperPNR() {
		return superPNR;
	}

	/**
	 * Sets the super pnr.
	 *
	 * @param superPNR the new super pnr
	 */
	public void setSuperPNR(String superPNR) {
		this.superPNR = superPNR;
	}
}
