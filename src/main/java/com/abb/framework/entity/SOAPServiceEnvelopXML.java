package com.abb.framework.entity;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * Container class for the ServiceAccessor request, ServiceProvider request and its response XMLs.
 * 
 * @author irfanak
 * 
 */
@Component
@Scope("prototype")
public class SOAPServiceEnvelopXML {

	/** The sp soap request header xml. */
	private String spSOAPRequestHeaderXML;
	
	/** The sp soap request body xml. */
	private String spSOAPRequestBodyXML;
	
	/** The sp soap action. */
	private String spSOAPAction;
	
	/** The sp soap service url. */
	private String spSOAPServiceURL;

	/** The service wrapper. */
	private String SERVICE_WRAPPER = "OpenAirSOAPEnvelop";
	
	/** The sp header wrapper. */
	private String SP_HEADER_WRAPPER = "OpenAirSOAPHeader";
	
	/** The sp body wrapper. */
	private String SP_BODY_WRAPPER = "OpenAirSOAPBody";

	/** The Constant LESSTHAN_SIGN. */
	private static final char LESSTHAN_SIGN = '<';
	
	/** The Constant CLOSETAG_SIGN. */
	private static final String CLOSETAG_SIGN = "</";
	
	/** The Constant GREATERTHAN_SIGN. */
	private static final char GREATERTHAN_SIGN = '>';

	/**
	 * Gets the service xml.
	 *
	 * @return the service xml
	 */
	public String getServiceXML() {

		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SERVICE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		// SP Request Header
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SP_HEADER_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);
		strBuilder.append(StringUtils.defaultString(this.spSOAPRequestHeaderXML).replaceAll("<\\?.*?>", ""));
		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SP_HEADER_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		// SP Request Body
		strBuilder.append(LESSTHAN_SIGN);
		strBuilder.append(SP_BODY_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);
		strBuilder.append(StringUtils.defaultString(this.spSOAPRequestBodyXML).replaceAll("<\\?.*?>", ""));
		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SP_BODY_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		strBuilder.append(CLOSETAG_SIGN);
		strBuilder.append(SERVICE_WRAPPER);
		strBuilder.append(GREATERTHAN_SIGN);

		return strBuilder.toString();
	}

	/**
	 * Gets the sp soap request header xml.
	 *
	 * @return the sp soap request header xml
	 */
	public String getSpSOAPRequestHeaderXML() {
		return spSOAPRequestHeaderXML;
	}

	/**
	 * Sets the sp soap request header xml.
	 *
	 * @param spSOAPRequestHeaderXML the new sp soap request header xml
	 */
	public void setSpSOAPRequestHeaderXML(String spSOAPRequestHeaderXML) {
		this.spSOAPRequestHeaderXML = spSOAPRequestHeaderXML;
	}

	/**
	 * Gets the sp soap request body xml.
	 *
	 * @return the sp soap request body xml
	 */
	public String getSpSOAPRequestBodyXML() {
		return spSOAPRequestBodyXML;
	}

	/**
	 * Sets the sp soap request body xml.
	 *
	 * @param spSOAPRequestBodyXML the new sp soap request body xml
	 */
	public void setSpSOAPRequestBodyXML(String spSOAPRequestBodyXML) {
		this.spSOAPRequestBodyXML = spSOAPRequestBodyXML;
	}

	/**
	 * Gets the sp soap action.
	 *
	 * @return the sp soap action
	 */
	public String getSpSOAPAction() {
		return spSOAPAction;
	}

	/**
	 * Sets the sp soap action.
	 *
	 * @param spSOAPAction the new sp soap action
	 */
	public void setSpSOAPAction(String spSOAPAction) {
		this.spSOAPAction = spSOAPAction;
	}

	/**
	 * Gets the sp soap service url.
	 *
	 * @return the sp soap service url
	 */
	public String getSpSOAPServiceURL() {
		return spSOAPServiceURL;
	}

	/**
	 * Sets the sp soap service url.
	 *
	 * @param spSOAPServiceURL the new sp soap service url
	 */
	public void setSpSOAPServiceURL(String spSOAPServiceURL) {
		this.spSOAPServiceURL = spSOAPServiceURL;
	}
}
