package com.abb.framework.manager;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.abb.framework.entity.OpenAirlinesServiceEnvelopXML;
import com.abb.framework.enums.OpenAxisService;
import com.abb.framework.exception.OpenAirException;
import com.abb.framework.exception.OpenAirServiceNotFoundForProviderException;
import com.abb.framework.serviceprovider.SPComposite;
import com.abb.framework.serviceprovider.SPService;
import com.abb.framework.serviceprovider.SPServiceExecutor;
import com.abb.framework.serviceprovider.ServiceProvider;
import com.abb.framework.util.CommonValidationUtil;
import com.abb.framework.util.XMLUtils;
import com.abb.serviceprovider.jetAirways.JAServiceProvider;
import com.abb.serviceprovider.jetStar.JSServiceProvider;

/**
 * The Class OpenAxisManager.
 */
@Service
public class OpenAxisManager {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(OpenAxisManager.class);

	/** The app context. */
	@Autowired
	private ApplicationContext appContext;

	/** The sp service executor. */
	@Autowired
	private SPServiceExecutor spServiceExecutor;

	/**
	 * Process request.
	 * 
	 * @param requestXML
	 *            the request xml
	 * @param openAxisService
	 *            the open axis service
	 * @return the string
	 * @throws OpenAirException
	 *             the open air exception
	 */
	public String processRequest(String requestXML, OpenAxisService openAxisService) throws OpenAirException {

		LOG.debug("Start Manager for==  {}", openAxisService);

		List<SPComposite> spCompositeList = new ArrayList<SPComposite>();
		String saResponseXMLs = null;
		SPService spService = null;
		SPComposite spComposite = null;

		String sourceName = XMLUtils.getAttributeValue(requestXML, "tc", "source");
		LOG.info("sourceName==> {}", sourceName);
		ServiceProvider serviceProviderForRequest = getServiceProvider(sourceName);
		LOG.info("serviceProviderForRequest==> {}", serviceProviderForRequest);

		// Get SPservice for SAService
		spService = getService(serviceProviderForRequest, openAxisService);

		// INTA-69 Remove special and space characters from First and Last name.
		if (openAxisService.equals(OpenAxisService.PNR_CREATE)) {
			CommonValidationUtil commonValidationUtil = new CommonValidationUtil();
			requestXML = commonValidationUtil.removeSpaceAndReplaceAccentChars(requestXML);
			// LOG.debug("Transformed Request Without Sp. Char==", requestXML);
		}
		OpenAirlinesServiceEnvelopXML serviceXML = appContext.getBean(OpenAirlinesServiceEnvelopXML.class);
		serviceXML.setSaRequestXML(requestXML);
		spService.setOpenAirlinesServiceEnvelopXML(serviceXML);

		spComposite = new SPComposite(serviceProviderForRequest, spService);
		spCompositeList.add(spComposite);

		LOG.debug("SPCompositeList Size =  {}", spCompositeList.size());
		LOG.info("SPCompositeList is prepared and now send the list to SPServiceExecutor.");

		saResponseXMLs = spServiceExecutor.execute(spCompositeList);

		return saResponseXMLs;

	}

	/**
	 * Gets the service.
	 * 
	 * @param serviceProvider
	 *            the service provider
	 * @param openAxisService
	 *            the open axis service
	 * @return the service
	 * @throws OpenAirServiceNotFoundForProviderException
	 *             the open air service not found for provider exception
	 */
	private SPService getService(ServiceProvider serviceProvider, OpenAxisService openAxisService) throws OpenAirServiceNotFoundForProviderException {
		if (openAxisService.equals(OpenAxisService.FARE_SEARCH)) {
			return serviceProvider.getFareSearchService();
		} else if (openAxisService.equals(OpenAxisService.FLIGHT_PRICE)) {
			return serviceProvider.getFlightPriceService();
		} else if (openAxisService.equals(OpenAxisService.PNR_CREATE)) {
			return serviceProvider.getPNRCreateService();
		} else if (openAxisService.equals(OpenAxisService.PNR_RETRIEVE)) {
			return serviceProvider.getPNRRetrieveService();
		} else if (openAxisService.equals(OpenAxisService.PNR_CANCEL)) {
			return serviceProvider.getPNRCancelService();
		} else if (openAxisService.equals(OpenAxisService.FARE_RULES)) {
			return serviceProvider.getFareRulesService();
		} else if (openAxisService.equals(OpenAxisService.ADD_PAYMENT_IN_JETSTAR)) {
			return serviceProvider.getAddPaymentsInJetStar();
		} else if (openAxisService.equals(OpenAxisService.TICKET_ISSUE)) {
			return serviceProvider.getTicketIssueService();
		} else if (openAxisService.equals(OpenAxisService.PROCESS_PAYMENT_IN_JETSTAR)) {
			return serviceProvider.getAddPaymentsInJetStar();
		} else {
			throw new OpenAirServiceNotFoundForProviderException(new Exception());
		}

	}

	/**
	 * Gets the service provider.
	 * 
	 * @param airBBServiceProviderCode
	 *            the air bb service provider code
	 * @return the service provider
	 */
	private ServiceProvider getServiceProvider(String airBBServiceProviderCode) {
		if ("JQ".equalsIgnoreCase(airBBServiceProviderCode)) {
			return appContext.getBean(JSServiceProvider.class);
		} else if ("9W".equalsIgnoreCase(airBBServiceProviderCode)) {
			return appContext.getBean(JAServiceProvider.class);
		} else {
			return null;
		}

	}
}
