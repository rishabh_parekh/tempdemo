package com.abb.framework.manager;

import org.springframework.stereotype.Service;

import com.abb.framework.serviceaccessor.SAErrorManager;


/**
 * The Class OpenAxisErrorManager.
 */
@Service
public class OpenAxisErrorManager extends SAErrorManager {

	/* (non-Javadoc)
	 * @see com.abb.framework.serviceaccessor.SAErrorManager#constructErrorResponse(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String constructErrorResponse(String errorCode, String errorDescription, String source) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<");
		stringBuilder.append(this.openAxisService.getResponseWrapper());
		stringBuilder.append(">");
		stringBuilder.append("<InfoGroup><Error ErrorType=\"");
		stringBuilder.append(this.openAxisService.getName());
		stringBuilder.append("\" Source=\"" + source + "\">");
		stringBuilder.append("<Code>");
		stringBuilder.append(errorCode);
		stringBuilder.append("</Code>");
		stringBuilder.append("<Text>");
		stringBuilder.append(errorDescription);
		stringBuilder.append("</Text>");
		stringBuilder.append("</Error></InfoGroup>");
		stringBuilder.append("</");
		stringBuilder.append(this.openAxisService.getResponseWrapper());
		stringBuilder.append(">");

		return stringBuilder.toString();
	}

}
