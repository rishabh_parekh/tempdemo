package com.abb.framework.serviceaccessor;

import org.springframework.beans.factory.annotation.Value;

import com.abb.framework.enums.OpenAxisService;
import com.abb.framework.exception.OpenAirException;
import com.abb.framework.exception.OpenAirInActiveServiceProvideException;
import com.abb.framework.exception.OpenAirInvalidAuthenticationException;
import com.abb.framework.exception.OpenAirInvalidServiceException;
import com.abb.framework.exception.OpenAirInvalidXMLException;
import com.abb.framework.exception.OpenAirRemoteException;
import com.abb.framework.exception.OpenAirRequestXMLNotFoundException;
import com.abb.framework.exception.OpenAirSPLoginException;
import com.abb.framework.exception.OpenAirSPServiceException;
import com.abb.framework.exception.OpenAirServiceNotFoundForProviderException;
import com.abb.framework.exception.OpenAirXMLMergingException;
import com.abb.framework.exception.OpenAirXMLTransformationException;


/**
 * The Class SAErrorManager.
 */
public abstract class SAErrorManager {

	/** The invalid xml code. */
	@Value("#{openAirWSError['INVALID_XML_CODE']}")
	protected String INVALID_XML_CODE = "";
	
	/** The invalid xml desc. */
	@Value("#{openAirWSError['INVALID_XML_DESC']}")
	protected String INVALID_XML_DESC = "";

	/** The invalid authentication code. */
	@Value("#{openAirWSError['INVALID_AUTHENTICATION_CODE']}")
	protected String INVALID_AUTHENTICATION_CODE = "";
	
	/** The invalid authentication desc. */
	@Value("#{openAirWSError['INVALID_AUTHENTICATION_DESC']}")
	protected String INVALID_AUTHENTICATION_DESC = "";

	/** The invalid unmarshalling code. */
	@Value("#{openAirWSError['INVALID_UNMARSHALLING_CODE']}")
	protected String INVALID_UNMARSHALLING_CODE = "";
	
	/** The invalid unmarshalling desc. */
	@Value("#{openAirWSError['INVALID_UNMARSHALLING_DESC']}")
	protected String INVALID_UNMARSHALLING_DESC = "";

	/** The invalid marshalling code. */
	@Value("#{openAirWSError['INVALID_MARSHALLING_CODE']}")
	protected String INVALID_MARSHALLING_CODE = "";
	
	/** The invalid marshalling desc. */
	@Value("#{openAirWSError['INVALID_MARSHALLING_DESC']}")
	protected String INVALID_MARSHALLING_DESC = "";

	/** The invalid object to object code. */
	@Value("#{openAirWSError['INVALID_OBJECT_TO_OBJECT_CODE']}")
	protected String INVALID_OBJECT_TO_OBJECT_CODE = "";
	
	/** The invalid object to object desc. */
	@Value("#{openAirWSError['INVALID_OBJECT_TO_OBJECT_DESC']}")
	protected String INVALID_OBJECT_TO_OBJECT_DESC = "";

	/** The invalid xml transformation code. */
	@Value("#{openAirWSError['INVALID_XML_TRANSFORMATION_CODE']}")
	protected String INVALID_XML_TRANSFORMATION_CODE = "";
	
	/** The invalid xml transformation desc. */
	@Value("#{openAirWSError['INVALID_XML_TRANSFORMATION_DESC']}")
	protected String INVALID_XML_TRANSFORMATION_DESC = "";

	/** The invalid xml merging code. */
	@Value("#{openAirWSError['INVALID_XML_MERGING_CODE']}")
	protected String INVALID_XML_MERGING_CODE = "";
	
	/** The invalid xml merging desc. */
	@Value("#{openAirWSError['INVALID_XML_MERGING_DESC']}")
	protected String INVALID_XML_MERGING_DESC = "";

	/** The invalid service request code. */
	@Value("#{openAirWSError['INVALID_SERVICE_REQUEST_CODE']}")
	protected String INVALID_SERVICE_REQUEST_CODE = "";
	
	/** The invalid service request desc. */
	@Value("#{openAirWSError['INVALID_SERVICE_REQUEST_DESC']}")
	protected String INVALID_SERVICE_REQUEST_DESC = "";

	/** The request xml not found code. */
	@Value("#{openAirWSError['REQUEST_XML_NOT_FOUND_CODE']}")
	protected String REQUEST_XML_NOT_FOUND_CODE;
	
	/** The request xml not found desc. */
	@Value("#{openAirWSError['REQUEST_XML_NOT_FOUND_DESC']}")
	protected String REQUEST_XML_NOT_FOUND_DESC;

	/** The service not found for provider code. */
	@Value("#{openAirWSError['SERVICE_NOT_FOUND_FOR_PROVIDER_CODE']}")
	protected String SERVICE_NOT_FOUND_FOR_PROVIDER_CODE;
	
	/** The service not found for provider desc. */
	@Value("#{openAirWSError['SERVICE_NOT_FOUND_FOR_PROVIDER_DESC']}")
	protected String SERVICE_NOT_FOUND_FOR_PROVIDER_DESC;

	/** The service provider error code. */
	@Value("#{openAirWSError['SERVICE_PROVIDER_ERROR_CODE']}")
	protected String SERVICE_PROVIDER_ERROR_CODE = null;
	
	/** The service provider error desc. */
	@Value("#{openAirWSError['SERVICE_PROVIDER_ERROR_DESC']}")
	protected String SERVICE_PROVIDER_ERROR_DESC = null;

	/** The sp login error code. */
	@Value("#{openAirWSError['SP_LOGIN_ERROR_CODE']}")
	protected String SP_LOGIN_ERROR_CODE = null;
	
	/** The sp login error desc. */
	@Value("#{openAirWSError['SP_LOGIN_ERROR_DESC']}")
	protected String SP_LOGIN_ERROR_DESC = null;

	/** The sp service error code. */
	@Value("#{openAirWSError['SP_SERVICE_ERROR_CODE']}")
	protected String SP_SERVICE_ERROR_CODE = null;
	
	/** The sp service error desc. */
	@Value("#{openAirWSError['SP_SERVICE_ERROR_DESC']}")
	protected String SP_SERVICE_ERROR_DESC = null;

	/** The inactive service provider code. */
	@Value("#{openAirWSError['INACTIVE_SERVICE_PROVIDER_CODE']}")
	protected String INACTIVE_SERVICE_PROVIDER_CODE = null;
	
	/** The inactive service provider desc. */
	@Value("#{openAirWSError['INACTIVE_SERVICE_PROVIDER_DESC']}")
	protected String INACTIVE_SERVICE_PROVIDER_DESC = null;

	/** The open axis service. */
	protected OpenAxisService openAxisService;

	/**
	 * Construct error response.
	 *
	 * @param errorCode the error code
	 * @param errorDescription the error description
	 * @param source the source
	 * @return the string
	 */
	public abstract String constructErrorResponse(String errorCode, String errorDescription, String source);

	/**
	 * Gets the error response for exception.
	 *
	 * @param ex the ex
	 * @param openAxisService the open axis service
	 * @return the error response for exception
	 */
	public String getErrorResponseForException(OpenAirException ex, OpenAxisService openAxisService) {
		this.openAxisService = openAxisService;

		if (ex instanceof OpenAirInvalidXMLException) {
			return this.getInvalidXMLErrorResponse();
		} else if (ex instanceof OpenAirInvalidAuthenticationException) {
			return this.getInvalidAuthenticationErrorResponse();
		} else if (ex instanceof OpenAirXMLTransformationException) {
			return this.getXMLTransformationErrorResponse();
		} else if (ex instanceof OpenAirXMLMergingException) {
			return this.getXMLMergingErrorResponse();
		} else if (ex instanceof OpenAirInvalidServiceException) {
			return this.getInvalidServiceErrorResponse(ex);
		} else if (ex instanceof OpenAirRequestXMLNotFoundException) {
			return this.getRequestXMLNotFoundErrorResponse(ex);
		} else if (ex instanceof OpenAirServiceNotFoundForProviderException) {
			return this.getServiceNotSupportedErrorResponse(ex);
		} else if (ex instanceof OpenAirRemoteException) {
			return this.getServiceProviderErrorResponse(ex);
		} else if (ex instanceof OpenAirSPLoginException) {
			return this.getSPLoginErrorResponse(ex);
		} else if (ex instanceof OpenAirSPServiceException) {
			return this.getSPServiceErrorResponse(ex);
		} else if (ex instanceof OpenAirInActiveServiceProvideException) {
			return this.getInActiveServiceProviderErrorResponse(ex);
		} else {
			return "Unkonw Exception";
		}
	}

	/**
	 * Return Error element for Invalid Authentication.
	 * 
	 * @return object of Error class
	 */
	public String getInvalidAuthenticationErrorResponse() {
		return constructErrorResponse(INVALID_AUTHENTICATION_CODE, INVALID_AUTHENTICATION_DESC, "");
	}

	/**
	 * Return Error element for Invalid XML against XSD.
	 * 
	 * @return object of Error class
	 */
	public String getInvalidXMLErrorResponse() {
		return constructErrorResponse(INVALID_XML_CODE, INVALID_XML_DESC, "");
	}

	/**
	 * Return Error element for XML to Object Transformation.
	 * 
	 * @return object of Error class
	 */
	public String getXMLToObjectTransformationErrorResponse() {
		return constructErrorResponse(INVALID_UNMARSHALLING_CODE, INVALID_UNMARSHALLING_DESC, "");
	}

	/**
	 * Return Error element for XML to Object Transformation.
	 * 
	 * @return object of Error class
	 */
	public String getObjectToXMLTransformationErrorResponse() {
		return constructErrorResponse(INVALID_MARSHALLING_CODE, INVALID_MARSHALLING_DESC, "");
	}

	/**
	 * Return Error element for Object to Object Transformation.
	 * 
	 * @return object of Error class
	 */
	public String getObjectToObjectTransformationErrorResponse() {
		return constructErrorResponse(INVALID_OBJECT_TO_OBJECT_CODE, INVALID_OBJECT_TO_OBJECT_DESC, "");
	}

	/**
	 * Return Error element for XML Transformation.
	 * 
	 * @return object of Error class
	 */
	public String getXMLTransformationErrorResponse() {
		return constructErrorResponse(INVALID_XML_TRANSFORMATION_CODE, INVALID_XML_TRANSFORMATION_DESC, "");
	}

	/**
	 * Return Error element for XML Merging.
	 * 
	 * @return object of Error class
	 */
	public String getXMLMergingErrorResponse() {
		return constructErrorResponse(INVALID_XML_MERGING_CODE, INVALID_XML_MERGING_DESC, "");
	}

	/**
	 * Return Error element for invalid service request.
	 *
	 * @param ex the ex
	 * @return object of Error class
	 */
	public String getInvalidServiceErrorResponse(OpenAirException ex) {
		return INVALID_SERVICE_REQUEST_DESC.replace("{REQUEST_NAME}", ex.getCause().getMessage());
	}

	/**
	 * Return Error message for RequestXML not found exception.
	 *
	 * @param ex the ex
	 * @return the request xml not found error response
	 */
	public String getRequestXMLNotFoundErrorResponse(OpenAirException ex) {
		return REQUEST_XML_NOT_FOUND_DESC;
	}

	/**
	 * Return Error message for service not supported.
	 *
	 * @param ex the ex
	 * @return the service not supported error response
	 */
	public String getServiceNotSupportedErrorResponse(OpenAirException ex) {
		return constructErrorResponse(SERVICE_NOT_FOUND_FOR_PROVIDER_CODE, SERVICE_NOT_FOUND_FOR_PROVIDER_DESC, "");
	}

	/**
	 * Return error message for service provide isn't working.
	 *
	 * @param ex the ex
	 * @return the service provider error response
	 */
	private String getServiceProviderErrorResponse(OpenAirException ex) {
		return constructErrorResponse(SERVICE_PROVIDER_ERROR_CODE, SERVICE_PROVIDER_ERROR_DESC, "");
	}

	/**
	 * Return error message for login service of service provider isn't working.
	 *
	 * @param ex the ex
	 * @return the SP login error response
	 */
	private String getSPLoginErrorResponse(OpenAirException ex) {
		return constructErrorResponse(SP_LOGIN_ERROR_CODE, SP_LOGIN_ERROR_DESC, "");
	}

	/**
	 * Return error message when face error/exception while executing the service.
	 *
	 * @param ex the ex
	 * @return the SP service error response
	 */
	private String getSPServiceErrorResponse(OpenAirException ex) {
		return constructErrorResponse(SP_SERVICE_ERROR_CODE, SP_SERVICE_ERROR_DESC, "");
	}

	/**
	 * Return error message when service provider is inactive.
	 *
	 * @param ex the ex
	 * @return the in active service provider error response
	 */
	private String getInActiveServiceProviderErrorResponse(OpenAirException ex) {
		return constructErrorResponse(INACTIVE_SERVICE_PROVIDER_CODE, INACTIVE_SERVICE_PROVIDER_DESC, ex.getCause().getMessage());
	}
}
