package com.abb.serviceAccessor.farelogix.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The Class FarelogixConfiguration.
 */
@Component
public class FarelogixConfiguration {

	/** The validate xml. */
	private static String validateXML;

	/** The validate response XML */
	private static String validateResponseXML;

	/** The authenticate xml. */
	private static String authenticateXML;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(FarelogixConfiguration.class);

	/**
	 * Checks if is validate xml.
	 * 
	 * @return true, if is validate xml
	 */
	public static boolean isValidateXML() {
		return Boolean.parseBoolean(validateXML);
	}

	/**
	 * Sets the validate xml.
	 * 
	 * @param validateXML
	 *            the new validate xml
	 */
	@Value("#{farelogixConfig['VALIDATE_XML']}")
	public void setValidateXML(String validateXML) {
		LOG.info("Setting Config Param");
		FarelogixConfiguration.validateXML = validateXML;
	}

	/**
	 * Checks if is validate xml.
	 * 
	 * @return true, if is validate xml
	 */
	public static boolean isValidateResponseXML() {
		return Boolean.parseBoolean(validateResponseXML);
	}

	/**
	 * Sets the validate xml.
	 * 
	 * @param validateXML
	 *            the new validate xml
	 */
	@Value("#{farelogixConfig['VALIDATE_RESPONSE_XML']}")
	public void setValidateResponseXML(String validateResponseXML) {
		LOG.info("Setting Config Param");
		FarelogixConfiguration.validateResponseXML = validateResponseXML;
	}

	/**
	 * Checks if is authenticate xml.
	 * 
	 * @return true, if is authenticate xml
	 */
	public static boolean isAuthenticateXML() {
		return Boolean.parseBoolean(authenticateXML);
	}

	/**
	 * Sets the authenticate xml.
	 * 
	 * @param authenticateXML
	 *            the new authenticate xml
	 */
	@Value("#{farelogixConfig['AUTHENTICATE_XML']}")
	public void setAuthenticateXML(String authenticateXML) {
		FarelogixConfiguration.authenticateXML = authenticateXML;
	}
}
